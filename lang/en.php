<?php
//for main.js modal files
define("_MAINJS_ATTENTION", "Attention");
define("_MAINJS_ENTER_USERNAME", "Please enter your username");
define("_MAINJS_ENTER_EMAIL", "Please enter your email address");
define("_MAINJS_ENTER_ICNO", "Please enter your ID number");
define("_MAINJS_SELECT_COUNTRY", "Please choose your country");
define("_MAINJS_ENTER_PHONENO", "Please enter your phone number");
//apply in all
define("_MAINJS_ALL_LOGOUT", "Logout");
//index
define("_MAINJS_INDEX_LOGIN", "Login");
define("_MAINJS_INDEX_USERNAME", "Username");
define("_MAINJS_INDEX_PASSWORD", "Password");
define("_MAINJS_INDEX_CONFIRM_PASSWORD", "Confirm Password");
define("_INDEX_NO_YET", "Don't have an account? Sign up here.");
define("_INDEX_LOGIN_NOW", "Already have an account? Log in here.");
define("_INDEX_IC_NO", "IC Number");
define("_INDEX_DOB", "Date of Birth");
define("_INDEX_MOBILE_NO", "Contact No.");
define("_INDEX_ADDRESS", "Address");
define("_INDEX_ADDRESS1", "Address 1");
define("_INDEX_ADDRESS2", "Address 2");
define("_INDEX_ZIPCODE", "Zip Code");
define("_INDEX_STATE", "State");
define("_INDEX_REGISTER", "Register");
define("_INDEX_NO_REPEAT_IC", "*Do Not Use REPEATED IC");
//JS
define("_JS_FOOTER", " Hygenie Group, All Rights Reserved.");
define("_JS_LOGIN", "Login");
define("_JS_USERNAME", "Username");
define("_JS_PASSWORD", "Password");
define("_JS_FULLNAME", "Fullname");
define("_JS_NEW_PASSWORD", "New Password");
define("_JS_CURRENT_PASSWORD", "Current Password");
define("_JS_RETYPE_PASSWORD", "Retype Password");
define("_JS_RETYPE_NEW_PASSWORD", "Retype New Password");
define("_JS_RETYPE_REFERRER_NAME", "Referrer Name");
define("_JS_REMEMBER_ME", "Remember Me");
define("_JS_FORGOT_PASSWORD", "Forgot Password?");
define("_JS_FORGOT_TITLE", "Forgot Password");
define("_JS_EMAIL", "Email");
define("_JS_SIGNUP", "Sign Up");
define("_JS_FIRSTNAME", "First Name");
define("_JS_LASTNAME", "Last Name");
define("_JS_GENDER", "Gender");
define("_JS_MALE", "Male");
define("_JS_FEMALE", "Female");
define("_JS_BIRTHDAY", "Birthday");
define("_JS_COUNTRY", "Country");
define("_JS_MALAYSIA", "Malaysia");
define("_JS_SINGAPORE", "Singpore");
define("_JS_PHONE", "Phone No.");
define("_JS_REQUEST_TAC", "Request TAC");
define("_JS_TYPE", "Type");
define("_JS_SUBMIT", "Confirm");
define("_JS_PLACEORDER", "Place Order");
define("_JS_WITHDRAW_AMOUNT", "Withdraw Amount");
define("_JS_SUCCESS", "Success");
define("_JS_CLOSE", "Close");
define("_JS_ERROR", "Error");
define("_JS_EDIT_USER_PROFILE", "Change Member Details");
define("_JS_CHANGE_USER_PASSWORD", "Change Member Password");
define("_JS_EDIT_PROFILE", "Change Personal Details");
define("_JS_RESET_PASSWORD", "Reset Password");
define("_JS_VERIFY_CODE", "Verify Code");
define("_JS_SELECT_COUNTRY", "Please Select a Country");
//UPLOAD
define("_UPLOAD_IC_FRONT", "Upload Front View of IC/Passport");
define("_UPLOAD_PROOF_OF_LEGAL_NAME", "Proof of Legal Name (Front)");
define("_UPLOAD_PROOF_OF_LEGAL_NAME_DESC", "*ID document or Passport to show your full name, date of birth, ID number and expiry date");
define("_UPLOAD_PROOF_OF_LEGAL_NAME_BACK", "Proof of Legal Name (Back)");
define("_UPLOAD_PROOF_OF_ADDRESS", "Proof of Address");
define("_UPLOAD_PROOF_OF_ADDRESS_DESC", "*Utility bill, Driving License or other documents to show your name and address");
define("_UPLOAD", "Upload");
define("_UPLOAD_PREVIEW", "Preview");
define("_UPLOAD_SELECT_DRAG", "Select a file or drag here");
define("_UPLOAD_PLS_SELECT_IMG", "Please select an image");
define("_UPLOAD_SELECT_A_FILE", "Select a file");
define("_UPLOAD_IC_BACK", "Upload Back View of IC/Passport");
define("_UPLOAD_UTILITY_BILL_DRIVING_LICENSE", "Upload Utility Bill or Driving License");
define("_UPLOAD_SIGNATURE_MAA", "Attach your signature for Limited Power of Attorney (LPOA)");
define("_UPLOAD_SIGNATURE", "Upload Signature");
define("_UPLOAD_CHOOSE_FILE_HERE", "Click Here to Choose File");
//USERDASHBOARD
define("_USERDASHBOARD_INVITATION_LINK", "Invitation Link");
define("_USERDASHBOARD_COPY", "Copy");
define("_USERDASHBOARD_LOGOUT", "Logout");
define("_USERDASHBOARD_INVITED_BY_ME", "Members Invited by Me");
define("_USERHEADER_PROFILE", "Personal Details");
define("_USERHEADER_BANK_ACC", "Bank Account");
define("_USERHEADER_UPLOAD_DOC", "Upload Doc");
define("_USERHEADER_REFER", "Refer");
define("_USERHEADER_HIERARCHY", "Organization");
define("RESET_HIERARCHY", "Reset");
define("_USERDASHBOARD_TOTAL_COMMISSION", "Total Commission");
define("_USERDASHBOARD_DAILY_COMMISSION", "Daily Profit");
define("_USERDASHBOARD_MONTHLY_COMMISSION", "Monthly Profit");
define("_USERDASHBOARD_PERSONAL_SALES", "My Sales");
define("_USERDASHBOARD_GROUP_SALES", "Team Sales");
define("_USERDASHBOARD_DIRECT_DOWNLINE", "Direct Downline");
define("_USERDASHBOARD_DIRECT_DOWNLINE2", "'s Direct Downline");
define("_USERDASHBOARD_GROUP_MEMBER", "Team Member");
define("_USERDASHBOARD_RANK", "Rank");
define("_USERHEADER_BANK_DETAILS", "Bank Details");
define("_USERDASHBOARD_DETAILS", "Details");
define("_USERDASHBOARD_PRODUCT_ORDER", "Product Ordering");
define("_USERDASHBOARD_ORDER_HISTORY", "Order History");
define("_USERHEADER_BONUS_REPORT", "Bonus Report");
define("_USERHEADER_WITHDRAWAL_HISTORY", "Withdrawal History");
define("_USERDASHBOARD_POOL_FUND", "Pool Fund");
define("_USERDASHBOARD_POOL_FUND_BONUS", "Pool Fund Bonus");
define("_USERDASHBOARD_POOL_BONUS", "Pool Bonus");
define("_USERDASHBOARD_SALES_MANAGER", "Sales Manager");
define("_USERDASHBOARD_MARKETING_MANAGER", "Marketing Manager");
define("_USERDASHBOARD_AREA_MANAGER", "Area Manager");
define("_USERDASHBOARD_REGION_MANAGER", "Regional Manager");
define("_USERDASHBOARD_MY_BONUS", "My Bonus");
define("_USERDASHBOARD_BONUS", "Bonus");
define("_USERDASHBOARD_MY_SALES", "My Sales");
define("_USERDASHBOARD_TEAM_SALES", "Team Sales");
define("_USERDASHBOARD_TEAM_MEMBERS", "Team Members");
define("_USERDASHBOARD_PERSONAL", "My Details");
define("_USERDASHBOARD_CREDIT", "Credit");
define("_USERDASHBOARD_EDIT_PASSWORD", "Edit Password");
//BANKDETAILS
define("_BANKDETAILS_ACC_NAME", "Bank Acc. Holder Name");
define("_BANKDETAILS_ACC_NO", "Bank Account Number");
define("_BANKDETAILS_ACC_TYPE", "Bank Account Type");
define("_BANKDETAILS_BANK", "Bank");
define("_BANKDETAILS_BANK_SWIFT_CODE", "Bank SWIFT Code");
define("_BANKDETAILS", "Bank Details");
//ADMINDASHBOARD
define("_ADMINDASHBOARD_ADD_ONS_CREDIT", "Add-ons Credit");
define("_ADMINDASHBOARD_UPLOAD_EQUITY_PL", "Upload Equity PL");
define("_ADMINDASHBOARD_PROFIT_SHARING", "Profit Sharing");
define("_ADMINDASHBOARD_RESET_MEMBERS", "Reset Members Balance?");
define("_ADMINDASHBOARD_RESET", "Reset Daily Profit?");
define("_ADMINDASHBOARD_RESET_MONTH", "Reset Monthly profit?");
//ADMINHEADER
define("_ADMINHEADER_DASHBOARD", "Dashboard");
define("_ADMINHEADER_INFO", "Statements");
define("_ADMINHEADER_MARKETING_PLAN", "Marketing Plan");
define("_ADMINHEADER_DAILY_SPREAD", "Daily Profit");
define("_ADMINHEADER_MONTHLY_PROFITS", "Monthly Profits");
define("_ADMINHEADER_MEMBERS", "Members");
define("_ADMINHEADER_REPORTS", "Statements");
define("_ADMINHEADER_MEMBER_BALANCE", "Member's Balance");
define("_ADMINHEADER_MEMBER_WITHDRAW", "Member's Withdrawal");
define("_ADMINHEADER_MEMBER_WITHDRAW_HISTORY", "Member's Withdrawal History");
define("_ADMINHEADER_MEMBER_PROFIT", "Member's Profit Sharing");
define("_ADMINHEADER_COMPANY_BALANCE", "Company's Balance");
define("_ADMINHEADER_ADD_USER", "Add Member");
define("_ADMINHEADER_VIEW_MEMBER", "View Member");
define("_ADMINHEADER_DAILY_BONUS", "Daily Profit");
define("_ADMINHEADER_MONTHLY_BONUS", "Monthly Profits");
define("_ADMINHEADER_VIEW_LEVEL", "View Level");
define("_ADMINHEADER_EDIT_PASSWORD", "Change Password");
define("_ADMINHEADER_MEMBER_PROFIT2", "Member's Profit");
//ADMINNEWCREDIT
define("_ADMINNEWCREDIT_SELECT_FILE", "Select File");
define("_ADMINNEWCREDIT_MEMBERS_RANKING", "Members Ranking");
define("_ADMINNEWCREDIT_FIRST_STEP", "1st Step");
define("_ADMINNEWCREDIT_DAILY_SPREAD", "Rank & Same Level Reward");
define("_ADMINNEWCREDIT_SECOND_STEP", "2nd Step");
define("_ADMINNEWCREDIT_MONTHLY_PROFITS", "Tier Reward");
//ADMINVIEWBALANCE
define("_ADMINVIEWBALANCE_NO", "No.");
define("_ADMINVIEWBALANCE_NAME", "Name");
define("_ADMINVIEWBALANCE_BALANCE", "Balance");
define("_ADMINVIEWBALANCE_EDIT", "Edit");
define("_ADMINVIEWBALANCE_VIEW", "View");
define("_ADMINVIEWBALANCE_COMPANY_TOTAL", "Company Total Balance (CTB)");
define("_ADMINVIEWBALANCE_TOTAL_LOT_SIZE", "Total Lot Size (TLS)");
define("_ADMINVIEWBALANCE_LAST_UPDATED", "Last Updated");
define("_ADMINVIEWBALANCE_ACTION", "Action");
define("_ADMINVIEWBALANCE_ISSUE", "Issue");
define("_ADMINVIEWBALANCE_EDIT_COMPANY_BALANCE", "Edit Company Balance");
define("_ADMINVIEWBALANCE_PAYOUT", "Total Payout :");
define("_ADMINVIEWBALANCE_PAYOUT_RELEASED", "Total Payout Released :");
define("_ADMINVIEWBALANCE_WITHDRAW_AMOUNT", "Withdraw Amount");
define("_ADMINVIEWBALANCE_PAYOUT_UNRELEASED", "Total Payout Unreleased :");
define("_ADMINVIEWBALANCE_APPROVE", "Approve");
define("_ADMINVIEWBALANCE_REJECT", "Reject");
//MULTIBANK
define("_MULTIBANK_PRINT", "Print");
define("_MULTIBANK_VIEW", "View");
define("_MULTIBANK_ID_DOCUMENT", "ID");
define("_MULTIBANK_UTILITY_BILL", "Utility Bill");
define("_MULTIBANK_ACTION", "Action");
define("_MULTIBANK_SEARCH", "Search");
define("_MULTIBANK_UPDATE", "Update");
define("_MULTIBANK_GO_BACK", "Go Back");
define("_MULTIBANK_IC_FRONT", "Front View of IC");
define("_MULTIBANK_IC_BACK", "Back View of IC");
define("_MULTIBANK_DETAILS", "Details");
define("_MULTIBANK_UPLINE", "Upline");
define("_MULTIBANK_PERFORMANCE_FEE", "Performance Fee");
define("_MULTIBANK_START_DATE", "Start Date");
define("_MULTIBANK_END_DATE", "End Date");
//USERHEADER
define("_USERHEADER_EDIT_PROFILE", "Change Personal Details");
define("_USERHEADER_EDIT_PASSWORD", "Change Password");
define("USERHEADER_DAILY_BONUS", "Daily Profit Statement");
define("USERHEADER_MONTHLY_BONUS", "Monthly Bonus Statement");
define("_USERHEADER_ADD_NEW", "Add New MT4ID");
//HIERARCHY
define("_HIERARCHY_GENERATION", "Generation");
define("_HIERARCHY_NAME", "Name");
define("_HIERARCHY_SPONSOR", "Sponsor");
define("_HIERARCHY_PERSONAL_SALES", "My Sales");
define("_HIERARCHY_GROUP_SALES", "Team Sales");
define("_HIERARCHY_GROUP_MEMBER", "Team Member");
define("_HIERARCHY_DIRECT_DOWNLINE", "Direct Downline");
define("_HIERARCHY_TOTAL_DOWNLINE", "Total Downline");
define("_HIERARCHY_RANK", "Rank");
define("_HIERARCHY_TREE_VIEW", "Tree View");
//REFER
define("_REFER_INVITATION_LINK", "Copy Invitation Link");
//DAILY BONUS
define("_DAILY_MEMBER_DAILY_BONUS", "Rank & Same Level Reward Statement");
define("_DAILY_FROM", "From");
define("_DAILY_BONUS", "Bonus");
define("_DAILY_DATE", "Date");
define("_DAILY_TIME", "Time");
define("_DAILY_NO_REPORT", "No Daily Profit Statement");
define("_DAILY_RELEASED_BONUS", "Released Bonus");
define("_DAILY_UNRELEASED_BONUS", "Unreleased Bonus");
//MONTHLY BONUS
define("_MONTHLY_MEMBER_DAILY_BONUS", "Tier Reward Statement");
define("_MONTHLY_LEVEL", "Level");
define("_MONTHLY_NO_REPORT", "No Monthly Profit Statement");
//LEVEL DETAILS
define("_VIEWLEVEL_LEVEL_DETAILS", "Level Details");
define("_VIEWLEVEL_AMOUNT", "Amount");
define("_VIEWLEVEL_REQUIREMENTS", "Requirements");
define("_VIEWLEVEL_DIRECT_SPONSOR", "Direct Sponsor");
define("_VIEWLEVEL_SELF_INVEST", "Self-Invest");
define("_VIEWLEVEL_PROFIT_SHARING", "Profit-Sharing");
//ADMIN ADD ON CREDIT
define("_ADMINADDON_RANK_UPDATED", "Rank Updated");
define("_ADMINADDON_DAILY_BONUS", "Calculate Reward");
define("_ADMINADDON_MONTHLY_BONUS", "Calculate Reward");
define("_ADMINADDON_RESET", "Reset");
//ADMIN DAILY SPREAD
define("_ADMINDAILY_RANKING", "Ranking");
define("_ADMINDAILY_WALLET", "Wallet");
define("_ADMINDAILY_TEAM_PERFORMANCE", "Team Performance");
define("_ADMINDAILY_SPREAD_SHARING", "Spread Sharing");
define("_ADMINDAILY_MEMBERS", "Members");
define("_ADMINDAILY_LEADER", "Leader");
define("_ADMINDAILY_DIRECT", "Direct Sponsor 3 Members");
define("_ADMINDAILY_TEAM_LEADER", "Team Leader");
define("_ADMINDAILY_INDIRECT_DIRECT", "Indirect + Direct Sponsor 2 Leaders");
define("_ADMINDAILY_GROUP_LEADER", "Team Leader");
define("_ADMINDAILY_INDIRECT_DIRECT_TEAM", "Indirect + Direct Sponsor 2 Team Leaders");
define("_ADMINDAILY_GROUP_LEADER_OVERRIDING", "Team Leader Overriding");
define("_MULTIBANK_SEARCH_START", "Start");
define("_MULTIBANK_SEARCH_END", "End");
// CAPPING
define("_MEMBERS_CAPPING_REPORTS", "Capping Statement");
define("_MEMBERS_CAPPING_REPORTS_COMPLETED", "Completed Capping Statement");
define("_NO_CAPPING_REPORT", "No Capping Statement");
define("_BONUS_TYPE", "Bonus Type");
//ADMIN WITHDRAWAL
define("_ADMINWITHDRAWAL_NO_MEMBER", "No New Member's Withdrawal");
//VICTORY
define("_VICTORY_DOCUMENTS", "Documents");
define("_VICTORY_URL", "Referral Link");
define("_VICTORY_Copy", "Copy");
define("_VICTORY_REFER", "Refer a Friend");
define("_VICTORY_MY_REFER", "My Referral");
define("_VICTORY_REFERRAL_LINK", "Referral Link");
//broker link
define("_BROKER_LINK", "Broker Link");
//product
define("_PRODUCT", "Product");
define("_PRODUCT_ADD_TO_CART", "Add To Cart");
define("_PRODUCT_CART", "Cart");
define("_PRODUCT_DELETE_ALL", "Delete All");
define("_PRODUCT_CHECK_OUT", "Check Out");
define("_PRODUCT_PAYMENT_GATEWAY", "Payment Gateway");
define("_PRODUCT_PAYMENT_DETAILS", "Payment Details");
define("_PRODUCT_AMOUNT", "Amount");
define("_PRODUCT_PROCEED_TO_PAYMENT", "Proceed To Payment");
define("_PRODUCT_ORDER_ID", "Order ID");
define("_TRANSACTION_ID", "Transaction ID");
define("_PRODUCT_PAYMENT_STATUS", "Payment Status");
define("_PRODUCT_SHIPPING_STATUS", "Shipping Status");
define("_PRODUCT_ORDER_DETAILS", "Order Details");
define("_PRODUCT_PURCHASER_NAME", "Purchaser Name");
define("_PRODUCT_PAYMENT_REFERENCE", "Payment Reference");
define("_PRODUCT_ORDER_DATE", "Order Date");
define("_PRODUCT_SHIPPING_METHOD", "Shipping Method");
define("_PRODUCT_TRACKING_NUMBER", "Tracking Number");
define("_PRODUCT_ITEM", "Item");
define("_PRODUCT_QUANTITY", "Quantity");
define("_PRODUCT_TOTAL_PRICE", "Total Price");
define("_PRODUCT_SERIAL_NUMBER", "Serial Number");
define("_PRODUCT_LINK", "Link");
define("_PRODUCT_STOCK_SALES", "Stock/Sales");
define("_PRODUCT_SALES_PAGE", "Sales");
define("_PRODUCT_ORDER", "Order");
define("_PRODUCT_ADD", "Add New Product");
define("_PRODUCT_PRICE", "Price");
define("_PRODUCT_VIEW_DETAILS", "View Details");
define("_PRODUCT_DELETE", "Delete");
define("_PRODUCT_EDIT", "Edit Product");
define("_PRODUCT_NAME", "Product Name");
define("_PRODUCT_COMMISSION", "Commission");
define("_PRODUCT_DESCRIPTION", "Description");
define("_PRODUCT_AVOID", "Avoid");
define("_PRODUCT_IMAGE", "Product Image");
define("_PRODUCT_SUBMIT", "Submit");
define("_PRODUCT_VIEW_STOCK", "View Stock And Sales");
define("_PRODUCT_ALL_STOCK", "All Stock And Sales");
define("_PRODUCT_ADD_STOCK", "Add Stock");
define("_PRODUCT_AVAILABLE_STOCK", "Available Stock");
define("_PRODUCT_TOTAL_SALES", "Total Sales");
define("_PRODUCT_PAYMENT_VERIFICATION", "Payment Verification");
define("_PRODUCT_PAYMENT_FAIL", "Payment Fail");
define("_PRODUCT_PENDING", "Pending");
define("_PRODUCT_SHIPPED", "Shipped");
define("_PRODUCT_REJECTED", "Rejected");
define("_PRODUCT_RECEIVED", "Payment Received");
define("_PRODUCT_DATE_UPDATED", "Date Updated");
//bonus
define("_BONUS_SYSTEM", "Bonus System");
define("_BONUS_DIRECT", "Direct Sponsor Bonus");
define("_BONUS_DIRECT2", "Direct Bonus");
define("_BONUS_PURCHASER", "Purchaser");
define("_BONUS_RECEIVER", "Receiver");
define("_BONUS_RECEIVER_RANKING", "Receiver's Ranking");
define("_BONUS_OVERRIDING", "Overriding of Pool Fund Bonus");
define("_BONUS_STATUS", "Status");
define("_BONUS_DIST", "Bonus Distribution");
define("_BONUS_OVERRIDING_BONUS", "Overriding Bonus");
define("_BONUS_VIEW_BONUS", "View Bonus");
define("_BONUS_RECORD", "Record");
//admin
define("_ADMIN_ADMINDASHBOARD", "Admin Dashboard");
define("_ADMIN_TOTAL_MEMBER", "Total Member");
define("_ADMIN_PENDING_ORDER", "Pending Order");
define("_ADMIN_DIST", "Distribute Bonus");
define("_ADMIN_AUTOWITHDRAWAL", "Auto Withdrawal");
define("_ADMIN_DISTBONUS", "Distribute");
define("_ADMIN_TOTAL_BONUS", "Total Bonus");
//server
define("_SERVER_MAINTENANCE", "Server Maintenance");
define("_SERVER_MAINTENANCE_TEXT", "Server maintenance is ongoing to upgrade the system. Sorry for any inconvenience caused.");
//home
define("_HOME_LEADING", "Leading Hydrogen brand in China");
define("_HOME_HQ", "We are disseminating hydrogen culture to benefiting the public health. Our HQ rooted in China and expanding Hydrogen Health technology to South East Asia. ");
define("_HOME_WATER_MIRALCE", "Miracle of Water");
define("_HOME_FOUR_COUNTRY", "Since more than 100 years ago, people in ");
define("_HOME_FOUR_COUNTRY1", "Lourdes,");
define("_HOME_FOUR_COUNTRY2", " Querétaro, ");
define("_HOME_FOUR_COUNTRY3", "Germany, ");
define("_HOME_FOUR_COUNTRY4", "and Pakistan ");
define("_HOME_FOUR_COUNTRY5", "have improved their health by taking local spring water. It shows that good quality water does play an important role in our health. Besides that, up to 70% of the human body is water. If we want to stay healthy, we shall pay attention to the quality of the water we absorb daily.");
define("_HOME_THE_HARM", "The Harm of Free Radicals");
define("_HOME_HARM_P", "The environmental pollution and pressure of fast-paced life will lead to excessive production of free radicals which will damage human body tissues and cells. Free radicals hazards are as follows:");
define("_HOME_V1", "Produce chemicals that damage cells and form carcinogens");
define("_HOME_V2", "Weaken the resistance of cells and make the body susceptible to bacterial and germ infections");
define("_HOME_V3", "Cause inflammation, aging and arthritis");
define("_HOME_V4", "Cardiovascular and cerebrovascular diseases");
define("_HOME_WHAT_IS", "What is Hydrogen?");
define("_HOME_WHAT_IS_P", "Hydrogen-rich water is water that contains a lot of hydrogen, and hydrogen is the nemesis of free radicals. Hydrogen is the smallest molecule in the universe and the best antioxidant. It enters the cell and actively combines with free radicals to form water and excreted it from the body. Hydrogen also used in curing some chronic diseases and improving physical fitness. Drinking eight glasses of hydrogen-rich water daily can help eliminate harmful free radicals.");
define("_HOME_WHAT_IS_A", " A Hydrogen gas atomization machine is used to assist the Covid-19 treatment.");
define("_HOME_COMPANY1", "The company established in June 2016, focusing on the expansion of the hydrogen health field, and took the lead in innovating the hydrogen health technology product series and the community hydrogen health service system in China. The company is located on Hangkong Road, Baoan District, Shenzhen. As an incubator of Shenzhen University Nanshan Industrial Research Institute, it has a core patented technology-ultra-high concentration hydrogen and oxygen separation hydrogen production technology. It has built China's first high-standard hydrogen technology experience hall. The company is committed to build and developing a 'community-centric' hydrogen health sharing ecosystem, and actively leading the upgrade and transformation of the global hydrogen industry.");
define("_HOME_COMPANY2", "Under the guidance of the entrepreneurial spirit of innovation, great love, and the unity of knowledge and action, with a humane, open and diversified management , it has deeply influenced and driven the employees and upstream and downstream of the hydrogen health technology industry. Industrial partners. And also effectively promote the development of China's hydrogen industry and the innovation of the service system, and have been praised by many media.");
define("_HOME_PREVENTION", "Prevention is never too early. Income is never too much. Be part of Hygieia Family and grab both at once.");
define("_HOME_MISSION1", "Mission");
define("_HOME_MISSION_DESC", "Introduce hydrogen technology to every households.");
define("_HOME_VISION1", "Vision");
define("_HOME_VISION_DESC", "Sharing hydrogen technology, sharing hydrogen health concept.");
define("_HOME_IDEA1", "Idea");
define("_HOME_IDEA_DESC", "Healthy for everyone and everyone are profitable.");
define("_HOME_HYDROGEN", "Hydrogen");
define("_HOME_HYDROGEN_DESC", "Let more people know the benefits of hydrogen.");
define("_HOME_LOVE", "Love");
define("_HOME_LOVE_DESC", "Hydrogen loves you and me, hydrogen is a family, love yourself, love your relatives, friends, and bring love home.");
define("_HOME_WORLD", "The World");
define("_HOME_WORLD_DESC", "To achieve a hydrogen tumbler for everyone, improve one's own protection ability, prevention is better than cure.");
define("_HOME_OUR_MISSION_AND", "Our Mission and Vision");
define("_HOME_TIMELINE4", "In May 2017, became the chairman unit of the Hydrogen Industry Committee of Shenzhen Science and Technology Economic Promotion Association<br><br>
In June 2017, together with the Shenzhen Association for Science and Technology, completed the China Hydrogen Industry Standard Project<br><br>
In September 2017, became the governing unit of the International Hydrogen Industry Association<br><br>
In September 2017, the new Xiaoman Waist One Cup was released and hosted by the International Hydrogen Industry Conference<br><br>
In December 2017, obtained the certificate of intellectual property management system certification");
define("_HOME_TIMELINE3", "In April 2018, became a member of Japan Hydrogen Water Promotion Association<br><br>
In December 2018, won the 9th “Red Action” Love Contribution Award by Shenzhen Lions Club and Shenzhen Blood Center");
define("_HOME_TIMELINE2", "In January 2019, the China Urban Development Alliance awarded the title of “Excellent Technology Enterprise in the Field of Natural Sciences”<br><br>
In June 2019, released the “neighborhood” mini water station brand strategy<br><br>
In October 2019, the “neighborhood hydrogen” min water station successfully developed its own water production equipment<br><br>
In December 2019, the first “neighborhood hydrogen” mini water station successfully landed in Longgang, Shenzhen
");
define("_HOME_TIMELINE1", "In March 2020, “Neighborhood Hydrogen” official store opened<br><br>
2021, officially enter Southeast Asia
");
define("_HOME_DEV", "Development Path");
define("_HOME_CORE_TEAM", "Core Team Member");
define("_HOME_CONSULTANTS", "Company Consultants");
define("_HOME_CORE1", "<li>Chairman of Shenzhen Hydrogen Love World Company</li>
<li>Founder of Hygieia Universe China brand, leader of core technical team</li>
<li>Chairman of Shenzhen Yishou Technology Company</li>
<li>Vice President of International Hydrogen Industry Association (IMHA)</li>
<li>Chairman of the Shenzhen Traditional Culture Research and Natural Medicine Promotion Committee of Traditional Chinese Medicine</li>");
define("_HOME_CORE2", "<li>Long-term cooperation with Tsingtao Brewery, many years of supply chain management experience</li>
<li>Have extensive industry resources, contacts and good government relations</li>
<li>Have a strong strategic layout and experience in the future manufacturing industry</li>");
define("_HOME_CORE3", "<li>Graduated from Tongji University in Mathematics Science</li>
<li>More than 20 years of experience in top management (TMT) in international companies</li>
<li>Good at combining traditional marketing theories with digital communication applications</li>
<li>Have a wealth of practical experience and a solid theoretical foundation</li>
<li>Serve more than 100 global customers, more than 20 industries</li>");
define("_HOME_CORE4", "<li>Graduated from the Department of Electronic Engineering, Fudan University</li>
<li>Served as technical director of foreign electronic engineering for many years</li>
<li>Sole proprietorship in electronic components and rubber products enterprises for 20 years</li>");
define("_HOME_SOURCE", "Source");
define("_HOME_CON1", "<li>International Biomedical pioneer of molecular hydrogen，Chief Professor, Institute of Geriatrics, Japan Medical University</li>
<li>Professor Shigeo Ohta, published a paper in the world-renowned journal “Natural Medicine”, explaining that hydrogen can selectively eliminate the human body’s evil active oxygen free radicals.</li>");
define("_HOME_CON2", "<li>the Founder and Executive Director of the science-based nonprofit Molecular Hydrogen Institute.</li>
<li>Member of The American Chemical Society</li>
<li>Rotary Leadership Representative Councilor</li>
<li>Director of International Molecular Hydrogen Association (IMHA)</li>
<li>Director of International Hydrogen Standards Association (IHSA)</li>
<li>Director of European Institute of Molecular Hydrogen (EIMH)</li>
<li>Academic Committee of Taishan Institute for Hydrogen Biomedical Research</li>
<li>Hydrogen Association (IMHA)</li>");
define("_HOME_CON3", "<li>Professor of the Second Military Medical University of China</li>
<li>Leading figure in Chinese hydrogen molecular medicine</li>
<li>The only expert who was invited by the famous Japanese scholar, Professor Naruo Ota of Japan Medical University to give a report at the Annual Conference of Hydrogen Medicine</li>");
define("_HOME_UNI", "University and Hospital Research (Partial)");
define("_HOME_OUR_PRODUCT", "Our Product");
define("_HOME_PRO1", "High-concentration Hydrogen-rich Water Dispenser");
define("_HOME_PRO_LI1", "Instant water production with hydrogen-rich water concentration above 1200ppb");
define("_HOME_PRO_LI2", "Equipped with Angel front water purification system");
define("_HOME_PRO_LI3", "Adjustable water temperature, intelligent voice reminder");
define("_HOME_PRO_LI4", "Large group customers such as home users and corporate units");
define("_HOME_PRO2", "High Concentration Proton Water Thumble");
define("_HOME_PRO2_LI1", "DuPont food-grade proton exchange membrane with higher hydrogen concentration");
define("_HOME_PRO2_LI2", "Food-grade safe material, visualized hydrogen cylinder");
define("_HOME_PRO2_LI3", "One-key hydrogen production, 5 minutes, 10 minutes dual mode can be selected");
define("_HOME_PRO2_LI4", "Suitable for office workers, mothers, students, people from enterprises and institutions");
define("_HOME_PRO2_LI5", "The highest concentration hydrogen water tumbler in the market - concentration reaching 6000ppb");
define("_HOME_PRO2_LI6", "The curative effect is comparable to the “Wonders water” of the Nordenau mine");
define("_HOME_PRO2_IMG1", "pro2-des");
define("_HOME_PRO3", "Ultra-high Concentration Proton Hydrogen Rich Cup");
define("_HOME_PRO3_LI1", "3000-6000ppb Polyester material (designated material for infant products in Europe and the United States) has passed the FDA certification of the US Food and Drug Administration");
define("_HOME_PRO3_LI2", "One-key hydrogen production, 5 minutes, 10 minutes dual mode can be selected");
define("_HOME_PRO3_LI3", "Suitable for entire family, especially suitable for sub-healthy people and middle-aged and elderly people
");
define("_HOME_PRO3_LI4", "8 glasses of hydrogen-rich water per day's antioxidant values equivalent to the sum of thousand of fruits and vegetables");
define("_HOME_PRO3_LI5", "Lets the hydrogen energy slow down your biological clock");
define("_HOME_PRO4", "Hydrogen Honey Bagged Drinking Water");
define("_HOME_PRO4_LI1", "High hydrogen-rich water concentration and long shelf life");
define("_HOME_PRO4_LI2", "Unique taste, clear and sweet");
define("_HOME_PRO4_LI3", "Food grade aluminum bag, no plasticization pollution");
define("_HOME_PRO4_LI4", "Moderate capacity, convenient and portable");
define("_HOME_PRO4_LI5", "Business people, stadiums, hotels, conferences and other customers");
define("_HOME_PRO5", "1.2 l Hydrogen Kettle");
define("_HOME_PRO6", "Multifunctional Household Hydrogen Enrichment Machine (Double Hole) FQJ-H401");
define("_HOME_PRO6_LI1", "One machine for dual use, can output dual pipeline gas phase at the same time");
define("_HOME_PRO6_LI2", "It can produce large amounts of hydrogen for a long time with stable output");
define("_HOME_PRO6_LI3", "The hydrogen purity can reach 99.9%, and the flow time can be controlled by buttons");
define("_HOME_PRO6_LI4", "Liquid crystal display hydrogen concentration, automatic reminder of water shortage");
define("_HOME_PRO6_LI5", "High-grade protective body design, safe and risk-free");
define("_HOME_PRO6_LI6", "Suitable for home users, social health centers, hospitals, special care groups, corporate customers");
define("_HOME_PRO7", "Hydrogen Hydrotherapy Apparatus");
define("_HOME_PRO7_LI1", "Large flow output, 30 minutes long hydrogen production");
define("_HOME_PRO7_LI2", "Quickly soothe tension parts, eliminate epidermal bacteria and repair damage");
define("_HOME_PRO7_LI3", "The machine with dual functions, can be used for foot bath and bath");
define("_HOME_PRO7_LI4", "User friendly, come with adjustable timer");
define("_HOME_PRO7_LI5", "Suitable for home user");
define("_HOME_PRO8", "Hydrogen-Rich Spa");
define("_HOME_PRO8_LI1", "Large flow output, 30 minutes long hydrogen production");
define("_HOME_PRO8_LI2", "Quickly soothe tension parts, eliminate epidermal bacteria and repair damage");
define("_HOME_PRO8_LI3", "The buttons are convenient and simple, and the time can be adjusted freely");
define("_HOME_PRO8_LI4", "Developed for SPA people, more in line with the needs of SPA people");
define("_HOME_PRO8_LI5", "Suitable for beauty salon customer");
define("_HOME_PRO9", "Hydrogen Honey Hydrogen Bath Powder");
define("_HOME_PRO9_LI1", "Molten water can produce hydrogen with a concentration of up to 1100ppb");
define("_HOME_PRO9_LI2", "Rich in a variety of beneficial ingredients for human body, beautiful feet and skin care");
define("_HOME_PRO9_LI3", "Single package, easy to store, easy to carry and use");
define("_HOME_PRO9_LI4", "For dull skin, cracked heels, itchy skin, foot odor has an effect");
define("_HOME_PRO9_LI5", "Suitable for female group, foot care group");
define("_HOME_PRO10", "Hydrogen Spring Deep Moisturizes Mask");
define("_HOME_PRO10_LI1", "A unique hydrogen-rich serum that addresses eight major skin problems");
define("_HOME_PRO10_LI2", "Cleans, moisturizes, detoxifies, refines, brightens, fades, and reduces inflammation");
define("_HOME_PRO10_LI3", "Optimum silk material, deep moisturizing, long-lasting moisturizing, more breathable");
define("_HOME_PRO10_LI4", "National patent long-term hydrogen lock technology, food-grade aseptic packaging");
define("_HOME_PRO10_LI5", "Low sensitivity, no additives, no fluorescent agents, hormones, antibiotics, alcohol thickeners, etc");
define("_HOME_PRO10_LI6", "Recommend for man and women care for their appearance");
define("_HOME_PRO11", "Smart Hydrogen Hot Spring Bathtub");
define("_HOME_PRO12p", "pro12-des2");
define("_HOME_PRO13", "High Concentration Hydrogen Enrichment Equipment");
define("_HOME_PRO13_LI1", "Fully automatic multi-media filtration");
define("_HOME_PRO13_LI2", "Fully automatic activated carbon adsorption");
define("_HOME_PRO13_LI3", "ATC sterilization security filter");
define("_HOME_PRO13_LI4", "NF Nanofiltration");
define("_HOME_PRO13_LI5", "High magnetization filter");
define("_HOME_PRO13_LI6", "High purity hydrogen water");
define("_HOME_PRO13_LI7", "Online monitoring");