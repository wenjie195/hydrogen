<?php
//for main.js modal files
define("_MAINJS_ATTENTION", "注意");
define("_MAINJS_ENTER_USERNAME", "请输入您的用户名");
define("_MAINJS_ENTER_EMAIL", "请输入您的电子邮件地址");
define("_MAINJS_ENTER_ICNO", "请输入您的身份证号码");
define("_MAINJS_SELECT_COUNTRY", "请选择你的国家");
define("_MAINJS_ENTER_PHONENO", "请输入您的电话号码");
//apply in all
define("_MAINJS_ALL_LOGOUT", "登出");
//index
define("_MAINJS_INDEX_LOGIN", "登录");
define("_MAINJS_INDEX_USERNAME", "用户名");
define("_MAINJS_INDEX_PASSWORD", "密码");
define("_MAINJS_INDEX_CONFIRM_PASSWORD", "再确认密码");
define("_INDEX_NO_YET", "还未拥有账号？点击此处注册。");
define("_INDEX_LOGIN_NOW", "已拥有账号？点击此处登入。");
define("_INDEX_IC_NO", "身份证号码");
define("_INDEX_DOB", "出生日期");
define("_INDEX_MOBILE_NO", "联络号码");
define("_INDEX_ADDRESS", "地址");
define("_INDEX_ADDRESS1", "地址第一行");
define("_INDEX_ADDRESS2", "地址第二行");
define("_INDEX_ZIPCODE", "邮政编码");
define("_INDEX_STATE", "州");
define("_INDEX_REGISTER", "注册");
define("_INDEX_NO_REPEAT_IC", "*身份证号码不可以与其他账号重复");
//JS
define("_JS_FOOTER", " 氢爱天下版权所有");
define("_JS_LOGIN", "登入");
define("_JS_USERNAME", "用户名");
define("_JS_PASSWORD", "密码");
define("_JS_FULLNAME", "全名");
define("_JS_NEW_PASSWORD", "新密码");
define("_JS_CURRENT_PASSWORD", "现在的密码");
define("_JS_RETYPE_PASSWORD", "再次输入密码");
define("_JS_RETYPE_NEW_PASSWORD", "再次输入新密码");
define("_JS_RETYPE_REFERRER_NAME", "推荐人名字");
define("_JS_REMEMBER_ME", "记住我");
define("_JS_FORGOT_PASSWORD", "忘记密码");
define("_JS_FORGOT_TITLE", "忘记密码");
define("_JS_EMAIL", "邮箱地址");
define("_JS_SIGNUP", "注册");
define("_JS_FIRSTNAME", "名字");
define("_JS_LASTNAME", "姓氏");
define("_JS_GENDER", "性别");
define("_JS_MALE", "男");
define("_JS_FEMALE", "女");
define("_JS_BIRTHDAY", "出生日期");
define("_JS_COUNTRY", "国家");
define("_JS_MALAYSIA", "马来西亚");
define("_JS_SINGAPORE", "新加坡");
define("_JS_PHONE", "电话号码");
define("_JS_REQUEST_TAC", "申请TAC");
define("_JS_TYPE", "类型");
define("_JS_SUBMIT", "确认");
define("_JS_PLACEORDER", "下单");
define("_JS_WITHDRAW_AMOUNT", "提现金额");
define("_JS_SUCCESS", "交易成功！");
define("_JS_CLOSE", "关闭");
define("_JS_ERROR", "错误");
define("_JS_EDIT_USER_PROFILE", "更改会员个资");
define("_JS_CHANGE_USER_PASSWORD", "更改密码");
define("_JS_EDIT_PROFILE", "更改个资");
define("_JS_RESET_PASSWORD", "重置密码");
define("_JS_VERIFY_CODE", "验证码");
define("_JS_SELECT_COUNTRY", "请选择一个国家");
//UPLOAD
define("_UPLOAD_IC_FRONT", "上载身份证/护照正面");
define("_UPLOAD_PROOF_OF_LEGAL_NAME", "拥有法律效应名字证件（正面）");
define("_UPLOAD_PROOF_OF_LEGAL_NAME_DESC", "*例如：能证明您的名字、生日、身份证号码和截止日期的登记或护照。");
define("_UPLOAD_PROOF_OF_LEGAL_NAME_BACK", "拥有法律效应名字证件（反面）");
define("_UPLOAD_PROOF_OF_ADDRESS", "能证明住家地址的证件");
define("_UPLOAD_PROOF_OF_ADDRESS_DESC", "*例如：水电单、驾照或其他有显示您的姓名与住家地址的文件");
define("_UPLOAD", "上载");
define("_UPLOAD_PREVIEW", "预览");
define("_UPLOAD_SELECT_DRAG", "选择您的文件或将它拖至此处");
define("_UPLOAD_PLS_SELECT_IMG", "请选择一张照片");
define("_UPLOAD_SELECT_A_FILE", "选择一个文件");
define("_UPLOAD_IC_BACK", "上载身份证/护照背面");
define("_UPLOAD_UTILITY_BILL_DRIVING_LICENSE", "上载水电费单或驾照");
define("_UPLOAD_SIGNATURE_MAA", "上载LPOA文件签名");
define("_UPLOAD_SIGNATURE", "上载签名");
define("_UPLOAD_CHOOSE_FILE_HERE", "点击这里选择要上载的文件");
//USERDASHBOARD
define("_USERDASHBOARD_INVITATION_LINK", "邀请链接");
define("_USERDASHBOARD_COPY", "复制");
define("_USERDASHBOARD_LOGOUT", "登出");
define("_USERDASHBOARD_INVITED_BY_ME", "我所邀请的会员");
define("_USERHEADER_PROFILE", "个资");
define("_USERHEADER_BANK_ACC", "银行账号");
define("_USERHEADER_UPLOAD_DOC", "上载资料");
define("_USERHEADER_REFER", "推荐");
define("_USERHEADER_HIERARCHY", "组织");
define("RESET_HIERARCHY", "收回");
define("_USERDASHBOARD_TOTAL_COMMISSION", "总佣金");
define("_USERDASHBOARD_DAILY_COMMISSION", "每日扩展");
define("_USERDASHBOARD_MONTHLY_COMMISSION", "每月收益");
define("_USERDASHBOARD_PERSONAL_SALES", "个人业绩");
define("_USERDASHBOARD_GROUP_SALES", "团队业绩");
define("_USERDASHBOARD_DIRECT_DOWNLINE", "直属下线");
define("_USERDASHBOARD_DIRECT_DOWNLINE2", "的直属下线");
define("_USERDASHBOARD_GROUP_MEMBER", "团队成员");
define("_USERDASHBOARD_RANK", "等级");
define("_USERHEADER_BANK_DETAILS", "银行资料");
define("_USERDASHBOARD_DETAILS", "详情");
define("_USERDASHBOARD_PRODUCT_ORDER", "购买商品");
define("_USERDASHBOARD_ORDER_HISTORY", "订单详情");
define("_USERHEADER_BONUS_REPORT", "奖励报告");
define("_USERHEADER_WITHDRAWAL_HISTORY", "提款记录");
define("_USERDASHBOARD_POOL_FUND", "共同基金");
define("_USERDASHBOARD_POOL_FUND_BONUS", "共同基金奖金");
define("_USERDASHBOARD_POOL_BONUS", "共同奖金");
define("_USERDASHBOARD_SALES_MANAGER", "销售经理");
define("_USERDASHBOARD_MARKETING_MANAGER", "市场经理");
define("_USERDASHBOARD_AREA_MANAGER", "区域经理");
define("_USERDASHBOARD_REGION_MANAGER", "地区经理");
define("_USERDASHBOARD_MY_BONUS", "我的奖金");
define("_USERDASHBOARD_BONUS", "奖金");
define("_USERDASHBOARD_MY_SALES", "我的销售");
define("_USERDASHBOARD_TEAM_SALES", "团队销售");
define("_USERDASHBOARD_TEAM_MEMBERS", "团队成员");
define("_USERDASHBOARD_PERSONAL", "我的资料");
define("_USERDASHBOARD_CREDIT", "资金");
define("_USERDASHBOARD_EDIT_PASSWORD", "修改密码");
//BANKDETAILS
define("_BANKDETAILS_ACC_NAME", "银行账号持有者姓名");
define("_BANKDETAILS_ACC_NO", "银行账号号码");
define("_BANKDETAILS_ACC_TYPE", "账号类别");
define("_BANKDETAILS_BANK", "银行");
define("_BANKDETAILS_BANK_SWIFT_CODE", "银行SWIFT码");
define("_BANKDETAILS", "银行账号资料");
//ADMINDASHBOARD
define("_ADMINDASHBOARD_ADD_ONS_CREDIT", "充值");
define("_ADMINDASHBOARD_UPLOAD_EQUITY_PL", "上载股本(Equity PL)");
define("_ADMINDASHBOARD_PROFIT_SHARING", "利润分享");
define("_ADMINDASHBOARD_RESET_MEMBERS", "重置会员余额？");
define("_ADMINDASHBOARD_RESET", "重置每日扩展");
define("_ADMINDASHBOARD_RESET_MONTH", "重置s每月收益？");
//ADMINDASHBOARD
define("_ADMINHEADER_DASHBOARD", "概览");
define("_ADMINHEADER_INFO", "结单");
define("_ADMINHEADER_MARKETING_PLAN", "营销方案");
define("_ADMINHEADER_DAILY_SPREAD", "每日扩展");
define("_ADMINHEADER_MONTHLY_PROFITS", "每月收益");
define("_ADMINHEADER_MEMBERS", "会员");
define("_ADMINHEADER_REPORTS", "结单");
define("_ADMINHEADER_MEMBER_BALANCE", "会员的余额");
define("_ADMINHEADER_MEMBER_WITHDRAW", "会员的提款");
define("_ADMINHEADER_MEMBER_WITHDRAW_HISTORY", "会员的提款记录");
define("_ADMINHEADER_MEMBER_PROFIT", "会员的利益分享");
define("_ADMINHEADER_COMPANY_BALANCE", "公司的余额");
define("_ADMINHEADER_ADD_USER", "添加会员");
define("_ADMINHEADER_VIEW_MEMBER", "浏览会员");
define("_ADMINHEADER_DAILY_BONUS", "每日收益");
define("_ADMINHEADER_MONTHLY_BONUS", "每月收益");
define("_ADMINHEADER_VIEW_LEVEL", "查看阶级");
define("_ADMINHEADER_EDIT_PASSWORD", "更改密码");
define("_ADMINHEADER_MEMBER_PROFIT2", "会员的盈利");
//ADMINNEWCREDIT
define("_ADMINNEWCREDIT_SELECT_FILE", "选择文件");
define("_ADMINNEWCREDIT_MEMBERS_RANKING", "会员阶级");
define("_ADMINNEWCREDIT_FIRST_STEP", "第一步");
define("_ADMINNEWCREDIT_DAILY_SPREAD", "等级和同阶级奖励");
define("_ADMINNEWCREDIT_SECOND_STEP", "第二步");
define("_ADMINNEWCREDIT_MONTHLY_PROFITS", "阶级奖励");
//ADMINVIEWBALANCE
define("_ADMINVIEWBALANCE_NO", "序");
define("_ADMINVIEWBALANCE_NAME", "名字");
define("_ADMINVIEWBALANCE_BALANCE", "余额");
define("_ADMINVIEWBALANCE_EDIT", "更改");
define("_ADMINVIEWBALANCE_VIEW", "查阅");
define("_ADMINVIEWBALANCE_COMPANY_TOTAL", "公司总余额 (CTB)");
define("_ADMINVIEWBALANCE_TOTAL_LOT_SIZE", "面积总大小 (TLS)");
define("_ADMINVIEWBALANCE_LAST_UPDATED", "最近更新");
define("_ADMINVIEWBALANCE_ACTION", "决策");
define("_ADMINVIEWBALANCE_ISSUE", "事件");
define("_ADMINVIEWBALANCE_EDIT_COMPANY_BALANCE", "更改公司余额");
define("_ADMINVIEWBALANCE_PAYOUT", "总支出 :");
define("_ADMINVIEWBALANCE_PAYOUT_RELEASED", "已释放总支出：");
define("_ADMINVIEWBALANCE_PAYOUT_UNRELEASED", "未释放总支出：");
define("_ADMINVIEWBALANCE_WITHDRAW_AMOUNT", "提款数额");
define("_ADMINVIEWBALANCE_APPROVE", "批准");
define("_ADMINVIEWBALANCE_REJECT", "拒绝");
//MULTIBANK
define("_MULTIBANK_PRINT", "打印");
define("_MULTIBANK_VIEW", "查询");
define("_MULTIBANK_ID_DOCUMENT", "ID");
define("_MULTIBANK_UTILITY_BILL", "杂费水电单");
define("_MULTIBANK_ACTION", "决策");
define("_MULTIBANK_SEARCH", "搜索");
define("_MULTIBANK_UPDATE", "更新");
define("_MULTIBANK_GO_BACK", "返回");
define("_MULTIBANK_IC_FRONT", "身份证正面");
define("_MULTIBANK_IC_BACK", "身份证反面");
define("_MULTIBANK_DETAILS", "资料");
define("_MULTIBANK_UPLINE", "上线");
define("_MULTIBANK_PERFORMANCE_FEE", "表现费");
define("_MULTIBANK_START_DATE", "开始日期");
define("_MULTIBANK_END_DATE", "结束日期");
//USERHEADER
define("_USERHEADER_EDIT_PROFILE", "更改个资");
define("_USERHEADER_EDIT_PASSWORD", "更改密码");
define("USERHEADER_DAILY_BONUS", "每日收益结单");
define("USERHEADER_MONTHLY_BONUS", "每月收益结单");
define("_USERHEADER_ADD_NEW", "添加新MT4ID");
//USERHEADER
define("_HIERARCHY_GENERATION", "代数");
define("_HIERARCHY_NAME", "名字");
define("_HIERARCHY_SPONSOR", "赞助商");
define("_HIERARCHY_PERSONAL_SALES", "个人业绩");
define("_HIERARCHY_GROUP_SALES", "团队业绩");
define("_HIERARCHY_GROUP_MEMBER", "团队成员");
define("_HIERARCHY_DIRECT_DOWNLINE", "直属下线");
define("_HIERARCHY_TOTAL_DOWNLINE", "总下线");
define("_HIERARCHY_RANK", "等级");
define("_HIERARCHY_TREE_VIEW", "详细资料");
//REFER
define("_REFER_INVITATION_LINK", "复制邀请链接");
//DAILY BONUS
define("_DAILY_MEMBER_DAILY_BONUS", "等级和同阶级奖励单");
define("_DAILY_FROM", "来自");
define("_DAILY_BONUS", "奖金");
define("_DAILY_DATE", "日期");
define("_DAILY_TIME", "时间");
define("_DAILY_NO_REPORT", "没结单");
define("_DAILY_RELEASED_BONUS", "已释放奖金");
define("_DAILY_UNRELEASED_BONUS", "未释放奖金");
//MONTHLY BONUS
define("_MONTHLY_MEMBER_DAILY_BONUS", "联盟盈利");
define("_MONTHLY_LEVEL", "阶级奖励单");
define("_MONTHLY_NO_REPORT", "没有联盟盈利结单");
//LEVEL DETAILS
define("_VIEWLEVEL_LEVEL_DETAILS", "阶级详情");
define("_VIEWLEVEL_AMOUNT", "数量");
define("_VIEWLEVEL_REQUIREMENTS", "条件");
define("_VIEWLEVEL_DIRECT_SPONSOR", "直属赞助商");
define("_VIEWLEVEL_SELF_INVEST", "自身投资");
define("_VIEWLEVEL_PROFIT_SHARING", "共享盈利");
//ADMIN ADD ON CREDIT
define("_ADMINADDON_RANK_UPDATED", "更新阶级");
define("_ADMINADDON_DAILY_BONUS", "计算奖励");
define("_ADMINADDON_MONTHLY_BONUS", "计算奖励");
define("_ADMINADDON_RESET", "重设");
//ADMIN DAILY SPREAD
define("_ADMINDAILY_RANKING", "阶级");
define("_ADMINDAILY_WALLET", "钱包");
define("_ADMINDAILY_TEAM_PERFORMANCE", "团队表现");
define("_ADMINDAILY_SPREAD_SHARING", "扩展分享");
define("_ADMINDAILY_MEMBERS", "会员");
define("_ADMINDAILY_LEADER", "领导");
define("_ADMINDAILY_DIRECT", "直接赞助3位会员");
define("_ADMINDAILY_TEAM_LEADER", "小队领导");
define("_ADMINDAILY_INDIRECT_DIRECT", "非直属+直属赞助2位领导");
define("_ADMINDAILY_GROUP_LEADER", "团队领导");
define("_ADMINDAILY_INDIRECT_DIRECT_TEAM", "非直属+直属赞助2位小队领导");
define("_ADMINDAILY_GROUP_LEADER_OVERRIDING", "团队领导优势");
define("_MULTIBANK_SEARCH_START", "开始");
define("_MULTIBANK_SEARCH_END", "结束");
// CAPPING
define("_MEMBERS_CAPPING_REPORTS", "封顶结单");
define("_MEMBERS_CAPPING_REPORTS_COMPLETED", "已完成封顶结单");
define("_NO_CAPPING_REPORT", "没有结单");
define("_BONUS_TYPE", "奖金种类");
//ADMIN WITHDRAWAL
define("_ADMINWITHDRAWAL_NO_MEMBER", "无新的会员提款");
//VICTORY
define("_VICTORY_DOCUMENTS", "文件");
define("_VICTORY_URL", "链接");
define("_VICTORY_Copy", "复制");
define("_VICTORY_REFER", "推荐给好友");
define("_VICTORY_MY_REFER", "我的转介");
define("_VICTORY_REFERRAL_LINK", "转介链接");
//broker link
define("_BROKER_LINK", "经纪商链接");
//product
define("_PRODUCT", "商品");
define("_PRODUCT_ADD_TO_CART", "加入购物车");
define("_PRODUCT_CART", "购物车");
define("_PRODUCT_DELETE_ALL", "清空");
define("_PRODUCT_CHECK_OUT", "结账");
define("_PRODUCT_PAYMENT_GATEWAY", "支付网关");
define("_PRODUCT_PAYMENT_DETAILS", "结账详情");
define("_PRODUCT_AMOUNT", "价值");
define("_PRODUCT_PROCEED_TO_PAYMENT", "付款");
define("_PRODUCT_ORDER_ID", "订单序号");
define("_TRANSACTION_ID", "交易序号");
define("_PRODUCT_PAYMENT_STATUS", "付款详情");
define("_PRODUCT_SHIPPING_STATUS", "寄货状态");
define("_PRODUCT_ORDER_DETAILS", "订单详情");
define("_PRODUCT_PURCHASER_NAME", "买家名字");
define("_PRODUCT_PAYMENT_REFERENCE", "付账备注");
define("_PRODUCT_ORDER_DATE", "下单日期");
define("_PRODUCT_SHIPPING_METHOD", "寄货方式");
define("_PRODUCT_TRACKING_NUMBER", "运单号");
define("_PRODUCT_ITEM", "商品");
define("_PRODUCT_QUANTITY", "数量");
define("_PRODUCT_TOTAL_PRICE", "总价");
define("_PRODUCT_SERIAL_NUMBER", "编号");
define("_PRODUCT_LINK", "链接");
define("_PRODUCT_STOCK_SALES", "商品/销售");
define("_PRODUCT_SALES_PAGE", "销售");
define("_PRODUCT_ORDER", "订单");
define("_PRODUCT_ADD", "添加新商品");
define("_PRODUCT_PRICE", "价钱");
define("_PRODUCT_VIEW_DETAILS", "查看资料");
define("_PRODUCT_DELETE", "删除");
define("_PRODUCT_EDIT", "修改商品");
define("_PRODUCT_NAME", "商品名字");
define("_PRODUCT_COMMISSION", "津贴");
define("_PRODUCT_DESCRIPTION", "详细资料");
define("_PRODUCT_AVOID", "切勿使用");
define("_PRODUCT_IMAGE", "商品照片");
define("_PRODUCT_SUBMIT", "提交");
define("_PRODUCT_VIEW_STOCK", "查看商品和销售量");
define("_PRODUCT_ALL_STOCK", "所有商品和销售成果");
define("_PRODUCT_ADD_STOCK", "添加商品");
define("_PRODUCT_AVAILABLE_STOCK", "存货");
define("_PRODUCT_TOTAL_SALES", "总销售额");
define("_PRODUCT_PAYMENT_VERIFICATION", "付款验证");
define("_PRODUCT_PAYMENT_FAIL", "汇款失败");
define("_PRODUCT_PENDING", "待处理");
define("_PRODUCT_SHIPPED", "已寄出");
define("_PRODUCT_REJECTED", "已拒绝");
define("_PRODUCT_RECEIVED", "已收到汇款");
define("_PRODUCT_DATE_UPDATED", "更新日期");
//bonus
define("_BONUS_SYSTEM", "奖金系统");
define("_BONUS_DIRECT", "直属赞助商奖励");
define("_BONUS_DIRECT2", "直属奖励");
define("_BONUS_PURCHASER", "买家");
define("_BONUS_RECEIVER", "收件人");
define("_BONUS_RECEIVER_RANKING", "收件人的等级");
define("_BONUS_OVERRIDING", "覆写共同基金奖金");
define("_BONUS_STATUS", "状态");
define("_BONUS_DIST", "奖金分配");
define("_BONUS_OVERRIDING_BONUS", "覆写奖金");
define("_BONUS_VIEW_BONUS", "查看奖金");
define("_BONUS_RECORD", "记录");
//admin
define("_ADMIN_ADMINDASHBOARD", "管理员概览");
define("_ADMIN_TOTAL_MEMBER", "会员总数");
define("_ADMIN_PENDING_ORDER", "待处理订单");
define("_ADMIN_DIST", "分发奖励");
define("_ADMIN_AUTOWITHDRAWAL", "自动提款");
define("_ADMIN_DISTBONUS", "分发");
define("_ADMIN_TOTAL_BONUS", "总奖励");
//server
define("_SERVER_MAINTENANCE", "系统维修中");
define("_SERVER_MAINTENANCE_TEXT", "系统正在维护中。造成用户们的困扰还请多多包涵。");
//home
define("_HOME_LEADING", "中国氢气第一品牌");
define("_HOME_HQ", "共享氢科技 共享氢健康 以中国为据 造福东南亚");
define("_HOME_WATER_MIRALCE", "水之奇迹");
define("_HOME_FOUR_COUNTRY", "自100多年前，位于");
define("_HOME_FOUR_COUNTRY1", "法國盧爾德鎮、");
define("_HOME_FOUR_COUNTRY2", "墨西哥克雷塔羅、");
define("_HOME_FOUR_COUNTRY3", "德国");
define("_HOME_FOUR_COUNTRY4", "和巴基斯坦");
define("_HOME_FOUR_COUNTRY5", "皆有人民因为摄取当地的泉水而改善了身体健康。可见良好品质的水确实对我们的健康扮演着举足轻重的角色。毕竟人的身体有70%是水，若想保持健康真的不可忽略我们日常吸取的水分品质。");
define("_HOME_THE_HARM", "自由基的危害");
define("_HOME_HARM_P", "随着时代发达，环境污染指数也日渐升高再加上快节奏生活的压力，会导致损害身体组织和细胞的自由基过量生产。自由基又稱游離基，它的危害如下：");
define("_HOME_V1", "產生破壞細胞的化學物質，形成致癌物質");
define("_HOME_V2", "削弱細胞的抵抗力，使身體易受細菌和病菌感染");
define("_HOME_V3", "引发炎症、衰老和關節炎");
define("_HOME_V4", "心腦血管疾病");
define("_HOME_WHAT_IS", "什么是氢？");
define("_HOME_WHAT_IS_P", "富氢水顾名思义即是含有大量氢气的水，而氢气则是自由基的克星。氢是宇宙间最小的分子也最好的抗氧化物，氢进入细胞氢主动与自由基结合成水，将它排出体外，从而治愈一些多年顽疾和改善体质。每天喝八杯富氢水有助清除有害自由基。");
define("_HOME_WHAT_IS_A", "而氢氧气雾化机也被用于新冠肺炎辅助治疗。");
define("_HOME_COMPANY1", "公司成立于2016年6月，专注于氢健康领域的拓展，率先在国内创新了氢健康科技类产品系列和社区氢健康服务体系。公司坐落在深圳宝安区航空路，作为深圳大学南山工研院孵化企业拥有核心专利技术——超高浓度氢氧分离制氢技术，目前已建成中国首个高规格氢科技体验馆。公司致力于构建和发展“以社区为中心”的氢健康共享生态圈，积极引领全球氢产业的升级和变革。");
define("_HOME_COMPANY2", "在创新、大爱、知行合一的企业精神引导下，以人性化，开放式、多元化的管理模式和产业共赢的合作方式，深深影响和带动了氢健康科技行业从业人员和上下游产业合作者，有效推动了中国氢行业的发展和服务体系的革新，被多家媒体誉为 :“中国氢产业最具影响力品牌”和“中国氢健康产业领导品牌”。");
define("_HOME_PREVENTION", "预防不嫌早,收入不能少,分享氢爱人人好。");
define("_HOME_MISSION1", "使命");
define("_HOME_MISSION_DESC", "人手一杯，杯不离手，让氢进入千家万户。");
define("_HOME_VISION1", "愿景");
define("_HOME_VISION_DESC", "分享氢科技，共享氢健康。");
define("_HOME_IDEA1", "理念");
define("_HOME_IDEA_DESC", "创新、大爱、全民健康，全民获利。");
define("_HOME_HYDROGEN", "氢");
define("_HOME_HYDROGEN_DESC", "让更多人知道氢气的好处, 人人离病痛。");
define("_HOME_LOVE", "爱");
define("_HOME_LOVE_DESC", "氢爱你我、氢爱家庭，爱护自己,爱护身边的亲戚朋友，把爱带回家。");
define("_HOME_WORLD", "天下");
define("_HOME_WORLD_DESC", "做到人手一杯，提高自身的防护能力，防范胜于治疗。");
define("_HOME_OUR_MISSION_AND", "我们的使命和愿景");
define("_HOME_TIMELINE1", "2020年3月，“邻里氢”官方直营店开业<br><br>
2021 进军东南亚
");
define("_HOME_TIMELINE2", "2019年1月，中国城市发展联盟授予“自然科学领域优秀科技企业”称号<br><br>
2019年6月，发布”邻里“mini水站品牌战略<br><br>
2019年10月，”邻里氢“min水站独自研发制水设备成功<br><br>
2019年12月，第一家”邻里氢“mini水站成功落地深圳龙岗
");
define("_HOME_TIMELINE3", "2018年4月，成为日本水素水振兴协会会员<br><br>
2018年12月，荣获深圳狮子会、深圳血液中心第9届“红色行动”爱心贡献奖
");
define("_HOME_TIMELINE4", "2017年5月，成为深圳市科技经济促进会氢产业委员会会长单位<br><br>
2017年6月，与深圳科协一起完成中国氢产业行业标准课题<br><br>
2017年9月，成为国际氢产业协会理事单位<br><br>
2017年9月，新款小蛮腰一体杯发布暨国际氢产业研讨会主办<br><br>
2017年12月，获得知识产权管理体系认证证书
");
define("_HOME_DEV", "发展历程");
define("_HOME_CORE_TEAM", "核心团队");
define("_HOME_CONSULTANTS", "氢爱天下公司顾问");
define("_HOME_CORE1", "<li>深圳氢爱天下公司董事长</li>
<li>氢爱天下品牌创始人，核心技术团队组长</li>
<li>深圳益寿科技公司董事长</li>
<li>国际氢产业协会（IMHA）副会长</li>
<li>深圳市传统文化研究中医自然医学促进专业委员会理事长</li>");
define("_HOME_CORE2", "<li>长期与青岛啤酒合作，多年供应链管理经验</li>
<li>拥有广泛的行业资源、人脉和良好的政府关系</li>
<li>对未来制造业有较强的战略布局和心得经验</li>");
define("_HOME_CORE3", "<li>毕业于同济大学数学科学</li>
<li>二十多年国际企业高层管理（TMT）经验</li>
<li>擅长传统营销理论结合数字化传播应用</li>
<li>拥有丰富的实操经验和扎实的理论基础</li>
<li>服务超过100个全球客户，二十多个行业</li>");
define("_HOME_CORE4", "<li>毕业于复旦大学电子工程系</li>
<li>曾任外资电子工程技术总监多年</li>
<li>独资经营电子元器和橡胶制品企业20年</li>");
define("_HOME_SOURCE", "出处");
define("_HOME_CON1", "<li>日本氢医学研究第一人</li>
<li>国际氢分子生物医学先驱</li>
<li>太田成男教授，在世界著名杂志《自然医学》上发表了长篇论文，说明氢可以选性地清除人体恶行活性氧自由基。</li>");
define("_HOME_CON2", "<li>美国氢分子研究所创始人</li>
<li>Tyler Lebaron，美国分子研究所创始人。</li>
<li>美国化学学会会员。</li>
<li>参加了第二届氢分子生。物</li>
<li>医学学术交流会暨中国医疗保健国际交流，促进会并参与了演讲。</li>");
define("_HOME_CON3", "<li>中国氢分子生物学第一人</li>
<li>中国第二军医大学教授</li>
<li>中国氢分子医学领军人物</li>
<li>唯一受日本著名学者</li>
<li>日本医科大学太田成男教授邀请在日本氢气医学学术年会发表报告专家</li>");
define("_HOME_UNI", "高校和医院研究（部分）");
define("_HOME_OUR_PRODUCT", "我们的产品");
define("_HOME_PRO1", "高浓度富氢饮水机");
define("_HOME_PRO_LI1", "即时制水，富氢水浓度1200ppb以上");
define("_HOME_PRO_LI2", "配套安吉尔前置净水系统");
define("_HOME_PRO_LI3", "自由调节水温，智能语音提醒");
define("_HOME_PRO_LI4", "家庭用户，企业单位等大集团客户");
define("_HOME_PRO2", "高浓度质子水素杯");
define("_HOME_PRO2_LI1", "杜邦食品级质子交换膜，富氢浓度更高");
define("_HOME_PRO2_LI2", "食品级安全材质，可视化氢气瓶");
define("_HOME_PRO2_LI3", "一键制氢，5分钟、10分钟双模式可以选");
define("_HOME_PRO2_LI4", "适合上班族，宝妈，学生，企事业单位人群");
define("_HOME_PRO2_LI5", "市场上最高浓度氢气水杯,浓度高达 6000ppb");
define("_HOME_PRO2_LI6", "疗效媲美诺尔登瑙矿坑的“神奇之水”");
define("_HOME_PRO2_IMG1", "pro2-des1");
define("_HOME_PRO3", "超高浓度质子富氢杯");
define("_HOME_PRO3_LI1", "3000- 6000ppb 采用共聚脂材料（ 欧美地区婴幼儿用品指定材质）通过美国食品药品管理局FDA认证");
define("_HOME_PRO3_LI2", "一键制氢，5分钟、10分钟双模式可以选");
define("_HOME_PRO3_LI3", "全家适用，特别适合亚健康人群及中老年人");
define("_HOME_PRO3_LI4", "每天8杯富氢水相等于千百个水果蔬菜抗氧化数值的总和");
define("_HOME_PRO3_LI5", "开启水素能量让生理时钟慢一点");
define("_HOME_PRO4", "氢蜜袋装饮用水");
define("_HOME_PRO4_LI1", "富氢水浓度高，保质期长");
define("_HOME_PRO4_LI2", "口感独特，清冽甘甜");
define("_HOME_PRO4_LI3", "食品级铝袋，无塑化污染");
define("_HOME_PRO4_LI4", "容量适中，方便便携");
define("_HOME_PRO4_LI5", "商务人士，体育场馆、酒店、会议等客户");
define("_HOME_PRO5", "电热富氢水壶");
define("_HOME_PRO6", "多功能家用富氢机（双孔） FQJ - H401 ");
define("_HOME_PRO6_LI1", "一机双用，可同时输出双管道氢气");
define("_HOME_PRO6_LI2", "可长时间大量制氢，输出稳定");
define("_HOME_PRO6_LI3", "氢纯度可达99.9%，可按键控流量时间");
define("_HOME_PRO6_LI4", "液晶显示氢浓度，缺水自动提醒");
define("_HOME_PRO6_LI5", "高级别防护机身设计，安全无风险");
define("_HOME_PRO6_LI6", "适合家庭用户、社康中心、医院、特殊看护人群、企业公司客户");
define("_HOME_PRO7", "富氢水疗仪");
define("_HOME_PRO7_LI1", "大流量输出，30分钟超长制氢");
define("_HOME_PRO7_LI2", "快速舒缓紧张部位，消灭表皮细菌，修复损伤");
define("_HOME_PRO7_LI3", "一机两用，可用于泡脚、泡澡");
define("_HOME_PRO7_LI4", "按键方便简单，可自由调控时间");
define("_HOME_PRO7_LI5", "适合家庭用户");
define("_HOME_PRO8", "富氢SPA仪");
define("_HOME_PRO8_LI1", "大流量输出，30分钟超长制氢");
define("_HOME_PRO8_LI2", "快速舒缓紧张部位，消灭表皮细菌，修复损伤");
define("_HOME_PRO8_LI3", "按键方便简单，可自由调控时间");
define("_HOME_PRO8_LI4", "针对SPA人群研发，更符合SPA人士需求");
define("_HOME_PRO8_LI5", "适合美容院客户");
define("_HOME_PRO9", "氢蜜氢浴粉");
define("_HOME_PRO9_LI1", "融水既可产氢，浓度可达1100ppb");
define("_HOME_PRO9_LI2", "富含多种对人体有益成分，美足护肤");
define("_HOME_PRO9_LI3", "单个包装，易储存，携带、使用方便");
define("_HOME_PRO9_LI4", "针对皮肤暗沉，足跟开裂，起皮发痒，脚部异味均有效果。");
define("_HOME_PRO9_LI5", "适合女性人群、护理脚部人群");
define("_HOME_PRO10", "氢春密润面膜");
define("_HOME_PRO10_LI1", "独特富氢精华液，针对肌肤八大问题");
define("_HOME_PRO10_LI2", "清洁、保湿、排毒、细致、提亮、淡印、消炎");
define("_HOME_PRO10_LI3", "优选蚕丝材质，深层补水，长效保湿，透气更莹润");
define("_HOME_PRO10_LI4", "国家专利长效锁氢技术，食品级无菌包装");
define("_HOME_PRO10_LI5", "低敏无添加，无荧光剂、激素、抗生素、酒精增稠剂等");
define("_HOME_PRO10_LI6", "爱美人群，肌肤护理人群");
define("_HOME_PRO11", "智能富氢温泉浴浴缸");
define("_HOME_PRO12p", "pro12-des");
define("_HOME_PRO13", "高浓度富氢设备");
define("_HOME_PRO13_LI1", "全自动多介质过滤");
define("_HOME_PRO13_LI2", "全自动活性碳吸附");
define("_HOME_PRO13_LI3", "ATC杀菌保安过滤");
define("_HOME_PRO13_LI4", "NF纳滤");
define("_HOME_PRO13_LI5", "高磁化过滤");
define("_HOME_PRO13_LI6", "高纯度氢水");
define("_HOME_PRO13_LI7", "在线监测");
