<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Bonus.php';
// require_once dirname(__FILE__) . '/classes/BonusPoolFund.php';
require_once dirname(__FILE__) . '/classes/BonusRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $directBonus = getBonus($conn);
// $poolBonus = getBonusRecord($conn);
$poolBonus = getBonusRecord($conn, " WHERE amount > 0 ");

$conn->close();

?>

<!DOCTYPE html>
<html>
<head>

	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://hygeniegroup.com/adminViewBonusPool.php" />
    <link rel="canonical" href="https://hygeniegroup.com/adminViewBonusPool.php" />
    <meta property="og:title" content="<?php echo _USERDASHBOARD_POOL_BONUS ?> | Hygenie Group" />
    <title><?php echo _USERDASHBOARD_POOL_BONUS ?> | Hygenie Group</title>

	<?php include 'css.php'; ?>

</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">

    <div class="width100 shipping-div2 margin-top15">

    <h1 class="small-h1-a text-center white-text"> <a class="blue-link" href="adminViewBonusDirect.php"><?php echo _BONUS_DIRECT2 ?></a> | <?php echo _USERDASHBOARD_POOL_BONUS ?> | <a class="blue-link" href="adminViewBonusOverriding.php"><?php echo _BONUS_OVERRIDING_BONUS ?></a> </h1>
		
		<div class="width100">
			<form method='post' action='adminViewBonusPoolSearch.php' target="_blank"> 
				<div class="fake-input-div overflow  three-input-with-btn">
					<img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
					<input type="text" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _MULTIBANK_START_DATE ?>" class="clean pop-input fake-input" name='fromDate' id="fromDate">
				</div>
				<div class="fake-input-div overflow three-input-with-btn second-three">
					<img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
					<input type="text" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _MULTIBANK_END_DATE ?>" class="clean pop-input fake-input" name='endDate' id="endDate">
				</div>    

                <div class="btn-div3">
                    <input type='submit' name='but_search' value='<?php echo _MULTIBANK_SEARCH ?>' class="clean blue-ow-btn">
                </div>

			</form>         
        </div>
<p class="white-text p-title"><b><a class="blue-link" href="adminViewBonusPoolRecord.php"><?php echo _USERDASHBOARD_POOL_BONUS ?> (<?php echo _BONUS_RECORD ?>)</a> | <?php echo _USERDASHBOARD_POOL_BONUS ?></b></p>
		<div class="overflow-scroll-div">
    		<!-- <p class="white-text p-title"><b>Direct Bonus</b></p> -->
			
			<table class="table-css fix-th tablesorter smaller-font-table">
				<thead>
					<tr>
						<th class="th"><?php echo _ADMINVIEWBALANCE_NO ?></th>
						<th class="th"><?php echo _BONUS_RECEIVER ?></th>
						<th class="th"><?php echo _PRODUCT_AMOUNT ?></th>
						<th class="th"><?php echo _BONUS_TYPE ?></th>
						<th class="th"><?php echo _DAILY_DATE ?></th>
					</tr>
				</thead>
				<tbody>
				<?php
				if($poolBonus)
				{
					for($cnt = 0;$cnt < count($poolBonus) ;$cnt++)
					{
					?>
						<tr>
							<td><?php echo ($cnt+1)?></td>
							<td><?php echo $poolBonus[$cnt]->getUsername();?></td>
							<td><?php echo $poolBonus[$cnt]->getAmount();?></td>

							<td>
								<?php 
									$userRank = $poolBonus[$cnt]->getBonusType();
									if($userRank == 'Bonus 2 Pool Fund (District Manager 3%)')
									{
										echo $userRanking = 'Bonus 2 Pool Fund (Region Manager 3%)';
									}
									elseif($userRank == 'Bonus 2 Pool Fund (Senior Manager 2%)')
									{
										echo $userRanking = 'Bonus 2 Pool Fund (Marketing Manager 2%)';
									}
									elseif($userRank == 'Bonus 2 Pool Fund (Manager 2%)')
									{
										echo $userRanking = 'Bonus 2 Pool Fund (Sales Manager 3%)';
									}
									else
									{
										echo $userRanking = $userRank;
									}
								?>
							</td>

							<td><?php echo $poolBonus[$cnt]->getDateCreated();?></td>
						</tr>
					<?php
					}
					?>
				<?php
				}
				?>
				</tbody>
			</table>
		</div>

	</div>

</div>

<?php include 'js.php'; ?>

<script src="js/headroom.js"></script>

<script>
  $(function()
  {
    $("#fromDate").datepicker(
    {
    dateFormat:'yy-mm-dd',
    changeMonth: true,
    changeYear:true,
    }

    );
  });
</script>

<script>
  $(function()
  {
    $("#endDate").datepicker(
    {
      dateFormat:'yy-mm-dd',
      changeMonth: true,
      changeYear:true,
    }
    );
  });
</script>

</body>
</html>