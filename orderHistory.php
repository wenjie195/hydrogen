<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$paymentStatus = getOrders($conn);
$paymentStatus = getOrders($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $paymentStatus = getOrders($conn, " WHERE payment_status = 'APPROVED' ");

$conn->close();

?>

<!DOCTYPE html>
<html>
<head>

	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://hygeniegroup.com/orderHistory.php" />
    <link rel="canonical" href="https://hygeniegroup.com/orderHistory.php" />
    <meta property="og:title" content="<?php echo _USERDASHBOARD_ORDER_HISTORY ?> | Hygenie Group" />
    <title><?php echo _USERDASHBOARD_ORDER_HISTORY ?> | Hygenie Group</title>

	<?php include 'css.php'; ?>

</head>

<!-- <body> -->

<body class="body">
<?php include 'header.php'; ?>

<!-- <div class="demo"> -->

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">

<h1 class="small-h1-a text-center white-text"><?php echo _USERDASHBOARD_ORDER_HISTORY ?></h1>

    <div class="width100 shipping-div2 margin-top15">

		<div class="overflow-scroll-div">
			<table class="table-css fix-th tablesorter smaller-font-table" id="myTable">
				<thead>
					<tr>
						<!-- <img src="img/sort.png" class="sort-img"> -->
						<th class="th"><?php echo _ADMINVIEWBALANCE_NO ?></th>
            <th class="th"><?php echo _PRODUCT_ORDER_ID ?></th>
            <th class="th"><?php echo _PRODUCT_AMOUNT ?></th>
            <th class="th"><?php echo _TRANSACTION_ID ?></th>
            <th class="th"><?php echo _PRODUCT_PAYMENT_STATUS ?></th>
            <th class="th"><?php echo _PRODUCT_SHIPPING_STATUS ?></th>
            <th class="th"><?php echo _MULTIBANK_DETAILS ?></th>
					</tr>
				</thead>
				<tbody id="myFilter">

					<?php
					if($paymentStatus)
					{
						for($cnt = 0;$cnt < count($paymentStatus) ;$cnt++)
						{
						?>
							<tr>
								<td><?php echo ($cnt+1)?></td>
								<td><?php echo $paymentStatus[$cnt]->getOrderId();?></td>
								<td><?php echo $paymentStatus[$cnt]->getSubtotal();?></td>
                <td><?php echo $paymentStatus[$cnt]->getPaymentBankReference();?></td>

                <td><?php echo $paymentStatus[$cnt]->getPaymentStatus();?></td>
                <td>
                  <?php 
                    $shippingStatus = $paymentStatus[$cnt]->getShippingStatus();
                    if($shippingStatus == 'TBC')
                    {}
                    else
                    {
                      echo $shippingStatus;
                    }
                  ?>
                </td>

                <!-- <td>
                    <form action="utilities/adminOrderApprovedFunction.php" method="POST">
                        <button class="clean blue-ow-btn" type="submit" name="order_uid" value="<?php //echo $paymentStatus[$cnt]->getOrderId();?>">
                            Approved
                        </button>
                    </form>
                </td> -->

                <td>
                  <form action="userOrderDetailsSR.php" method="POST">
                      <button class="clean blue-ow-btn" type="submit" name="order_uid" value="<?php echo $paymentStatus[$cnt]->getOrderId();?>">
                          <?php echo _MULTIBANK_VIEW ?>
                      </button>
                  </form>
                </td>

							</tr>
						<?php
						}
						?>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- <div class="width100 same-padding footer-div">
	<p class="footer-p white-text"><?php //echo _JS_FOOTER ?></p>
</div> -->
<?php include 'js.php'; ?>
<!-- </div> -->

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput2");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>


<script>
function myFunctionD() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput4");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionE() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput5");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[4];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script src="js/headroom.js"></script>

<script>
  $(function(){
    $("#myTable").tablesorter( {dateFormat: 'pt'} );
  });
</script>
<?php if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Update User Profile.";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Successfully Change User Password.";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Successfully Update User Bank Details.";
        }
        else if($_GET['type'] == 12)
        {
            $messageType = "Fail to upload.";
        }
        else if($_GET['type'] == 13)
        {
            $messageType = "Fail to upload.";
        }
        else if($_GET['type'] == 14)
        {
            $messageType = "EROR.";
        }
        else if($_GET['type'] == 15)
        {
            $messageType = "EROR 2.";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong Current Password Entered !";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "Successfully Issue Payroll !";
        }
        // echo '<script>
        // $("#audio")[0].play();
        // putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        echo '<script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        unset($_SESSION['messageType']);
    }
}
?>

</body>
</html>