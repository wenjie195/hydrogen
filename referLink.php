<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $countryList = getCountries($conn);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>

    <meta property="og:url" content="https://hygeniegroup.com/referLink.php" />
    <link rel="canonical" href="https://hygeniegroup.com/referLink.php" />
    <meta property="og:title" content="Refer Link  | Hygenie Group" />
    <title>Refer Link  | Hygenie Group</title>

	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height text-center" id="firefly">
    <div class="width100 overflow text-center">
    	<img src="img/share.png" class="middle-title-icon" alt="<?php echo _VICTORY_REFER ?>" title="<?php echo _VICTORY_REFER ?>">
    </div>

	<div class="width100 overflow">
		<h1 class="pop-h1"><?php echo _VICTORY_REFER ?></h1>
	</div>

    <div class="spacing-div"></div>

    <div class="clear"></div>

    <?php
    if(isset($_GET['id']))
    {
    ?>

    <?php
        $actual_link = $path = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $fullPath = dirname($path); 
    ?>
    <input type="hidden" id="linkCopy" value="<?php echo "https://".$fullPath."/register.php?referrerUID=".$_GET['id']?>">
    <h3 class="invite-h3"><b><?php echo _VICTORY_URL ?>:</b><br> <a id="invest-now-referral-link" href="<?php echo 'https://'.$fullPath.'/register.php?referrerUID='.$_GET['id']?>" class="invitation-link-a black-link-underline"><?php echo "https://".$fullPath."/register.php?referrerUID=".$_GET['id']?></a></h3>
    <div class="width100 text-center overflow">
        <button class="invite-btn blue-button margin-top15 pill-button margin-auto clean" id="copy-referral-link"><?php echo _VICTORY_Copy ?></button>
    </div>

    <?php
    }
    ?>

    <div class="clear"></div>

</div>
<?php include 'js.php'; ?>
</body>
</html>