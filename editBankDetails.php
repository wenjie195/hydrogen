<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$countryList = getCountries($conn);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://hygeniegroup.com/editBankDetails.php" />
    <link rel="canonical" href="https://hygeniegroup.com/editBankDetails.php" />
    <meta property="og:title" content="<?php echo _BANKDETAILS ?> | Hygenie Group" />
    <title><?php echo _BANKDETAILS ?> | Hygenie Group</title>
	<?php include 'css.php'; ?>
</head>
<body class="body">

<!-- <?php //include 'userHeader.php'; ?> -->

<?php
$userAccNumber = $userData->getBankAccNumber();
if($userAccNumber == '')
{}
else
{
?>
    <?php include 'header.php'; ?>
<?php
}
?>

<!-- <div class="width100 same-padding menu-distance darkbg min-height big-black-text user-dash user-dash2 ow-white-css" id="firefly"> -->
<div class="width100 same-padding menu-distance darkbg min-height" id="firefly" style="padding-left:4.5% !important; padding-right:4.5% !important;">

    <form action="utilities/editBankDetailsFunction.php" method="POST">

        <div class="width100 overflow text-center">
            <img src="img/edit-profile2.png" class="middle-title-icon" alt="<?php echo _BANKDETAILS ?>" title="<?php echo _BANKDETAILS ?>">
        </div>

        <div class="width100 overflow">   
            <h1 class="pop-h1 h1-title text-center"><?php echo _BANKDETAILS ?></h1>
        </div>

        <div class="dual-input">
            <p class="input-top-text"><?php echo _BANKDETAILS_BANK ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _BANKDETAILS_BANK ?>" value="<?php echo $userData->getBankName();?>" id="bank_name" name="bank_name" required>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text"><?php echo _BANKDETAILS_ACC_NAME ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _BANKDETAILS_ACC_NAME ?>" value="<?php echo $userData->getBankAccName();?>" id="bank_acc_holder" name="bank_acc_holder" required>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-text"><?php echo _BANKDETAILS_ACC_NO ?></p>
            <input class="clean pop-input" type="text" placeholder="<?php echo _BANKDETAILS_ACC_NO ?>" value="<?php echo $userData->getBankAccNumber();?>" id="bank_acc_number" name="bank_acc_number">      
        </div>

        <div class="clear"></div>

		<div class="width100 text-center">
        	<button class="clean blue-button one-button-width pill-button margin-auto" name="submit"><?php echo _JS_SUBMIT ?></button>
        </div>

    </form>

</div>

<?php include 'js.php'; ?>
</body>
</html>