<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
	<meta property="og:url" content="https://hygeniegroup.com/successPayment.php" />
    <link rel="canonical" href="https://hygeniegroup.com/successPayment.php" />
    <meta property="og:title" content="Success Payment  | Hygenie Group" />
    <title>Success Payment  | Hygenie Group</title>

	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height profile-big-div user-dash user-dash2" id="firefly">

    <div class="width100 overflow text-center">
    	<img src="img/payment-success.png" class="middle-title-icon">
    </div>

    <div class="width100 overflow">
		  <h1 class="text-center white-text">Payment Success</h1>
    </div>
    
	<div class="clear"></div>

</div>

<?php include 'js.php'; ?>

</body>
</html>
