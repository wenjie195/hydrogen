<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$userDetails = getUser($conn, "WHERE user_type = 1 AND bank_acc_number != '' ");
if ($userDetails)
{
     for ($i=0; $i <count($userDetails) ; $i++)
     {
          $uid = $userDetails[$i]->getUid();
          $withdrawUid = md5(uniqid());

          //convertion
          $currentAmount = $userDetails[$i]->getWallet();
          $finalAmount = ($currentAmount * 0.6);

          $bankName = $userDetails[$i]->getBankName();
          $bankAccHolder = $userDetails[$i]->getBankAccName();
          $bankAccNo = $userDetails[$i]->getBankAccNumber();

          $empty = 0;
          $status = 'PENDING';
          // if ($wallet > 0 && $bankAccHolder && $bankAccNo)
          if ($currentAmount > 0)
          {
               $autoWithdrawal = insertDynamicData($conn, 'withdrawal',array("uid","withdrawal_uid","current_amount","final_amount","status","bank_name","bank_account_holder","bank_account_no"),array($uid,$withdrawUid,$currentAmount,$finalAmount,$status,$bankName,$bankAccHolder,$bankAccNo), "ssssssss");
               if ($autoWithdrawal)
               {
                    // echo "success";
                    // echo "<br>";
                    // $userWalletUpdated = updateDynamicData($conn,"user", "WHERE uid =?",array("sales"),array($empty,$uid), "ss");
                    $userWalletUpdated = updateDynamicData($conn,"user", "WHERE uid =?",array("wallet"),array($empty,$uid), "ss");
                    $_SESSION['messageType'] = 1;
                    header('Location: adminDashboard.php?type=6');
               }
               else
               {
                    echo "fail";
                    echo "<br>";
               }
          }
     }
}

?>