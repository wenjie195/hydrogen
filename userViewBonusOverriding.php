<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Bonus.php';
require_once dirname(__FILE__) . '/classes/BonusOverriding.php';
require_once dirname(__FILE__) . '/classes/BonusPoolFund.php';
require_once dirname(__FILE__) . '/classes/BonusRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

$bonusThree = getBonusOverriding($conn, "WHERE uid = ? ORDER BY date_created DESC ",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>

    <meta property="og:url" content="https://hygeniegroup.com/userViewBonusOverriding.php" />
    <link rel="canonical" href="https://hygeniegroup.com/userViewBonusOverriding.php" />
    <meta property="og:title" content="<?php echo _BONUS_OVERRIDING ?>  | Hygenie Group" />
    <title><?php echo _BONUS_OVERRIDING ?>  | Hygenie Group</title>

	<?php include 'css.php'; ?>
</head>
<style media="screen">
  .blue-button{
  font-size: 10px;
  width: auto;
  margin-top: 50px;
  padding: 5px;
  margin-left: -20px;
  }
  ul{
    list-style: none;
    list-style-image: url('img/li.png');
  }
  li{
    vertical-align: middle;
    cursor: pointer;
  }
</style>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height" id="firefly">
  <div class="width100 overflow text-center">
    <img src="img/hierachy.png" class="middle-title-icon" alt="<?php echo _BONUS_SYSTEM ?>" title="<?php echo _BONUS_SYSTEM ?>">
  </div>

  <div class="width100 overflow">
    <h1 class="small-h1-a text-center white-text"> <a class="blue-link" href="userViewBonusDirect.php"><?php echo _BONUS_DIRECT2 ?></a> | <a class="blue-link" href="userViewBonusPool.php"><?php echo _USERDASHBOARD_POOL_BONUS ?></a> | <?php echo _BONUS_OVERRIDING_BONUS ?></h1>
  </div>

  <div class="clear"></div>

  <div class="four-box-size text-center white-div-css">
    <img src="img/bonus-3.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_BONUS ?> 3" title="<?php echo _USERDASHBOARD_BONUS ?> 3">
    <div class="inner-white-div">
      <p class="white-div-p"><?php echo _ADMIN_TOTAL_BONUS ?> 3</p>
      <?php
      if($bonusThree)
      {
        $totalBonusThree = 0;
        for ($cnt=0; $cnt <count($bonusThree) ; $cnt++)
        {
          $totalBonusThree += $bonusThree[$cnt]->getAmount();
        }
      }
      else
      {
        $totalBonusThree = 0 ;
      }
      ?>
      <p class="white-div-p white-div-amount">PV <?php echo $totalBonusThree;?></p>
    </div>
  </div> 

  <div class="clear"></div>

  <div class="width100">
    <form method='post' action='userViewBonusOverridingSearch.php' target="_blank"> 
      <div class="fake-input-div overflow  three-input-with-btn">
        <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
        <input type="text" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _MULTIBANK_START_DATE ?>" class="clean pop-input fake-input" name='fromDate' id="fromDate">
      </div>
      <div class="fake-input-div overflow three-input-with-btn second-three">
        <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
        <input type="text" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _MULTIBANK_END_DATE ?>" class="clean pop-input fake-input" name='endDate' id="endDate">
      </div>    
      <div class="btn-div3">
          <input type='submit' name='but_search' value='<?php echo _MULTIBANK_SEARCH ?>' class="clean blue-ow-btn">
      </div>
    </form>         
  </div>

  <div class="clear"></div>

  <div class="overflow-scroll-div">
    <table class="table-css fix-th">
				<thead>
					<tr>
						<th class="th"><?php echo _ADMINVIEWBALANCE_NO ?></th>
						<th class="th"><?php echo _BONUS_PURCHASER ?></th>
						<th class="th"><?php echo _PRODUCT_AMOUNT ?></th>
						<th class="th"><?php echo _BONUS_TYPE ?></th>
            <th><?php echo _DAILY_DATE ?></th>
					</tr>
				</thead>
        <tbody>
        <?php
        if($bonusThree)
        {
          for($cnt = 0;$cnt < count($bonusThree) ;$cnt++)
          {
          ?>
            <tr>
              <td><?php echo ($cnt+1)?></td>
              <td><?php echo $bonusThree[$cnt]->getAmount();?></td>
              <td><?php echo $bonusThree[$cnt]->getBonusType();?></td>
              <td><?php echo $bonusThree[$cnt]->getDateCreated();?></td>
            </tr>
          <?php
          }
          ?>
        <?php
        }
        ?>
			</tbody>
		</table>
  </div>

  <div class="clear"></div>

</div>

<?php include 'js.php'; ?>

<script>
  $(function()
  {
    $("#fromDate").datepicker(
    {
    dateFormat:'yy-mm-dd',
    changeMonth: true,
    changeYear:true,
    }

    );
  });
</script>

<script>
  $(function()
  {
    $("#endDate").datepicker(
    {
      dateFormat:'yy-mm-dd',
      changeMonth: true,
      changeYear:true,
    }
    );
  });
</script>

</body>
</html>