<?php
// if (session_id() == "")
// {
//   session_start();
// }
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Shipping.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
$date = date("Y-m-d");
$time = date("h:i a");

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$id = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
$orderID = $id[0]->getId();
$orderUID = $id[0]->getOrderId();

$paymentMethod = 'BILLPLZ';
$paymentStatus = 'WAITING';
// $paymentStatus = 'PENDING';
// $shippingStatus = 'PENDING';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $conn = connDB();
    
    $orderIDpass = ($_POST['uid']);

    $name = ($_POST["insert_name"]);
    $contactNo = ($_POST["insert_contactNo"]);
    $address_1 = ($_POST["insert_address_1"]);
    $subtotal = ($_POST["subtotal"]);

    $commission = ($_POST["totalCommission"]);

    $adjustTotal = ($subtotal * 100);

    if(isset($_POST['shippingSubmit']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
    
        //echo "save to database";
        if($name)
        {
            array_push($tableName,"name");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }
    
        if($contactNo)
        {
            array_push($tableName,"contactNo");
            array_push($tableValue,$contactNo);
            $stringType .=  "s";
        }
    
        if($address_1)
        {
            array_push($tableName,"address_line_1");
            array_push($tableValue,$address_1);
            $stringType .=  "s";
        }

        if($subtotal)
        {
            array_push($tableName,"subtotal");
            array_push($tableValue,$subtotal);
            $stringType .=  "d";
        }

        if($commission)
        {
            array_push($tableName,"commission");
            array_push($tableValue,$commission);
            $stringType .=  "s";
        }

        if($paymentMethod)
        {
            array_push($tableName,"payment_method");
            array_push($tableValue,$paymentMethod);
            $stringType .=  "s";
        }
        if($paymentStatus)
        {
            array_push($tableName,"payment_status");
            array_push($tableValue,$paymentStatus);
            $stringType .=  "s";
        }
        // if($shippingStatus)
        // {
        //     array_push($tableName,"shipping_status");
        //     array_push($tableValue,$shippingStatus);
        //     $stringType .=  "s";
        // }

        // echo $uid. "<br>";
        // echo $name. "<br>";
        // echo $contactNo. "<br>";
        // echo $address_1. "<br>";
        // echo $subtotal. "<br>";

    
        array_push($tableValue,$orderIDpass);
        $stringType .=  "s";
        $updateOrderDetails = updateDynamicData($conn,"orders"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($updateOrderDetails)
        {
            // echo "<script>alert('Successfully added shipping details!');</script>";
        }
        else
        {
            echo "<script>alert('Fail to add shipping details!');window.location='../checkout.php'</script>"; 
        }
    }
    else
    {
        echo "<script>alert('ERROR 2');window.location='../checkout.php'</script>"; 
    }

         
}
else 
{
     header('Location: ../index.php');
}

// $userRows = getUser($conn," WHERE user_id = ? ",array("user_id"),array($uid),"s");
// $userDetails = $userRows[0];

// $userOrder = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
// $orderDetails = $userOrder[0];

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,2);
}else
{}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
	<meta property="og:url" content="https://hygeniegroup.com/shipping.php" />
    <link rel="canonical" href="https://hygeniegroup.com/shipping.php" />
    <meta property="og:title" content="<?php echo _PRODUCT_PAYMENT_GATEWAY ?>  | Hygenie Group" />
    <title><?php echo _PRODUCT_PAYMENT_GATEWAY ?>  | Hygenie Group</title>

	<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance min-height big-black-text user-dash user-dash2 product-padding ow-black-everything">

    <div class="width100 overflow">
        <!-- <h1 class="text-center pop-h1 ow-black-everything">Shipping</h1> -->
        <h1 class="text-center pop-h1 ow-black-everything"><?php echo _PRODUCT_PAYMENT_DETAILS ?></h1>
    </div>

    <form method="post" action="billplzpost.php">

        <div class="width100 overflow border-bottom"> 

        <div class="width100 all-center">
            <p class="input-top-p ow-input-top-p"><?php echo _JS_FULLNAME ?></p>
            <input class="input-name clean no-input" type="text" id="name" name="name" value="<?php echo $name; ?>" readonly>
        </div>
		<div class="clear"></div>
        <div class="width100 all-center">
            <p class="input-top-p ow-input-top-p"><?php echo _INDEX_MOBILE_NO ?></p>
            <input class="input-name clean no-input" type="text" id="mobile" name="mobile" value="<?php echo $contactNo; ?>" readonly> 
        </div>

        <div class="clear"></div>

        <div class="width100 all-center">
            <p class="input-top-p ow-input-top-p"><?php echo _PRODUCT_AMOUNT ?></p>
            <input class="input-name clean no-input" type="text" value="<?php echo $subtotal; ?>" readonly> 
        </div>

        <div class="clear"></div>

        <input class="input-name clean" type="hidden" id="email" name="email" value="<?php echo $userData->getEmail();?>" readonly> 
        <input class="input-name clean" type="hidden" id="amount" name="amount" value="<?php echo $adjustTotal; ?>" readonly> 
        <input class="input-name clean" type="hidden" id="reference_1_label" name="reference_1_label" value="Order UID" readonly> 
        <input class="input-name clean" type="hidden" id="reference_1" name="reference_1" value="<?php echo $orderUID; ?>" readonly>
        <input class="input-name clean" type="hidden" id="reference_2_label" name="reference_2_label" value="Order ID" readonly> 
        <input class="input-name clean" type="hidden" id="reference_2" name="reference_2" value="<?php echo $orderID; ?>" readonly> 

        <div class="clear"></div> 

        <div class="width100 text-center extra-spacing-up-down">
            <button class="clean blue-button white-text pill-button ow-blue-button1" name="submit"><?php echo _PRODUCT_PROCEED_TO_PAYMENT ?></button>
        </div> 

        <div class="clear"></div> 

        </div> 
                

    </form>

</div>

<script>
function la(src){
    window.location=src;
}
</script>
 
<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.green-footer{
		display:none;}
    .footer-div{
	display:none;}
</style>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>