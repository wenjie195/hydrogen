<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = $_SESSION['uid'];

    $bankName = rewrite($_POST['bank_name']);
    $bankAccHolder = rewrite($_POST["bank_acc_holder"]);
    $bankAccNumber = rewrite($_POST["bank_acc_number"]);

    $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");    

    if(!$user)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($bankName)
        {
            array_push($tableName,"bank_name");
            array_push($tableValue,$bankName);
            $stringType .=  "s";
        }
        if($bankAccHolder)
        {
            array_push($tableName,"bank_acc_name");
            array_push($tableValue,$bankAccHolder);
            $stringType .=  "s";
        }
        if($bankAccNumber)
        {
            array_push($tableName,"bank_acc_number");
            array_push($tableValue,$bankAccNumber);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // echo "success";
            header('Location: ../userDashboard.php');
        }
        else
        {
            // echo "fail";
            echo "<script>alert('fail to update bank details !');window.location='../editBankDetails.php'</script>";
        }
    }
    else
    {
        //echo "";
        // header('Location: ../editProfile.php?type=3');
        echo "<script>alert('ERROR !!');window.location='../editBankDetails.php'</script>";
    }

}
else 
{
    header('Location: ../index.php');
}
?>
