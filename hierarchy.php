<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

$userRHDetails = getReferralHistory($conn, " WHERE referral_id =? ", array("referral_id"), array($uid), "s");
$userLevel = $userRHDetails[0];

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);


$PersonalSales = 0;

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>

    <meta property="og:url" content="https://hygeniegroup.com/hierarchy.php" />
    <link rel="canonical" href="https://hygeniegroup.com/hierarchy.php" />
    <meta property="og:title" content="<?php echo _USERHEADER_HIERARCHY ?>  | Hygenie Group" />
    <title><?php echo _USERHEADER_HIERARCHY ?>  | Hygenie Group</title>

	<?php include 'css.php'; ?>
</head>
<style media="screen">
  .blue-button{
  font-size: 10px;
  width: auto;
  margin-top: 50px;
  padding: 5px;
  margin-left: -20px;
  }
  ul{
    list-style: none;
    list-style-image: url('img/li.png');
  }
  li{
    vertical-align: middle;
    cursor: pointer;
  }
</style>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height" id="firefly">
  <div class="width100 overflow text-center">
    <img src="img/hierachy.png" class="middle-title-icon" alt="<?php echo _USERHEADER_HIERARCHY ?>" title="<?php echo _USERHEADER_HIERARCHY ?>">
  </div>

  <div class="width100 overflow">
    <h1 class="pop-h1 text-center"><?php echo _USERHEADER_HIERARCHY ?></h1>
  </div>

  <?php $level = $userLevel->getCurrentLevel();?>

  <div class="overflow-scroll-div">
    <table class="table-css fix-th">
      <thead>
        <tr>
          <th><?php echo _HIERARCHY_GENERATION ?></th>
          <th><?php echo _VICTORY_URL ?></th>
          <th><?php echo _MAINJS_INDEX_USERNAME ?></th>
          <th><?php echo _HIERARCHY_SPONSOR ?></th>
          <th><?php echo _MONTHLY_LEVEL ?></th>
          <th><?php echo _USERDASHBOARD_MY_SALES ?></th>
          <th><?php echo _USERDASHBOARD_TEAM_SALES ?></th>
          <th><?php echo _HIERARCHY_DIRECT_DOWNLINE ?></th>
          <th><?php echo _HIERARCHY_GROUP_MEMBER ?></th>
        </tr>
      </thead>
      <tbody>
        <?php
        $conn = connDB();
        if($getWho)
        {
          for($cnt = 0;$cnt < count($getWho) ;$cnt++)
          {
            // $totalGroupSales = 0;
            // $groupSales = 0;

            // $personalSalesApproved = 0;
            // $personalSalesAccepted = 0;

            $totalDownline = 0;
            $downline = 0;
            $directDownline = 0;
            $downlineCurrentLvl = 0;
            $totalDirectDownline = 0;
            $downline = $getWho[$cnt]->getCurrentLevel();
            $directDownline = $downline + 1;

            // $myOrder = getOrders($conn, "WHERE uid = ? AND payment_status = 'APPROVED' ",array("uid"), array($getWho[$cnt]->getReferralId()), "s");
            // if ($myOrder)
            // {
            //   for ($a=0; $a <count($myOrder) ; $a++) {
            //     $personalSalesApproved += $myOrder[$a]->getCommission();
            //   }
            // }

            // $myOrder2 = getOrders($conn, "WHERE uid = ? AND payment_status = 'APPROVED' ",array("uid"), array($getWho[$cnt]->getReferralId()), "s");
            // if ($myOrder2)
            // {
            //   for ($a=0; $a <count($myOrder2) ; $a++) {
            //     $personalSalesAccepted += $myOrder2[$a]->getCommission();
            //   }
            // }

            $downlineUid = $getWho[$cnt]->getReferralId();
            $getWhoII = getWholeDownlineTree($conn,$downlineUid,false);
            if ($getWhoII)
            {
              for ($i=0; $i <count($getWhoII) ; $i++)
              {
                $allDownlineUid = $getWhoII[$i]->getReferralId();
                $downlineCurrentLvl = $getWhoII[$i]->getCurrentLevel();
                if ($directDownline == $downlineCurrentLvl)
                {
                  $totalDirectDownline++;
                }
              }
            }
            ?>
            <tr>
              <td>
                <?php
                  $downlineLvl = $getWho[$cnt]->getCurrentLevel();
                  echo $lvl = $downlineLvl - $level;
                ?>
              </td>

              <td>
                <u><a href='referLink.php?id=<?php echo $getWho[$cnt]->getReferralId();?>' class="blue-link"><?php echo _PRODUCT_LINK ?></a></u>
              </td>

              <td>
              <?php
                $userUid = $getWho[$cnt]->getReferralId();
                $thisUserDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($userUid), "s");
                echo $username = $thisUserDetails[0]->getUsername();
              ?>
              </td>

              <td>
                <?php
                  $uplineUid = $getWho[$cnt]->getReferrerId();
                  $uplineDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uplineUid), "s");
                  echo $uplineUsername = $uplineDetails[0]->getUsername();
                ?>
              </td>

              <td>
                <?php 
                  $userRank = $thisUserDetails[0]->getRank();
                  if($userRank == 'District Manager')
                  {
                    $userRanking = 'Region Manager';
                  }
                  elseif($userRank == 'Manager')
                  {
                    $userRanking = 'Sales Manager';
                  }
                  elseif($userRank == 'Senior Manager')
                  {
                    $userRanking = 'Markerting Manager';
                  }
                  else
                  {
                    $userRanking = $userRank;
                  }
                  echo $userRanking;
                ?>
              </td>

              <td>
                <?php
                $conn = connDB();
                {
                  // $uid = $_POST['downline_uid'];
                  $downlineUid = $getWho[$cnt]->getReferralId();
                  // $userUid = $getWho[$cnt]->getReferralId();

                  $myOrder = getOrders($conn, "WHERE uid = ? AND payment_status = 'APPROVED' ",array("uid"), array($downlineUid), "s");
                  $personalSales = 0; // initital
                  if($myOrder)
                  {
                    for ($b=0; $b <count($myOrder) ; $b++)
                    {
                      $personalSales += $myOrder[$b]->getCommission();
                    }
                  }

                  $myOrder2 = getOrders($conn, "WHERE uid = ? AND payment_status = 'ACCEPTED' ",array("uid"), array($downlineUid), "s");
                  $personalSales2 = 0; // initital
                  if ($myOrder2)
                  {
                    for ($b=0; $b <count($myOrder2) ; $b++)
                    {
                      $personalSales2 += $myOrder2[$b]->getCommission();
                    }
                  }
                  // echo $personalSales2;
                  echo $totalPV = $personalSales + $personalSales2;
                }
                ?>
              </td>

              <td>
                <?php
                $conn = connDB();
                {
                  // $uid = $_POST['downline_uid'];
                  $downlineUid = $getWho[$cnt]->getReferralId();

                  $groupSales = 0; // initital
                  $groupSalesFormat = number_format(0,4); // initital
                  $groupSales2 = 0; // initital
                  $groupSales2Format = number_format(0,4); // initital
                  $directDownline = 0; // initital
                  $personalSales = 0; // initital
                  $personalSales2 = 0; // initital    

                  $referralDetails = getReferralHistory($conn, "WHERE referral_id = ?",array("referral_id"),array($downlineUid), "s");
                  $referralCurrentLevel = $referralDetails[0]->getCurrentLevel();
                  $directDownlineLevel = $referralCurrentLevel + 1;
                  $referrerDetails = $referrerDetails = getWholeDownlineTree($conn, $downlineUid, false);
                  if ($referrerDetails)
                  {
                    for ($i=0; $i <count($referrerDetails) ; $i++)
                    {
                      $currentLevel = $referrerDetails[$i]->getCurrentLevel();
                      if ($currentLevel == $directDownlineLevel)
                      {
                        $directDownline++;
                      }
                      $referralId = $referrerDetails[$i]->getReferralId();
                      // $downlineDetails = getOrders($conn, "WHERE uid = ?",array("uid"),array($referralId), "s");
                      $downlineDetails = getOrders($conn, "WHERE uid = ? AND payment_status = 'APPROVED' ",array("uid"),array($referralId), "s");
                      // $downlineDetailsTwo = getOrders($conn, "WHERE uid = ? AND payment_status = 'ACCEPTED' ",array("uid"),array($referralId), "s");
                      if ($downlineDetails)
                      {
                        for ($b=0; $b <count($downlineDetails) ; $b++)
                        {
                          // $personalSales += $downlineDetails[$b]->getSubtotal();
                          $personalSales += $downlineDetails[$b]->getCommission();
                        }
                      }
                  
                      $downlineDetailsTwo = getOrders($conn, "WHERE uid = ? AND payment_status = 'ACCEPTED' ",array("uid"),array($referralId), "s");
                      if ($downlineDetailsTwo)
                      {
                        for ($b=0; $b <count($downlineDetailsTwo) ; $b++)
                        {
                          $personalSales2 += $downlineDetailsTwo[$b]->getCommission();
                        }
                      }
                    }
                    $groupSales += $personalSales;
                    $groupSales2 += $personalSales2;
                    $groupSalesFormat = number_format($groupSales,4);
                    $groupSales2Format = number_format($groupSales2,4);
                  }
                }
                ?>
                <?php echo $totalGS = $groupSales + $groupSales2;?>
              </td>

              <td><?php echo $totalDirectDownline;?></td>

              <td>
                <?php
                  $getDownlineAmount = getWholeDownlineTree($conn, $userUid,false);
                  if ($getDownlineAmount)
                  {
                    echo $groupMember = count($getDownlineAmount);
                  }
                  else
                  {
                    echo $groupMember = 0;
                  }
                ?>
              </td>
            </tr>
          <?php
          }
          ?>
        <?php
        }
        $conn->close();
        ?>
      </tbody>
    </table>
  </div>

<div class="clear"></div>

</div>

<?php include 'js.php'; ?>

</body>
</html>