<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = null;
$userRows = null;
$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>

<meta property="og:url" content="https://hygeniegroup.com/resetPassword.php" />
<link rel="canonical" href="https://hygeniegroup.com/resetPassword.php" />
<meta property="og:title" content=">Reset Password  | Hygenie Group" />
<title>Reset Password  | Hygenie Group</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<style>
.header1, .footer-div, .ow-blue-button1{
background: #48A9FE;
background: -moz-linear-gradient(left, #48A9FE 0%, #0BEEF9 100%);
background: -webkit-gradient(left top, right top, color-stop(0%, #48A9FE), color-stop(100%, #0BEEF9));
background: -webkit-linear-gradient(left, #48A9FE 0%, #0BEEF9 100%);
background: -o-linear-gradient(left, #48A9FE 0%, #0BEEF9 100%);
background: -ms-linear-gradient(left, #48A9FE 0%, #0BEEF9 100%);
background: linear-gradient(to right, #48A9FE 0%, #0BEEF9 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#045ce9', endColorstr='#09c5f9', GradientType=1 );	
	}
 .ow-blue-button1{	
    background: #ffffff40;}
.ow-blue-button1:hover{
    background: #ffffff60;	
}
::placeholder {
	color:white !Important;
}
</style>
<?php include 'header.php'; ?>
<div class="width100 home-same-padding menu-distance darkbg min-height text-center blue-bg-ow" id="firefly">
    
    <div class="index-login-width">
		<img src="img/victory5-silver.png" alt="<?php echo _JS_LOGIN ?>" title="<?php echo _JS_LOGIN ?>"   class="index-logo">
    	<h1 class="h1-title"><?php echo _JS_RESET_PASSWORD ?></h1>
        <form action="utilities/resetPasswordFunction.php" method="POST">
            <input type="hidden" name="checkThat" value="<?php if(isset($_GET['uid'])){echo $_GET['uid'] ;}?>">
           

            <div class="fake-input-bg">
                <input class="clean pop-input  no-bg-input" type="text" placeholder="<?php echo _JS_VERIFY_CODE ?>" id="verify_code" name="verify_code" required>
            </div>
            
            <div class="clear"></div>

            <div class="fake-input-bg">
                <input class="clean pop-input  no-bg-input" type="password" placeholder="<?php echo _JS_NEW_PASSWORD ?>" id="new_password" name="new_password" required>
                <img src="img/eye-white.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
            </div>

            <div class="clear"></div>

            <div class="fake-input-bg">
                <input class="clean pop-input  no-bg-input" type="password" placeholder="<?php echo _JS_RETYPE_NEW_PASSWORD ?>" id="retype_new_password" name="retype_new_password" required>
                <img src="img/eye-white.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
            </div>

            <div class="clear"></div>

            <button class="clean width100 blue-button white-text pill-button ow-blue-button1" name="submit"><?php echo _JS_SUBMIT ?></button>
          
            <div class="clear"></div>
        </form>


    </div>   


</div>


<?php include 'js.php'; ?>

<script>
function myFunctionA()
{
    var x = document.getElementById("new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("retype_new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "wrong verification code"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "password must be more than 5"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "password does not match with retype password";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "ERROR";
        }
        echo '
        <script>
            putNoticeJavascript("Notice ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>