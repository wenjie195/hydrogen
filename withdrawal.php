<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
	<meta property="og:url" content="https://hygeniegroup.com/withdrawal.php" />
    <link rel="canonical" href="https://hygeniegroup.com/withdrawal.php" />
    <meta property="og:title" content="User Dashboard  | Hygenie Group" />
    <title>User Dashboard  | Hygenie Group</title>

	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<!-- <div class="width100 same-padding menu-distance darkbg min-height big-black-text user-dash user-dash2 ow-white-css" id="firefly"> -->
<div class="width100 same-padding menu-distance darkbg min-height" id="firefly">

    <div class="width100 overflow">
		<h1 class="pop-h1 text-center">Withdrawal</h1>
	</div>

    <div class="spacing-div"></div>

    <form action="utilities/submitWithdrawalFunction.php" method="POST">

        <div class="dual-input">
            <p class="input-top-text">Bank</p>
            <input class="clean pop-input" type="text" placeholder="Bank" id="bank_name" name="bank_name" required>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text">Bank Account Number</p>
            <input class="clean pop-input" type="text" placeholder="Bank Account Number" id="bank_account_no" name="bank_account_no" required>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-text">Bank Account Holder Name</p>
            <input class="clean pop-input" type="text" placeholder="Bank Account Holder Name" id="bank_acc_holder" name="bank_acc_holder" required>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-text">Amount</p>
            <input class="clean pop-input" type="number" placeholder="Amount" id="withdrawal_amount" name="withdrawal_amount" required>
        </div>

        <div class="clear"></div>     

		<div class="width100 text-center">
        	<button class="clean blue-button one-button-width pill-button margin-auto" name="submit"><?php echo _INDEX_REGISTER ?></button>
        </div>
    </form>

</div>

<?php include 'js.php'; ?>

</body>
</html>