<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Withdrawal.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
  $fromDate = rewrite($_POST["fromDate"]);
  $endDate = rewrite($_POST["endDate"]);
  $newEndDate = date('Y-m-d', strtotime($endDate. ' + 1 days'));

  // $withdrawalDetails = getWithdrawal($conn, "WHERE status != 'PENDING' ");
  // $withdrawalDetails = getWithdrawal($conn, "WHERE status != 'PENDING'  AND date_created >= '$fromDate' AND date_created <= '$endDate'  ");
  $withdrawalDetails = getWithdrawal($conn, "WHERE status != 'PENDING'  AND date_created >= '$fromDate' AND date_created <= '$newEndDate'  ");
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://hygeniegroup.com/adminWithdrawalHistory.php" />
    <link rel="canonical" href="https://hygeniegroup.com/adminWithdrawalHistory.php" />
    <meta property="og:title" content="<?php echo _ADMINHEADER_MEMBER_WITHDRAW ?>  | Hygenie Group" />
    <title><?php echo _ADMINHEADER_MEMBER_WITHDRAW ?>  | Hygenie Group</title>
    
	<?php include 'css.php'; ?>
</head>
<style media="screen">
  .red{
    background-color: red;
  }
  .red:hover{
    background-color: maroon;
  }
  h1 .h1{
    color: maroon;
  }
  h1 .h1:hover{
    color: maroon;
  }
</style>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">
  <h1 class="small-h1-a text-center white-text"><a class="blue-link" href="adminWithdrawal.php"><?php echo _ADMINHEADER_MEMBER_WITHDRAW ?></a> | <?php echo _ADMINHEADER_MEMBER_WITHDRAW_HISTORY ?></h1>
    <div class="width100 shipping-div2">
    <h3 class="white-text"><?php echo _ADMINVIEWBALANCE_PAYOUT ?> </h3>
      <div class="overflow-scroll-div">
        <table class="table-css fix-th">
          <thead>
          <tr>
            <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
            <th><?php echo _BANKDETAILS_BANK ?></th>
            <th><?php echo _BANKDETAILS_ACC_NAME ?></th>
            <th><?php echo _BANKDETAILS_ACC_NO ?></th>
            <th><?php echo _PRODUCT_AMOUNT ?> (PV)</th>
            <th><?php echo _PRODUCT_AMOUNT ?> (RM)</th>
            <th><?php echo _BONUS_STATUS ?></th>
            <th><?php echo _DAILY_DATE ?></th>
          </tr>
          </thead>
          <tbody>
          <?php
          if($withdrawalDetails)
          {
            for($cnt = 0;$cnt < count($withdrawalDetails) ;$cnt++)
            {
            ?>
              <tr>
                <td><?php echo ($cnt+1)?></td>
                <td><?php echo $withdrawalDetails[$cnt]->getBankName();?></td>
                <td><?php echo $withdrawalDetails[$cnt]->getBankAccHolder();?></td>
                <td><?php echo $withdrawalDetails[$cnt]->getBankAccNo();?></td>
                <td><?php echo $withdrawalDetails[$cnt]->getCurrentAmount();?></td>
                <td><?php echo $withdrawalDetails[$cnt]->getFinalAmount();?></td>
                <td><?php echo $withdrawalDetails[$cnt]->getStatus();?></td>
                <td><?php echo date('d/m/Y',strtotime($withdrawalDetails[$cnt]->getDateUpdated()));?></td>
              </tr>
            <?php
            }
            ?>
          <?php
          }
          ?>
          </tbody>
        </table>
      </div>
    </div>
</div>

<?php include 'js.php'; ?>

</body>
</html>