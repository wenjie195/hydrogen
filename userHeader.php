<div id="wrapper" class="toggled-2">
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
                <li>
                <a href="userDashboard.php"><img src="img/logo.png" class="logo-img" alt="Logo" title="Logo"></a>
                <!-- <a href="#"><img src="img/logo.png" class="logo-img" alt="Logo" title="Logo"></a> -->
                </li>
                <!-- <li class="sidebar-li">
                        <img src="img/add-new.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="mt4idDetails.php" class="overflow"><?php echo _USERHEADER_ADD_NEW ?></a></p>
                </li> -->
                <li class="sidebar-li">
                        <img src="img/user-profile.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="profile.php" class="overflow"><?php echo _USERHEADER_PROFILE ?></a></p>
                </li>
                <li class="sidebar-li">
                        <img src="img/bank-details.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="editBankDetails.php" class="overflow"><?php echo _BANKDETAILS ?></a></p>
                </li>
                <!-- <li class="sidebar-li">
                        <img src="img/broker.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="brokerLink.php" class="overflow"><?php echo _BROKER_LINK ?></a></p>
                </li> -->
                <!-- <li class="sidebar-li">
                        <img src="img/password2.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="editPassword.php" class="overflow"><?php echo _USERHEADER_EDIT_PASSWORD ?></a></p>
                </li> -->
                <!-- <li class="sidebar-li">
                        <img src="img/documents.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="uploadDoc.php" class="overflow"><?php echo _VICTORY_DOCUMENTS ?></a></p>
                </li> -->

                <li class="sidebar-li">
                        <img src="img/product.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="product.php" class="overflow"><?php echo _USERDASHBOARD_PRODUCT_ORDER ?></a></p>
                </li>

                <li class="sidebar-li">
                        <img src="img/report.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="orderHistory.php" class="overflow"><?php echo _USERDASHBOARD_ORDER_HISTORY ?></a></p>
                </li>

                <!-- <li class="sidebar-li">
                        <img src="img/maa.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="lpoa.php" class="overflow">LPOA</a></p>
                </li> -->
                <li class="sidebar-li">
                        <img src="img/link.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="refer.php" class="overflow"><?php echo _VICTORY_REFERRAL_LINK ?></a></p>
                </li>
                <li class="sidebar-li">
                        <img src="img/group.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="hierarchy.php" class="overflow"><?php echo _USERHEADER_HIERARCHY ?></a></p>
                </li>
                <!-- <li class="sidebar-li">
                        <img src="img/daily2.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="userViewDaily.php" class="overflow"><?php echo _DAILY_MEMBER_DAILY_BONUS ?></a></p>
                </li> -->

                <li class="sidebar-li">
                        <img src="img/monthly.png" alt="" title="" class="sidebar-icon-img">
                        <!-- <p class="sidebar-span white-text"><a href="userViewBonus.php" class="overflow"><?php echo _USERHEADER_BONUS_REPORT ?></a></p> -->
                        <p class="sidebar-span white-text"><a href="userViewBonusDirect.php" class="overflow"><?php echo _USERHEADER_BONUS_REPORT ?></a></p>
                </li>

                <li class="sidebar-li">
                        <img src="img/withdrawal.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="withdrawalHistory.php" class="overflow"><?php echo _USERHEADER_WITHDRAWAL_HISTORY ?></a></p>
                </li>

                <!-- <li class="sidebar-li">
                        <img src="img/monthly.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="userViewMonthly.php" class="overflow"><?php echo _MONTHLY_MEMBER_DAILY_BONUS ?></a></p>
                </li> -->
                <li class="sidebar-li">
                        <img src="img/english.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="<?php $link ?>?lang=en" class="overflow">EN</a></p>
                </li>
                <li class="sidebar-li">
                        <img src="img/chinese.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="<?php $link ?>?lang=ch" class="overflow">中文</a></p>
                </li>
                <li class="sidebar-li">
                        <img src="img/exit.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="logout.php" class="overflow"><?php echo _USERDASHBOARD_LOGOUT ?></a></p>
                </li>
        </ul>
    </div>
</div>