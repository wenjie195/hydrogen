<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$withdrawalDetails = getWithdrawal($conn, "WHERE uid = ? ORDER BY date_created DESC ",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://hygeniegroup.com/withdrawalHistory.php" />
<link rel="canonical" href="https://hygeniegroup.com/withdrawalHistory.php" />
<meta property="og:title" content="<?php echo _USERHEADER_WITHDRAWAL_HISTORY ?> | Hygenie Group" />
<title><?php echo _USERHEADER_WITHDRAWAL_HISTORY ?> | Hygenie Group</title>

<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">

	<div class="width100 overflow">
		<h1 class="pop-h1 text-center"><?php echo _USERHEADER_WITHDRAWAL_HISTORY ?></h1>
	</div>

	<div class="clear"></div>

	<div class="four-box-size text-center white-div-css">
	<img src="img/commission.png" class="white-div-img" alt="<?php echo _ADMINVIEWBALANCE_PAYOUT ?>" title="<?php echo _ADMINVIEWBALANCE_PAYOUT ?>">
	<div class="inner-white-div">
		<p class="white-div-p"><?php echo _ADMINVIEWBALANCE_PAYOUT ?></p>
		<?php
		if($withdrawalDetails)
		{
		$totalBonusOne = 0;
		for ($cnt=0; $cnt <count($withdrawalDetails) ; $cnt++)
		{
			$totalBonusOne += $withdrawalDetails[$cnt]->getFinalAmount();
		}
		}
		else
		{
		$totalBonusOne = 0 ;
		}
		?>
		<p class="white-div-p white-div-amount">RM <?php echo $totalBonusOne;?></p>
	</div>
	</div>    

	<div class="clear"></div>

	<div class="width100">
	<form method='post' action='withdrawalHistorySearch.php' target="_blank"> 
		<div class="fake-input-div overflow  three-input-with-btn">
		<img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
		<input type="text" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _MULTIBANK_START_DATE ?>" class="clean pop-input fake-input" name='fromDate' id="fromDate">
		</div>
		<div class="fake-input-div overflow three-input-with-btn second-three">
		<img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
		<input type="text" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _MULTIBANK_END_DATE ?>" class="clean pop-input fake-input" name='endDate' id="endDate">
		</div>    
		<div class="btn-div3">
			<input type='submit' name='but_search' value='<?php echo _MULTIBANK_SEARCH ?>' class="clean blue-ow-btn">
		</div>
	</form>         
	</div>

	<div class="clear"></div>

  <div class="overflow-scroll-div">
    <table class="table-css fix-th">
				<thead>
					<tr>
						<th class="th"><?php echo _ADMINVIEWBALANCE_NO ?></th>

                        <th><?php echo _PRODUCT_AMOUNT ?> (PV)</th>
                        <th><?php echo _PRODUCT_AMOUNT ?> (RM)</th>

						<th class="th"><?php echo _BONUS_STATUS ?></th>
						<th class="th"><?php echo _DAILY_DATE ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					if($withdrawalDetails)
					{
						for($cnt = 0;$cnt < count($withdrawalDetails) ;$cnt++)
						{
						?>
							<tr>
								<td><?php echo ($cnt+1)?></td>

								<td><?php echo $withdrawalDetails[$cnt]->getCurrentAmount();?></td>
            					<td><?php echo $withdrawalDetails[$cnt]->getFinalAmount();?></td>

								<td><?php echo $withdrawalDetails[$cnt]->getStatus();?></td>
								<td><?php echo $withdrawalDetails[$cnt]->getDateCreated();?></td>
							</tr>
						<?php
						}
						?>
					<?php
					}
					?>
				</tbody>
		</table>
  </div>

</div>

<?php include 'js.php'; ?>

<script>
  $(function()
  {
    $("#fromDate").datepicker(
    {
    dateFormat:'yy-mm-dd',
    changeMonth: true,
    changeYear:true,
    }

    );
  });
</script>

<script>
  $(function()
  {
    $("#endDate").datepicker(
    {
      dateFormat:'yy-mm-dd',
      changeMonth: true,
      changeYear:true,
    }
    );
  });
</script>

</body>
</html>