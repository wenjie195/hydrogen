<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$conn->close();

?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://hygeniegroup.com/editPassword.php" />
<link rel="canonical" href="https://hygeniegroup.com/editPassword.php" />
<meta property="og:title" content="Edit Password  | Hygenie Group" />
<title>Edit Password  | Hygenie Group</title>
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height">

    <?php
    if(isset($_POST['user_uid']))
    {
    $conn = connDB();
    // $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
    // $orderId = $ordersDetails[0]->getId();
    ?>

        <form action="utilities/adminEditUserPasswordFunction.php" method="POST">

        <div class="width100 overflow text-center">
            <img src="img/password.png" class="middle-title-icon" alt="<?php echo _USERHEADER_EDIT_PASSWORD ?>" title="<?php echo _USERHEADER_EDIT_PASSWORD ?>">
        </div>    

        <div class="width100 overflow"> 
            <h1 class="h1-title pop-h1 text-center"><?php echo _USERHEADER_EDIT_PASSWORD ?></h1>
        </div>

        <div class="password-width margin-auto overflow">

            <div class="per-input">
                <p class="input-top-text"><?php echo _JS_NEW_PASSWORD ?></p>
                <div class="fake-input-bg">
                    <input class="clean pop-input no-bg-input" type="password" placeholder="<?php echo _JS_NEW_PASSWORD ?>"  id="new_password" name="new_password">
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
                </div>
            </div>

        </div>

        <input class="clean pop-input no-bg-input" type="hidden" value="<?php echo $_POST['user_uid']; ?>"  id="user_uid" name="user_uid" readonly>

        <div class="clear"></div>

        <div class="width100 text-center">
            <button class="clean blue-button one-button-width pill-button margin-auto" name="submit"><?php echo _JS_SUBMIT ?></button>
        </div>

        </form>

    <?php
    }
    ?>

</div>

<?php include 'js.php'; ?>
</body>
</html>