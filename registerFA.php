<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$countryList = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    
    <meta property="og:url" content="https://hygeniegroup.com/register.php" />
    <link rel="canonical" href="https://hygeniegroup.com/register.php" />
    <meta property="og:title" content="<?php echo _JS_SIGNUP ?>  | Hygenie Group" />
    <title><?php echo _JS_SIGNUP ?> | Hygenie Group</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance2 darkbg min-height">

    <form action="utilities/registerFAFunction.php" method="POST">
        <?php
        // Program to display URL of current page.
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
        $link = "https";
        else
        $link = "http";

        // Here append the common URL characters.
        $link .= "://";

        // Append the host(domain name, ip) to the URL.
        $link .= $_SERVER['HTTP_HOST'];

        // Append the requested resource location to the URL
        $link .= $_SERVER['REQUEST_URI'];

        // Print the link
        // echo $link;
        ?>

        <?php
        $str1 = $link;
        // $referrerUidLink = str_replace( 'http://localhost/poppifx/register.php?referrerUID=', '', $str1);
        if(isset($_GET['referrerUID'])){
          $referrerUidLink = $_GET['referrerUID'];
        }else {
          $referrerUidLink = "";
        }
        // $referrerUidLink = str_replace( 'https://poppifx4u.com/register.php?referrerUID=', '', $str1);

        // $referrerUidLink = str_replace( 'https://vidatechft.com/poppifx/register.php?referrerUID=', '', $str1);
        // echo $referrerUidLink;
        ?>
		<div class="center-width-div">
            <div class="width100">
                <p class="input-top-text"><?php echo _JS_USERNAME ?></p>
                <input class="clean pop-input" type="text" placeholder="<?php echo _JS_USERNAME ?>" id="register_username" name="register_username" required>
            </div>
    
            <div class="clear"></div>
    
            <div class="width100">
                <p class="input-top-text"><?php echo _INDEX_MOBILE_NO ?></p>
                <input class="clean pop-input" type="text" placeholder="<?php echo _INDEX_MOBILE_NO ?>" id="register_mobileno" name="register_mobileno" required>
            </div>
		</div>
        <div class="clear"></div>

        <input readonly class="clean pop-input" type="hidden" value="<?php echo $referrerUidLink ?>" id="upline_uid" name="upline_uid" readonly>

        <div class="clear"></div>

		<div class="width100 text-center">
        	<button class="clean blue-button one-button-width pill-button margin-auto" name="register"><?php echo _INDEX_REGISTER ?></button>
        </div>
    </form>

</div>

<?php include 'js.php'; ?>

</body>
</html>