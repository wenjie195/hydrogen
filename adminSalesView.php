<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE user_type = 1");
// $paymentStatus = getOrders($conn, " WHERE payment_status = 'ACCEPTED' AND shipping_status = 'DELIVERED' ");
$paymentStatus = getOrders($conn, " WHERE payment_status = 'ACCEPTED' OR `payment_status` = 'APPROVED' ORDER BY date_created DESC ");

$conn->close();

?>

<!DOCTYPE html>
<html>
<head>

	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://hygeniegroup.com/adminSalesView.php" />
    <link rel="canonical" href="https://hygeniegroup.com/adminSalesView.php" />
    <meta property="og:title" content="<?php echo _PRODUCT_SALES_PAGE ?> | Hygenie Group" />
    <title><?php echo _PRODUCT_SALES_PAGE ?> | Hygenie Group</title>

	<?php include 'css.php'; ?>

</head>

<!-- <body> -->

<body class="body">
<?php include 'header.php'; ?>

<!-- <div class="demo"> -->

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">

<h1 class="small-h1-a text-center white-text"><?php echo _PRODUCT_TOTAL_SALES ?></h1>

  <div class="width100">
    <form method='post' action='adminSalesViewSearch.php' target="_blank"> 
    <!-- <form method='post' action=# target="_blank">  -->
      <div class="fake-input-div overflow  three-input-with-btn">
        <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
        <input type="text" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _MULTIBANK_START_DATE ?>" class="clean pop-input fake-input" name='fromDate' id="fromDate">
      </div>
      <div class="fake-input-div overflow three-input-with-btn second-three">
        <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
        <input type="text" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _MULTIBANK_END_DATE ?>" class="clean pop-input fake-input" name='endDate' id="endDate">
      </div>    
      <div class="btn-div3">
        <input type='submit' name='but_search' value='<?php echo _MULTIBANK_SEARCH ?>' class="clean blue-ow-btn">
      </div>
    </form>         
  </div>

    <div class="width100 shipping-div2 margin-top15">

		<div class="overflow-scroll-div">
			<table class="table-css fix-th tablesorter smaller-font-table" id="myTable">
				<thead>
					<tr>
						<!-- <img src="img/sort.png" class="sort-img"> -->
						<th class="th"><?php echo _ADMINVIEWBALANCE_NO ?></th>
						<th class="th"><?php echo _PRODUCT_ORDER_ID ?></th>
						<th class="th"><?php echo _MAINJS_INDEX_USERNAME ?></th>
						<th class="th"><?php echo _PRODUCT_AMOUNT ?></th>
						<th class="th"><?php echo _DAILY_DATE ?></th>
						<th class="th"><?php echo _MULTIBANK_DETAILS ?></th>
					</tr>
				</thead>
				<tbody id="myFilter">

					<?php
					if($paymentStatus)
					{
						for($cnt = 0;$cnt < count($paymentStatus) ;$cnt++)
						{
						?>
							<tr>
								<td><?php echo ($cnt+1)?></td>
								<td><?php echo $paymentStatus[$cnt]->getOrderId();?></td>
								<td><?php echo $paymentStatus[$cnt]->getName();?></td>
								<td><?php echo $paymentStatus[$cnt]->getSubtotal();?></td>
								<td><?php echo $paymentStatus[$cnt]->getDateCreated();?></td>
								<td>
								<form action="adminSalesDetails.php" method="POST">
									<button class="clean blue-ow-btn" type="submit" name="order_uid" value="<?php echo $paymentStatus[$cnt]->getOrderId();?>">
										<?php echo _MULTIBANK_VIEW ?>
									</button>
								</form>
								</td>

							</tr>
						<?php
						}
						?>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php include 'js.php'; ?>


<!-- <script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput2");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>


<script>
function myFunctionD() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput4");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionE() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput5");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[4];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script> -->

<script>
  $(function()
  {
    $("#fromDate").datepicker(
    {
    dateFormat:'yy-mm-dd',
    changeMonth: true,
    changeYear:true,
    }

    );
  });
</script>

<script>
  $(function()
  {
    $("#endDate").datepicker(
    {
      dateFormat:'yy-mm-dd',
      changeMonth: true,
      changeYear:true,
    }
    );
  });
</script>

</body>
</html>