<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html>
<head>

	<?php include 'meta.php'; ?>
	<meta property="og:url" content="https://hygeniegroup.com/adminProductEdit.php" />
    <link rel="canonical" href="https://hygeniegroup.com/adminProductEdit.php" /> 
    <meta property="og:title" content="<?php echo _PRODUCT ?> | Hygenie Group" />
    <title><?php echo _PRODUCT ?> | Hygenie Group</title>

	<?php include 'css.php'; ?>

</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">

  <?php
  if(isset($_POST['product_uid']))
  {
  $conn = connDB();
  $productDetails = getProduct($conn,"WHERE uid = ? ", array("uid") ,array($_POST['product_uid']),"s");
  ?>
    <!-- <h2 class="h1-title">Edit Product</h2>  -->
    <h1 class="small-h1-a text-center white-text"><?php echo _PRODUCT_EDIT ?></h1>

    <form action="utilities/updateProductFunction.php" method="POST" enctype="multipart/form-data">

      <div class="border-separation">

        <div class="dual-input">
          <p class="input-top-p admin-top-p"><?php echo _PRODUCT_NAME ?>*</p>
          <input class="clean pop-input" type="text" placeholder="<?php echo _PRODUCT_NAME ?>" value="<?php echo $productDetails[0]->getName();?>" name="update_product_name" id="update_product_name" readonly>
        </div>
        <div class="dual-input second-dual-input">
          <p class="input-top-p admin-top-p"><?php echo _PRODUCT_PRICE ?></p>
          <input class="clean pop-input" type="text" placeholder="<?php echo _PRODUCT_PRICE ?>" value="<?php echo $productDetails[0]->getPrice();?>" name="update_price" id="update_price" required>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
          <p class="input-top-p admin-top-p"><?php echo _PRODUCT_COMMISSION ?></p>
          <input class="clean pop-input" type="text" placeholder="<?php echo _PRODUCT_COMMISSION ?>" value="<?php echo $productDetails[0]->getCommission();?>" name="update_commission" id="update_commission" required>
        </div>

        <div class="clear"></div>

        <div class="width100 overflow">
          <p class="input-top-p admin-top-p"><?php echo _PRODUCT_DESCRIPTION ?>* (<?php echo _PRODUCT_AVOID ?> " , '')</p>
          <textarea class="clean pop-input textarea-css" type="text" placeholder="<?php echo _PRODUCT_DESCRIPTION ?>" name="update_description" id="update_description" required><?php echo $productDetails[0]->getDescription();?></textarea>
        </div>

        <div class="clear"></div>

        <div class="width100 overflow margin-bottom10">
          <p class="input-top-p admin-top-p"><?php echo _PRODUCT_IMAGE ?> 1 : <a href="productImage/<?php echo $productDetails[0]->getImageOne();?>" class="blue-link2" target="_blank"><?php echo _ADMINVIEWBALANCE_VIEW ?></a></p>
          <p><input id="file-upload" type="file"  style="color:white !important;"  name="image_one" id="image_one" accept="image/*" class="margin-bottom10 pointer" /></p>
          <input class="aidex-input clean" type="hidden"  style="color:white !important;"  value="<?php echo $productDetails[0]->getImageOne();?>" name="ori_fileone" id="ori_fileone"> 
        </div>

        <div class="clear"></div>

        <?php 
          $imageTwo = $productDetails[0]->getImageTwo();
          if($imageTwo != '')
          {
          ?>

            <div class="width100 overflow margin-bottom10">
              <p class="input-top-p admin-top-p"><?php echo _PRODUCT_IMAGE ?> 2 : <a href="productImage/<?php echo $productDetails[0]->getImageTwo();?>" class="blue-link" target="_blank"><?php echo _ADMINVIEWBALANCE_VIEW ?></a></p>
              <p><input id="file-upload" type="file" name="image_two" id="image_two" accept="image/*" class="margin-bottom10 pointer" /></p>
              <input class="aidex-input clean" style="color:white !important;" s type="hidden" value="<?php echo $productDetails[0]->getImageTwo();?>" name="ori_filetwo" id="ori_filetwo"> 
            </div>

            <div class="clear"></div>

          <?php
          }
          else
          {
          ?>

          <div class="width100 overflow margin-bottom10">
            <p class="input-top-p admin-top-p"><?php echo _PRODUCT_IMAGE ?> 2</p>
            <p><input id="file-upload" style="color:white !important;" type="file" name="add_image_two" id="add_image_two" accept="image/*" class="margin-bottom10 pointer" /></p>
          </div>

          <div class="clear"></div>

          <?php
          }
        ?>

        <input class="aidex-input clean" type="hidden" value="<?php echo $productDetails[0]->getUid();?>" name="product_uid" id="product_uid"> 

        <div class="width100 overflow text-center">
          <button class="clean blue-button one-button-width pill-button margin-auto" type="submit">
            <?php echo _PRODUCT_SUBMIT ?>
          </button>
        </div>

      </div>

    </form>
    
  <?php
  }
  ?>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>