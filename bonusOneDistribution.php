<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Bonus.php';
require_once dirname(__FILE__) . '/classes/BonusPoolFund.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlTwo,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlTwo,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlFour($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlFive($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlSix($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

$conn = connDB();

$orderDetails = getOrders($conn, "WHERE state = 'PENDING' ORDER BY zipcode ASC");
// $orderDetails = getOrders($conn, "WHERE state = 'PENDING' ");
// $orderDetails = getOrders($conn, "WHERE state = 'PENDING'");
if($orderDetails)
{
    for ($cnt=0; $cnt <count($orderDetails) ; $cnt++)
     {
          echo "Order ID :";
          echo $orderUid = $orderDetails[$cnt]->getOrderId();
          echo "<br>";
          echo $orderUserUid = $orderDetails[$cnt]->getUid();
          echo "<br>";
          echo $orderUserName = $orderDetails[$cnt]->getName();
          echo "<br>";
          echo $orderPrice = $orderDetails[$cnt]->getCommission();
          echo "<br>";
          // echo "<br>";

          $bonusOneStatus = "CLEAR";


 
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($bonusOneStatus)
          {
               array_push($tableName,"state");
               array_push($tableValue,$bonusOneStatus);
               $stringType .=  "s";
          }
          array_push($tableValue,$orderUid);
          $stringType .=  "s";
          $updateOrders = updateDynamicData($conn,"orders"," WHERE order_id = ? ",$tableName,$tableValue,$stringType);
          if($updateOrders)
          {


               //user details in referral history
               $userRH = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($orderUserUid),"s");
               if($userRH)
               {
                    for ($cntA=0; $cntA <count($userRH) ; $cntA++)
                    {
                         echo "<br>";  
                         echo "<br>";  
                         echo "bonus one start <br>";
                         $userLevel = $userRH[$cntA]->getCurrentLevel();
                         //Direct Upline
                         echo "identify direct upline <br>";
                         echo $userUplineDetails = $userRH[$cntA]->getReferrerId(); //william
                         echo "<br>";  

                         // ------------------------------ direct upline start ------------------------------
                         $upline1stDetails = getReferralHistory($conn," WHERE referral_id = ?  ",array("referral_id"),array($userUplineDetails),"s");
                         $uplineLevel = $upline1stDetails[$cntA]->getCurrentLevel();
                    
                         //identify level 2 upline
                         $uplineDetailsLvl2 = $upline1stDetails[$cntA]->getReferrerId();
                    
                         $uplineStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($userUplineDetails),"s");
                         echo $uplineCredit = $uplineStatus[$cntA]->getWallet();
                         echo "<br>"; 
                         echo $uplineUsername = $uplineStatus[$cntA]->getUsername();
                         echo "<br>"; 
                         $bonusLvlOne = ($orderPrice * 0.16);
                         echo $bonusName = 'Direct Sponsor (16%)';
                         echo "<br>"; 
                         echo $newUplineCredit = $uplineCredit + $bonusLvlOne;
                         echo "<br>"; 

                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         //echo "save to database";
                         if($newUplineCredit)
                         {
                             array_push($tableName,"wallet");
                             array_push($tableValue,$newUplineCredit);
                             $stringType .=  "d";
                         }
                         array_push($tableValue,$userUplineDetails);
                         $stringType .=  "s";
                         $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
                         if($passwordUpdated)
                         {
                              $uplineOrderStatus = $uplineStatus[$cntA]->getOrderStatus();
                              if($uplineOrderStatus == 'YES')
                              {
                                   if(directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName))
                                   {
                                        echo "success 1 <br>";  
                                        // header('Location: adminOrderPending.php');
                                   }
                                   else
                                   {
                                        echo "fail 1a <br>";    
                                   } 
                              }
                              else
                              {
                                   echo "Upline 1 : Order Status NO <br>";  
                                   // header('Location: adminOrderPending.php');
                              }
                         }
                         else
                         {
                             echo "fail 1b <br>";     
                             // header('Location: adminOrderPending.php');
                         }
                         // ------------------------------ direct upline end ------------------------------


                         // ------------------------------ level 2 start ------------------------------
                         if(!$uplineDetailsLvl2)
                         {   
                              echo "undefined level 2 upline";   
                              echo "<br>"; 
                              // header('Location: adminOrderPending.php');
                         }
                         else
                         {  
                         $upline2ndDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl2),"s");
                         // identify how many direct downline
                         $upline2ndDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? AND order_status = 'YES' ",array("referrer_id"),array($uplineDetailsLvl2),"s");
                    
                         //identify level 3 upline
                         echo "identify level 3 upline <br>";
                         echo $uplineDetailsLvl3 = $upline2ndDetails[$cntA]->getReferrerId();
                         echo "<br>"; 
                    
                         if ($upline2ndDownlineNum)
                         {
                              echo $totalDownlineTwo = count($upline2ndDownlineNum);
                              echo "<br>"; 
                              if($totalDownlineTwo >= 2)
                              {   
                                   // proceed with bonus calculation
                                   $upline2ndStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl2),"s");
                                   $upline2ndCredit = $upline2ndStatus[$cntA]->getWallet();
                                   echo $upline2ndUsername = $upline2ndStatus[$cntA]->getUsername();
                                   echo "<br>"; 
                                   $bonusLvlTwo = ($orderPrice * 0.05);
                                   echo $bonusName = 'Direct Sponsor (5%)';
                                   echo "<br>"; 
                                   echo $newUpline2ndCredit = $upline2ndCredit + $bonusLvlTwo;
                                   echo "<br>"; 
                    
                                   $tableName = array();
                                   $tableValue =  array();
                                   $stringType =  "";
                                   //echo "save to database";
                                   if($newUpline2ndCredit)
                                   {
                                       array_push($tableName,"wallet");
                                       array_push($tableValue,$newUpline2ndCredit);
                                       $stringType .=  "d";
                                   }
                                   array_push($tableValue,$uplineDetailsLvl2);
                                   $stringType .=  "s";
                                   $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
                                   if($passwordUpdated)
                                   {
                                        $upline2ndOrderStatus = $upline2ndStatus[$cntA]->getOrderStatus();
                                        if($upline2ndOrderStatus == 'YES')
                                        {
                                             if(directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlTwo,$bonusName))
                                             {
                                                  echo "success 2 <br>";  
                                             //   header('Location: adminOrderPending.php');
                                             }
                                             else
                                             {
                                                  echo "fail 2a <br>";    
                                             } 
                                        }
                                        else
                                        {
                                             echo "Upline 2 : Order Status NO <br>";   
                                        //    header('Location: adminOrderPending.php');
                                        }
                                   }
                                   else
                                   {
                                        echo "fail 2c <br>";    
                                        // header('Location: adminOrderPending.php');
                                   }
                              }
                              elseif($totalDownlineTwo < 2)
                              // elseif($totalDownlineTwo < $userLevel)
                              {  
                                   echo "no bonus due to not enough direct downline lvl 2";   
                                   echo "<br>"; 
                                   // header('Location: adminOrderPending.php');
                              }
                         }
                         }
                         // ------------------------------ level 2 end ------------------------------


                         // ------------------------------ level 3 start ------------------------------
                         if(!$uplineDetailsLvl3)
                         {  
                              echo "undefined level 3 upline";   
                              echo "<br>"; 
                              // header('Location: adminOrderPending.php');
                         }
                         else
                         {  
                              $upline3rdDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl3),"s");
                              // identify how many direct downline
                              $upline3rddDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? AND order_status = 'YES' ",array("referrer_id"),array($uplineDetailsLvl3),"s");

                              //identify level 4 upline
                              echo "identify level 4 upline <br>";
                              echo $uplineDetailsLvl4 = $upline3rdDetails[$cntA]->getReferrerId();
                              echo "<br>"; 

                              if ($upline3rddDownlineNum)
                              {
                                   echo $totalDownlineThree = count($upline3rddDownlineNum);
                                   echo "<br>"; 
                                   if($totalDownlineThree >= 3)
                                   {   
                                        // proceed with bonus calculation
                                        $upline3rdStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl3),"s");
                                        $upline3rdCredit = $upline3rdStatus[$cntA]->getWallet();
                                        echo $upline3rdUsername = $upline3rdStatus[$cntA]->getUsername();
                                        echo "<br>"; 
                                        $bonusLvlThree = ($orderPrice * 0.04);
                                        echo $bonusName = 'Direct Sponsor (4%)';
                                        echo "<br>"; 
                                        echo $newUpline3rdCredit = $upline3rdCredit + $bonusLvlThree;
                                        echo "<br>"; 
                    
                                        $tableName = array();
                                        $tableValue =  array();
                                        $stringType =  "";
                                        //echo "save to database";
                                        if($newUpline3rdCredit)
                                        {
                                        array_push($tableName,"wallet");
                                        array_push($tableValue,$newUpline3rdCredit);
                                        $stringType .=  "d";
                                        }
                                        array_push($tableValue,$uplineDetailsLvl3);
                                        $stringType .=  "s";
                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
                                        if($passwordUpdated)
                                        {
                                             $upline3rdOrderStatus = $upline3rdStatus[$cntA]->getOrderStatus();
                                             if($upline3rdOrderStatus == 'YES')
                                             {
                                                  if(directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName))
                                                  {
                                                       echo "success 3 <br>";  
                                                       // header('Location: adminOrderPending.php');
                                                  }
                                                  else
                                                  {
                                                       echo "fail 3a <br>";    
                                                  } 
                                             }
                                             else
                                             {
                                                  echo "Upline 3 : Order Status NO <br>";  
                                                  // header('Location: ../adminOrderPending.php');
                                             }
                                        }
                                        else
                                        {
                                             echo "fail 3c <br>";     
                                             // header('Location: ../adminOrderPending.php');
                                        }
                                   }
                                   // elseif($totalDownlineThree < $userLevel)
                                   elseif($totalDownlineThree < 3)
                                   {  
                                        echo "no bonus due to not enough direct downline lvl 3";   
                                        echo "<br>"; 
                                        // header('Location: adminOrderPending.php');
                                   }
                              }
                         }
                         // ------------------------------ level 3 end ------------------------------


                         // ------------------------------ level 4 start ------------------------------
                         if(!$uplineDetailsLvl4)
                         {  
                              echo "undefined level 4 upline";   
                              echo "<br>"; 
                              // header('Location: adminOrderPending.php');
                         }
                         else
                         {  
                              $upline4thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl4),"s");
                              // identify how many direct downline
                              $upline4thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? AND order_status = 'YES' ",array("referrer_id"),array($uplineDetailsLvl4),"s");
                    
                              //identify level 5 upline
                              echo "identify level 5 upline <br>";
                              echo $uplineDetailsLvl5 = $upline4thDetails[$cntA]->getReferrerId();
                              echo "<br>"; 
                              if ($upline4thDownlineNum)
                              {
                                   echo $totalDownlineFour = count($upline4thDownlineNum);
                                   echo "<br>"; 
                                   if($totalDownlineFour >= 4 )
                                   {   
                                        // proceed with bonus calculation
                                        $upline4thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl4),"s");
                                        $upline4thCredit = $upline4thStatus[$cntA]->getWallet();
                                        echo $upline4thUsername = $upline4thStatus[$cntA]->getUsername();
                                        echo "<br>"; 
                                        $bonusLvlFour = ($orderPrice * 0.02);
                                        echo $bonusName = 'Direct Sponsor (2%)';
                                        echo "<br>"; 
                                        echo $newUpline4thCredit = $upline4thCredit + $bonusLvlFour;
                                        echo "<br>"; 
                    
                                        $tableName = array();
                                        $tableValue =  array();
                                        $stringType =  "";
                                        //echo "save to database";
                                        if($newUpline4thCredit)
                                        {
                                             array_push($tableName,"wallet");
                                             array_push($tableValue,$newUpline4thCredit);
                                             $stringType .=  "d";
                                        }
                                        array_push($tableValue,$uplineDetailsLvl4);
                                        $stringType .=  "s";
                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
                                        if($passwordUpdated)
                                        {
                                             $upline4thOrderStatus = $upline4thStatus[$cntA]->getOrderStatus();
                                             if($upline4thOrderStatus == 'YES')
                                             {
                                                  if(directSponsorBonusLvlFour($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName))
                                                  {
                                                       echo "success 4 <br>";  
                                                       // header('Location: adminOrderPending.php');
                                                  }
                                                  else
                                                  {
                                                       echo "fail 4a <br>";    
                                                  } 
                                             }
                                             else
                                             {
                                                  echo "Upline 4 : Order Status NO <br>";  
                                                  // header('Location: ../adminOrderPending.php');
                                             }
                                        }
                                        else
                                        {
                                             echo "fail 4c <br>";    
                                             // header('Location: adminOrderPending.php');
                                        }
                                   }
                                   // elseif($totalDownlineFour < $userLevel)
                                   elseif($totalDownlineFour < 4)
                                   {  
                                        echo "no bonus due to not enough direct downline lvl 4";   
                                        echo "<br>"; 
                                        // header('Location: adminOrderPending.php');
                                   }
                              }
                         }
                         // ------------------------------ level 4 end ------------------------------


                         // ------------------------------ level 5 start ------------------------------
                         //level 5 bonus
                         if(!$uplineDetailsLvl5)
                         {  
                              echo "undefined level 5 upline";   
                              echo "<br>"; 
                              // header('Location: adminOrderPending.php');
                         }
                         else
                         {  
                              $upline5thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl5),"s");
                              // identify how many direct downline
                              $upline5thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? AND order_status = 'YES' ",array("referrer_id"),array($uplineDetailsLvl5),"s");
                    
                              //identify level 6 upline
                              echo "identify level 6 upline <br>";
                              echo $uplineDetailsLvl6 = $upline5thDetails[$cntA]->getReferrerId();
                              echo "<br>"; 
                    
                              if ($upline5thDownlineNum)
                              {
                                   echo $totalDownlineFive = count($upline5thDownlineNum);
                                   echo "<br>"; 
                                   // if($totalDownlineFive >= $userLevel)
                                   if($totalDownlineFive >= 5)
                                   {   
                                        // proceed with bonus calculation
                                        $upline5thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl5),"s");
                                        $upline5thCredit = $upline5thStatus[$cntA]->getWallet();
                                        echo $upline5thUsername = $upline5thStatus[$cntA]->getUsername();
                                        echo "<br>"; 
                                        $bonusLvlFive = ($orderPrice * 0.0125);
                                        echo $bonusName = 'Direct Sponsor (1.25%)';
                                        echo "<br>"; 
                                        echo $newUpline5thCredit = $upline5thCredit + $bonusLvlFive;
                                        echo "<br>"; 
                    
                                        $tableName = array();
                                        $tableValue =  array();
                                        $stringType =  "";
                                        //echo "save to database";
                                        if($newUpline5thCredit)
                                        {
                                             array_push($tableName,"wallet");
                                             array_push($tableValue,$newUpline5thCredit);
                                             $stringType .=  "d";
                                        }
                                        array_push($tableValue,$uplineDetailsLvl5);
                                        $stringType .=  "s";
                                        // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                        // 301220 new requirement 
                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
                                        if($passwordUpdated)
                                        {
                                             $upline5thOrderStatus = $upline5thStatus[$cntA]->getOrderStatus();
                                             if($upline5thOrderStatus == 'YES')
                                             {
                                                  if(directSponsorBonusLvlFive($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName))
                                                  {
                                                       echo "success 5 <br>";  
                                                       // header('Location: adminOrderPending.php');
                                                  }
                                                  else
                                                  {
                                                       echo "fail 5a <br>";    
                                                  } 
                                             }
                                             else
                                             {
                                                  echo "Upline 5 : Order Status NO <br>";  
                                                  // header('Location: ../adminOrderPending.php');
                                             }
                                        }
                                        else
                                        {
                                             echo "fail 5c <br>";    
                                             // header('Location: adminOrderPending.php');
                                        }
                                   }
                                   // elseif($totalDownlineFive < $userLevel)
                                   elseif($totalDownlineFive < 5)
                                   {  
                                        echo "no bonus due to not enough direct downline lvl 5";   
                                        echo "<br>"; 
                                        // header('Location: adminOrderPending.php');
                                   }
                              }
                         }
                         // ------------------------------ level 5 end ------------------------------


                         // ------------------------------ level 6 start ------------------------------
                         //level 6 bonus
                         if(!$uplineDetailsLvl6)
                         {  
                              // echo "undefined upline level 6";   
                              // echo "<br>"; 
                              // header('Location: ../adminOrderPending.php');
                         }
                         else
                         {  
                              $upline6thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl6),"s");
                              // identify how many direct downline
                              $upline6thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? AND order_status = 'YES' ",array("referrer_id"),array($uplineDetailsLvl6),"s");

                              // //identify 6th upline
                              if ($upline6thDownlineNum)
                              {
                                   echo $totalDownlineSix = count($upline6thDownlineNum);
                                   echo "<br>"; 
                                   if($totalDownlineSix >= 6)
                                   {   
                                        // proceed with bonus calculation
                                        $upline6thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl6),"s");
                                        $upline6thCredit = $upline6thStatus[$cntA]->getWallet();
                                        echo $upline6thUsername = $upline6thStatus[$cntA]->getUsername();
                                        echo "<br>"; 
                                        $bonusLvlSix = ($orderPrice * 0.01);
                                        echo $bonusName = 'Direct Sponsor (1%)';
                                        echo "<br>"; 
                                        echo $newUpline6thCredit = $upline6thCredit + $bonusLvlSix;
                                        echo "<br>"; 
                                        echo "CASE CLOSE"; 
                                        echo "<br>"; 
                                        echo "<br>"; 
                                        echo "<br>"; 
                    
                                        $tableName = array();
                                        $tableValue =  array();
                                        $stringType =  "";
                                        //echo "save to database";
                                        if($newUpline6thCredit)
                                        {
                                             array_push($tableName,"wallet");
                                             array_push($tableValue,$newUpline6thCredit);
                                             $stringType .=  "d";
                                        }
                                        array_push($tableValue,$uplineDetailsLvl6);
                                        $stringType .=  "s";
                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
                                        if($passwordUpdated)
                                        {
                                             $upline6thOrderStatus = $upline6thStatus[$cntA]->getOrderStatus();
                                             if($upline6thOrderStatus == 'YES')
                                             {
                                                  if(directSponsorBonusLvlSix($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName))
                                                  {
                                                       echo "success 6 <br>";  
                                                       // header('Location: adminOrderPending.php');
                                                  }
                                                  else
                                                  {
                                                       echo "fail 6a <br>";    
                                                  }
                                             }
                                             else
                                             {
                                                  echo "Upline 6 : Order Status NO <br>";  
                                                  // header('Location: adminOrderPending.php');
                                             }
                                        }
                                        else
                                        {
                                             echo "fail 6c <br>";      
                                             // header('Location: adminOrderPending.php');
                                        }                                        
                                   }
                                   // elseif($totalDownlineSix < $userLevel)
                                   elseif($totalDownlineSix < 6)
                                   {  
                                        echo "no bonus due to not enough direct downline lvl 6";   
                                        echo "<br>"; 
                                        // header('Location: /adminOrderPending.php');
                                   }
                              }
                         }
                         // ------------------------------ level 6 end ------------------------------


                    }
               }


          }
          else
          {
               echo "ERROR TO UPDATE BONUS 1 CLEAR STATUS";
          }



          // //user details in referral history
          // $userRH = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($orderUserUid),"s");
          // if($userRH)
          // {
          //      for ($cntA=0; $cntA <count($userRH) ; $cntA++)
          //      {
          //           echo "<br>";  
          //           echo "<br>";  
          //           echo "bonus one start <br>";
          //           $userLevel = $userRH[$cntA]->getCurrentLevel();
          //           //Direct Upline
          //           echo "identify direct upline <br>";
          //           echo $userUplineDetails = $userRH[$cntA]->getReferrerId(); //william
          //           echo "<br>";  

          //           // ------------------------------ direct upline start ------------------------------
          //           $upline1stDetails = getReferralHistory($conn," WHERE referral_id = ?  ",array("referral_id"),array($userUplineDetails),"s");
          //           $uplineLevel = $upline1stDetails[$cntA]->getCurrentLevel();
               
          //           //identify level 2 upline
          //           $uplineDetailsLvl2 = $upline1stDetails[$cntA]->getReferrerId();
               
          //           $uplineStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($userUplineDetails),"s");
          //           echo $uplineCredit = $uplineStatus[$cntA]->getWallet();
          //           echo "<br>"; 
          //           echo $uplineUsername = $uplineStatus[$cntA]->getUsername();
          //           echo "<br>"; 
          //           $bonusLvlOne = ($orderPrice * 0.16);
          //           echo $bonusName = 'Direct Sponsor (16%)';
          //           echo "<br>"; 
          //           echo $newUplineCredit = $uplineCredit + $bonusLvlOne;
          //           echo "<br>"; 

          //           // $tableName = array();
          //           // $tableValue =  array();
          //           // $stringType =  "";
          //           // //echo "save to database";
          //           // if($newUplineCredit)
          //           // {
          //           //     array_push($tableName,"wallet");
          //           //     array_push($tableValue,$newUplineCredit);
          //           //     $stringType .=  "d";
          //           // }
          //           // array_push($tableValue,$userUplineDetails);
          //           // $stringType .=  "s";
          //           // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
          //           // if($passwordUpdated)
          //           // {
          //           //      $uplineOrderStatus = $uplineStatus[$cnt]->getOrderStatus();
          //           //      if($uplineOrderStatus == 'YES')
          //           //      {
          //           //           if(directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName))
          //           //           {
          //           //                echo "success 1 <br>";  
          //           //                // header('Location: adminOrderPending.php');
          //           //           }
          //           //           else
          //           //           {
          //           //                echo "fail 1a <br>";    
          //           //           } 
          //           //      }
          //           //      else
          //           //      {
          //           //           echo "Upline 1 : Order Status NO <br>";  
          //           //           // header('Location: adminOrderPending.php');
          //           //      }
          //           // }
          //           // else
          //           // {
          //           //     echo "fail 1b <br>";     
          //           //     // header('Location: adminOrderPending.php');
          //           // }
          //           // ------------------------------ direct upline end ------------------------------


          //           // ------------------------------ level 2 start ------------------------------
          //           if(!$uplineDetailsLvl2)
          //           {   
          //                echo "undefined level 2 upline";   
          //                echo "<br>"; 
          //                // header('Location: adminOrderPending.php');
          //           }
          //           else
          //           {  
          //           $upline2ndDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl2),"s");
          //           // identify how many direct downline
          //           $upline2ndDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? AND order_status = 'YES' ",array("referrer_id"),array($uplineDetailsLvl2),"s");
               
          //           //identify level 3 upline
          //           echo "identify level 3 upline <br>";
          //           echo $uplineDetailsLvl3 = $upline2ndDetails[$cntA]->getReferrerId();
          //           echo "<br>"; 
               
          //           if ($upline2ndDownlineNum)
          //           {
          //                echo $totalDownlineTwo = count($upline2ndDownlineNum);
          //                echo "<br>"; 
          //                if($totalDownlineTwo >= 2)
          //                {   
          //                     // proceed with bonus calculation
          //                     $upline2ndStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl2),"s");
          //                     $upline2ndCredit = $upline2ndStatus[$cntA]->getWallet();
          //                     echo $upline2ndUsername = $upline2ndStatus[$cntA]->getUsername();
          //                     echo "<br>"; 
          //                     $bonusLvlTwo = ($orderPrice * 0.05);
          //                     echo $bonusName = 'Direct Sponsor (5%)';
          //                     echo "<br>"; 
          //                     echo $newUpline2ndCredit = $upline2ndCredit + $bonusLvlTwo;
          //                     echo "<br>"; 
               
          //                     // $tableName = array();
          //                     // $tableValue =  array();
          //                     // $stringType =  "";
          //                     // //echo "save to database";
          //                     // if($newUpline2ndCredit)
          //                     // {
          //                     //     array_push($tableName,"wallet");
          //                     //     array_push($tableValue,$newUpline2ndCredit);
          //                     //     $stringType .=  "d";
          //                     // }
          //                     // array_push($tableValue,$uplineDetailsLvl2);
          //                     // $stringType .=  "s";
          //                     // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
          //                     // if($passwordUpdated)
          //                     // {
          //                     //      $upline2ndOrderStatus = $upline2ndStatus[$cnt]->getOrderStatus();
          //                     //      if($upline2ndOrderStatus == 'YES')
          //                     //      {
          //                     //           if(directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlTwo,$bonusName))
          //                     //           {
          //                     //                echo "success 2 <br>";  
          //                     //           //   header('Location: adminOrderPending.php');
          //                     //           }
          //                     //           else
          //                     //           {
          //                     //                echo "fail 2a <br>";    
          //                     //           } 
          //                     //      }
          //                     //      else
          //                     //      {
          //                     //           echo "Upline 2 : Order Status NO <br>";   
          //                     //      //    header('Location: adminOrderPending.php');
          //                     //      }
          //                     // }
          //                     // else
          //                     // {
          //                     //      echo "fail 2c <br>";    
          //                     //      // header('Location: adminOrderPending.php');
          //                     // }
          //                }
          //                elseif($totalDownlineTwo < 2)
          //                // elseif($totalDownlineTwo < $userLevel)
          //                {  
          //                     echo "no bonus due to not enough direct downline lvl 2";   
          //                     echo "<br>"; 
          //                     // header('Location: adminOrderPending.php');
          //                }
          //           }
          //           }
          //           // ------------------------------ level 2 end ------------------------------


          //           // ------------------------------ level 3 start ------------------------------
          //           if(!$uplineDetailsLvl3)
          //           {  
          //                echo "undefined level 3 upline";   
          //                echo "<br>"; 
          //                // header('Location: adminOrderPending.php');
          //           }
          //           else
          //           {  
          //                $upline3rdDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl3),"s");
          //                // identify how many direct downline
          //                $upline3rddDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? AND order_status = 'YES' ",array("referrer_id"),array($uplineDetailsLvl3),"s");

          //                //identify level 4 upline
          //                echo "identify level 4 upline <br>";
          //                echo $uplineDetailsLvl4 = $upline3rdDetails[$cntA]->getReferrerId();
          //                echo "<br>"; 

          //                if ($upline3rddDownlineNum)
          //                {
          //                     echo $totalDownlineThree = count($upline3rddDownlineNum);
          //                     echo "<br>"; 
          //                     if($totalDownlineThree >= 3)
          //                     {   
          //                          // proceed with bonus calculation
          //                          $upline3rdStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl3),"s");
          //                          $upline3rdCredit = $upline3rdStatus[$cntA]->getWallet();
          //                          echo $upline3rdUsername = $upline3rdStatus[$cntA]->getUsername();
          //                          echo "<br>"; 
          //                          $bonusLvlThree = ($orderPrice * 0.04);
          //                          echo $bonusName = 'Direct Sponsor (4%)';
          //                          echo "<br>"; 
          //                          echo $newUpline3rdCredit = $upline3rdCredit + $bonusLvlThree;
          //                          echo "<br>"; 
               
          //                          // $tableName = array();
          //                          // $tableValue =  array();
          //                          // $stringType =  "";
          //                          // //echo "save to database";
          //                          // if($newUpline3rdCredit)
          //                          // {
          //                          // array_push($tableName,"wallet");
          //                          // array_push($tableValue,$newUpline3rdCredit);
          //                          // $stringType .=  "d";
          //                          // }
          //                          // array_push($tableValue,$uplineDetailsLvl3);
          //                          // $stringType .=  "s";
          //                          // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
          //                          // if($passwordUpdated)
          //                          // {
          //                          //      $upline3rdOrderStatus = $upline3rdStatus[$cntA]->getOrderStatus();
          //                          //      if($upline3rdOrderStatus == 'YES')
          //                          //      {
          //                          //           if(directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName))
          //                          //           {
          //                          //                echo "success 3 <br>";  
          //                          //                // header('Location: adminOrderPending.php');
          //                          //           }
          //                          //           else
          //                          //           {
          //                          //                echo "fail 3a <br>";    
          //                          //           } 
          //                          //      }
          //                          //      else
          //                          //      {
          //                          //           echo "Upline 3 : Order Status NO <br>";  
          //                          //           // header('Location: ../adminOrderPending.php');
          //                          //      }
          //                          // }
          //                          // else
          //                          // {
          //                          //      echo "fail 3c <br>";     
          //                          //      // header('Location: ../adminOrderPending.php');
          //                          // }
          //                     }
          //                     // elseif($totalDownlineThree < $userLevel)
          //                     elseif($totalDownlineThree < 3)
          //                     {  
          //                          echo "no bonus due to not enough direct downline lvl 3";   
          //                          echo "<br>"; 
          //                          // header('Location: adminOrderPending.php');
          //                     }
          //                }
          //           }
          //           // ------------------------------ level 3 end ------------------------------


          //           // ------------------------------ level 4 start ------------------------------
          //           if(!$uplineDetailsLvl4)
          //           {  
          //                echo "undefined level 4 upline";   
          //                echo "<br>"; 
          //                // header('Location: adminOrderPending.php');
          //           }
          //           else
          //           {  
          //                $upline4thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl4),"s");
          //                // identify how many direct downline
          //                $upline4thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? AND order_status = 'YES' ",array("referrer_id"),array($uplineDetailsLvl4),"s");
               
          //                //identify level 5 upline
          //                echo "identify level 5 upline <br>";
          //                echo $uplineDetailsLvl5 = $upline4thDetails[$cntA]->getReferrerId();
          //                echo "<br>"; 
          //                if ($upline4thDownlineNum)
          //                {
          //                     echo $totalDownlineFour = count($upline4thDownlineNum);
          //                     echo "<br>"; 
          //                     if($totalDownlineFour >= 4 )
          //                     {   
          //                          // proceed with bonus calculation
          //                          $upline4thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl4),"s");
          //                          $upline4thCredit = $upline4thStatus[$cntA]->getWallet();
          //                          echo $upline4thUsername = $upline4thStatus[$cntA]->getUsername();
          //                          echo "<br>"; 
          //                          $bonusLvlFour = ($orderPrice * 0.02);
          //                          echo $bonusName = 'Direct Sponsor (2%)';
          //                          echo "<br>"; 
          //                          echo $newUpline4thCredit = $upline4thCredit + $bonusLvlFour;
          //                          echo "<br>"; 
               
          //                          // $tableName = array();
          //                          // $tableValue =  array();
          //                          // $stringType =  "";
          //                          // //echo "save to database";
          //                          // if($newUpline4thCredit)
          //                          // {
          //                          //      array_push($tableName,"wallet");
          //                          //      array_push($tableValue,$newUpline4thCredit);
          //                          //      $stringType .=  "d";
          //                          // }
          //                          // array_push($tableValue,$uplineDetailsLvl4);
          //                          // $stringType .=  "s";
          //                          // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
          //                          // if($passwordUpdated)
          //                          // {
          //                          //      $upline4thOrderStatus = $upline4thStatus[$cntA]->getOrderStatus();
          //                          //      if($upline4thOrderStatus == 'YES')
          //                          //      {
          //                          //           if(directSponsorBonusLvlFour($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName))
          //                          //           {
          //                          //                echo "success 4 <br>";  
          //                          //                // header('Location: adminOrderPending.php');
          //                          //           }
          //                          //           else
          //                          //           {
          //                          //                echo "fail 4a <br>";    
          //                          //           } 
          //                          //      }
          //                          //      else
          //                          //      {
          //                          //           echo "Upline 4 : Order Status NO <br>";  
          //                          //           // header('Location: ../adminOrderPending.php');
          //                          //      }
          //                          // }
          //                          // else
          //                          // {
          //                          //      echo "fail 4c <br>";    
          //                          //      // header('Location: adminOrderPending.php');
          //                          // }
          //                     }
          //                     // elseif($totalDownlineFour < $userLevel)
          //                     elseif($totalDownlineFour < 4)
          //                     {  
          //                          echo "no bonus due to not enough direct downline lvl 4";   
          //                          echo "<br>"; 
          //                          // header('Location: adminOrderPending.php');
          //                     }
          //                }
          //           }
          //           // ------------------------------ level 4 end ------------------------------


          //           // ------------------------------ level 5 start ------------------------------
          //           //level 5 bonus
          //           if(!$uplineDetailsLvl5)
          //           {  
          //                echo "undefined level 5 upline";   
          //                echo "<br>"; 
          //                // header('Location: adminOrderPending.php');
          //           }
          //           else
          //           {  
          //                $upline5thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl5),"s");
          //                // identify how many direct downline
          //                $upline5thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? AND order_status = 'YES' ",array("referrer_id"),array($uplineDetailsLvl5),"s");
               
          //                //identify level 6 upline
          //                echo "identify level 6 upline <br>";
          //                echo $uplineDetailsLvl6 = $upline5thDetails[$cntA]->getReferrerId();
          //                echo "<br>"; 
               
          //                if ($upline5thDownlineNum)
          //                {
          //                     echo $totalDownlineFive = count($upline5thDownlineNum);
          //                     echo "<br>"; 
          //                     // if($totalDownlineFive >= $userLevel)
          //                     if($totalDownlineFive >= 5)
          //                     {   
          //                          // proceed with bonus calculation
          //                          $upline5thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl5),"s");
          //                          $upline5thCredit = $upline5thStatus[$cntA]->getWallet();
          //                          echo $upline5thUsername = $upline5thStatus[$cntA]->getUsername();
          //                          echo "<br>"; 
          //                          $bonusLvlFive = ($orderPrice * 0.0125);
          //                          echo $bonusName = 'Direct Sponsor (1.25%)';
          //                          echo "<br>"; 
          //                          echo $newUpline5thCredit = $upline5thCredit + $bonusLvlFive;
          //                          echo "<br>"; 
               
          //                          // $tableName = array();
          //                          // $tableValue =  array();
          //                          // $stringType =  "";
          //                          // //echo "save to database";
          //                          // if($newUpline5thCredit)
          //                          // {
          //                          //      array_push($tableName,"wallet");
          //                          //      array_push($tableValue,$newUpline5thCredit);
          //                          //      $stringType .=  "d";
          //                          // }
          //                          // array_push($tableValue,$uplineDetailsLvl5);
          //                          // $stringType .=  "s";
          //                          // // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          //                          // // 301220 new requirement 
          //                          // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
          //                          // if($passwordUpdated)
          //                          // {
          //                          //      $upline5thOrderStatus = $upline5thStatus[$cntA]->getOrderStatus();
          //                          //      if($upline5thOrderStatus == 'YES')
          //                          //      {
          //                          //           if(directSponsorBonusLvlFive($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName))
          //                          //           {
          //                          //                echo "success 5 <br>";  
          //                          //                // header('Location: adminOrderPending.php');
          //                          //           }
          //                          //           else
          //                          //           {
          //                          //                echo "fail 5a <br>";    
          //                          //           } 
          //                          //      }
          //                          //      else
          //                          //      {
          //                          //           echo "Upline 5 : Order Status NO <br>";  
          //                          //           // header('Location: ../adminOrderPending.php');
          //                          //      }
          //                          // }
          //                          // else
          //                          // {
          //                          //      echo "fail 5c <br>";    
          //                          //      // header('Location: adminOrderPending.php');
          //                          // }
          //                     }
          //                     // elseif($totalDownlineFive < $userLevel)
          //                     elseif($totalDownlineFive < 5)
          //                     {  
          //                          echo "no bonus due to not enough direct downline lvl 5";   
          //                          echo "<br>"; 
          //                          // header('Location: adminOrderPending.php');
          //                     }
          //                }
          //           }
          //           // ------------------------------ level 5 end ------------------------------


          //           // ------------------------------ level 6 start ------------------------------
          //           //level 6 bonus
          //           if(!$uplineDetailsLvl6)
          //           {  
          //                // echo "undefined upline level 6";   
          //                // echo "<br>"; 
          //                header('Location: ../adminOrderPending.php');
          //           }
          //           else
          //           {  
          //                $upline6thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl6),"s");
          //                // identify how many direct downline
          //                $upline6thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? AND order_status = 'YES' ",array("referrer_id"),array($uplineDetailsLvl6),"s");

          //                // //identify 6th upline
          //                if ($upline6thDownlineNum)
          //                {
          //                     echo $totalDownlineSix = count($upline6thDownlineNum);
          //                     echo "<br>"; 
          //                     if($totalDownlineSix >= 6)
          //                     {   
          //                          // proceed with bonus calculation
          //                          $upline6thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl6),"s");
          //                          $upline6thCredit = $upline6thStatus[$cntA]->getWallet();
          //                          echo $upline6thUsername = $upline6thStatus[$cntA]->getUsername();
          //                          echo "<br>"; 
          //                          $bonusLvlSix = ($orderPrice * 0.01);
          //                          echo $bonusName = 'Direct Sponsor (1%)';
          //                          echo "<br>"; 
          //                          echo $newUpline6thCredit = $upline6thCredit + $bonusLvlSix;
          //                          echo "<br>"; 
          //                          echo "CASE CLOSE"; 
          //                          echo "<br>"; 
          //                          echo "<br>"; 
          //                          echo "<br>"; 
               
          //                          // $tableName = array();
          //                          // $tableValue =  array();
          //                          // $stringType =  "";
          //                          // //echo "save to database";
          //                          // if($newUpline6thCredit)
          //                          // {
          //                          //      array_push($tableName,"wallet");
          //                          //      array_push($tableValue,$newUpline6thCredit);
          //                          //      $stringType .=  "d";
          //                          // }
          //                          // array_push($tableValue,$uplineDetailsLvl6);
          //                          // $stringType .=  "s";
          //                          // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
          //                          // if($passwordUpdated)
          //                          // {
          //                          //      $upline6thOrderStatus = $upline6thStatus[$cntA]->getOrderStatus();
          //                          //      if($upline6thOrderStatus == 'YES')
          //                          //      {
          //                          //           if(directSponsorBonusLvlSix($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName))
          //                          //           {
          //                          //                echo "success 6 <br>";  
          //                          //                // header('Location: adminOrderPending.php');
          //                          //           }
          //                          //           else
          //                          //           {
          //                          //                echo "fail 6a <br>";    
          //                          //           }
          //                          //      }
          //                          //      else
          //                          //      {
          //                          //           echo "Upline 6 : Order Status NO <br>";  
          //                          //           // header('Location: ../adminOrderPending.php');
          //                          //      }
          //                          // }
          //                          // else
          //                          // {
          //                          //      echo "fail 6c <br>";      
          //                          //      // header('Location: adminOrderPending.php');
          //                          // }                                        
          //                     }
          //                     // elseif($totalDownlineSix < $userLevel)
          //                     elseif($totalDownlineSix < 6)
          //                     {  
          //                          echo "no bonus due to not enough direct downline lvl 6";   
          //                          echo "<br>"; 
          //                          // header('Location: adminOrderPending.php');
          //                     }
          //                }
          //           }
          //           // ------------------------------ level 6 end ------------------------------


          //      }
          // }

    
    }
}
?>