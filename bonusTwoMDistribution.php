<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BonusPoolFund.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function poolBonsRecord($conn,$bonusUid,$uid,$username,$amount,$bonusType)
{
     if(insertDynamicData($conn,"bonus_record",array("bonus_uid","uid","username","amount","bonus_type"),
     array($bonusUid,$uid,$username,$amount,$bonusType),"sssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

$conn = connDB();

$receiverStatus = 'Manager';
// $receiverStatus = 'Senior Manager';
// $receiverStatus = 'Area Manager';
// $receiverStatus = 'District Manager';

//identify amount to distribute
// $managerBonusPool = getBonusPoolFund($conn, "WHERE receiver = ? ", array("receiver"), array($receiverStatus), "s");
$managerBonusPool = getBonusPoolFund($conn, "WHERE receiver = ? AND status = 'PENDING' ", array("receiver"), array($receiverStatus), "s");
if($managerBonusPool)
{
    $totalManagerBonusPool = 0;
    for ($cnt=0; $cnt <count($managerBonusPool) ; $cnt++)
    {
      // echo $managerBonusId = $managerBonusPool[$cnt]->getId();
      // echo "<br>";
      // echo "AAA";
      // echo "<br>";
      // echo $totalManagerBonusPool += $managerBonusPool[$cnt]->getAmount();
      // echo "<br>";

      $managerBonusId = $managerBonusPool[$cnt]->getId();
      $totalManagerBonusPool += $managerBonusPool[$cnt]->getAmount();

      $status = "CLEAR";

      $tableName = array();
      $tableValue =  array();
      $stringType =  "";
      //echo "save to database";
      if($status)
      {
          array_push($tableName,"status");
          array_push($tableValue,$status);
          $stringType .=  "s";
      }    
      array_push($tableValue,$managerBonusPool[$cnt]->getId());
      $stringType .=  "s";
      $updateBonusPool = updateDynamicData($conn,"bonus_poolfund"," WHERE id = ? ",$tableName,$tableValue,$stringType);
      if($updateBonusPool)
      {
        echo "BONUS CLEAR";
        // echo "<br>";
        // header('Location: ../adminDashboard.php');
      }
      else
      {
        echo "FAIL TO CLEAR BONUS !!";
      }

    }
}
else
{
  // echo "BBB";
  // echo "<br>";
  // echo $totalManagerBonusPool = 0 ;
  // echo "<br>";
  $totalManagerBonusPool = 0 ;
}

// echo $distributeAmount = $totalManagerBonusPool / $totalManger;

//identify ranking
// $allManager = getUser($conn, "WHERE rank = ? ", array("rank"), array($receiverStatus), "s");
// 301220 new requirement 
$allManager = getUser($conn, "WHERE rank = ? AND order_status = 'YES' ", array("rank"), array($receiverStatus), "s");
if($allManager)
{   
    echo $totalManger = count($allManager);
    echo "<br>";

    for ($cnt=0; $cnt < count($allManager) ; $cnt++)
    {
      // echo $uid = $allManager[$cnt]->getUid();
      // echo "<br>";
      // echo $username = $allManager[$cnt]->getUsername();
      // echo "<br>";
      // echo $wallet = $allManager[$cnt]->getWallet();
      // echo "<br>";

      $uid = $allManager[$cnt]->getUid();
      $username = $allManager[$cnt]->getUsername();
      $wallet = $allManager[$cnt]->getWallet();
      $amount = $totalManagerBonusPool;
      $bonusType = "Bonus 2 Pool Fund (Manager 2%)";

      $monthlyPoolBonus = $totalManagerBonusPool + $wallet;

      $bonusUid = md5(uniqid());

      $tableName = array();
      $tableValue =  array();
      $stringType =  "";
      //echo "save to database";
      if($monthlyPoolBonus)
      {
          array_push($tableName,"wallet");
          array_push($tableValue,$monthlyPoolBonus);
          $stringType .=  "s";
      }    
      array_push($tableValue,$uid);
      $stringType .=  "s";
      $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
      if($passwordUpdated)
      {

        if(poolBonsRecord($conn,$bonusUid,$uid,$username,$amount,$bonusType))
        {
          // echo "success add pool bonus";  
          // echo "<br>";
          // header('Location: adminDashboard.php');
          $_SESSION['messageType'] = 1;
          header('Location: adminDashboard.php?type=1');
          
        }
        else
        {
          echo "ERROR BONUS CLEAR";  
        } 

      }
      else
      {
        // echo "ERROR TO UPDATE WALLET !!";
        // header('Location: adminDashboard.php');
        $_SESSION['messageType'] = 2;
        header('Location: adminDashboard.php?type=1');
      }
    }

}
else
{   
  $totalManger = 0;  
  // echo "<br>";
  echo "NO MANAGER !!";  
  header('Location: adminDashboard.php');
}

if($totalManger > 0)
{   
  // echo "CLEAR";
  // echo "<br>";
}
else
{   
  // echo "CARRY FORWARD";
  // echo "<br>";
}

// $distributeAmount = $totalManagerBonusPool / $totalManger;
?>