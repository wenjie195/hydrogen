<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

$userRHDetails = getReferralHistory($conn, " WHERE referral_id =? ", array("referral_id"), array($uid), "s");
$userLevel = $userRHDetails[0];

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$PersonalSales = 0;

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>

    <meta property="og:url" content="https://hygeniegroup.com/userViewBonus_viewBonusStatement.php" />
    <link rel="canonical" href="https://hygeniegroup.com/userViewBonus_viewBonusStatement.php" />
    <meta property="og:title" content="Bonus System  | Hygenie Group" />
    <title>Bonus System  | Hygenie Group</title>

	<?php include 'css.php'; ?>
</head>
<style media="screen">
  .blue-button{
  font-size: 10px;
  width: auto;
  margin-top: 50px;
  padding: 5px;
  margin-left: -20px;
  }
  ul{
    list-style: none;
    list-style-image: url('img/li.png');
  }
  li{
    vertical-align: middle;
    cursor: pointer;
  }
</style>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height" id="firefly">
  <div class="width100 overflow text-center">
    <img src="img/hierachy.png" class="middle-title-icon" alt="Bonus System" title="Bonus System">
  </div>

  <div class="width100 overflow">
    <h1 class="pop-h1 text-center">Direct Sponsor Bonus</h1>
  </div>

  <div class="overflow-scroll-div">
    <table class="table-css fix-th">
      <thead>
        <tr>
          <th>Level</th>
          <th>Bonus (%)</th>
        </tr>
      </thead>
      <tbody>
            <tr>
              <td>Level 1</td>
              <td>40</td>
            </tr>
            <tr>
              <td>Level 2</td>
              <td>12</td>
            </tr>
            <tr>
              <td>Level 3</td>
              <td>10</td>
            </tr>
            <tr>
              <td>Level 4</td>
              <td>5</td>
            </tr>
            <tr>
              <td>Level 5</td>
              <td>3</td>
            </tr>
            <tr>
              <td>Level 6</td>
              <td>2</td>
            </tr>
      </tbody>
    </table>
  </div>

  <div class="clear"></div>

  <div class="width100 overflow">
    <h1 class="pop-h1 text-center">Pool Fund Bonus</h1>
  </div>

  <div class="overflow-scroll-div">
    <table class="table-css fix-th">
      <thead>
        <tr>
          <th>Level</th>
          <th>Bonus (%)</th>
        </tr>
      </thead>
      <tbody>
            <tr>
              <td>Manager</td>
              <td>2</td>
            </tr>
            <tr>
              <td>Senior Manager</td>
              <td>2</td>
            </tr>
            <tr>
              <td>Area Manager</td>
              <td>3</td>
            </tr>
            <tr>
              <td>District Manager</td>
              <td>3</td>
            </tr>
      </tbody>
    </table>
  </div>

  <div class="clear"></div>

  <div class="width100 overflow">
    <h1 class="pop-h1 text-center">Overiding of Pool Fund Bonus</h1>
  </div>

  <div class="overflow-scroll-div">
    <table class="table-css fix-th">
      <thead>
        <tr>
          <th>Level</th>
          <th>Bonus (%)</th>
        </tr>
      </thead>
      <tbody>
            <tr>
              <td>Upline</td>
              <td>10</td>
            </tr>
      </tbody>
    </table>
  </div>

<div class="clear"></div>

</div>

<?php include 'js.php'; ?>

</body>
</html>