<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html>
<head>

	<?php include 'meta.php'; ?>
	<meta property="og:url" content="https://hygeniegroup.com/adminProductAdd.php" />
    <link rel="canonical" href="https://hygeniegroup.com/adminProductAdd.php" /> 
    <meta property="og:title" content="<?php echo _PRODUCT ?> | Hygenie Group" />
    <title><?php echo _PRODUCT ?> | Hygenie Group</title>

	<?php include 'css.php'; ?>

</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">

  <h1 class="small-h1-a text-center white-text"> <a class="blue-link" href="adminProductAll.php"><?php echo _PRODUCT ?></a> | <?php echo _PRODUCT_ADD ?></h1>

  <div class="clear"></div>

  <form action="utilities/registerProductFunction.php" method="POST" enctype="multipart/form-data">

    <div class="border-separation">

      <!-- <div class="dual-input">
        <p class="input-top-p admin-top-p">Category</p>
        <input class="input-name clean input-textarea admin-input" type="text" placeholder="Category" name="register_category" id="register_category" required>
      </div>

      <div class="dual-input second-dual-input">
        <p class="input-top-p admin-top-p">Brand</p>
        <input class="input-name clean input-textarea admin-input" type="text" placeholder="Brand" name="register_brand" id="register_brand" required>
      </div> -->

      <div class="clear"></div>

      <div class="dual-input">
        <p class="input-top-p admin-top-p"><?php echo _PRODUCT_NAME ?>*</p>
        <input class="clean pop-input" type="text" placeholder="<?php echo _PRODUCT_NAME ?>" name="register_name" id="register_name" required>
      </div>

      <div class="dual-input second-dual-input">
        <p class="input-top-p admin-top-p"><?php echo _PRODUCT_PRICE ?></p>
        <input class="clean pop-input" type="text" placeholder="<?php echo _PRODUCT_PRICE ?>" name="register_price" id="register_price" required>
      </div>

      <div class="clear"></div>

      <div class="dual-input">
        <p class="input-top-p admin-top-p"><?php echo _PRODUCT_COMMISSION ?></p>
        <input class="clean pop-input" type="text" placeholder="<?php echo _PRODUCT_COMMISSION ?>" name="register_commission" id="register_commission" required>
      </div>

      <div class="clear"></div>

      <div class="width100 overflow">
        <p class="input-top-p admin-top-p"><?php echo _PRODUCT_DESCRIPTION ?>* (<?php echo _PRODUCT_AVOID ?> " , '')</p>
        <input class="clean pop-input textarea-css" type="text" placeholder="<?php echo _PRODUCT_DESCRIPTION ?>" name="register_description" id="register_description" required>
      </div>

      <div class="clear"></div>

      <div class="width100 overflow margin-bottom10">
        <p class="input-top-p admin-top-p"><?php echo _PRODUCT_IMAGE ?> 1</p>
        <p><input id="file-upload" style="color:white !important;" type="file" name="image_one" id="image_one" accept="image/*" class="margin-bottom10 pointer" required/></p>
      </div>

      <div class="clear"></div>

      <div class="width100 overflow margin-bottom10">
        <p class="input-top-p admin-top-p"><?php echo _PRODUCT_IMAGE ?> 2</p>
        <p><input id="file-upload" style="color:white !important;" type="file" name="image_two" id="image_two" accept="image/*" class="margin-bottom10 pointer" /></p>
      </div>

    </div>

    <div class="clear"></div>

    <div class="width100 overflow text-center">
      <button class="clean blue-button one-button-width pill-button margin-auto" type="submit">
        <?php echo _PRODUCT_SUBMIT ?>
      </button>
    </div>

  </form>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>