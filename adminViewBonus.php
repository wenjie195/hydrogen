<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Bonus.php';
require_once dirname(__FILE__) . '/classes/BonusPoolFund.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$directBonus = getBonus($conn);
$poolBonus = getBonusPoolFund($conn);

$conn->close();

?>

<!DOCTYPE html>
<html>
<head>

	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://hygeniegroup.com/adminViewBonus.php" />
    <link rel="canonical" href="https://hygeniegroup.com/adminViewBonus.php" />
    <meta property="og:title" content="View Bonus| Hygenie Group" />
    <title>View Bonus | Hygenie Group</title>

	<?php include 'css.php'; ?>

</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">

    <div class="width100 shipping-div2 margin-top15">

		<div class="overflow-scroll-div">

    <p class="white-text p-title"><b>Direct Bonus</b></p>

			<table class="table-css fix-th tablesorter smaller-font-table">
				<thead>
					<tr>
            <th class="th">No</th>
            <th class="th">Purchaser</th>
            <th class="th">Receiver</th>
            <th class="th">Amount</th>
            <th class="th">Bonus Type</th>
					</tr>
				</thead>
				<tbody>

					<?php
					if($directBonus)
					{
						for($cnt = 0;$cnt < count($directBonus) ;$cnt++)
						{
						?>
							<tr>
								<td><?php echo ($cnt+1)?></td>
								<td><?php echo $directBonus[$cnt]->getUsername();?></td>
								<td><?php echo $directBonus[$cnt]->getReceiver();?></td>
								<td><?php echo $directBonus[$cnt]->getAmount();?></td>
                <td><?php echo $directBonus[$cnt]->getBonusType();?></td>
							</tr>
						<?php
						}
						?>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>

    <div class="overflow-scroll-div">

<p class="white-text p-title"><b>Bonus 2 (Pool Fund)</b></p>

  <table class="table-css fix-th tablesorter smaller-font-table">
    <thead>
      <tr>
        <th class="th">No</th>
        <th class="th">Purchaser</th>
        <th class="th">Receiver (Ranking)</th>
        <th class="th">Amount</th>
      </tr>
    </thead>
    <tbody>

      <?php
      if($poolBonus)
      {
        for($cnt = 0;$cnt < count($poolBonus) ;$cnt++)
        {
        ?>
          <tr>
            <td><?php echo ($cnt+1)?></td>
            <td><?php echo $poolBonus[$cnt]->getUsername();?></td>
            <td><?php echo $poolBonus[$cnt]->getReceiver();?></td>
            <td><?php echo $poolBonus[$cnt]->getAmount();?></td>
          </tr>
        <?php
        }
        ?>
      <?php
      }
      ?>
    </tbody>
  </table>
</div>

	</div>

</div>

<?php include 'js.php'; ?>


<script src="js/headroom.js"></script>

<script>
  $(function(){
    $("#myTable").tablesorter( {dateFormat: 'pt'} );
  });
</script>
<?php if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Update User Profile.";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Successfully Change User Password.";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Successfully Update User Bank Details.";
        }
        else if($_GET['type'] == 12)
        {
            $messageType = "Fail to upload.";
        }
        else if($_GET['type'] == 13)
        {
            $messageType = "Fail to upload.";
        }
        else if($_GET['type'] == 14)
        {
            $messageType = "EROR.";
        }
        else if($_GET['type'] == 15)
        {
            $messageType = "EROR 2.";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong Current Password Entered !";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "Successfully Issue Payroll !";
        }
        // echo '<script>
        // $("#audio")[0].play();
        // putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        echo '<script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        unset($_SESSION['messageType']);
    }
}
?>

</body>
</html>