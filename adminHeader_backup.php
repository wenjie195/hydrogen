<div id="wrapper" class="toggled-2">
    <div id="sidebar-wrapper">
    <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
        <li>
            <a href="adminDashboard.php"><img src="img/logo.png" class="logo-img" alt="Logo" title="Logo"></a>
        </li>
        <li class="sidebar-li">
            
                <img src="img/daily2.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><a href="adminDailySpread.php" class="overflow"><?php echo _ADMINHEADER_DAILY_SPREAD ?></a></p>
            
        </li>
        <li class="sidebar-li">
            
                <img src="img/monthly.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><a href="viewLevel.php" class="overflow"><?php echo _ADMINHEADER_MONTHLY_PROFITS ?></a></p>
            
        </li>
        <li class="sidebar-li">
            
                <img src="img/user-profile.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><a href="adminAddUser.php" class="overflow"><?php echo _ADMINHEADER_ADD_USER ?></a></p>
            
        </li>  
        <li class="sidebar-li">
            
                <img src="img/group.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><a href="adminViewMember.php" class="overflow"><?php echo _ADMINHEADER_VIEW_MEMBER ?></a></p>
            
        </li>     
        <li class="sidebar-li">
            
                <img src="img/withdrawal.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><a href="adminWithdrawal.php" class="overflow"><?php echo _ADMINHEADER_MEMBER_WITHDRAW ?></a></p>
            
        </li>       
        <li class="sidebar-li">
            
                <img src="img/documents.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><a href="adminViewDaily.php" class="overflow"><?php echo _DAILY_MEMBER_DAILY_BONUS ?></a></p>
            
        </li>     
        <li class="sidebar-li">
            
                <img src="img/report.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><a href="adminViewMonthly.php" class="overflow"><?php echo _MONTHLY_MEMBER_DAILY_BONUS ?></a></p>
           
        </li>      

        <li class="sidebar-li">
            
                <img src="img/english.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><a href="<?php $link ?>?lang=en" class="overflow">EN</a></p>
            
        </li>    
        <li class="sidebar-li">
            
                <img src="img/chinese.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><a href="<?php $link ?>?lang=ch" class="overflow">中文</a></p>
            
        </li>      
        <li class="sidebar-li">
            
                <img src="img/exit.png" alt="" title="" class="sidebar-icon-img">
                
                <p class="sidebar-span white-text"><a href="logout.php" class="overflow"><?php echo _USERDASHBOARD_LOGOUT ?></a></p>
            
        </li>       
            
    </ul>
    </div>
</div>