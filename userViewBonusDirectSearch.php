<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Bonus.php';
require_once dirname(__FILE__) . '/classes/BonusOverriding.php';
require_once dirname(__FILE__) . '/classes/BonusPoolFund.php';
require_once dirname(__FILE__) . '/classes/BonusRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, " WHERE uid =? ", array("uid"), array($uid), "s");
$userData = $userDetails[0];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$fromDate = rewrite($_POST["fromDate"]);
	$endDate = rewrite($_POST["endDate"]);
  $newEndDate = date('Y-m-d', strtotime($endDate. ' + 1 days'));

  // $bonusOne = getBonus($conn, "WHERE receiver_uid = ? AND date_created >= '$fromDate' AND date_created <= '$endDate' ",array("receiver_uid"),array($uid),"s");
  $bonusOne = getBonus($conn, "WHERE receiver_uid = ? AND date_created >= '$fromDate' AND date_created <= '$newEndDate' ",array("receiver_uid"),array($uid),"s");

}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>

    <meta property="og:url" content="https://hygeniegroup.com/userViewBonus.php" />
    <link rel="canonical" href="https://hygeniegroup.com/userViewBonus.php" />
    <meta property="og:title" content="<?php echo _BONUS_DIRECT ?>  | Hygenie Group" />
    <title><?php echo _BONUS_DIRECT ?>  | Hygenie Group</title>

	<?php include 'css.php'; ?>
</head>
<style media="screen">
  .blue-button{
  font-size: 10px;
  width: auto;
  margin-top: 50px;
  padding: 5px;
  margin-left: -20px;
  }
  ul{
    list-style: none;
    list-style-image: url('img/li.png');
  }
  li{
    vertical-align: middle;
    cursor: pointer;
  }
</style>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height" id="firefly">
  <div class="width100 overflow text-center">
    <img src="img/hierachy.png" class="middle-title-icon" alt="<?php echo _BONUS_SYSTEM ?>" title="<?php echo _BONUS_SYSTEM ?>">
  </div>

  <div class="width100 overflow">
    <h1 class="small-h1-a text-center white-text"><?php echo _BONUS_DIRECT ?> | <a class="blue-link" href="userViewBonusPool.php"><?php echo _USERDASHBOARD_POOL_BONUS ?></a> | <a class="blue-link" href="userViewBonusOverriding.php"><?php echo _BONUS_OVERRIDING_BONUS ?></a> </h1>
  </div>

  <div class="overflow-scroll-div">
    <table class="table-css fix-th">
				<thead>
					<tr>
						<th class="th"><?php echo _ADMINVIEWBALANCE_NO ?></th>
						<th class="th"><?php echo _BONUS_PURCHASER ?></th>
						<th class="th"><?php echo _PRODUCT_AMOUNT ?></th>
						<th class="th"><?php echo _BONUS_TYPE ?></th>
            <th><?php echo _DAILY_DATE ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					if($bonusOne)
					{
						for($cnt = 0;$cnt < count($bonusOne) ;$cnt++)
						{
						?>
							<tr>
                <td><?php echo ($cnt+1)?></td>
                <td><?php echo $bonusOne[$cnt]->getUsername();?></td>
                <td><?php echo $bonusOne[$cnt]->getAmount();?></td>
                <td><?php echo $bonusOne[$cnt]->getBonusType();?></td>
                <td><?php echo $bonusOne[$cnt]->getDateCreated();?></td>
							</tr>
						<?php
						}
						?>
					<?php
					}
					?>
				</tbody>
			</table>
  </div>

  <div class="clear"></div>

</div>

<?php include 'js.php'; ?>

</body>
</html>