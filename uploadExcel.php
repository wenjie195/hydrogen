<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';
// require_once dirname(__FILE__) . '/classes/PoolFund.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

include 'selectFilecss.php';
// include 'js.php';

require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
  $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

  if(in_array($_FILES["file"]["type"],$allowedFileType))
  {
    $targetPath = 'uploadsExcel/'.$_FILES['file']['name'];
    move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
    $Reader = new SpreadsheetReader($targetPath);
    $sheetCount = count($Reader->sheets());
    for($i=0;$i<$sheetCount;$i++)
    {
      $Reader->ChangeSheet($i);
      foreach ($Reader as $Row)
      {

        $name = "";
        if(isset($Row[0])) 
        {
          $name = mysqli_real_escape_string($conn,$Row[0]);
        }
        $brand = "";
        if(isset($Row[0])) 
        {
          $brand = mysqli_real_escape_string($conn,$Row[1]);
        }
        // $productCode = "";
        // if(isset($Row[0])) 
        // {
        //   $productCode = mysqli_real_escape_string($conn,$Row[2]);
        // }
        // $quantity = "";
        // if(isset($Row[0])) 
        // {
        //   $quantity = mysqli_real_escape_string($conn,$Row[3]);
        // }
        // $partNumber = "";
        // if(isset($Row[0])) 
        // {
        //   $partNumber = mysqli_real_escape_string($conn,$Row[4]);
        // }

        //generate unique uid
        // $uid = md5(uniqid());

        // $status ="Pending";
        $status ="Available";

        if (!empty($name) || !empty($brand) )
        {
          // $query = "INSERT INTO stock (product_name,category) VALUES ('".$productName."','".$category."') ";
          $query = "INSERT INTO stock (name,brand,status) VALUES ('".$name."','".$brand."','".$status."') ";
          $result = mysqli_query($conn, $query);
          if (! empty($result))
          {
            // echo "<script>alert('Excel Uploaded !');window.location='../inventory/uploadExcel.php'</script>";         
            // echo "<script>alert('Excel Uploaded !');window.location='../testing/uploadExcel.php'</script>";   
            echo "<script>alert('Excel Uploaded !');window.location='../uploadExcel.php'</script>";   
          }
          else 
          {
            echo "<script>alert('Problem in Importing Excel Data !');window.location='../uploadExcel.php'</script>";
            // echo "<script>alert('Problem in Importing Excel Data !');window.location='../testing/uploadExcel.php'</script>";
          }
        }
      }
    }
  }
  else
  {
    echo "<script>alert('Invalid File Type. Upload Excel File.');window.location='../uploadExcel.php'</script>";
    // echo "<script>alert('Invalid File Type. Upload Excel File.');window.location='../testing/uploadExcel.php'</script>";
  }
}
?>

<!DOCTYPE html>
<html>
<head>
	<?php include 'meta.php'; ?>

	<meta property="og:url" content="https://hygeniegroup.com/uploadExcel.php" />
    <link rel="canonical" href="https://hygeniegroup.com/uploadExcel.php" /> 
    <meta property="og:title" content="<?php echo _PRODUCT_ADD_STOCK ?>  | Hygenie Group" />
    <title><?php echo _PRODUCT_ADD_STOCK ?>  | Hygenie Group</title>

    <link rel="stylesheet" href="css/font-awesome.min.css">
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php include 'header.php'; ?>
<!-- <div class="next-to-sidebar">  -->
<div class="width100 same-padding menu-distance darkbg min-height big-black-text text-center ow-white-css extra-margin-bottom" id="firefly">

<!-- <h1 class="h1-title white-text">Import Excel File</h1> -->
<h1 class="small-h1-a text-center white-text"><a class="blue-link" href="adminStockView.php"><?php echo _PRODUCT_ALL_STOCK ?></a> | <?php echo _PRODUCT_ADD_STOCK ?> (Import Excel)</h1>

  <div class="outer-container">
  <!-- <div class="five-box-size text-center white-div-css center-five-ow"> -->
    <form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
      <!--<label style="color:white !important;">Select File</label><br>--><input type="file" name="file" id="file" accept=".xls,.xlsx" style="color:white !important;"><div class="clear"></div>
      <!-- <button type="submit" id="submit" name="import"  class="clean red-btn margin-top30 fix300-btn margin-left0">Submit</button> -->
        <button class="clean blue-ow-btn distribute-btn" style="margin-top:20px;" type="submit" id="submit" name="import">
          <?php echo _PRODUCT_SUBMIT ?>
        </button>
      <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
    </form>
  </div>

</div>

<style>
  .import-li{
  color:#264a9c;
  background-color:white;}
  .import-li .hover1a{
  display:none;}
  .import-li .hover1b{
  display:block;}
</style>

<?php include 'js.php'; ?>
<script  src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>

<script>
  $(function() {
    $( '#dl-menu' ).dlmenu({
      animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
    });
  });
</script>

</body>
</html>