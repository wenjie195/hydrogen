<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE user_type = 1");
$paymentStatus = getOrders($conn, " WHERE payment_status = 'ACCEPTED' AND shipping_status = 'DELIVERED' ");

$conn->close();

?>

<!DOCTYPE html>
<html>
<head>

	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://hygeniegroup.com/adminOrderApproved.php" />
    <link rel="canonical" href="https://hygeniegroup.com/adminOrderApproved.php" />
    <meta property="og:title" content="<?php echo _PRODUCT_SHIPPED ?> | Hygenie Group" />
    <title><?php echo _PRODUCT_SHIPPED ?> | Hygenie Group</title>

	<?php include 'css.php'; ?>

</head>

<!-- <body> -->

<body class="body">
<?php include 'header.php'; ?>

<!-- <div class="demo"> -->

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">

<h1 class="small-h1-a text-center white-text"><a class="blue-link" href="adminOrderPaymentVerification.php"><?php echo _PRODUCT_PAYMENT_VERIFICATION ?></a> | <a class="blue-link" href="adminOrderPending.php"><?php echo _PRODUCT_PENDING ?></a> | <?php echo _PRODUCT_SHIPPED ?> | <a class="blue-link" href="adminOrderRejected.php"><?php echo _PRODUCT_REJECTED ?></a> </h1>

    <div class="width100 shipping-div2 margin-top15">

		<div class="search-big-div">
            <div class="fake-input-div overflow profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput" onkeyup="myFunction()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_USERNAME ?>" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3 mid-profile-h3 second-profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput2" onkeyup="myFunctionB()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_FULLNAME ?>" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3 second-profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput4" onkeyup="myFunctionD()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_PHONE ?>" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput5" onkeyup="myFunctionE()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_EMAIL ?>" class="clean pop-input fake-input">
            </div>

        </div>

		<div class="overflow-scroll-div">
			<table class="table-css fix-th tablesorter smaller-font-table" id="myTable">
				<thead>
					<tr>
						<!-- <img src="img/sort.png" class="sort-img"> -->
						<th class="th"><?php echo _ADMINVIEWBALANCE_NO ?> <img src="img/sort.png" class="sort-img"></th>
            <th class="th"><?php echo _PRODUCT_ORDER_ID ?> <img src="img/sort.png" class="sort-img"></th>
            <th class="th"><?php echo _MAINJS_INDEX_USERNAME ?> <img src="img/sort.png" class="sort-img"></th>
            <th class="th"><?php echo _JS_PHONE ?> <img src="img/sort.png" class="sort-img"></th>
            <th class="th"><?php echo _PRODUCT_AMOUNT ?> <img src="img/sort.png" class="sort-img"></th>
            <th class="th"><?php echo _TRANSACTION_ID ?> <img src="img/sort.png" class="sort-img"></th>
            <th class="th"><?php echo _MULTIBANK_DETAILS ?> <img src="img/sort.png" class="sort-img"></th>
					</tr>
				</thead>
				<tbody id="myFilter">

					<?php
					if($paymentStatus)
					{
						for($cnt = 0;$cnt < count($paymentStatus) ;$cnt++)
						{
						?>
							<tr>
								<td><?php echo ($cnt+1)?></td>
								<td><?php echo $paymentStatus[$cnt]->getOrderId();?></td>
                <td><?php echo $paymentStatus[$cnt]->getName();?></td>
								<td><?php echo $paymentStatus[$cnt]->getContactNo();?></td>
								<td><?php echo $paymentStatus[$cnt]->getSubtotal();?></td>
                <td><?php echo $paymentStatus[$cnt]->getPaymentBankReference();?></td>

                <td>
                  <form action="adminOrderDetailsSR.php" method="POST">
                      <button class="clean blue-ow-btn" type="submit" name="order_uid" value="<?php echo $paymentStatus[$cnt]->getOrderId();?>">
                          <?php echo _MULTIBANK_VIEW ?>
                      </button>
                  </form>
                </td>

							</tr>
						<?php
						}
						?>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- <div class="width100 same-padding footer-div">
	<p class="footer-p white-text"><?php //echo _JS_FOOTER ?></p>
</div> -->
<?php include 'js.php'; ?>
<!-- </div> -->

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput2");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>


<script>
function myFunctionD() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput4");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionE() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput5");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[4];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script src="js/headroom.js"></script>

<script>
  $(function(){
    $("#myTable").tablesorter( {dateFormat: 'pt'} );
  });
</script>
<?php if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Update User Profile.";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Successfully Change User Password.";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Successfully Update User Bank Details.";
        }
        else if($_GET['type'] == 12)
        {
            $messageType = "Fail to upload.";
        }
        else if($_GET['type'] == 13)
        {
            $messageType = "Fail to upload.";
        }
        else if($_GET['type'] == 14)
        {
            $messageType = "EROR.";
        }
        else if($_GET['type'] == 15)
        {
            $messageType = "EROR 2.";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong Current Password Entered !";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "Successfully Issue Payroll !";
        }
        // echo '<script>
        // $("#audio")[0].play();
        // putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        echo '<script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");</script>';
        unset($_SESSION['messageType']);
    }
}
?>

</body>
</html>