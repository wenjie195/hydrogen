<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $currentStatus = 'Manager';
$currentStatus = 'Member';
// $uid = $_SESSION['uid'];
$allUser = getUser($conn, "WHERE rank = ? ", array("rank"), array($currentStatus), "s");
// $allUser = getUser($conn);

if ($allUser) 
{
  for ($cnt=0; $cnt < count($allUser) ; $cnt++)
  {
    $uid = $allUser[$cnt]->getUid();
    // echo "<br>";
    // $getDownline = getReferralHistory($conn, "WHERE referrer_id = ? ", array("referrer_id"), array($uid), "s");
    // $getDownline = getReferralHistory($conn, "WHERE referrer_id = ? AND current_status = 'Manager' ", array("referrer_id"), array($uid), "s");
    $getDownline = getReferralHistory($conn, "WHERE referrer_id = ? AND current_status = 'Member' AND order_status = 'YES' ", array("referrer_id"), array($uid), "s");
    if ($getDownline)
    {

      $totalManager = count($getDownline);

      // if($totalManager >= 3)
      if($totalManager >= 7)
      {   
        // echo "promote to manager";
        $newStatus = 'Manager';

        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($newStatus)
        {
            array_push($tableName,"rank");
            array_push($tableValue,$newStatus);
            $stringType .=  "s";
        }    
        array_push($tableValue,$uid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
          // echo "status updated !!";

          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
  
          if($newStatus)
          {
              array_push($tableName,"current_status");
              array_push($tableValue,$newStatus);
              $stringType .=  "s";
          }    
          array_push($tableValue,$uid);
          $stringType .=  "s";
          $passwordUpdated = updateDynamicData($conn,"referral_history"," WHERE referral_id = ? ",$tableName,$tableValue,$stringType);
          if($passwordUpdated)
          {
            echo "status updated !!";
          }
          else
          {
            // echo "fail 1";
          }

        }
        else
        {
          // echo "fail 2";
        }

      }
      elseif($totalManager < 3)
      {  
        // echo "status remain";
      }

    }
  }
}
?>