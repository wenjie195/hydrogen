<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/Stock.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE user_type = 1");
$paymentStatus = getOrders($conn, " WHERE payment_status = 'APPROVED' ");

$conn->close();

?>

<!DOCTYPE html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://hygeniegroup.com/adminSalesDetails.php" />
<link rel="canonical" href="https://hygeniegroup.com/adminSalesDetails.php" />
<meta property="og:title" content="<?php echo _PRODUCT_ORDER_DETAILS ?> | Hygenie Group" />
<title><?php echo _PRODUCT_ORDER_DETAILS ?> | Hygenie Group</title>
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<!-- <div class="demo"> -->

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">
  <div class="width100 overflow">
    <h1 class="text-center pop-h1"><?php echo _PRODUCT_ORDER_DETAILS ?></h1>
  </div>

    <?php
    if(isset($_POST['order_uid']))
    {
      $conn = connDB();
      $ordersDetails = getOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_uid']),"s");
      $orderId = $ordersDetails[0]->getId();

      $productOrder = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($orderId),"s");

      $stockList = getStock($conn,"WHERE order_uid = ? ", array("order_uid") ,array($_POST['order_uid']),"s");
      ?>

        <div class="dual-input">
          <p class="input-top-text"><?php echo _PRODUCT_ORDER_ID ?>: <b>#<?php echo $orderId;?></b></p>
        </div>

        <div class="dual-input second-dual-input">
          <p class="input-top-text"><?php echo _PRODUCT_PURCHASER_NAME ?>: <b><?php echo $ordersDetails[0]->getName();?></b></p>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
          <p class="input-top-text"><?php echo _INDEX_MOBILE_NO ?>: <b><?php echo $ordersDetails[0]->getContactNo();?></b></p>
        </div>

        <div class="dual-input second-dual-input">
          <p class="input-top-text"><?php echo _INDEX_ADDRESS ?>: <b><?php echo $ordersDetails[0]->getAddressLine1();?></b></p>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
          <p class="input-top-text"><?php echo _PRODUCT_PAYMENT_REFERENCE ?>: <b><?php echo $ordersDetails[0]->getPaymentBankReference();?></b></p>
        </div>

        <div class="dual-input second-dual-input">
          <p class="input-top-text"><?php echo _PRODUCT_ORDER_DATE ?>: <b><?php echo $ordersDetails[0]->getDateCreated();?></b></p>
        </div>

        <div class="clear"></div>

        <!-- <div class="width100 border-separation margin-bottom30">
            <table class="green-table width100"> -->
        <div class="overflow-scroll-div">
			      <table class="table-css fix-th tablesorter smaller-font-table">
                <thead>
                    <tr>
                        <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th><?php echo _PRODUCT_ITEM ?></th>
                        <th><?php echo _PRODUCT_QUANTITY ?></th>
                        <th><?php echo _PRODUCT_TOTAL_PRICE ?></th>
                        <th>PV</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($productOrder)
                        {
                            
                            for($cnt = 0;$cnt < count($productOrder) ;$cnt++)
                            {
                            ?>
                                
                                <tr class="link-to-details">
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $productOrder[$cnt]->getProductName();?></td>
                                    <td><?php echo $productOrder[$cnt]->getQuantity();?></td>
                                    <td>RM<?php echo $productOrder[$cnt]->getTotalProductPrice();?></td>
                                    <td><?php echo $productOrder[$cnt]->getTotalCommission();?></td>
                                </tr>
                            <?php
                            }
                        }
                    ?>                                 
                </tbody>
            </table>
        </div>

        <div class="clear"></div>

        <div class="overflow-scroll-div">
			      <table class="table-css fix-th tablesorter smaller-font-table">
                <thead>
                    <tr>
                        <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th><?php echo _PRODUCT_ITEM ?></th>
                        <th><?php echo _PRODUCT_SERIAL_NUMBER ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($stockList)
                        {
                            
                            for($cnt = 0;$cnt < count($stockList) ;$cnt++)
                            {
                            ?>
                                
                                <tr class="link-to-details">
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $stockList[$cnt]->getBrand();?></td>
                                    <td><?php echo $stockList[$cnt]->getName();?></td>
                                </tr>
                            <?php
                            }
                        }
                    ?>                                 
                </tbody>
            </table>
        </div>

        <div class="clear"></div>

        <?php 
          $shippingStatus = $ordersDetails[0]->getShippingStatus();
          if($shippingStatus == 'DELIVERED')
          {
          ?>
              <div class="dual-input">
                <p class="input-top-p admin-top-p"><?php echo _PRODUCT_SHIPPING_STATUS ?>: <?php echo $shippingStatus; ?></p>
              </div>    

              <div class="clear"></div>

              <div class="dual-input">
                <p class="input-top-p admin-top-p"><?php echo _PRODUCT_SHIPPING_METHOD ?>: <?php echo $ordersDetails[0]->getShippingMethod();?></p>
              </div>        
              
              <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p"><?php echo _PRODUCT_TRACKING_NUMBER ?>: <?php echo $ordersDetails[0]->getTrackingNumber();?></p>
              </div>
          <?php
          }
          elseif($shippingStatus == 'REJECTED')
          {
          ?>
            <div class="dual-input">
              <p class="input-top-p admin-top-p"><?php echo _PRODUCT_SHIPPING_STATUS ?>: <?php echo $shippingStatus; ?></p>
            </div>    
          <?php
          }
        ?>

        <div class="clear"></div> 
      
      <?php
    }
    ?>

</div>
<style>
input:-webkit-autofill,
input:-webkit-autofill:hover, 
input:-webkit-autofill:focus,
textarea:-webkit-autofill,
textarea:-webkit-autofill:hover,
textarea:-webkit-autofill:focus,
select:-webkit-autofill,
select:-webkit-autofill:hover,
select:-webkit-autofill:focus {

  -webkit-text-fill-color:white !important;

}

</style>
<?php include 'js.php'; ?>

</body>
</html>