<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $referrerUID = $_SESSION['uid'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://hygeniegroup.com/" />
    <link rel="canonical" href="https://hygeniegroup.com/" />
    <meta property="og:title" content="氢爱天下 Hygenie Group" />
    <title>氢爱天下 Hygenie Group</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">
<style>
.header1, .footer-div, .ow-blue-button1{
background: #0BEEF9;
background: -moz-linear-gradient(left, #0BEEF9 0%, #48A9FE 100%);
background: -webkit-gradient(left top, right top, color-stop(0%, #0BEEF9), color-stop(100%, #48A9FE));
background: -webkit-linear-gradient(left, #0BEEF9 0%, #48A9FE 100%);
background: -o-linear-gradient(left, #0BEEF9 0%, #48A9FE 100%);
background: -ms-linear-gradient(left, #0BEEF9 0%, #48A9FE 100%);
background: linear-gradient(to right, #0BEEF9 0%, #48A9FE 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0BEEF9', endColorstr='#48A9FE', GradientType=1 );	
	}
 .ow-blue-button1{	
    background: #ffffff40;}
.ow-blue-button1:hover{
    background: #ffffff60;	
}
::placeholder {
	color:white !Important;
}
</style>
<?php include 'header.php'; ?>
<div class="width100 home-bg overflow home-padding-top index-padding">
    <p class="logo-p wow fadeIn" data-wow-delay="0.3s"><img src="img/logo4.png" alt="氢爱天下 Hygenie Group" title="氢爱天下 Hygenie Group" class="home-logo"></p>
    <h2 class="black-text2 home-h1 wow fadeIn" data-wow-delay="0.6s"><?php echo _HOME_LEADING ?></h2>
    <p class="black-text2 banner-p wow fadeIn" data-wow-delay="0.9s"><?php echo _HOME_HQ ?></p>
</div>
<div class="width100 bottom-sea-bg overflow"></div>
<div class="width100 waterfall-bg overflow index-padding">
	<h2 class="index-title-h2 black-text2 text-center wow fadeIn" data-wow-delay="1.2s"><?php echo _HOME_WATER_MIRALCE ?></h2>
	<p class="text-center waterfall-p black-text2 wow fadeIn" data-wow-delay="1.5s"><?php echo _HOME_FOUR_COUNTRY ?><a href="https://en.wikipedia.org/wiki/Lourdes_water
" target="_blank" class="blue-link3"><?php echo _HOME_FOUR_COUNTRY1 ?></a><a href="https://www.washingtonpost.com/archive/politics/1992/01/27/foreign-journal/9b0916e8-6c35-4470-8c00-562ecd1d2d87/" target="_blank" class="blue-link3"><?php echo _HOME_FOUR_COUNTRY2 ?></a><a href="https://themiraclespage.info/waters/heilwasser.htm" target="_blank" class="blue-link3"><?php echo _HOME_FOUR_COUNTRY3 ?></a><a href="https://happybodyformula.com/all-about-hunza-water/" target="_blank" class="blue-link3"><?php echo _HOME_FOUR_COUNTRY4 ?></a><?php echo _HOME_FOUR_COUNTRY5 ?></p>
</div>
<div class="width100 bottom-waterfall-bg overflow"></div>
<div class="width100 index-padding padding-top-bottom">
    <div class="text-center">
        <img src="img/virus1.png" class="center-virus wow fadeIn" data-wow-delay="0.3s" alt="<?php echo _HOME_THE_HARM ?>" title="<?php echo _HOME_THE_HARM ?>">
        <h2 class="pink-text harm-h2 wow fadeIn" data-wow-delay="0.6s"><?php echo _HOME_THE_HARM ?></h2>
        <p class="harm-p h-blue-text wow fadeIn" data-wow-delay="0.9s"><?php echo _HOME_HARM_P ?></p>
    </div>
    <div class="width103 overflow">
    	<div class="four-div-css">
        	<img src="img/virus2.png" class="virus2 wow fadeIn" data-wow-delay="1.2s" >
            <p class="four-div-css-p black-text2 wow fadeIn" data-wow-delay="1.5s"><?php echo _HOME_V1 ?></p>
        </div>
    	<div class="four-div-css">
        	<img src="img/virus3.png" class="virus2 wow fadeIn" data-wow-delay="1.8s" >
            <p class="four-div-css-p black-text2 wow fadeIn" data-wow-delay="2.1s"><?php echo _HOME_V2 ?></p>
        </div>        
    	<div class="four-div-css">
        	<img src="img/virus4.png" class="virus2 wow fadeIn" data-wow-delay="2.4s" >
            <p class="four-div-css-p black-text2 wow fadeIn" data-wow-delay="2.7s"><?php echo _HOME_V3 ?></p>
        </div>        
    	<div class="four-div-css">
        	<img src="img/virus5.png" class="virus2 wow fadeIn" data-wow-delay="3s" >
            <p class="four-div-css-p black-text2 wow fadeIn" data-wow-delay="3.3s"><?php echo _HOME_V4 ?></p>
        </div>        
    </div>
</div>
<div class="clear"></div>
<div class="width100 index-padding padding-top-bottom sap-bg">
	<div class="text-center">
		<img src="img/h2o.png" class="h2o wow fadeIn" data-wow-delay="0.3s">
        <h2 class="white-text harm-h2 wow fadeIn" data-wow-delay="0.6s"><?php echo _HOME_WHAT_IS ?></h2>
        <p class="harm-p white-text margin-bottom0 wow fadeIn" data-wow-delay="0.9s"><?php echo _HOME_WHAT_IS_P ?><a href="https://wiki.antpedia.com/qingyanghunhexirukezhiliaoxinguanfeiyanzhuanjiaxuyaogengduoyanjinzhengju-2364066-news" target="_blank" class="opacity-hover white-text"><u class="white-text"><?php echo _HOME_WHAT_IS_A ?></u></a></p>        
    </div>
</div>
<div class="clear"></div>
<div class="width100 index-padding padding-top-bottom overflow">
	<h2 class="dark-tur-text harm-h2 text-center ow-margin-title wow fadeIn" data-wow-delay="0.3s"><?php echo _HOME_OUR_PRODUCT ?></h2>
    <div class="clear"></div>
    <div class="four-pro text-center open-pro1">
    	<img src="img/product1.png" class="uni-jpg wow fadeIn" data-wow-delay="0.6s">
    </div>    
    <div class="four-pro text-center open-pro2">
    	<img src="img/product2.png" class="uni-jpg wow fadeIn" data-wow-delay="0.9s">
    </div>      
    <div class="four-pro text-center open-pro3">
    	<img src="img/product3.png" class="uni-jpg wow fadeIn" data-wow-delay="1.2s">
    </div>      
    <div class="four-pro text-center open-pro4">
    	<img src="img/product4.png" class="uni-jpg wow fadeIn" data-wow-delay="1.5s">
    </div> 
    <div class="four-pro text-center open-pro5">
    	<img src="img/product5.png" class="uni-jpg wow fadeIn" data-wow-delay="1.8s">
    </div>    
    <div class="four-pro text-center open-pro6">
    	<img src="img/product6.png" class="uni-jpg wow fadeIn" data-wow-delay="2.1s">
    </div>      
    <div class="four-pro text-center open-pro7">
    	<img src="img/product7.png" class="uni-jpg wow fadeIn" data-wow-delay="2.4s">
    </div>      
    <div class="four-pro text-center open-pro8">
    	<img src="img/product8.png" class="uni-jpg wow fadeIn" data-wow-delay="2.7s">
    </div>    
     <div class="four-pro text-center open-pro9">
    	<img src="img/product9.png" class="uni-jpg wow fadeIn" data-wow-delay="3s">
    </div>    
    <div class="four-pro text-center open-pro10">
    	<img src="img/product10.png" class="uni-jpg wow fadeIn" data-wow-delay="3.3s">
    </div>      
    <div class="four-pro text-center open-pro11">
    	<img src="img/product11.png" class="uni-jpg wow fadeIn" data-wow-delay="3.6s">
    </div>      
    <div class="four-pro text-center open-pro12">
    	<img src="img/product12.png" class="uni-jpg wow fadeIn" data-wow-delay="3.9s">
    </div>  
    <div class="four-pro text-center open-pro13">
    	<img src="img/product13.png" class="uni-jpg wow fadeIn" data-wow-delay="4.2s">
    </div>     
       
</div>
<div class="clear"></div>
<div class="width100 index-padding padding-top-bottom overflow">
	<h2 class="dark-tur-text harm-h2 text-center ow-margin-title wow fadeIn" data-wow-delay="0.3s"><?php echo _HOME_UNI ?></h2>
    <div class="clear"></div>
    <div class="three-css text-center">
    	<a href="./img/school1.jpg" data-fancybox="images-preview" ><img src="img/school1.jpg" class="school-jpg wow fadeIn" data-wow-delay="0.6s"></a>
    </div>
     <div class="three-css mid-three-css text-center">
    	<a href="./img/school2.jpg" data-fancybox="images-preview" ><img src="img/school2.jpg" class="school-jpg wow fadeIn" data-wow-delay="0.9s"></a>
    </div>
    <div class="three-css text-center">
    	<a href="./img/school3.jpg" data-fancybox="images-preview" ><img src="img/school3.jpg" class="school-jpg wow fadeIn" data-wow-delay="1.2s"></a>
    </div>  
    <div class="clear"></div>
    <div class="four-uni text-center">
    	<a href="./img/school4.jpg" data-fancybox="images-preview" ><img src="img/school4.jpg" class="uni-jpg wow fadeIn" data-wow-delay="1.5s"></a>
    </div>    
    <div class="four-uni left-mid-four text-center">
    	<a href="./img/school5.jpg" data-fancybox="images-preview" ><img src="img/school5.jpg" class="uni-jpg wow fadeIn" data-wow-delay="1.8s"></a>
    </div>      
    <div class="four-uni right-mid-four text-center">
    	<a href="./img/school6.jpg" data-fancybox="images-preview" ><img src="img/school6.jpg" class="uni-jpg wow fadeIn" data-wow-delay="2.1s"></a>
    </div>      
    <div class="four-uni text-center">
    	<a href="./img/school7.jpg" data-fancybox="images-preview" ><img src="img/school7.jpg" class="uni-jpg wow fadeIn" data-wow-delay="2.4s"></a>
    </div>     
     
</div>
<div class="clear"></div>
<div class="width100 index-padding padding-top-bottom sap-bg">
	<div class="text-center">
		<img src="img/white-logo.png" class="h2o-logo wow fadeIn" data-wow-delay="0.3s" alt="氢爱天下 Hygenie Group" title="氢爱天下 Hygenie Group">
       
        <p class="harm-p white-text margin-bottom0 wow fadeIn" data-wow-delay="0.6s"><?php echo _HOME_COMPANY1 ?><br><br><?php echo _HOME_COMPANY2 ?></p>        
    </div>
</div>
<div class="clear"></div>
<div class="width100 index-padding padding-top-bottom">
	<h2 class="dark-tur-text harm-h2 text-center ow-margin-title wow fadeIn" data-wow-delay="0.3s"><?php echo _HOME_CORE_TEAM ?></h2>
    <div class="overflow pointer"  onclick="showDiv()">
        <div class="four-profile-div">
            <img src="img/photo1.jpg" class="width100 wow fadeIn" data-wow-delay="0.6s" alt="李士刚董事长" title="李士刚董事长">
            <div class="blue-bg1 name-div white-text text-center wow fadeIn" data-wow-delay="0.9s">李士刚董事长</div>
            <div class="coreli" style="display:none;">
            	<ul>
                	<?php echo _HOME_CORE1 ?>

                    
                </ul>
            </div>
        </div>
        <div class="four-profile-div">
            <img src="img/photo2.jpg" class="width100 wow fadeIn" data-wow-delay="1.2s" alt="戴伟副董事长" title="戴伟副董事长">
            <div class="blue-bg2 name-div white-text text-center wow fadeIn" data-wow-delay="1.5s">戴伟副董事长</div>
             <div class="coreli" style="display:none;">
            	<ul>
                	<?php echo _HOME_CORE2 ?>

                    
                </ul>
            </div>           
        </div>   
        <div class="tempo-dual-clear"></div> 
        <div class="four-profile-div">
            <img src="img/photo3.jpg" class="width100 wow fadeIn" data-wow-delay="1.8s" alt="余威CEO" title="余威CEO">
            <div class="blue-bg1 name-div white-text text-center wow fadeIn" data-wow-delay="2.1s">余威CEO</div>
             <div class="coreli" style="display:none;">
            	<ul>
                	<?php echo _HOME_CORE3 ?>

                    
                </ul>
            </div>               
        </div>    
        <div class="four-profile-div">
            <img src="img/photo4a.jpg" class="width100 wow fadeIn" data-wow-delay="2.4s" alt="严宏研发副总裁" title="严宏研发副总裁">
            <div class="blue-bg2 name-div white-text text-center wow fadeIn" data-wow-delay="2.7s">严宏研发副总裁</div>
             <div class="coreli" style="display:none;">
            	<ul>
                	<?php echo _HOME_CORE4 ?>

                    
                </ul>
            </div>               
        </div>    
    </div>
    
</div>
<div class="clear"></div>
<div class="width100 index-padding padding-top-bottom">
	<h2 class="dark-tur-text harm-h2 text-center ow-margin-title wow fadeIn" data-wow-delay="0.3s"><?php echo _HOME_CONSULTANTS ?></h2>
    <div class="overflow pointer"  onclick="showDiv1()">
        <div class="three-profile-div">
            <img src="img/photo5.jpg" class="width100 wow fadeIn" data-wow-delay="0.6s" alt="太田成男" title="太田成男">
            <div class="blue-bg1 name-div white-text text-center wow fadeIn" data-wow-delay="0.9s">太田成男</div>
            <div class="coreli1" style="display:none;">
            	<ul>
                	<?php echo _HOME_CON1 ?>

                    
                </ul>
                <p class="source-p"><i><a href="http://blog.sciencenet.cn/blog-41174-713480.html#:~:text=%E5%A4%AA%E7%94%B0%E6%88%90%E7%94%B7%EF%BC%88Shigeo%20Ohta,1994%E5%B9%B4%E8%B5%B7%E4%BB%BB%E7%8E%B0%E8%81%8C%E3%80%82
" target="_blank" class="blue-link3"><?php echo _HOME_SOURCE ?></a></i></p>
            </div>
        </div>
        <div class="three-profile-div">
            <img src="img/photo6.jpg" class="width100 wow fadeIn" data-wow-delay="1.2s" alt="Tyler Lebaron" title="Tyler Lebaron">
            <div class="blue-bg2 name-div white-text text-center wow fadeIn" data-wow-delay="1.5s">Tyler Lebaron</div>
             <div class="coreli1" style="display:none;">
            	<ul>
                	<?php echo _HOME_CON2 ?>

                    
                </ul>
                <p class="source-p"><i><a href="https://www.molecularhydrogeninstitute.com/tyler-lebaron
" target="_blank" class="blue-link3"><?php echo _HOME_SOURCE ?></a></i></p>
            </div>           
        </div>   
        <div class="tempo-dual-clear3"></div> 
        <div class="three-profile-div">
            <img src="img/photo7.jpg" class="width100 wow fadeIn" data-wow-delay="1.8s" alt="孙学军" title="孙学军">
            <div class="blue-bg1 name-div white-text text-center wow fadeIn" data-wow-delay="2.1s">孙学军</div>
             <div class="coreli1" style="display:none;">
            	<ul>
                	<?php echo _HOME_CON3 ?>

                    
                </ul>
                <p class="source-p"><i><a href="https://baike.baidu.com/item/%E5%AD%99%E5%AD%A6%E5%86%9B/8431176#:~:text=%E5%AD%99%E5%AD%A6%E5%86%9B%EF%BC%8C%E7%94%B7%EF%BC%8C1970%E5%B9%B4%E7%94%9F%EF%BC%8C,%E5%81%A5%E5%BA%B7%E7%A7%91%E6%99%AE%E4%B8%93%E5%AE%B6%E5%BA%93%E5%90%8D%E5%8D%95%E3%80%82
" target="_blank" class="blue-link3"><?php echo _HOME_SOURCE ?></a></i></p>
            </div>               
        </div>        
    </div>
</div>
<div class="clear"></div>
<div class="width100 index-padding padding-top-bottom sap-bg">
	<h2 class="white-text harm-h2 text-center wow fadeIn" data-wow-delay="0.3s"><?php echo _HOME_DEV ?></h2>
	<div class=" follow-harm-width margin-top50">
	<div class="timeline-item wow fadeIn" data-wow-delay="0.6s" date-is='2020'>
		
		<p><?php echo _HOME_TIMELINE1 ?></p>
	</div>
	
	<div class="timeline-item wow fadeIn" data-wow-delay="0.9s" date-is='2019'>
		
		<p><?php echo _HOME_TIMELINE2 ?></p>
	</div>
	<div class="timeline-item wow fadeIn" data-wow-delay="1.2s" date-is='2018'>
		
		<p><?php echo _HOME_TIMELINE3 ?></p>
	</div>	
	<div class="timeline-item wow fadeIn" data-wow-delay="1.5s" date-is='2017'>
		
		<p><?php echo _HOME_TIMELINE4 ?></p>
	</div>     
    </div>
</div>
<div class="clear"></div>
<div class="width100 index-padding padding-top-bottom">
	<h2 class="pink-text harm-h2 text-center margin-bottom50 wow fadeIn" data-wow-delay="0.3s"><?php echo _HOME_OUR_MISSION_AND ?></h2>
	<div class="width105">
    	<div class="h-three-div">
        	<img src="img/icon1.png" class="icon-czz wow fadeIn" data-wow-delay="0.6s" alt="<?php echo _HOME_MISSION1 ?>" title="<?php echo _HOME_MISSION1 ?>">
            <p class="icon-title h-blue-text wow fadeIn" data-wow-delay="0.9s"><?php echo _HOME_MISSION1 ?></p>
            <p class="icon-p black-text2 wow fadeIn" data-wow-delay="1.2s"><?php echo _HOME_MISSION_DESC ?></p>
        </div>
    	<div class="h-three-div">
        	<img src="img/icon2.png" class="icon-czz wow fadeIn" data-wow-delay="1.5s" alt="<?php echo _HOME_VISION1 ?>" title="<?php echo _HOME_VISION1 ?>">
            <p class="icon-title h-blue-text wow fadeIn" data-wow-delay="1.8s"><?php echo _HOME_VISION1 ?></p>
            <p class="icon-p black-text2 wow fadeIn" data-wow-delay="2.1s"><?php echo _HOME_VISION_DESC ?></p>
        </div>        
    	<div class="h-three-div">
        	<img src="img/icon3.png" class="icon-czz wow fadeIn" data-wow-delay="2.4s" alt="<?php echo _HOME_IDEA1 ?>" title="<?php echo _HOME_IDEA1 ?>">
            <p class="icon-title h-blue-text wow fadeIn" data-wow-delay="2.7s"><?php echo _HOME_IDEA1 ?></p>
            <p class="icon-p black-text2 wow fadeIn" data-wow-delay="3s"><?php echo _HOME_IDEA_DESC ?></p>
        </div>        
     	<div class="h-three-div">
        	<img src="img/icon4.png" class="icon-czz wow fadeIn" data-wow-delay="3.3s" alt="<?php echo _HOME_HYDROGEN ?>" title="<?php echo _HOME_HYDROGEN ?>">
            <p class="icon-title h-blue-text wow fadeIn" data-wow-delay="3.6s"><?php echo _HOME_HYDROGEN ?></p>
            <p class="icon-p black-text2 wow fadeIn" data-wow-delay="3.9s"><?php echo _HOME_HYDROGEN_DESC ?></p>
        </div>       
    	<div class="h-three-div">
        	<img src="img/icon5.png" class="icon-czz wow fadeIn" data-wow-delay="4.2s" alt="<?php echo _HOME_LOVE ?>" title="<?php echo _HOME_LOVE ?>">
            <p class="icon-title h-blue-text wow fadeIn" data-wow-delay="4.5s"><?php echo _HOME_LOVE ?></p>
            <p class="icon-p black-text2 wow fadeIn" data-wow-delay="4.8s"><?php echo _HOME_LOVE_DESC ?></p>
        </div>        
    	<div class="h-three-div">
        	<img src="img/icon6.png" class="icon-czz wow fadeIn" data-wow-delay="5.1s" alt="<?php echo _HOME_WORLD ?>" title="<?php echo _HOME_WORLD ?>">
            <p class="icon-title h-blue-text wow fadeIn" data-wow-delay="5.4s"><?php echo _HOME_WORLD ?></p>
            <p class="icon-p black-text2 wow fadeIn" data-wow-delay="5.7s"><?php echo _HOME_WORLD_DESC ?></p>
        </div>          
        
    </div>
</div>
<div class="clear"></div>
<div class="width100 index-padding footer-bg">
	<div class="text-center">
        <h2 class="black-text2 harm-h2 prevent-h2 wow fadeIn" data-wow-delay="0.3s"><?php echo _HOME_PREVENTION ?></h2>       
    </div>
</div>

<?php include 'js.php'; ?>



<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Incorrect Password"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Please Register";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Password Changed Successfully";
        }
        echo '
        <script>
            putNoticeJavascript("Notice ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>