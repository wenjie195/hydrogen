<?php
if(isset($_SESSION['uid']))
{
    ?>
    <?php
    if($_SESSION['usertype_level'] == 0)
    //admin
    {
    ?>

        <div id="wrapper" class="toggled-2">
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
                    <li>
                        <a href="adminDashboard.php"><img src="img/logo4.png" class="logo-img" alt="Logo" title="Logo"></a>
                    </li>
                    <li class="sidebar-li">
                        <img src="img/group.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="adminViewMember.php" class="overflow"><?php echo _ADMINHEADER_VIEW_MEMBER ?></a></p>
                    </li>     
                    <li class="sidebar-li">
                        <img src="img/withdrawal.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="adminWithdrawal.php" class="overflow"><?php echo _ADMINHEADER_MEMBER_WITHDRAW ?></a></p>
                    </li>       
                    <li class="sidebar-li">
                        <img src="img/product.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="adminProductAll.php" class="overflow"><?php echo _PRODUCT ?></a></p>
                    </li>   
                    <li class="sidebar-li">
                        <img src="img/documents.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="adminStockView.php" class="overflow"><?php echo _PRODUCT_STOCK_SALES ?></a></p>
                    </li>     
                    <li class="sidebar-li">
                        <img src="img/sales.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="adminSalesView.php" class="overflow"><?php echo _PRODUCT_SALES_PAGE ?></a></p>
                    </li> 
                    <li class="sidebar-li">
                        <img src="img/report.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="adminOrderPaymentVerification.php" class="overflow"><?php echo _PRODUCT_ORDER ?></a></p>
                    </li>      
                    <li class="sidebar-li">
                        <img src="img/capping2.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="adminViewBonusDirect.php" class="overflow"><?php echo _BONUS_DIST ?></a></p>
                    </li>    
                    <li class="sidebar-li">
                        <img src="img/english.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="<?php $link ?>?lang=en" class="overflow">EN</a></p>
                    </li>    
                    <li class="sidebar-li">
                        <img src="img/chinese.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="<?php $link ?>?lang=ch" class="overflow">中文</a></p>
                    </li>      
                    <li class="sidebar-li">
                        <img src="img/exit.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="logout.php" class="overflow"><?php echo _USERDASHBOARD_LOGOUT ?></a></p>
                    </li>       
                </ul>
            </div>
        </div>

    <?php
    }
    elseif($_SESSION['usertype_level'] == 1)
    //user
    {
    ?>

        <div id="wrapper" class="toggled-2">
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
                    <li>
                        <a href="userDashboard.php"><img src="img/logo.png" class="logo-img" alt="Logo" title="Logo"></a>
                    </li>
                    <li class="sidebar-li">
                        <img src="img/user-profile.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="profile.php" class="overflow"><?php echo _USERHEADER_PROFILE ?></a></p>
                    </li>
                    <li class="sidebar-li">
                        <img src="img/bank-details.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="editBankDetails.php" class="overflow"><?php echo _BANKDETAILS ?></a></p>
                    </li>
                    <li class="sidebar-li">
                        <img src="img/product.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="product.php" class="overflow"><?php echo _USERDASHBOARD_PRODUCT_ORDER ?></a></p>
                    </li>
                    <li class="sidebar-li">
                        <img src="img/report.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="orderHistory.php" class="overflow"><?php echo _USERDASHBOARD_ORDER_HISTORY ?></a></p>
                    </li>
                    <li class="sidebar-li">
                        <img src="img/link.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="refer.php" class="overflow"><?php echo _VICTORY_REFERRAL_LINK ?></a></p>
                    </li>
                    <li class="sidebar-li">
                        <img src="img/group.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="hierarchy.php" class="overflow"><?php echo _USERHEADER_HIERARCHY ?></a></p>
                    </li>
                    <li class="sidebar-li">
                        <img src="img/monthly.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="userViewBonusDirect.php" class="overflow"><?php echo _USERHEADER_BONUS_REPORT ?></a></p>
                    </li>
                    <li class="sidebar-li">
                        <img src="img/withdrawal.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="withdrawalHistory.php" class="overflow"><?php echo _USERHEADER_WITHDRAWAL_HISTORY ?></a></p>
                    </li>
                    <li class="sidebar-li">
                        <img src="img/english.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="<?php $link ?>?lang=en" class="overflow">EN</a></p>
                    </li>
                    <li class="sidebar-li">
                        <img src="img/chinese.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="<?php $link ?>?lang=ch" class="overflow">中文</a></p>
                    </li>
                    <li class="sidebar-li">
                        <img src="img/exit.png" alt="" title="" class="sidebar-icon-img">
                        <p class="sidebar-span white-text"><a href="logout.php" class="overflow"><?php echo _USERDASHBOARD_LOGOUT ?></a></p>
                    </li>
                </ul>
            </div>
        </div>

    <?php
    }
    ?>
    <?php
}
else
{
?>

    <header id="header" class="header header--fixed index-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <a href="index.php"><img src="img/logo4.png" class="logo-img" alt="Logo" title="Logo"></a>
            </div>
            <div class="float-right">
            	
                <div class="right-header-p dropdown  menu-item menu-a">
                    <img src="img/language.png" class="dropdown-img lang-img" alt="Language/语言" title="Language/语言">
                    <img src="img/dropdown.png" class="dropdown-img hover" alt="Language / 语言" title="Language / 语言">
                    <div class="dropdown-content yellow-dropdown-content">
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p>
                    </div>
                </div> 
                <p class="right-header-p right-header-p2 open-login opacity-hover"><?php echo _MAINJS_INDEX_LOGIN ?></p> 
            </div>
        </div>
    </header>

<?php
}
?>


<!-- <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
    <div class="big-container-size hidden-padding" id="top-menu">
        <div class="float-left left-logo-div">
            <a href="index.php"><img src="img/logo4.png" class="logo-img" alt="Logo" title="Logo"></a>
        </div>
        <div class="right-menu-div float-right">
            <div class="dropdown  menu-item menu-a">
                <img src="img/language.png" class="dropdown-img lang-img" alt="Language/语言" title="Language/语言">
                <img src="img/dropdown.png" class="dropdown-img hover" alt="Language / 语言" title="Language / 语言">
                <div class="dropdown-content yellow-dropdown-content">
                    <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                    <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p>
                </div>
            </div>  
        </div>
    </div>
</header> -->