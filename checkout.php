<?php
// if (session_id() == "")
// {
//   session_start();
// }
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Checkout.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$id = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
$orderUid = $id[0]->getId();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

if($_SERVER['REQUEST_METHOD'] == 'POST'){

    $id = rewrite($_POST["order_id"]);
    $name = rewrite($_POST["insert_name"]);
    $contactNo = rewrite($_POST["insert_contactNo"]);
    $address_1 = rewrite($_POST["insert_address_1"]);
    $address_2 = rewrite($_POST["insert_address_2"]);
    $city = rewrite($_POST["insert_city"]);
    $zipcode = rewrite($_POST["insert_zipcode"]);
    $state = rewrite($_POST["insert_state"]);
    $country = rewrite($_POST["insert_country"]);
}

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,2);
}


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
	<meta property="og:url" content="https://hygeniegroup.com/checkout.php" />
    <link rel="canonical" href="https://hygeniegroup.com/checkout.php" />
    <meta property="og:title" content="<?php echo _PRODUCT_CHECK_OUT ?>  |  Hygenie Group" />
    <title><?php echo _PRODUCT_CHECK_OUT ?>  | Hygenie Group</title>

	<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance min-height big-black-text user-dash user-dash2 product-padding ow-black-everything">
        <div class="width100 overflow">
			<h1 class="text-center pop-h1 ow-black-everything"><?php echo _PRODUCT_CHECK_OUT ?></h1>
    	</div>
	        <div  id="Cart" class="tabcontent">
        
                    <form method="POST"  action="shipping.php"  enctype="multipart/form-data">
                    
                        <div class="width100 overflow border-bottom"> 
                        <?php
                        if($userDetails)
                        {
                            $conn = connDB();
                            //$orders = getUser($conn,"WHERE id = ? ", array("id") ,array($_POST['uid']),"s");
                            $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
                        ?>                  
                            <div class="dual-input">
                                <p class="input-top-p"><?php echo _JS_FULLNAME ?></p>
                                <input class="input-name clean" type="text" id="insert_name" name="insert_name" placeholder="<?php echo _JS_FULLNAME ?>" value="<?php echo $userRows[0]->getUsername();?>" required>
                            </div>
                
                            <div class="dual-input second-dual-input">
                                <p class="input-top-p"><?php echo _INDEX_MOBILE_NO ?></p>
                                <input class="input-name clean" type="text" id="insert_contactNo" name="insert_contactNo" placeholder="<?php echo _INDEX_MOBILE_NO ?>" value="<?php echo $userRows[0]->getPhoneNo();?>" required> 
                            </div>
                        
                            <div class="clear"></div>
                            <div class="dual-input">
                                <p class="input-top-p"><?php echo _INDEX_ADDRESS ?></p>
                                <input class="input-name clean" type="text" id="insert_address_1" name="insert_address_1" placeholder="<?php echo _INDEX_ADDRESS ?>" value="<?php echo $userRows[0]->getAddress();?> <?php echo $userRows[0]->getAddressB();?> <?php echo $userRows[0]->getZipcode();?> <?php echo $userRows[0]->getZipcode();?> <?php echo $userRows[0]->getCountry();?>" required>   
                            </div>
                        
                            <div class="clear"></div> 
        
                                <div class="dual-input">
                                    <input type="hidden" id="uid" name="uid" value="<?php echo $orderUid ?>">
                                </div>    
                                <div class="clear"></div>  
                            </div>
                            <?php
                        }
                    ?> 
    
                        <div class="right-status-div">        
                            <?php echo $productListHtml; ?>
                        </div>

                        
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php 
        if(isset($_GET['type']))
        {
            $messageType = null;

            if($_SESSION['messageType'] == 1)
            {
                if($_GET['type'] == 1)
                {
                    $messageType = "Please Fill Up The Required Details !";
                }
                if($_GET['type'] == 2)
                {
                    $messageType = "Fail To Make Order !";
                }

                echo '
                <script>
                    putNoticeJavascript("Notice !! ","'.$messageType.'");
                </script>
                ';   
                $_SESSION['messageType'] = 0;
            }
        }
    ?>
<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.green-footer{
		display:none;}
</style>
<div class="clear"></div>
<style>
.footer-div{
	display:none;}

</style>
<?php include 'js.php'; ?>

</body>
</html>