<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Cart.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    createOrder($conn,$uid);
    header('Location: ./checkout.php');
}

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $slider = getSlider($conn," WHERE status = 'Show' ");
// $products = getProduct($conn, "WHERE status = 'Available' ");

$productListHtml = "";

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
	<meta property="og:url" content="https://hygeniegroup.com/cartEmpty.php" />
    <link rel="canonical" href="https://hygeniegroup.com/cartEmpty.php" />
    <meta property="og:title" content="Cart  |  Hygenie Group" />
    <title>Cart  | Hygenie Group</title>

	<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance min-height big-black-text user-dash user-dash2 product-padding">

        <div class="width100 overflow">
        <?php
            $conn = connDB();
            if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'])
            {
                unset($_SESSION['shoppingCart']);
            ?>
			    <h1 class="text-center pop-h1 ow-black-everything">YOUR CART IS CLEAR !</h1>
            <?php
            }
            else
            {   }
            $conn->close();
        ?>
    	</div> 

        <div class="width100 overflow text-center margin-bottom-5px">
            <a href="product.php" class="green-a">View All Products</a>
        </div>

</div>

<style>
.footer-div{
	display:none;}

</style>

<?php include 'js.php'; ?>

</body>
</html>
