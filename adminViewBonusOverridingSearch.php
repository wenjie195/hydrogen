<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BonusOverriding.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$fromDate = rewrite($_POST["fromDate"]);
	$endDate = rewrite($_POST["endDate"]);
	$newEndDate = date('Y-m-d', strtotime($endDate. ' + 1 days'));

	// $overridingBonus = getBonusOverriding($conn);
	// $overridingBonus = getBonusOverriding($conn, "WHERE date_created >= '$fromDate' AND date_created <= '$endDate'  ");
	$overridingBonus = getBonusOverriding($conn, "WHERE date_created >= '$fromDate' AND date_created <= '$newEndDate'  ");

}

$conn->close();

?>

<!DOCTYPE html>
<html>
<head>

	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://hygeniegroup.com/adminViewBonusOverriding.php" />
    <link rel="canonical" href="https://hygeniegroup.com/adminViewBonusOverriding.php" />
    <meta property="og:title" content="<?php echo _BONUS_OVERRIDING_BONUS ?> | Hygenie Group" />
    <title><?php echo _BONUS_OVERRIDING_BONUS ?> | Hygenie Group</title>

	<?php include 'css.php'; ?>

</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">

    <div class="width100 shipping-div2 margin-top15">

    <h1 class="small-h1-a text-center white-text"> <a class="blue-link" href="adminViewBonusDirect.php"><?php echo _BONUS_DIRECT2 ?></a> | <a class="blue-link" href="adminViewBonusPoolRecord.php"><?php echo _USERDASHBOARD_POOL_BONUS ?></a> | <?php echo _BONUS_OVERRIDING_BONUS ?></h1>

		<div class="overflow-scroll-div">

    <p class="white-text p-title"><b><?php echo _BONUS_OVERRIDING_BONUS ?></b></p>

			<table class="table-css fix-th tablesorter smaller-font-table">
				<thead>
					<tr>
						<th class="th"><?php echo _ADMINVIEWBALANCE_NO ?></th>
						<th class="th"><?php echo _BONUS_RECEIVER ?></th>
						<th class="th"><?php echo _PRODUCT_AMOUNT ?></th>
						<th class="th"><?php echo _BONUS_TYPE ?></th>
						<th class="th"><?php echo _DAILY_DATE ?></th>
					</tr>
				</thead>
				<tbody>

					<?php
					if($overridingBonus)
					{
						for($cnt = 0;$cnt < count($overridingBonus) ;$cnt++)
						{
						?>
							<tr>
								<td><?php echo ($cnt+1)?></td>
								<td><?php echo $overridingBonus[$cnt]->getUsername();?></td>
								<td><?php echo $overridingBonus[$cnt]->getAmount();?></td>
                                <td><?php echo $overridingBonus[$cnt]->getBonusType();?></td>
								<td><?php echo $overridingBonus[$cnt]->getDateCreated();?></td>
							</tr>
						<?php
						}
						?>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>

	</div>

</div>

<?php include 'js.php'; ?>

<script src="js/headroom.js"></script>

</body>
</html>