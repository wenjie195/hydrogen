<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Stock.php';
// require_once dirname(__FILE__) . '/classes/BonusPoolFund.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$stockA = getStock($conn, "WHERE brand = 'GD' ");
$stockB = getStock($conn, "WHERE brand = 'GN' ");
// $poolBonus = getBonusPoolFund($conn);

$countStockA = getStock($conn, "WHERE brand = 'GD' AND status = 'Available' ");
$countStockB = getStock($conn, "WHERE brand = 'GN' AND status = 'Available' ");
$salesA = getStock($conn, "WHERE brand = 'GD' AND status = 'Sold' ");
$salesB = getStock($conn, "WHERE brand = 'GN' AND status = 'Sold' ");

$conn->close();

?>

<!DOCTYPE html>
<html>
<head>

	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://hygeniegroup.com/adminStockView.php" />
    <link rel="canonical" href="https://hygeniegroup.com/adminStockView.php" />
    <meta property="og:title" content="<?php echo _PRODUCT_VIEW_STOCK ?> | Hygenie Group" />
    <title><?php echo _PRODUCT_VIEW_STOCK ?> | Hygenie Group</title>

	<?php include 'css.php'; ?>

</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">

    <div class="width100 shipping-div2 margin-top15">

    <!-- <h1 class="small-h1-a text-center white-text">All Stock And Sales</h1> -->
	<h1 class="small-h1-a text-center white-text"><?php echo _PRODUCT_ALL_STOCK ?> | <a class="blue-link" href="uploadExcel.php"><?php echo _PRODUCT_ADD_STOCK ?></a> </h1>
	<div class="left-div-css">

	<?php
	if($salesA)
	{   
		$totalSalesA = count($salesA);
	}
	else
	{   $totalSalesA = 0;   }
	?>

	<?php
	if($countStockA)
	{   
		$totalStockA = count($countStockA);
	}
	else
	{   $totalStockA = 0;   }
	?>

	<?php
		$availableStockA = $totalStockA - $totalSalesA ;
	?>

    <p class="white-text p-title"><b>GD</b></p>
	<p class="white-text p-title"><b><?php echo _PRODUCT_AVAILABLE_STOCK ?>: <?php echo $totalStockA ;?></b></p>
	<p class="white-text p-title"><b><?php echo _PRODUCT_TOTAL_SALES ?>: <?php echo $totalSalesA ;?></b></p>
		<div class="overflow-scroll-div">
    		
			<table class="table-css fix-th tablesorter smaller-font-table">
				<thead>
					<tr>
						<th class="th"><?php echo _ADMINVIEWBALANCE_NO ?></th>
						<th class="th"><?php echo _PRODUCT_SERIAL_NUMBER ?></th>
						<th class="th"><?php echo _BONUS_STATUS ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					if($stockA)
					{
						for($cnt = 0;$cnt < count($stockA) ;$cnt++)
						{
						?>
							<tr>
								<td><?php echo ($cnt+1)?></td>
								<td><?php echo $stockA[$cnt]->getName();?></td>
								<td><?php echo $stockA[$cnt]->getStatus();?></td>
							</tr>
						<?php
						}
						?>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>

		</div>
        <div class="right-div-css">

		<?php
		if($salesB)
		{   
			$totalSalesB = count($salesB);
		}
		else
		{   $totalSalesB = 0;   }
		?>

		<?php
		if($countStockB)
		{   
			$totalStockB = count($countStockB);
		}
		else
		{   $totalStockB = 0;   }
		?>

		<p class="white-text p-title"><b>GN</b></p>
		<p class="white-text p-title"><b><?php echo _PRODUCT_AVAILABLE_STOCK ?>: <?php echo $totalStockB ;?></b></p>
		<p class="white-text p-title"><b><?php echo _PRODUCT_TOTAL_SALES ?>: <?php echo $totalSalesB ;?></b></p>
		<div class="overflow-scroll-div">
    		
			<table class="table-css fix-th tablesorter smaller-font-table">
				<thead>
					<tr>
						<th class="th"><?php echo _ADMINVIEWBALANCE_NO ?></th>
						<th class="th"><?php echo _PRODUCT_SERIAL_NUMBER ?></th>
						<th class="th"><?php echo _BONUS_STATUS ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					if($stockB)
					{
						for($cnt = 0;$cnt < count($stockB) ;$cnt++)
						{
						?>
							<tr>
								<td><?php echo ($cnt+1)?></td>
								<td><?php echo $stockB[$cnt]->getName();?></td>
								<td><?php echo $stockB[$cnt]->getStatus();?></td>
							</tr>
						<?php
						}
						?>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>
		</div>
	</div>

</div>

<?php include 'js.php'; ?>

<script src="js/headroom.js"></script>

</body>
</html>