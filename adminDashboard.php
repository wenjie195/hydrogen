<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/User.php';
// require_once dirname(__FILE__) . '/classes/PoolFund.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$paymentStatus = getOrders($conn, " WHERE payment_status = 'PENDING' ");
// $shippingRequest = getOrders($conn, " WHERE payment_status = 'PENDING' AND shipping_status = 'PENDING' ");

$allUser = getUser($conn, " WHERE user_type = 1 ");

$allManager = getUser($conn, " WHERE rank = 'Manager' ");
$allSeniorManager = getUser($conn, " WHERE rank = 'Senior Manager' ");
$allAreaManager = getUser($conn, " WHERE rank = 'Area Manager' ");
$allDistrictManager = getUser($conn, " WHERE rank = 'District Manager' ");

// $allManager = getUser($conn);
// $allSeniorManager = getUser($conn);
// $allAreaManager = getUser($conn);
// $allDistrictManager = getUser($conn);


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>

	<meta property="og:url" content="https://hygeniegroup.com/adminDashboard.php" />
    <link rel="canonical" href="https://hygeniegroup.com/adminDashboard.php" />
    <meta property="og:title" content="<?php echo _ADMIN_ADMINDASHBOARD ?> | Hygenie Group" />
    <title><?php echo _ADMIN_ADMINDASHBOARD ?>  | Hygenie Group</title>

    <link rel="stylesheet" href="css/font-awesome.min.css">
	<?php include 'css.php'; ?>
</head>
<style media="screen">


td.tdh{
    text-align: center;
  }
  th.thh{
    text-align: center;
  }
  #Reset, #Reset2, #ResetMembers{
  background-color: maroon;
  }
  #Reset:hover, #ResetMembers:hover, #Reset2:hover{
  background-color: #650000;
  }
  .reset-checkbox, #nameCheckMembers, #nameCheck, #nameCheck2, .reset-checkbox2{
	color:white;
    cursor: pointer;
    text-align: center;

  }
</style>
<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance darkbg min-height big-black-text text-center ow-white-css extra-margin-bottom dash-ow" id="firefly">

    <div class="width100 overflow margin-top50">

        <div class="profile-h3 text-center white-div-css overflow">
            <img src="img/group-member.png" class="center-icon-img" alt="<?php echo _ADMIN_TOTAL_MEMBER ?>" title="<?php echo _ADMIN_TOTAL_MEMBER ?>">
            
                <p class="white-div-p"><?php echo _ADMIN_TOTAL_MEMBER ?></p>
                <p class="white-div-p white-div-amount">
                    <?php
                    if($allUser)
                    {   
                        $totalUser = count($allUser);
                    }
                    else
                    {   $totalUser = 0;   }
                    ?>
                    <?php echo $totalUser;?>
                </p>
           
        </div>

        <div class="profile-h3 text-center white-div-css overflow">
            <img src="img/commission.png" class="center-icon-img" >
            
                <p class="white-div-p"><?php echo _ADMIN_PENDING_ORDER ?></p>
                <p class="white-div-p white-div-amount">
                    <?php
                    if($paymentStatus)
                    {   
                        $totalOrder = count($paymentStatus);
                    }
                    else
                    {   $totalOrder = 0;   }
                    ?>
                    <?php echo $totalOrder;?>    
                </p>
           
        </div>   

        <!-- <div class="profile-h3 text-center white-div-css overflow">
            <img src="img/group-sales.png" class="white-div-img" alt="Total ......" title="Total ......">
            <div class="inner-white-div">
                <p class="white-div-p">Total ......</p>
                <p class="white-div-p white-div-amount">
                Example:300
                </p>
            </div>
        </div>   -->

        <div class="profile-h3 text-center white-div-css overflow">

            <img src="img/manager.png" class="center-icon-img" alt="<?php echo _BONUS_DIRECT2 ?>" title="<?php echo _BONUS_DIRECT2 ?>">
           
                <p class="white-div-p"><?php echo _DAILY_BONUS ?> 1 (<?php echo _BONUS_DIRECT2 ?>)</p>
                <form action="bonusOneDistribution.php" method="POST" target="_blank">
                    <button class="clean blue-ow-btn distribute-btn" type="submit">
                        <?php echo _ADMIN_DIST ?>
                    </button>
                </form>
            
        </div>


    <?php
    if($allManager)
    {   
      $totalManager = count($allManager);
    }
    else
    {   $totalManager = 0;   }
    ?>

    <?php
    if($totalManager > 0)
    {   
    ?>
       <div class="profile-h3 text-center white-div-css overflow">
            <img src="img/manager.png" class="center-icon-img" alt="<?php echo _USERDASHBOARD_SALES_MANAGER ?>" title="<?php echo _USERDASHBOARD_SALES_MANAGER ?>">
           
                <p class="white-div-p"><?php echo _DAILY_BONUS ?> 2 (<?php echo _USERDASHBOARD_SALES_MANAGER ?>)</p>
                <form action="bonusTwoMDistribution.php" method="POST">
                    <button class="clean blue-ow-btn distribute-btn" type="submit">
                        <?php echo _ADMIN_DIST ?>
                    </button>
                </form>
            
        </div>
    <?php
    }
    else
    {}
    ?>

    <?php
    if($allSeniorManager)
    {   
      $totalSeniorManager = count($allSeniorManager);
    }
    else
    {   $totalSeniorManager = 0;   }
    ?>

    <?php
    if($totalSeniorManager > 0)
    {   
    ?>
        <div class="profile-h3 text-center white-div-css overflow">
            <img src="img/senior-manager.png" class="center-icon-img" alt="<?php echo _USERDASHBOARD_MARKETING_MANAGER ?>" title="<?php echo _USERDASHBOARD_MARKETING_MANAGER ?>">
           
                <p class="white-div-p"><?php echo _DAILY_BONUS ?> 2 (<?php echo _USERDASHBOARD_MARKETING_MANAGER ?>)</p>
                <form action="bonusTwoSMDistribution.php" method="POST">
                    <button class="clean blue-ow-btn distribute-btn" type="submit">
                       <?php echo _ADMIN_DIST ?>
                    </button>
                </form>
            
        </div>    

    <?php
    }
    else
    {}
    ?>

    <?php
    if($allAreaManager)
    {   
      $totalAreaManager = count($allAreaManager);
    }
    else
    {   $totalAreaManager = 0;   }
    ?>

    <?php
    if($totalAreaManager > 0)
    {   
    ?>
    
       <div class="profile-h3 text-center white-div-css overflow">
            <img src="img/area-manager.png" class="center-icon-img" alt="<?php echo _USERDASHBOARD_AREA_MANAGER ?>" title="<?php echo _USERDASHBOARD_AREA_MANAGER ?>">
           
                <p class="white-div-p"><?php echo _DAILY_BONUS ?> 2 (<?php echo _USERDASHBOARD_AREA_MANAGER ?>)</p>
                <form action="bonusTwoAMDistribution.php" method="POST">
                    <button class="clean blue-ow-btn distribute-btn" type="submit">
                       <?php echo _ADMIN_DIST ?>
                    </button>
                </form>
            
        </div>    
    

    <?php
    }
    else
    {}
    ?>

    <?php
    if($allDistrictManager)
    {   
      $totalDistrictManager = count($allDistrictManager);
    }
    else
    {   $totalDistrictManager = 0;   }
    ?>

    <?php
    if($totalDistrictManager > 0)
    {   
    ?>
         <div class="profile-h3 text-center white-div-css overflow">
            <img src="img/district-manager.png" class="center-icon-img" alt="<?php echo _USERDASHBOARD_REGION_MANAGER ?>" title="<?php echo _USERDASHBOARD_REGION_MANAGER ?>">
           
                <p class="white-div-p"><?php echo _DAILY_BONUS ?> 2 (<?php echo _USERDASHBOARD_REGION_MANAGER ?>)</p>
                <form action="bonusTwoDMDistribution.php" method="POST">
                    <button class="clean blue-ow-btn distribute-btn" type="submit">
                       <?php echo _ADMIN_DIST ?>
                    </button>
                </form>
            
        </div>   
    
    
    
    <?php
    }
    else
    {}
    ?>
        <div class="profile-h3 text-center white-div-css overflow">
            <img src="img/bonus-3.png" class="center-icon-img" alt="<?php echo _DAILY_BONUS ?> 3 (<?php echo _BONUS_OVERRIDING_BONUS ?>)" title="<?php echo _DAILY_BONUS ?> 3 (<?php echo _BONUS_OVERRIDING_BONUS ?>)">
           
                <p class="white-div-p"><?php echo _DAILY_BONUS ?> 3 (<?php echo _BONUS_OVERRIDING_BONUS ?>)</p>
                <form action="bonusThreeDistribution.php" method="POST">
                    <button class="clean blue-ow-btn distribute-btn" type="submit">
                       <?php echo _ADMIN_DIST ?>
                    </button>
                </form>
            
        </div>  

    <div class="profile-h3 text-center white-div-css overflow">
        <img src="img/commission.png" class="center-icon-img" alt="<?php echo _ADMIN_AUTOWITHDRAWAL ?>" title="<?php echo _ADMIN_AUTOWITHDRAWAL ?>">
        
            <p class="white-div-p"><?php echo _ADMIN_AUTOWITHDRAWAL ?></p>
            <form action="adminWithdrawalFunction.php" method="POST">
                <button class="clean blue-ow-btn distribute-btn" type="submit">
                    <?php echo _ADMIN_DISTBONUS ?>
                </button>
            </form>
        
    </div>  


</div>

 

<div class="clear"></div>

</div>

<?php include 'js.php'; ?>



<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Sales Manager Pool Bonus Distributed";
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Marketing Manager Pool Bonus Distributed";
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Area Manager Pool Bonus Distributed";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Region Manager Pool Bonus Distributed";
        }
        elseif($_GET['type'] == 5)
        {
            $messageType = "Overriding Bonus Distributed";
        }
        elseif($_GET['type'] == 6)
        {
            $messageType = "Auto Withdrawal Success";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "ERROR During Distributing Manger Pool Bonus";
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "ERROR During Distributing Senior Manger Pool Bonus";
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "ERROR During Distributing Area Manger Pool Bonus";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "ERROR During Distributing District Manger Pool Bonus";
        }
        elseif($_GET['type'] == 5)
        {
            $messageType = "No Overriding Bonus";
        }
        echo '
        <script>
            putNoticeJavascript("Notice","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>