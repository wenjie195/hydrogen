<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
// require_once dirname(__FILE__) . '/classes/MpIdData.php';
// require_once dirname(__FILE__) . '/classes/DailyBonus.php';
// require_once dirname(__FILE__) . '/classes/MonthlyBonus.php';
// require_once dirname(__FILE__) . '/classes/Withdrawal.php';
// require_once dirname(__FILE__) . '/classes/PoolFund.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$referralDetails = getReferralHistory($conn, "WHERE referral_id = ?",array("referral_id"),array($uid), "s");
$referralCurrentLevel = $referralDetails[0]->getCurrentLevel();
$directDownlineLevel = $referralCurrentLevel + 1;
$referrerDetails = $referrerDetails = getWholeDownlineTree($conn, $uid, false);
if ($referrerDetails)
{
  for ($i=0; $i <count($referrerDetails) ; $i++)
  {
    $currentLevel = $referrerDetails[$i]->getCurrentLevel();
    if ($currentLevel == $directDownlineLevel)
    {
      $directDownline++;
    }
    // $referralId = $referrerDetails[$i]->getReferralId();
    // $downlineDetails = getMpIdData($conn, "WHERE uid = ?",array("uid"),array($referralId), "s");
    // if ($downlineDetails)
    // {
    //   for ($b=0; $b <count($downlineDetails) ; $b++) {
    //     $personalSales += $downlineDetails[$b]->getBalance();
    //   }
    // }
  }
  // $groupSales += $personalSales;
  // $groupSalesFormat = number_format($groupSales,4);
}

// if ($getWho)
// {
// $groupMember = count($getWho);
// }
// else
// {
//   $groupMember = 0;
// }

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://hygeniegroup.com/userDashboard.php" />
    <link rel="canonical" href="https://hygeniegroup.com/userDashboard.php" />
    <meta property="og:title" content="User Dashboard  | Hygenie Group" />
    <title>User Dashboard  | Hygenie Group</title>

	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text user-dash user-dash2" id="firefly">

    <h3 class="white-text"><?php echo $userData->getUsername() ?></h3>

    <div class="clear"></div>

    <!-- <div class="width100 height15"></div> -->

    <div class="profile-h3 text-center white-div-css">
      <img src="img/direct-downline.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_DIRECT_DOWNLINE ?>" title="<?php echo _USERDASHBOARD_DIRECT_DOWNLINE ?>">
      <div class="inner-white-div">
        <p class="white-div-p"><?php echo _USERDASHBOARD_DIRECT_DOWNLINE ?></p>
        <p class="white-div-p white-div-amount">
          <!-- <?php //echo $directDownline ?> -->
        </p>
      </div>
    </div>

    <div class="profile-h3 text-center white-div-css mid-profile-h3 second-profile-h3">
      <img src="img/group-member.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_GROUP_MEMBER ?>" title="<?php echo _USERDASHBOARD_GROUP_MEMBER ?>">
      <div class="inner-white-div">
        <p class="white-div-p"><?php echo _USERDASHBOARD_GROUP_MEMBER ?></p>
        <p class="white-div-p white-div-amount">
          <!-- <?php //echo $groupMember ?> -->
        </p>
      </div>
    </div>

</div>

<?php include 'js.php'; ?>

</body>
</html>

<script>
  $(function(){
    $("#displayCommission").click(function(){
      $("#dailyMonthlyCommission").fadeToggle();
    });
  });
</script>