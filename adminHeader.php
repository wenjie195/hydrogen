<div id="wrapper" class="toggled-2">
        <div id="sidebar-wrapper">
                <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
                        <li>
                                <a href="adminDashboard.php"><img src="img/logo4.png" class="logo-img" alt="Logo" title="Logo"></a>
                        </li>
                        <li class="sidebar-li">
                                <img src="img/group.png" alt="" title="" class="sidebar-icon-img">
                                <p class="sidebar-span white-text"><a href="adminViewMember.php" class="overflow"><?php echo _ADMINHEADER_VIEW_MEMBER ?></a></p>
                        </li>     
                        <li class="sidebar-li">
                                <img src="img/withdrawal.png" alt="" title="" class="sidebar-icon-img">
                                <p class="sidebar-span white-text"><a href="adminWithdrawal.php" class="overflow"><?php echo _ADMINHEADER_MEMBER_WITHDRAW ?></a></p>
                        </li>       

                        <li class="sidebar-li">
                                <img src="img/product.png" alt="" title="" class="sidebar-icon-img">
                                <p class="sidebar-span white-text"><a href="adminProductAll.php" class="overflow"><?php echo _PRODUCT ?></a></p>
                        </li>   

                        <li class="sidebar-li">
                                <img src="img/documents.png" alt="" title="" class="sidebar-icon-img">
                                <p class="sidebar-span white-text"><a href="adminStockView.php" class="overflow"><?php echo _PRODUCT_STOCK_SALES ?></a></p>
                        </li>     

                        <li class="sidebar-li">
                                <img src="img/sales.png" alt="" title="" class="sidebar-icon-img">
                                <p class="sidebar-span white-text"><a href="adminSalesView.php" class="overflow"><?php echo _PRODUCT_SALES_PAGE ?></a></p>
                        </li> 

                        <li class="sidebar-li">
                                <img src="img/report.png" alt="" title="" class="sidebar-icon-img">
                                <!-- <p class="sidebar-span white-text"><a href="adminOrderPending.php" class="overflow">Ordering</a></p> -->
                                <p class="sidebar-span white-text"><a href="adminOrderPaymentVerification.php" class="overflow"><?php echo _PRODUCT_ORDER ?></a></p>
                        </li>      
                        <li class="sidebar-li">
                                <img src="img/capping2.png" alt="" title="" class="sidebar-icon-img">
                                <!-- <p class="sidebar-span white-text"><a href="adminViewBonus.php" class="overflow">Bonus Distribution</a></p> -->
                                <p class="sidebar-span white-text"><a href="adminViewBonusDirect.php" class="overflow"><?php echo _BONUS_DIST ?></a></p>
                        </li>    
                        <li class="sidebar-li">
                                <img src="img/english.png" alt="" title="" class="sidebar-icon-img">
                                <p class="sidebar-span white-text"><a href="<?php $link ?>?lang=en" class="overflow">EN</a></p>
                        </li>    
                        <li class="sidebar-li">
                                <img src="img/chinese.png" alt="" title="" class="sidebar-icon-img">
                                <p class="sidebar-span white-text"><a href="<?php $link ?>?lang=ch" class="overflow">中文</a></p>
                        </li>      
                        <li class="sidebar-li">
                                <img src="img/exit.png" alt="" title="" class="sidebar-icon-img">
                                <p class="sidebar-span white-text"><a href="logout.php" class="overflow"><?php echo _USERDASHBOARD_LOGOUT ?></a></p>
                        </li>       
                </ul>
        </div>
</div>