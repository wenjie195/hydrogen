<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$fromDate = rewrite($_POST["fromDate"]);
	$endDate = rewrite($_POST["endDate"]);
	$newEndDate = date('Y-m-d', strtotime($endDate. ' + 1 days'));

	//   $withdrawalDetails = getWithdrawal($conn, "WHERE uid = ? AND date_created >= '$fromDate' AND date_created <= '$endDate' ",array("uid"),array($uid),"s");
	$withdrawalDetails = getWithdrawal($conn, "WHERE uid = ? AND date_created >= '$fromDate' AND date_created <= '$newEndDate' ",array("uid"),array($uid),"s");
}

// $withdrawalDetails = getWithdrawal($conn, "WHERE uid = ? ORDER BY date_created DESC ",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://hygeniegroup.com/withdrawalHistory.php" />
<link rel="canonical" href="https://hygeniegroup.com/withdrawalHistory.php" />
<meta property="og:title" content="<?php echo _USERHEADER_WITHDRAWAL_HISTORY ?> | Hygenie Group" />
<title><?php echo _USERHEADER_WITHDRAWAL_HISTORY ?> | Hygenie Group</title>

<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">

<div class="width100 overflow">
    <h1 class="pop-h1 text-center"><?php echo _USERHEADER_WITHDRAWAL_HISTORY ?></h1>
  </div>

  <div class="overflow-scroll-div">
    <table class="table-css fix-th">
				<thead>
					<tr>
						<th class="th"><?php echo _ADMINVIEWBALANCE_NO ?></th>

                        <th><?php echo _PRODUCT_AMOUNT ?> (PV)</th>
                        <th><?php echo _PRODUCT_AMOUNT ?> (RM)</th>

						<th class="th"><?php echo _BONUS_STATUS ?></th>
						<th class="th"><?php echo _DAILY_DATE ?></th>
					</tr>
				</thead>
				<tbody>
					<?php
					if($withdrawalDetails)
					{
						for($cnt = 0;$cnt < count($withdrawalDetails) ;$cnt++)
						{
						?>
							<tr>
								<td><?php echo ($cnt+1)?></td>

								<td><?php echo $withdrawalDetails[$cnt]->getCurrentAmount();?></td>
            					<td><?php echo $withdrawalDetails[$cnt]->getFinalAmount();?></td>

								<td><?php echo $withdrawalDetails[$cnt]->getStatus();?></td>
								<td><?php echo $withdrawalDetails[$cnt]->getDateCreated();?></td>
							</tr>
						<?php
						}
						?>
					<?php
					}
					?>
				</tbody>
		</table>
  </div>

</div>

<?php include 'js.php'; ?>

</body>
</html>