<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/Stock.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE user_type = 1");
// $paymentStatus = getOrders($conn, " WHERE payment_status = 'APPROVED' ");

$conn->close();

?>

<!DOCTYPE html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://hygeniegroup.com/adminOrderDetailsSR.php" />
<link rel="canonical" href="https://hygeniegroup.com/adminOrderDetailsSR.php" />
<meta property="og:title" content="<?php echo _PRODUCT_ORDER_DETAILS ?> | Hygenie Group" />
<title><?php echo _PRODUCT_ORDER_DETAILS ?> | Hygenie Group</title>
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<!-- <div class="demo"> -->

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">
  <div class="width100 overflow">
    <h1 class="text-center pop-h1">Replacement Details</h1>
  </div>

    <?php
    if(isset($_POST['product_id']))
    {
      // echo $_POST['product_id'];
      $conn = connDB();
      $stockDetails = getStock($conn,"WHERE id = ? ", array("id") ,array($_POST['product_id']),"s");
      $oldId = $stockDetails[0]->getId();
      $orderId = $stockDetails[0]->getOrderId();
      $orderUid = $stockDetails[0]->getOrderUid();
      $brand = $stockDetails[0]->getBrand();
      $oldName = $stockDetails[0]->getName();
    ?>
    
      <div class="dual-input">
        <p class="input-top-text"><?php echo _PRODUCT_ORDER_ID ?>: <b>#<?php echo $orderId;?></b></p>
      </div>

      <div class="dual-input second-dual-input">
        <p class="input-top-text">Order Uid: <b><?php echo $orderUid;?></b></p>
      </div>

      <div class="clear"></div>

      <?php
      if($brand == 'GD')
      {
      ?>

        <form action="utilities/adminOrderReplacementFunction.php" method="POST">
          <div class="dual-input">
            <p class="input-top-text"><?php echo _PRODUCT_ITEM ?>: <b><?php echo $brand;?></b></p>
          </div>

          <div class="clear"></div>

          <?php
            $stockGD = getStock($conn, " WHERE status = 'Available' AND brand = 'GD' LIMIT 1 ");
            $replacementNameA = $stockGD[0]->getName();
            $replacementIdA = $stockGD[0]->getId();
          ?>

          <div class="dual-input">
            <p class="input-top-text">Old Serial No. : <b><?php echo $oldName;?></b></p>
          </div>

          <div class="dual-input second-dual-input">
            <p class="input-top-text">New Serial No. : <b><?php echo $replacementNameA;?></b></p>
          </div>

          <div class="clear"></div>

          <button class="clean blue-ow-btn" type="submit" name="product_id" value="<?php echo $_POST['product_id'];?>">
            SUBMIT
            <input class="clean line-input" type="hidden" value="<?php echo $replacementNameA;?>" name="new_name" id="new_name" readonly>
            <input class="clean line-input" type="hidden" value="<?php echo $replacementIdA;?>" name="new_id" id="new_id" readonly>
            <input class="clean line-input" type="hidden" value="<?php echo $oldName;?>" name="old_name" id="old_name" readonly>
            <input class="clean line-input" type="hidden" value="<?php echo $orderId;?>" name="order_id" id="order_id" readonly>
            <input class="clean line-input" type="hidden" value="<?php echo $orderUid;?>" name="order_uid" id="order_uid" readonly>
            <input class="clean line-input" type="hidden" value="<?php echo $oldId;?>" name="old_id" id="old_id" readonly>
          </button>
        </form>

      <?php
      }
      elseif($brand == 'GN')
      {
      ?>
        <form action="utilities/adminOrderReplacementFunction.php" method="POST">
          <div class="dual-input">
            <p class="input-top-text"><?php echo _PRODUCT_ITEM ?>: <b><?php echo $brand;?></b></p>
          </div>

          <div class="clear"></div>

          <?php
            $stockGD = getStock($conn, " WHERE status = 'Available' AND brand = 'GN' LIMIT 1 ");
            $replacementNameB = $stockGD[0]->getName();
            $replacementIdB = $stockGD[0]->getId();
          ?>

          <div class="dual-input">
            <p class="input-top-text">Old Serial No. : <b><?php echo $oldName;?></b></p>
          </div>

          <div class="dual-input second-dual-input">
            <p class="input-top-text">New Serial No. : <b><?php echo $replacementNameB;?></b></p>
          </div>

          <div class="clear"></div>

          <button class="clean blue-ow-btn" type="submit" name="product_id" value="<?php echo $_POST['product_id'];?>">
            SUBMIT
            <input class="clean line-input" type="hidden" value="<?php echo $replacementNameB;?>" name="new_name" id="new_name" readonly>
            <input class="clean line-input" type="hidden" value="<?php echo $replacementIdB;?>" name="new_id" id="new_id" readonly>
            <input class="clean line-input" type="hidden" value="<?php echo $oldName;?>" name="old_name" id="old_name" readonly>
            <input class="clean line-input" type="hidden" value="<?php echo $orderId;?>" name="order_id" id="order_id" readonly>
            <input class="clean line-input" type="hidden" value="<?php echo $orderUid;?>" name="order_uid" id="order_uid" readonly>
            <input class="clean line-input" type="hidden" value="<?php echo $oldId;?>" name="old_id" id="old_id" readonly>
          </button>
        </form>

      <?php
      }
      else
      {
      ?>
        <p class="input-top-text">=<b>NO SUCH ITEM</b></p>
      <?php
      }
      ?>

    <?php
    }
    ?>

  <div class="clear"></div>

</div>
<style>
input:-webkit-autofill,
input:-webkit-autofill:hover, 
input:-webkit-autofill:focus,
textarea:-webkit-autofill,
textarea:-webkit-autofill:hover,
textarea:-webkit-autofill:focus,
select:-webkit-autofill,
select:-webkit-autofill:hover,
select:-webkit-autofill:focus {

  -webkit-text-fill-color:white !important;

}

</style>
<?php include 'js.php'; ?>

</body>
</html>