<?php
// DB SQL Connection
function connDB(){
    // Create connection
    $conn = new mysqli("localhost", "root", "","vidatech_hydrogen");//for localhost
    // $conn = new mysqli("localhost", "qlianmen_hydro", "l6sP8tmtJeCO","qlianmen_hydrogen"); // qlianmen test server
    // $conn = new mysqli("localhost", "victoryc_user", "py8gVEykAOMR","victoryc_live"); // victory5 server
    // $conn = new mysqli("localhost", "ichibang_hydro", "9ZAAXGyno7S4","ichibang_hydrogen"); // ichiban server
    // $conn = new mysqli("localhost", "hygenieg_user", "E0nBCNCPybOh","hygenieg_live"); // big domain live server
    // $conn = new mysqli("localhost", "hygenieg_tester", "JohGmnHGhpbE","hygenieg_test"); // big domain test server

    mysqli_set_charset($conn,'UTF8');//because cant show chinese characters so need to include this to show
    //for when u insert chinese characters inside, because if dont include this then it will become unknown characters in the database
    mysqli_query($conn,"SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'")or die(mysqli_error($conn));

// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}
