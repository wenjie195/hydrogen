<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Bonus.php';
require_once dirname(__FILE__) . '/classes/BonusOverriding.php';
require_once dirname(__FILE__) . '/classes/BonusPoolFund.php';
require_once dirname(__FILE__) . '/classes/BonusRecord.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $bonusOne = getBonus($conn, "WHERE receiver_uid =?",array("receiver_uid"),array($uid),"s");

// $bonusPool = getBonusPoolFund($conn, "WHERE status = 'PENDING' ");
$managerPool = getBonusPoolFund($conn, "WHERE receiver = 'Manager' AND status = 'PENDING' ");
$seniorManagerPool = getBonusPoolFund($conn, "WHERE receiver = 'Senior Manager' AND status = 'PENDING' ");
$areaManagerPool = getBonusPoolFund($conn, "WHERE receiver = 'Area Manager' AND status = 'PENDING' ");
$districtManagerPool = getBonusPoolFund($conn, "WHERE receiver = 'District Manager' AND status = 'PENDING' ");

$bonusOne = getBonus($conn, "WHERE receiver_uid =?",array("receiver_uid"),array($uid),"s");
$bonusTwo = getBonusRecord($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$bonusThree = getBonusOverriding($conn, "WHERE uid =?",array("uid"),array($uid),"s");

// $myOrders = getOrders($conn);
// $myOrders = getOrders($conn, " WHERE payment_status = 'APPROVED' ");
$myOrders = getOrders($conn, "WHERE uid =? AND payment_status = 'APPROVED' ",array("uid"),array($uid),"s");
$myOrders2 = getOrders($conn, "WHERE uid =? AND payment_status = 'ACCEPTED' ",array("uid"),array($uid),"s");

$groupSales = 0; // initital
$groupSalesFormat = number_format(0,4); // initital
$groupSales2 = 0; // initital
$groupSales2Format = number_format(0,4); // initital
$directDownline = 0; // initital
$personalSales = 0; // initital
$personalSales2 = 0; // initital

$referralDetails = getReferralHistory($conn, "WHERE referral_id = ?",array("referral_id"),array($uid), "s");
$referralCurrentLevel = $referralDetails[0]->getCurrentLevel();
$directDownlineLevel = $referralCurrentLevel + 1;
$referrerDetails = $referrerDetails = getWholeDownlineTree($conn, $uid, false);
if ($referrerDetails)
{
  for ($i=0; $i <count($referrerDetails) ; $i++)
  {
    $currentLevel = $referrerDetails[$i]->getCurrentLevel();
    if ($currentLevel == $directDownlineLevel)
    {
      $directDownline++;
    }
    $referralId = $referrerDetails[$i]->getReferralId();
    // $downlineDetails = getOrders($conn, "WHERE uid = ?",array("uid"),array($referralId), "s");
    $downlineDetails = getOrders($conn, "WHERE uid = ? AND payment_status = 'APPROVED' ",array("uid"),array($referralId), "s");
    // $downlineDetailsTwo = getOrders($conn, "WHERE uid = ? AND payment_status = 'ACCEPTED' ",array("uid"),array($referralId), "s");
    if ($downlineDetails)
    {
      for ($b=0; $b <count($downlineDetails) ; $b++)
      {
        // $personalSales += $downlineDetails[$b]->getSubtotal();
        $personalSales += $downlineDetails[$b]->getCommission();
      }
    }

    $downlineDetailsTwo = getOrders($conn, "WHERE uid = ? AND payment_status = 'ACCEPTED' ",array("uid"),array($referralId), "s");
    if ($downlineDetailsTwo)
    {
      for ($b=0; $b <count($downlineDetailsTwo) ; $b++)
      {
        $personalSales2 += $downlineDetailsTwo[$b]->getCommission();
      }
    }
  }
  $groupSales += $personalSales;
  $groupSales2 += $personalSales2;
  $groupSalesFormat = number_format($groupSales,4);
  $groupSales2Format = number_format($groupSales2,4);
}

$myDownline = getReferralHistory($conn, "WHERE referrer_id =?",array("referrer_id"),array($uid),"s");

$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);
if ($getWho)
{
    $groupMember = count($getWho);
}
else
{
    $groupMember = 0;
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
	<meta property="og:url" content="https://hygeniegroup.com/userDashboard.php" />
    <link rel="canonical" href="https://hygeniegroup.com/userDashboard.php" /> 
    <meta property="og:title" content="User Dashboard  | Hygenie Group" />
    <title>User Dashboard  | Hygenie Group</title>

	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text user-dash user-dash2 ow-white-css" id="firefly">

  <?php 
    $userRank = $userDetails[0]->getRank();
    if($userRank == 'District Manager')
    {
      $userRanking = 'Region Manager';
    }
    elseif($userRank == 'Manager')
    {
      $userRanking = 'Sales Manager';
    }
    elseif($userRank == 'Senior Manager')
    {
      $userRanking = 'Markerting Manager';
    }
    else
    {
      $userRanking = $userRank;
    }
  ?>

  <h3 class="white-text"><?php echo $userData->getUsername();?> | <?php echo $userRanking;?></h3>

  <!-- <div style="cursor: pointer" id="displayCommission" class="five-div-width div-css"> -->
  <div class="width103 overflow">
    <h3 class="white-text pool"><?php echo _USERDASHBOARD_POOL_FUND ?></h3>

    <div class="clear"></div>

    <div class="four-box-size text-center white-div-css">
      <img src="img/manager.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_SALES_MANAGER ?>" title="<?php echo _USERDASHBOARD_SALES_MANAGER ?>">
      <div class="inner-white-div">
        <p class="white-div-p"><?php echo _USERDASHBOARD_SALES_MANAGER ?></p>
        <?php
        if($managerPool)
        {
          $totalManagerBonus = 0;
          for ($cnt=0; $cnt <count($managerPool) ; $cnt++)
          {
            $totalManagerBonus += $managerPool[$cnt]->getAmount();
          }
        }
        else
        {
          $totalManagerBonus = 0 ;
        }
        ?>
        <p class="white-div-p white-div-amount">PV <?php echo $totalManagerBonus;?></p>
      </div>
    </div>

    <div class="four-box-size text-center white-div-css">
      <img src="img/senior-manager.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_MARKETING_MANAGER ?>" title="<?php echo _USERDASHBOARD_MARKETING_MANAGER ?>">
      <div class="inner-white-div">
      <p class="white-div-p"><?php echo _USERDASHBOARD_MARKETING_MANAGER ?></p>
      <?php
      if($seniorManagerPool)
      {
        $totalSeniorManagerBonus = 0;
        for ($cnt=0; $cnt <count($seniorManagerPool) ; $cnt++)
        {
          $totalSeniorManagerBonus += $seniorManagerPool[$cnt]->getAmount();
        }
      }
      else
      {
        $totalSeniorManagerBonus = 0 ;
      }
      ?>
      <p class="white-div-p white-div-amount">PV <?php echo $totalSeniorManagerBonus;?></p>
      </div>
    </div>

    <div class="four-box-size text-center white-div-css">
      <img src="img/area-manager.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_AREA_MANAGER ?>" title="<?php echo _USERDASHBOARD_AREA_MANAGER ?>">
      <div class="inner-white-div">
        <p class="white-div-p"><?php echo _USERDASHBOARD_AREA_MANAGER ?></p>
        <?php
        if($areaManagerPool)
        {
          $totalAreaManagerBonus = 0;
          for ($cnt=0; $cnt <count($areaManagerPool) ; $cnt++)
          {
            $totalAreaManagerBonus += $areaManagerPool[$cnt]->getAmount();
          }
        }
        else
        {
          $totalAreaManagerBonus = 0 ;
        }
        ?>
        <p class="white-div-p white-div-amount">PV <?php echo $totalAreaManagerBonus;?></p>
      </div>
    </div>

    <div class="four-box-size text-center white-div-css">
      <img src="img/district-manager.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_REGION_MANAGER ?>" title="<?php echo _USERDASHBOARD_REGION_MANAGER ?>">
      <div class="inner-white-div">
        <p class="white-div-p"><?php echo _USERDASHBOARD_REGION_MANAGER ?></p>
        <?php
        if($districtManagerPool)
        {
          $totalDistrictManagerBonus = 0;
          for ($cnt=0; $cnt <count($districtManagerPool) ; $cnt++)
          {
            $totalDistrictManagerBonus += $districtManagerPool[$cnt]->getAmount();
          }
        }
        else
        {
          $totalDistrictManagerBonus = 0 ;
        }
        ?>
        <p class="white-div-p white-div-amount">PV <?php echo $totalDistrictManagerBonus;?></p>
      </div>
    </div> 

    <div class="clear"></div>   

    <h3 class="white-text pool"><?php echo _USERDASHBOARD_MY_BONUS ?></h3>

    <div class="clear"></div>     

    <div class="four-box-size text-center white-div-css">
      <a href="userViewBonusDirect.php">
        <img src="img/bonus-1.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_BONUS ?> 1" title="<?php echo _USERDASHBOARD_BONUS ?> 1">
        <div class="inner-white-div">
          <p class="white-div-p"><?php echo _USERDASHBOARD_BONUS ?> 1</p>
          <?php
          if($bonusOne)
          {
            $totalBonusOne = 0;
            for ($cnt=0; $cnt <count($bonusOne) ; $cnt++)
            {
              $totalBonusOne += $bonusOne[$cnt]->getAmount();
            }
          }
          else
          {
            $totalBonusOne = 0 ;
          }
          ?>
          <p class="white-div-p white-div-amount">PV <?php echo $totalBonusOne;?></p>
        </div>
      </a>
    </div>    
           
    <div class="four-box-size text-center white-div-css">
      <a href="userViewBonusPool.php">
        <img src="img/bonus-2.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_BONUS ?> 2" title="<?php echo _USERDASHBOARD_BONUS ?> 2">
        <div class="inner-white-div">
          <p class="white-div-p"><?php echo _USERDASHBOARD_BONUS ?> 2</p>
          <?php
          if($bonusTwo)
          {
            $totalBonusTwo = 0;
            for ($cnt=0; $cnt <count($bonusTwo) ; $cnt++)
            {
              $totalBonusTwo += $bonusTwo[$cnt]->getAmount();
            }
          }
          else
          {
            $totalBonusTwo = 0 ;
          }
          ?>
          <p class="white-div-p white-div-amount">PV <?php echo $totalBonusTwo;?></p>
        </div>
      </a>
    </div>    

    <div class="four-box-size text-center white-div-css">
      <a href="userViewBonusOverriding.php">
        <img src="img/bonus-3.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_BONUS ?> 3" title="<?php echo _USERDASHBOARD_BONUS ?> 3">
        <div class="inner-white-div">
          <p class="white-div-p"><?php echo _USERDASHBOARD_BONUS ?> 3</p>
          <?php
          if($bonusThree)
          {
            $totalBonusThree = 0;
            for ($cnt=0; $cnt <count($bonusThree) ; $cnt++)
            {
              $totalBonusThree += $bonusThree[$cnt]->getAmount();
            }
          }
          else
          {
            $totalBonusThree = 0 ;
          }
          ?>
          <p class="white-div-p white-div-amount">PV <?php echo $totalBonusThree;?></p>
        </div>
      </a>
    </div> 

    <div class="clear"></div>

    <div class="four-box-size text-center white-div-css">
      <img src="img/personal-sales.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_MY_SALES ?>" title="<?php echo _USERDASHBOARD_MY_SALES ?>">
      <div class="inner-white-div">
        <p class="white-div-p"><?php echo _USERDASHBOARD_MY_SALES ?></p>
        <?php
        if($myOrders)
        {
          $totalSales = 0;
          for ($cnt=0; $cnt <count($myOrders) ; $cnt++)
          {
            // $totalSales += $myOrders[$cnt]->getSubtotal();
            $totalSales += $myOrders[$cnt]->getCommission();
          }
        }
        else
        {
          $totalSales = 0 ;
        }
        ?>
        <?php
        if($myOrders2)
        {
          $totalSales2 = 0;
          for ($cnt=0; $cnt <count($myOrders2) ; $cnt++)
          {
            // $totalSales += $myOrders[$cnt]->getSubtotal();
            $totalSales2 += $myOrders2[$cnt]->getCommission();
          }
        }
        else
        {
          $totalSales2 = 0 ;
        }
        ?>
        <!-- <p class="white-div-p white-div-amount">PV <?php //echo $totalSales;?></p> -->
        <p class="white-div-p white-div-amount">PV <?php echo $totalMyOrder = $totalSales + $totalSales2;?></p>
      </div>
    </div>         

    <div class="four-box-size text-center white-div-css">
      <img src="img/group-sales.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_TEAM_SALES ?>" title="<?php echo _USERDASHBOARD_TEAM_SALES ?>">
      <div class="inner-white-div">
        <p class="white-div-p"><?php echo _USERDASHBOARD_TEAM_SALES ?></p>
        <p class="white-div-p white-div-amount">PV <?php echo $totalGS = $groupSales + $groupSales2;?></p>
      </div>
    </div>       

    <div class="four-box-size text-center white-div-css">
      <img src="img/direct-downline.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_DIRECT_DOWNLINE ?>" title="<?php echo _USERDASHBOARD_DIRECT_DOWNLINE ?>">
      <div class="inner-white-div">
        <p class="white-div-p"><?php echo _USERDASHBOARD_DIRECT_DOWNLINE ?></p>
        <?php
        if($myDownline)
        {   
          $totalDirectDownline = count($myDownline);
        }
        else
        {   $totalDirectDownline = 0;   }
        ?>
        <p class="white-div-p white-div-amount"><?php echo $totalDirectDownline;?></p>
      </div>
    </div>       

    <div class="four-box-size text-center white-div-css">
      <img src="img/group-member.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_TEAM_MEMBERS ?>" title="<?php echo _USERDASHBOARD_TEAM_MEMBERS ?>">
      <div class="inner-white-div">
        <p class="white-div-p"><?php echo _USERDASHBOARD_TEAM_MEMBERS ?></p>
        <p class="white-div-p white-div-amount"><?php echo $groupMember;?></p>
      </div>
    </div> 

    <div class="clear"></div>     

    <div class="clear"></div>   

    <h3 class="white-text pool"><?php echo _USERDASHBOARD_PERSONAL ?></h3>

    <div class="four-box-size text-center white-div-css">
      <!-- <a href="withdrawal.php"> -->
      <img src="img/commission.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_CREDIT ?>" title="<?php echo _USERDASHBOARD_CREDIT ?>">
      <div class="inner-white-div">
        <p class="white-div-p"><?php echo _USERDASHBOARD_CREDIT ?></p>
        <p class="white-div-p white-div-amount">PV <?php echo $userDetails[0]->getWallet() ?></p>
      </div>
      <!-- </a> -->
    </div> 

    <div class="four-box-size text-center white-div-css">
      <a href="editPassword.php">
      <img src="img/lock.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_EDIT_PASSWORD ?>" title="<?php echo _USERDASHBOARD_CREDIT ?>">
      <div class="inner-white-div">
        <p class="white-div-p"><?php echo _USERDASHBOARD_EDIT_PASSWORD ?></p>
        
      </div>
      </a>
    </div>


  </div>         
  
  <div class="clear"></div> 

</div>

<?php include 'js.php'; ?>

</body>
</html>

<!-- <script>
  $(function(){
    $("#displayCommission").click(function(){
      $("#dailyMonthlyCommission").fadeToggle();
    });
  });
</script> -->