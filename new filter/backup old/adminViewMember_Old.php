<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/MpIdData.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userCnt = getUser($conn, "WHERE user_type = 1");

$userCount = count($userCnt);

if (isset($_GET['page'])) {
  $page = $_GET['page'];  // get page from link ?page=5
  $limitMax = 50; // 50 user per page
  $startColumn = ($page - 1) * $limitMax; // start from? page = 2.. (2-1) * 50 = 50, start 50, LIMIT 50,50 start from 50 to next 50 user.
  $userDetails = getUser($conn, "WHERE user_type = 1 LIMIT $startColumn,$limitMax ");
}else {
  $limitMax = 50;
  $startColumn = 0;
  $userDetails = getUser($conn, "WHERE user_type = 1 LIMIT $startColumn,$limitMax ");
}
$startColumn += 1; // for table no column
$pagination = $userCount / $limitMax; // no of page
// $userDetails = getUser($conn, "WHERE user_type = 1 ");
// $userDetails = getUser($conn, "WHERE user_type = 1 AND date_created >=  '2020-04-09 12:00:00' ");
// $monthlyProBon = getMonProBon($conn);

// $conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/adminViewMember.php" />
    <meta property="og:title" content="View Member  | Hygenie Group" />
    <title>View Member  | Hygenie Group</title>
    <link rel="canonical" href="https://poppifx4u.com/adminViewMember.php" />
	<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text">

    <div class="width100 shipping-div2">

        <div class="search-big-div">
            <div class="fake-input-div overflow profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput" onkeyup="myFunction()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_USERNAME ?>" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3 mid-profile-h3 second-profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput2" onkeyup="myFunctionB()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_FULLNAME ?>" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput3" onkeyup="myFunctionC()" placeholder="<?php echo _MULTIBANK_SEARCH ?> MT4" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3 second-profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput4" onkeyup="myFunctionD()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_PHONE ?>" class="clean pop-input fake-input">
            </div>

            <div class="fake-input-div overflow profile-h3 mid-profile-h3">
                <img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
                <input type="text" id="myInput5" onkeyup="myFunctionE()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_EMAIL ?>" class="clean pop-input fake-input">
            </div>
        </div>

        <div class="overflow-scroll-div">
            <table class="table-css fix-th smaller-font-table" id="myTable">
                <thead>
                    <tr>
                        <th onclick="sortTable(0)"><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th onclick="sortTable(1)">
                            <?php echo wordwrap(_JS_USERNAME,8,"</br>\n");?>
                        </th>
                        <th onclick="sortTable(2)">
                            <?php echo wordwrap(_JS_FULLNAME,8,"</br>\n");?>
                        </th>
                        <th onclick="sortTable(3)">MT4 ID</th>
                        <th onclick="sortTable(4)"><?php echo _JS_PHONE ?></th>
                        <th onclick="sortTable(5)"><?php echo _JS_EMAIL ?></th>

                        <th onclick="sortTable(6)"><?php echo _USERDASHBOARD_PERSONAL_SALES ?> ($)</th>
                        <th onclick="sortTable(7)"><?php echo _USERDASHBOARD_GROUP_SALES ?> ($)</th>
                        <th onclick="sortTable(8)"><?php echo _USERDASHBOARD_DIRECT_DOWNLINE ?></th>
                        <th onclick="sortTable(9)"><?php echo _USERDASHBOARD_GROUP_MEMBER ?></th>

                        <th onclick="sortTable(10)"><?php echo _MULTIBANK_ID_DOCUMENT ?></th>
                        <th onclick="sortTable(11)"><?php echo _MULTIBANK_UTILITY_BILL ?></th>
                        <th onclick="sortTable(12)">LPOA</th>
                        <th><?php echo _ADMINVIEWBALANCE_ACTION ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if($userDetails)
                    {
                        for($cnt = 0;$cnt < count($userDetails) ;$cnt++)
                        {
                          $mpIdData = getMpIdData($conn, "WHERE uid = ?",array("uid"),array($userDetails[$cnt]->getUid()), "s");
                          $directDownline = 0;
                          $groupSales = 0;
                          $personalSales = 0;
                          $personalSalesUser = 0;
                          $groupMember = 0;
                          $referrerDetails = getWholeDownlineTree($conn, $userDetails[$cnt]->getUid(), false);
                          if ($mpIdData) {
                            $personalSalesUser = $mpIdData[0]->getBalance();
                          }
                          $referralDetails = getReferralHistory($conn, "WHERE referral_id = ?",array("referral_id"),array($userDetails[$cnt]->getUid()), "s");
                          $referralCurrentLevel = $referralDetails[0]->getCurrentLevel();
                          $directDownlineLevel = $referralCurrentLevel + 1;
                          if ($referrerDetails)
                          {
                            $groupMember = count($referrerDetails);
                            for ($i=0; $i <count($referrerDetails) ; $i++)
                            {
                              $currentLevel = $referrerDetails[$i]->getCurrentLevel();
                              if ($currentLevel == $directDownlineLevel)
                              {
                                $directDownline++;
                              }
                              $referralId = $referrerDetails[$i]->getReferralId();
                              $downlineDetails = getMpIdData($conn, "WHERE uid = ?",array("uid"),array($referralId), "s");
                              if ($downlineDetails)
                              {
                                $personalSales = $downlineDetails[0]->getBalance();
                                $groupSales += $personalSales;
                                $groupSalesFormat = number_format($groupSales,4);
                              }
                            }
                          }
                        ?>
                            <tr>
                                <td><?php echo ($startColumn++)?></td>
                                <td><?php echo $userDetails[$cnt]->getUsername();?></td>
                                <td><?php echo $userDetails[$cnt]->getFullname();?></td>
                                <td><?php echo $userDetails[$cnt]->getMpId();?></td>
                                <td><?php echo $userDetails[$cnt]->getPhoneNo();?></td>
                                <td><?php echo $userDetails[$cnt]->getEmail();?></td>
                                <td><?php echo number_format($personalSalesUser,2) ?></td>
                                <td><?php echo number_format($groupSales,2) ?></td>
                                <td><?php echo  $directDownline ?></td>
                                <td><?php echo  $groupMember ?></td>

                                <td>
                                    <?php $idDoc = $userDetails[$cnt]->getIcBack();
                                    if($idDoc == '2')
                                    {
                                    ?>

                                        <!-- <form action="multibankViewIDDoc.php" method="POST"> -->
                                        <form action="adminViewIDDoc.php" method="POST">
                                            <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                                <?php echo _MULTIBANK_VIEW ?>
                                            </button>
                                        </form>

                                    <?php
                                    }
                                    elseif($idDoc == '1')
                                    {
                                        echo "NO";
                                    }
                                    ?>
                                </td>

                                <td>
                                    <?php $utiBill = $userDetails[$cnt]->getLicense();
                                    if($utiBill == '2')
                                    {
                                    ?>

                                        <!-- <form action="multibankViewBills.php" method="POST"> -->
                                        <form action="adminViewBills.php" method="POST">
                                            <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                                <?php echo _MULTIBANK_VIEW ?>
                                            </button>
                                        </form>

                                    <?php
                                    }
                                    elseif($utiBill == '1')
                                    {
                                        echo "NO";
                                    }
                                    ?>
                                </td>

                                <td>
                                    <?php $sform = $userDetails[$cnt]->getSignature();
                                    if($sform == '2')
                                    {
                                    ?>

                                        <!-- <form action="multibankViewSignature.php" method="POST"> -->
                                        <form action="adminViewSignature.php" method="POST">
                                            <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                                <?php echo _MULTIBANK_VIEW ?>
                                            </button>
                                        </form>

                                    <?php
                                    }
                                    elseif($sform == '1')
                                    {
                                        echo "NO";
                                    }
                                    ?>
                                </td>
                                <td>

                                    <form action="adminUpdateCustomerDetails.php" method="POST">
                                        <button class="clean blue-ow-btn" type="submit" name="customer_id" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                            <?php echo _MULTIBANK_UPDATE ?>
                                        </button>
                                    </form>

                                </td>

                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>
	<div class="pagination pagination-pop">
    <?php for ($i=1; $i < $pagination ; $i++) {
      ?>

      <?php if (isset($_GET['page']) && $_GET['page'] == $i) {
        ?><a class="active pagination-pop-a pagination-pop-a-active" href="<?php echo "adminViewMember.php?page=".$i ?>"><?php echo $i ?></a><?php
      }else {
        ?><a class="pagination-pop-a" href="<?php echo "adminViewMember.php?page=".$i ?>"><?php echo $i ?></a><?php
      } ?>
     <?php
    } ?>
	</div>

</div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput2");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput3");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionD() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput4");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[4];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
function myFunctionE() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput5");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[5];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>

<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>

<script>
function sortTable(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("myTable");
  switching = true;
  // Set the sorting direction to ascending:
  dir = "asc";
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /* Loop through all table rows (except the
    first, which contains table headers): */
    for (i = 1; i < (rows.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /* Check if the two rows should switch place,
      based on the direction, asc or desc: */
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      // Each time a switch is done, increase this count by 1:
      switchcount ++;
    } else {
      /* If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again. */
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
</script>

</body>
</html>
