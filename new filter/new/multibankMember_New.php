<?php
require_once dirname(__FILE__) . '/multibankAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/MonthlyProfitBonus.php';
// require_once dirname(__FILE__) . '/classes/MpIdData.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn);
$userDetails = getUser($conn, "WHERE user_type = 1 ");

$userCnt = getUser($conn, "WHERE user_type = 1");

$userCount = count($userCnt);

if (isset($_GET['page']))
{
  $page = $_GET['page'];
  // $limitMax = 50;
  $limitMax = 100;
  $startColumn = ($page - 1) * $limitMax;
  $userDetails = getUser($conn, "WHERE user_type = 1 LIMIT $startColumn,$limitMax ");
}
else
{
  // $limitMax = 50;
  $limitMax = 100;
  $startColumn = 0;
  $userDetails = getUser($conn, "WHERE user_type = 1 LIMIT $startColumn,$limitMax ");
}
$startColumn += 1; // for table no column
$pagination = $userCount / $limitMax;
// $userDetails = getUser($conn, "WHERE user_type = 1 ");
// $userDetails = getUser($conn, "WHERE user_type = 1 AND date_created >=  '2020-04-09 12:00:00' ");
// $monthlyProBon = getMonProBon($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://poppifx4u.com/multibankMember.php" />
    <meta property="og:title" content="MultiBank View Member  | Hygenie Group" />
    <title>MultiBank View Member  | Hygenie Group</title>
    <link rel="canonical" href="https://poppifx4u.com/multibankMember.php" />
  <?php include 'css.php'; ?>
  
	<script src="js/jquery-3.5.1.min.js"></script>

	<script src="dist/js/jquery.tablesorter.min.js"></script>
	<script src="dist/js/jquery.tablesorter.widgets.min.js"></script>
	<script>
	$(function(){
		$('table').tablesorter({
			widgets        : ['zebra', 'columns'],
			usNumberFormat : false,
			sortReset      : true,
			sortRestart    : true
		});
	});
	</script>

</head>


<body class="body">
<!-- <?php //include 'adminHeader.php'; ?> -->
<?php include 'multibankHeader.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text">

    <div class="width100 shipping-div2">
	 <div class="search-big-div">
        <div class="fake-input-div overflow profile-h3">
        	<img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
            <input type="text" id="myInput" onkeyup="myFunction()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_USERNAME ?>" class="clean pop-input fake-input">
        </div>

        <div class="fake-input-div overflow profile-h3 mid-profile-h3 second-profile-h3">
        	<img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
            <input type="text" id="myInput2" onkeyup="myFunctionB()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_FULLNAME ?>" class="clean pop-input fake-input">
        </div>

        <div class="fake-input-div overflow profile-h3">
        	<img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
            <input type="text" id="myInput3" onkeyup="myFunctionC()" placeholder="<?php echo _MULTIBANK_SEARCH ?> MT4" class="clean pop-input fake-input">
        </div>

        <div class="fake-input-div overflow profile-h3 second-profile-h3">
        	<img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
            <input type="text" id="myInput4" onkeyup="myFunctionD()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_PHONE ?>" class="clean pop-input fake-input">
        </div>

        <div class="fake-input-div overflow profile-h3 mid-profile-h3">
        	<img src="img/search.png" class="search-png" alt="<?php echo _MULTIBANK_SEARCH ?>" title="<?php echo _MULTIBANK_SEARCH ?>">
            <input type="text" id="myInput5" onkeyup="myFunctionE()" placeholder="<?php echo _MULTIBANK_SEARCH ?> <?php echo _JS_EMAIL ?>" class="clean pop-input fake-input">
        </div>
	 </div>
    	<div class="clear"></div>
        <div class="overflow-scroll-div">
            <!-- <table class="table-css fix-th" id="myTable"> -->
            <table class="table-css fix-th tablesorter smaller-font-table" id="myTable">
                <thead>
                    <tr>
                        <th><?php echo _JS_USERNAME ?><img src="img/sort.png" class="sort-img"></th>
                        <th><?php echo _JS_FULLNAME ?><img src="img/sort.png" class="sort-img"></th>
                        <th><?php echo _MULTIBANK_PERFORMANCE_FEE ?><img src="img/sort.png" class="sort-img"></th>
                        <th>MT4<img src="img/sort.png" class="sort-img"></th>
                        <th><?php echo _JS_PHONE ?><img src="img/sort.png" class="sort-img"></th>
                        <th><?php echo _JS_EMAIL ?><img src="img/sort.png" class="sort-img"></th>
                        <th><?php echo _MULTIBANK_ID_DOCUMENT ?><img src="img/sort.png" class="sort-img"></th>
                        <th><?php echo _MULTIBANK_UTILITY_BILL ?><img src="img/sort.png" class="sort-img"></th>
                        <th>LPOA<img src="img/sort.png" class="sort-img"></th>
                        <th><?php echo _MULTIBANK_ACTION ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    if($userDetails)
                    {
                        for($cnt = 0;$cnt < count($userDetails) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo $userDetails[$cnt]->getUsername();?></td>
                                <td><?php echo $userDetails[$cnt]->getFullname();?></td>
                                <td>
                                  <?php 
                                    $cenvertedTime = "2020-04-09 12:00:00";
                                    $userSignUpDate = $userDetails[$cnt]->getDateCreated();
                                    if($userSignUpDate <= "2020-04-09 12:00:00")
                                    {
                                        echo "20%";
                                    }
                                    else
                                    {
                                        echo "40%";
                                    }
                                  ?>
                                </td>
                                <td><?php echo $userDetails[$cnt]->getMpId();?></td>
                                <td><?php echo $userDetails[$cnt]->getPhoneNo();?></td>
                                <td><?php echo $userDetails[$cnt]->getEmail();?></td>
                                <td>
                                    <?php $idDoc = $userDetails[$cnt]->getIcBack();
                                    if($idDoc == '2')
                                    {
                                    ?>

                                        <form action="multibankViewIDDoc.php" method="POST">
                                            <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                                <?php echo _MULTIBANK_VIEW ?>
                                            </button>
                                        </form>

                                    <?php
                                    }
                                    elseif($idDoc == '1')
                                    {
                                        echo "NO";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php $utiBill = $userDetails[$cnt]->getLicense();
                                    if($utiBill == '2')
                                    {
                                    ?>

                                        <form action="multibankViewBills.php" method="POST">
                                            <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                                <?php echo _MULTIBANK_VIEW ?>
                                            </button>
                                        </form>

                                    <?php
                                    }
                                    elseif($utiBill == '1')
                                    {
                                        echo "NO";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php $sform = $userDetails[$cnt]->getSignature();
                                    if($sform == '2')
                                    {
                                    ?>

                                        <form action="multibankViewSignature.php" method="POST">
                                            <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                                <?php echo _MULTIBANK_VIEW ?>
                                            </button>
                                        </form>

                                    <?php
                                    }
                                    elseif($sform == '1')
                                    {
                                        echo "NO";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <form action="multibankViewForm.php" method="POST">
                                        <button class="clean blue-ow-btn" type="submit" name="user_uid" value="<?php echo $userDetails[$cnt]->getUid();?>">
                                            <?php echo _MULTIBANK_VIEW ?>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>
	<div class="pagination pagination-pop">
    <?php for ($i=1; $i < $pagination ; $i++) {
      ?>
      
      <?php if (isset($_GET['page']) && $_GET['page'] == $i) {
        ?><a class="active pagination-pop-a pagination-pop-a-active" href="<?php echo "multibankMember.php?page=".$i ?>"><?php echo $i ?></a><?php
      }else {
        ?><a class="pagination-pop-a" href="<?php echo "multibankMember.php?page=".$i ?>"><?php echo $i ?></a><?php
      } ?>
    <?php
    } ?>
	</div> 
</div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput2");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput3");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionD() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput4");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[4];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionE() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput5");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[5];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>


<script src="js/headroom.js"></script>

<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();

    }());
</script>

	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });

    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });

    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;

       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";

    });
    </script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>


<!-- <script>
function sortTable(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("myTable");
  switching = true;
  // Set the sorting direction to ascending:
  dir = "asc";
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /* Loop through all table rows (except the
    first, which contains table headers): */
    for (i = 1; i < (rows.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /* Check if the two rows should switch place,
      based on the direction, asc or desc: */
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      // Each time a switch is done, increase this count by 1:
      switchcount ++;
    } else {
      /* If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again. */
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
</script> -->

</body>
</html>