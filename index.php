<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $referrerUID = $_SESSION['uid'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://hygeniegroup.com/" />
    <link rel="canonical" href="https://hygeniegroup.com/" />
    <meta property="og:title" content="氢爱天下 Hygenie Group" />
    <title>氢爱天下 Hygenie Group</title>
    
	<?php include 'css.php'; ?>
</head>
<body class="body">
<style>
.header1, .footer-div, .ow-blue-button1{
background: #0BEEF9;
background: -moz-linear-gradient(left, #0BEEF9 0%, #48A9FE 100%);
background: -webkit-gradient(left top, right top, color-stop(0%, #0BEEF9), color-stop(100%, #48A9FE));
background: -webkit-linear-gradient(left, #0BEEF9 0%, #48A9FE 100%);
background: -o-linear-gradient(left, #0BEEF9 0%, #48A9FE 100%);
background: -ms-linear-gradient(left, #0BEEF9 0%, #48A9FE 100%);
background: linear-gradient(to right, #0BEEF9 0%, #48A9FE 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0BEEF9', endColorstr='#48A9FE', GradientType=1 );	
	}
 .ow-blue-button1{	
    background: #ffffff40;}
.ow-blue-button1:hover{
    background: #ffffff60;	
}
::placeholder {
	color:white !Important;
}
</style>
<?php include 'header.php'; ?>
<div class="width100 home-same-padding menu-distance darkbg blue-bg-ow min-height text-center bottom-water-div"  id="firefly">
    
   <div class="index-login-width">
    
		
		<img src="img/logo.png" alt="<?php echo _JS_LOGIN ?>" title="<?php echo _JS_LOGIN ?>"   class="index-logo">
    	<h1 class="h1-title"><?php echo _JS_LOGIN ?></h1>
        <form action="utilities/loginFunction.php" method="POST">
            <input class="clean pop-input" type="text" placeholder="<?php echo _MAINJS_INDEX_USERNAME ?>" id="username" name="username" required>
            
            <div class="fake-input-bg">
                <input class="clean pop-input  no-bg-input" type="password" placeholder="<?php echo _MAINJS_INDEX_PASSWORD ?>" id="password" name="password" required>
                <img src="img/eye-white.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionD()" alt="View Password" title="View Password">
            </div>
            <div class="clear"></div>
            <button style="width:100% !important;" class="clean width100 blue-button white-text pill-button ow-blue-button1" name="loginButton"><?php echo _JS_LOGIN ?></button>
            <p class="width100 text-center">	
                <a href="forgotPassword.php" class="blue-link forgot-a opacity-hover"><?php echo _JS_FORGOT_TITLE ?></a>
            </p>
          
            <div class="clear"></div>
        </form>
	</div>
    
    	


</div>
<?php include 'js.php'; ?>



<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Incorrect Password"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Please Register";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Password Changed Successfully";
        }
        echo '
        <script>
            putNoticeJavascript("Notice ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>