<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>
<div class="width100 same-padding footer-div">
	<p class="footer-p white-text">©<?php echo $time;?> <?php echo _JS_FOOTER ?></p>
</div>
<div id="login-modal" class="login-css2 modal-css">
  <span class="close-login close-css close-css2">&times;</span>
  <div class="modal-content-css  login-modal-content2">
  
        	<div class="width100 text-center ow-login-css">
        		<h1 class="h1-title"><?php echo _JS_LOGIN ?></h1>
        <form action="utilities/loginFunction.php" method="POST">
            <input class="clean pop-input" type="text" placeholder="<?php echo _MAINJS_INDEX_USERNAME ?>" id="username" name="username" required>
            
            <div class="fake-input-bg">
                <input class="clean pop-input  no-bg-input" type="password" placeholder="<?php echo _MAINJS_INDEX_PASSWORD ?>" id="password" name="password" required>
                <img src="img/eye3.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionD()" alt="View Password" title="View Password">
            </div>
            <div class="clear"></div>
            <button style="width:100% !important;" class="clean width100 blue-button white-text pill-button" name="loginButton"><?php echo _JS_LOGIN ?></button>
            <p class="width100 text-center">	
                <a  class="blue-link forgot-a opacity-hover open-forgot"><?php echo _JS_FORGOT_TITLE ?></a>
            </p>
            </div> 
            </form>            				
  </div>
</div>
<div id="forgot-modal" class="login-css2 modal-css">
  <span class="close-forgot close-css close-css2">&times;</span>
  <div class="modal-content-css  login-modal-content2">
  
        	<div class="width100 text-center ow-login-css">
        		<h1 class="h1-title"><?php echo _JS_FORGOT_TITLE ?></h1>
        <form action="utilities/forgotPasswordFunction.php" method="POST">
            <input class="clean pop-input" type="text" placeholder="<?php echo _MAINJS_INDEX_USERNAME ?>" id="username" name="username" required>
            
             <div class="clear"></div>

            <input class="clean pop-input" type="email" placeholder="<?php echo _JS_EMAIL ?>" id="forgot_password" name="forgot_password" required>
            <div class="clear"></div>
            <button style="width:100% !important;" class="clean width100 blue-button white-text pill-button" name="submit"><?php echo _JS_SUBMIT ?></button>
</form>    
            </div>             				
  </div>
</div>
<div id="pro1" class="pro1-css modal-css">
  <span class="close-pro1 close-css pro-close-css">&times;</span>
  <span class="arrow-span left-arrow open-pro13"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-pro2"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content-css  pro-modal-content">
  
        	<div class="width100 text-center">
        		<img src="img/product1.png" class="pro-jpg text-center">
        	</div>
            <h2 class="pro-title text-center"><?php echo _HOME_PRO1 ?></h2>
            <ul class="service-ul">
            	<li class="service-li"><?php echo _HOME_PRO_LI1 ?></li>
                <li class="service-li"><?php echo _HOME_PRO_LI2 ?></li>
                <li class="service-li"><?php echo _HOME_PRO_LI3 ?></li>
                <li class="service-li"><?php echo _HOME_PRO_LI4 ?></li>
            </ul>
            <div class="text-center">
                <img src="img/pro1-des.jpg" class="pro-des" alt="<?php echo _HOME_PRO1 ?>" title="<?php echo _HOME_PRO1 ?>">
                <div class="clear"></div>
                <img src="img/pro1-des2.jpg" class="pro-des" alt="<?php echo _HOME_PRO1 ?>" title="<?php echo _HOME_PRO1 ?>">
            </div>             				
  </div>
</div>

<div id="pro2" class="pro1-css modal-css">
  <span class="close-pro2 close-css pro-close-css">&times;</span>
  <span class="arrow-span left-arrow open-pro1"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-pro3"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content-css  pro-modal-content">
  
        	<div class="width100 text-center">
        		<img src="img/product2.png" class="pro-jpg text-center">
        	</div>
            <h2 class="pro-title text-center"><?php echo _HOME_PRO2 ?></h2>
            <ul class="service-ul">
            	<li class="service-li"><?php echo _HOME_PRO2_LI1 ?></li>
                <li class="service-li"><?php echo _HOME_PRO2_LI2 ?></li>
                <li class="service-li"><?php echo _HOME_PRO2_LI3 ?></li>
                <li class="service-li"><?php echo _HOME_PRO2_LI4 ?></li>
                <li class="service-li"><?php echo _HOME_PRO2_LI5 ?></li>
                <li class="service-li"><?php echo _HOME_PRO2_LI6 ?></li>               
            </ul>
            <div class="text-center">
                <img src="img/<?php echo _HOME_PRO2_IMG1 ?>.jpg" class="pro-des2" alt="<?php echo _HOME_PRO2 ?>" title="<?php echo _HOME_PRO2 ?>">
                <div class="clear"></div>
                <img src="img/pro2-des2.jpg" class="pro-des2" alt="<?php echo _HOME_PRO2 ?>" title="<?php echo _HOME_PRO2 ?>">
                                
            </div>             				
  </div>
</div>
<div id="pro3" class="pro1-css modal-css">
  <span class="close-pro3 close-css pro-close-css">&times;</span>
  <span class="arrow-span left-arrow open-pro2"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-pro4"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content-css  pro-modal-content">
  
        	<div class="width100 text-center">
        		<img src="img/product3.png" class="pro-jpg text-center">
        	</div>
            <h2 class="pro-title text-center"><?php echo _HOME_PRO3 ?></h2>
            <ul class="service-ul">
            	<li class="service-li"><?php echo _HOME_PRO3_LI1 ?></li>
                <li class="service-li"><?php echo _HOME_PRO3_LI2 ?></li>
                <li class="service-li"><?php echo _HOME_PRO3_LI3 ?></li>
                <li class="service-li"><?php echo _HOME_PRO3_LI4 ?></li>
                <li class="service-li"><?php echo _HOME_PRO3_LI5 ?></li>             
            </ul>
            <div class="text-center">
                <img src="img/pro2-des3.jpg" class="pro-des2" alt="<?php echo _HOME_PRO3 ?>" title="<?php echo _HOME_PRO3 ?>">
                <div class="clear"></div>
                
                                
            </div>             				
  </div>
</div>
<div id="pro4" class="pro1-css modal-css">
  <span class="close-pro4 close-css pro-close-css">&times;</span>
  <span class="arrow-span left-arrow open-pro3"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-pro5"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content-css  pro-modal-content">
  
        	<div class="width100 text-center">
        		<img src="img/product4.png" class="pro-jpg text-center">
        	</div>
            <h2 class="pro-title text-center"><?php echo _HOME_PRO4 ?></h2>
            <ul class="service-ul">
            	<li class="service-li"><?php echo _HOME_PRO4_LI1 ?></li>
                <li class="service-li"><?php echo _HOME_PRO4_LI2 ?></li>
                <li class="service-li"><?php echo _HOME_PRO4_LI3 ?></li>
                <li class="service-li"><?php echo _HOME_PRO4_LI4 ?></li>
                <li class="service-li"><?php echo _HOME_PRO4_LI5 ?></li>             
            </ul>
            				
  </div>
</div>
<div id="pro5" class="pro1-css modal-css">
  <span class="close-pro5 close-css pro-close-css">&times;</span>
  <span class="arrow-span left-arrow open-pro4"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-pro6"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content-css  pro-modal-content">
  
        	<div class="width100 text-center">
        		<img src="img/product5.png" class="pro-jpg text-center">
        	</div>
            <h2 class="pro-title text-center"><?php echo _HOME_PRO5 ?></h2>

            <div class="text-center">
                <img src="img/pro5-des.jpg" class="pro-des2" alt="<?php echo _HOME_PRO5 ?>" title="<?php echo _HOME_PRO5 ?>">
                <div class="clear"></div>
                
                                
            </div>             				
  </div>
</div>
<div id="pro6" class="pro1-css modal-css">
  <span class="close-pro6 close-css pro-close-css">&times;</span>
  <span class="arrow-span left-arrow open-pro5"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-pro7"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content-css  pro-modal-content">
  
        	<div class="width100 text-center">
        		<img src="img/product6.png" class="pro-jpg text-center">
        	</div>
            <h2 class="pro-title text-center"><?php echo _HOME_PRO6 ?></h2>
            <ul class="service-ul">
            	<li class="service-li"><?php echo _HOME_PRO6_LI1 ?></li>
                <li class="service-li"><?php echo _HOME_PRO6_LI2 ?></li>
                <li class="service-li"><?php echo _HOME_PRO6_LI3 ?></li>
                <li class="service-li"><?php echo _HOME_PRO6_LI4 ?></li>
                <li class="service-li"><?php echo _HOME_PRO6_LI5 ?></li>
                <li class="service-li"><?php echo _HOME_PRO6_LI6 ?></li>             
            </ul>
            <div class="text-center">
                <img src="img/pro6-des.jpg" class="pro-des2" alt="<?php echo _HOME_PRO6 ?>" title="<?php echo _HOME_PRO6 ?>">
                <div class="clear"></div>
                
                                
            </div>             				
  </div>
</div>
<div id="pro7" class="pro1-css modal-css">
  <span class="close-pro7 close-css pro-close-css">&times;</span>
  <span class="arrow-span left-arrow open-pro6"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-pro8"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content-css  pro-modal-content">
  
        	<div class="width100 text-center">
        		<img src="img/product7.png" class="pro-jpg text-center">
        	</div>
            <h2 class="pro-title text-center"><?php echo _HOME_PRO7 ?></h2>
            <ul class="service-ul">
            	<li class="service-li"><?php echo _HOME_PRO7_LI1 ?></li>
                <li class="service-li"><?php echo _HOME_PRO7_LI2 ?></li>
                <li class="service-li"><?php echo _HOME_PRO7_LI3 ?></li>
                <li class="service-li"><?php echo _HOME_PRO7_LI4 ?></li>
                <li class="service-li"><?php echo _HOME_PRO7_LI5 ?></li>             
            </ul>            				
  </div>
</div>
<div id="pro8" class="pro1-css modal-css">
  <span class="close-pro8 close-css pro-close-css">&times;</span>
  <span class="arrow-span left-arrow open-pro7"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-pro9"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content-css  pro-modal-content">
  
        	<div class="width100 text-center">
        		<img src="img/product8.png" class="pro-jpg text-center">
        	</div>
            <h2 class="pro-title text-center"><?php echo _HOME_PRO8 ?></h2>
            <ul class="service-ul">
            	<li class="service-li"><?php echo _HOME_PRO8_LI1 ?></li>
                <li class="service-li"><?php echo _HOME_PRO8_LI2 ?></li>
                <li class="service-li"><?php echo _HOME_PRO8_LI3 ?></li>
                <li class="service-li"><?php echo _HOME_PRO8_LI4 ?></li>
                <li class="service-li"><?php echo _HOME_PRO8_LI5 ?></li>
                         
            </ul>
            <div class="text-center">
                <img src="img/pro8-des.jpg" class="pro-des2" alt="<?php echo _HOME_PRO8 ?>" title="<?php echo _HOME_PRO8 ?>">
                <div class="clear"></div>
                
                                
            </div>             				
  </div>
</div>
<div id="pro9" class="pro1-css modal-css">
  <span class="close-pro9 close-css pro-close-css">&times;</span>
  <span class="arrow-span left-arrow open-pro8"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-pro10"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content-css  pro-modal-content">
  
        	<div class="width100 text-center">
        		<img src="img/product9.png" class="pro-jpg text-center">
        	</div>
            <h2 class="pro-title text-center"><?php echo _HOME_PRO9 ?></h2>
            <ul class="service-ul">
            	<li class="service-li"><?php echo _HOME_PRO9_LI1 ?></li>
                <li class="service-li"><?php echo _HOME_PRO9_LI2 ?></li>
                <li class="service-li"><?php echo _HOME_PRO9_LI3 ?></li>
                <li class="service-li"><?php echo _HOME_PRO9_LI4 ?></li>
                <li class="service-li"><?php echo _HOME_PRO9_LI5 ?></li>
                         
            </ul>
               				
  </div>
</div>
<div id="pro10" class="pro1-css modal-css">
  <span class="close-pro10 close-css pro-close-css">&times;</span>
  <span class="arrow-span left-arrow open-pro9"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-pro11"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content-css  pro-modal-content">
  
        	<div class="width100 text-center">
        		<img src="img/product10.png" class="pro-jpg text-center">
        	</div>
            <h2 class="pro-title text-center"><?php echo _HOME_PRO10 ?></h2>
            <ul class="service-ul">
            	<li class="service-li"><?php echo _HOME_PRO10_LI1 ?></li>
                <li class="service-li"><?php echo _HOME_PRO10_LI2 ?></li>
                <li class="service-li"><?php echo _HOME_PRO10_LI3 ?></li>
                <li class="service-li"><?php echo _HOME_PRO10_LI4 ?></li>
                <li class="service-li"><?php echo _HOME_PRO10_LI5 ?></li>
                <li class="service-li"><?php echo _HOME_PRO10_LI6 ?></li>       
            </ul>
             <div class="text-center">
                <img src="img/pro10-des.jpg" class="pro-des2" alt="<?php echo _HOME_PRO10 ?>" title="<?php echo _HOME_PRO10 ?>">
                <div class="clear"></div>
                
                                
            </div>           				
  </div>
</div>
<div id="pro11" class="pro1-css modal-css">
  <span class="close-pro10 close-css pro-close-css">&times;</span>
  <span class="arrow-span left-arrow open-pro10"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-pro12"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content-css  pro-modal-content">
  
        	<div class="width100 text-center">
        		<img src="img/product11.png" class="pro-jpg text-center">
        	</div>
            <h2 class="pro-title text-center"><?php echo _HOME_PRO11 ?></h2>

             <div class="text-center">
                <img src="img/pro11-des.jpg" class="pro-des2" alt="<?php echo _HOME_PRO11 ?>" title="<?php echo _HOME_PRO11 ?>">
                <div class="clear"></div>
                
                                
            </div>           				
  </div>
</div>
<div id="pro12" class="pro1-css modal-css">
  <span class="close-pro12 close-css pro-close-css">&times;</span>
  <span class="arrow-span left-arrow open-pro11"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-pro13"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content-css  pro-modal-content">
  


             <div class="text-center">
                <img src="img/<?php echo _HOME_PRO12p ?>.jpg" class="pro-des2" >
                <div class="clear"></div>
                
                                
            </div>           				
  </div>
</div>
<div id="pro13" class="pro1-css modal-css">
  <span class="close-pro13 close-css pro-close-css">&times;</span>
  <span class="arrow-span left-arrow open-pro12"><img src="img/prev.png" class="arrow-img"></span>
  <span class="arrow-span right-arrow open-pro1"><img src="img/next.png" class="arrow-img"></span>
  <div class="modal-content-css  pro-modal-content">
  
        	<div class="width100 text-center">
        		<img src="img/product13.png" class="pro-jpg text-center">
        	</div>
            <h2 class="pro-title text-center"><?php echo _HOME_PRO13 ?></h2>
            <ul class="service-ul">
            	<li class="service-li"><?php echo _HOME_PRO13_LI1 ?></li>
                <li class="service-li"><?php echo _HOME_PRO13_LI2 ?></li>
                <li class="service-li"><?php echo _HOME_PRO13_LI3 ?></li>
                <li class="service-li"><?php echo _HOME_PRO13_LI4 ?></li>
                <li class="service-li"><?php echo _HOME_PRO13_LI5 ?></li>
                <li class="service-li"><?php echo _HOME_PRO13_LI6 ?></li>
                <li class="service-li"><?php echo _HOME_PRO13_LI7 ?></li>       
            </ul>
             <div class="text-center">
                <img src="img/pro13-des.jpg" class="pro-des2" alt="<?php echo _HOME_PRO13 ?>" title="<?php echo _HOME_PRO13 ?>">
                <div class="clear"></div>
                
                                
            </div>           				
  </div>
</div>
<script src="js/jquery-3.2.0.min.js" type="text/javascript"></script>
<script src="js/jquery.fancybox.js" type="text/javascript"></script>  
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/headroom.js"></script>
<script src="js/main.js"></script>
<link href='https://code.jquery.com/ui/1.12.1/themes/dot-luv/jquery-ui.css' rel='stylesheet'>
<script src="js/jquery-ui.js"></script>
<?php echo '<script type="text/javascript" src="js/jquery.tablesorter.js"></script>'; ?>

<div id="notice-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css notice-modal-content">
    <span class="close-css close-notice" id="close-notice">&times;</span>
    <div class="clear"></div>
    <h1 class="menu-h1 white-text" id="noticeTitle">Title Here</h1>
	<div class="menu-link-container white-text" id="noticeMessage">Message Here</div>
  </div>
</div>
<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();

    }());
</script>

	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });

    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });

    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;

       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";

    });
    </script>
	<script src="js/wow.min.js"></script>
    <script>
     new WOW().init();
    </script>
<script>
// File Upload
//
function ekUpload(){
  function Init() {

    console.log("Upload Initialised");

    var fileSelect    = document.getElementById('file-upload'),
        fileDrag      = document.getElementById('file-drag'),
        submitButton  = document.getElementById('submit-button');

    fileSelect.addEventListener('change', fileSelectHandler, false);

    // Is XHR2 available?
    var xhr = new XMLHttpRequest();
    if (xhr.upload) {
      // File Drop
      fileDrag.addEventListener('dragover', fileDragHover, false);
      fileDrag.addEventListener('dragleave', fileDragHover, false);
      fileDrag.addEventListener('drop', fileSelectHandler, false);
    }
  }

  function fileDragHover(e) {
    var fileDrag = document.getElementById('file-drag');

    e.stopPropagation();
    e.preventDefault();

    fileDrag.className = (e.type === 'dragover' ? 'hover' : 'modal-body file-upload');
  }

  function fileSelectHandler(e) {
    // Fetch FileList object
    var files = e.target.files || e.dataTransfer.files;

    // Cancel event and hover styling
    fileDragHover(e);

    // Process all File objects
    for (var i = 0, f; f = files[i]; i++) {
      parseFile(f);
      uploadFile(f);
    }
  }

  // Output
  function output(msg) {
    // Response
    var m = document.getElementById('messages');
    m.innerHTML = msg;
  }

  function parseFile(file) {

    console.log(file.name);
    output(
      '<strong>' + encodeURI(file.name) + '</strong>'
    );

    // var fileType = file.type;
    // console.log(fileType);
    var imageName = file.name;

    var isGood = (/\.(?=gif|jpg|png|jpeg)/gi).test(imageName);
    if (isGood) {
      document.getElementById('start').classList.add("hidden");
      document.getElementById('response').classList.remove("hidden");
      document.getElementById('notimage').classList.add("hidden");
      // Thumbnail Preview
      document.getElementById('file-image').classList.remove("hidden");
      document.getElementById('file-image').src = URL.createObjectURL(file);
    }
    else {
      document.getElementById('file-image').classList.add("hidden");
      document.getElementById('notimage').classList.remove("hidden");
      document.getElementById('start').classList.remove("hidden");
      document.getElementById('response').classList.add("hidden");
      document.getElementById("file-upload-form").reset();
    }
  }

  function setProgressMaxValue(e) {
    var pBar = document.getElementById('file-progress');

    if (e.lengthComputable) {
      pBar.max = e.total;
    }
  }

  function updateFileProgress(e) {
    var pBar = document.getElementById('file-progress');

    if (e.lengthComputable) {
      pBar.value = e.loaded;
    }
  }

  function uploadFile(file) {

    var xhr = new XMLHttpRequest(),
      fileInput = document.getElementById('class-roster-file'),
      pBar = document.getElementById('file-progress'),
      fileSizeLimit = 1024; // In MB
    if (xhr.upload) {
      // Check if file is less than x MB
      if (file.size <= fileSizeLimit * 1024 * 1024) {
        // Progress bar
        pBar.style.display = 'inline';
        xhr.upload.addEventListener('loadstart', setProgressMaxValue, false);
        xhr.upload.addEventListener('progress', updateFileProgress, false);

        // File received / failed
        xhr.onreadystatechange = function(e) {
          if (xhr.readyState == 4) {
            // Everything is good!

            // progress.className = (xhr.status == 200 ? "success" : "failure");
            // document.location.reload(true);
          }
        };

        // Start upload
        xhr.open('POST', document.getElementById('file-upload-form').action, true);
        xhr.setRequestHeader('X-File-Name', file.name);
        xhr.setRequestHeader('X-File-Size', file.size);
        xhr.setRequestHeader('Content-Type', 'multipart/form-data');
        xhr.send(file);
      } else {
        output('Please upload a smaller file (< ' + fileSizeLimit + ' MB).');
      }
    }
  }

  // Check for the various File API support.
  if (window.File && window.FileList && window.FileReader) {
    Init();
  } else {
    document.getElementById('file-drag').style.display = 'none';
  }
}
ekUpload();
</script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>

<script>
$("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          $(this).text("Copied");
          $(this).css("background-color","#002b5d");
          // putNoticeJavascript("Copied!! ","");
      });
      $("#invest-now-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
</script>

<script type="text/javascript" src="js/rolldate.min.js"></script>
<script>
		window.onload = function() {



			new Rolldate({
				el: '#register_dob',
				format: 'YYYY-MM-DD',
				beginYear: 1900,
				endYear: 2100,
				init: function() {
					console.log('start');
				},
				moveEnd: function(scroll) {
					console.log(scroll)
					console.log('end');
				},
				confirm: function(date) {
					console.log(date)
					console.log('confirm');
				},
				cancel: function() {
					console.log('cancel');
				}
			})
			new Rolldate({
				el: '#update_dob',
				format: 'YYYY-MM-DD',
				beginYear: 1900,
				endYear: 2100,
				init: function() {
					console.log('start');
				},
				moveEnd: function(scroll) {
					console.log(scroll)
					console.log('end');
				},
				confirm: function(date) {
					console.log(date)
					console.log('confirm');
				},
				cancel: function() {
					console.log('cancel');
				}
			})
}
</script>
<script>
function myFunctionA()
{
    var x = document.getElementById("current_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("retype_new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionD()
{
    var x = document.getElementById("password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionE()
{
    var x = document.getElementById("confirmPassword");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>
<script>
function goBack() {
  window.history.back();
}
</script>
<script>
  $(function(){
    var click;

		window.onbeforeunload = function() {	// prevent reload alert
 		if(click == 1) {
   return 'Request in progress....are you sure you want to continue?';
 		}
	};

		var type = $("input[name='getValue']").val();
		var session = $("input[name='getValueSession']").val();
		if (type == 11 && session == 1) {
			$("#frmExcelImport").slideUp(function(){
				$("#frmExcelImport").hide();
			});
		}
    $("#Rank").click(function(){
			click = 1;
      $("#loadingAnimate").show();
      $("#buttonName").text("Loading 1 of 3...");
      $.ajax({
        url: 'rankIdentify.php',
        // data : '{}',
        type: 'post',
        // dataType: 'json',

        success:function(response){
          // alert("done");
          $("#buttonName").text("Loading 2 of 3...");
          $.ajax({
            url: 'rankIdentify.php',
            type: 'post',
            success:function(response){
              $("#buttonName").text("Loading 3 of 3...");
              $.ajax({
                url: 'rankIdentify.php',
                type: 'post',
                success:function(response){
                  putNoticeJavascript("Notice !! ","Rank Successfully Updated.");
                  $("#buttonName").text("Done Update");
                  $("#loadingAnimate").hide();
                  $("#Rank").prop("disabled",true);
                  $("#Rank").css("background-color","#2c186f");
									click = 0;
                }
              });
            }
          });
        }
      });
    });

    $("#Daily").click(function(){
      click = 1;
      $("#loadingAnimate2").show();
      $("#buttonName2").text("Loading 1 of 2...");
      $.ajax({
        url: 'dailyBonus.php',
        // data : '{}',
        type: 'post',
        // dataType: 'json',

        success:function(response){
          // alert("done");
					$("#buttonName2").text("Loading 2 of 2...");
					$.ajax({
						url: 'utilities/releaseUnreleaseBonus.php',
						type: 'post',
						success:function(response){
							putNoticeJavascript("Notice !! ","Daily Spread Successfully Updated.");
							$("#buttonName2").text("Done Update");
							$("#loadingAnimate2").hide();
							$("#Daily").prop("disabled",true);
							$("#Daily").css("background-color","#2c186f");
							click = 0;
						}
					});
        }
      });
    });
		var type2 = $("input[name='getValue2']").val();
		if (type2 == 10) {
			$("#frmExcelImport2").slideUp(function(){
				$("#frmExcelImport2").hide();
			});
		}
		$("#Monthly").click(function(){
			click = 1;
			$("#loadingAnimate3").show();
			$("#buttonName3").text("Loading 1 of 1...");
			$.ajax({
				url: 'monthlyProfit.php',
				// data : '{}',
				type: 'post',
				// dataType: 'json',

				success:function(response){
					// alert("done");
					$("#loadingAnimate3").hide();
					$("#buttonName3").text("Done Update");
					$("#Monthly").prop("disabled",true);
					$("#Monthly").css("background-color","#2c186f");
					putNoticeJavascript("Notice !! ","Monthly Profit Successfully Updated.");
					click = 0;
				}
			});
		});
		$("#Reset").click(function(){
			var confirmReset = confirm("The Reset Report Can't be Restored!");
			var resetBtn = $("#Reset").val();
			if (confirmReset == true) {
				click = 1;
				$("#loadingAnimateDelete").show();
				$("#buttonNameDelete").text("Loading...");
				$.ajax({
					url: 'utilities/resetDailySpread.php',
					// data : {resetButton:resetBtn},
					type: 'post',
					// dataType: 'json',

					success:function(response){
						// alert("done");
						// var success = response[0]['success'];
						$("#loadingAnimateDelete").hide();
						// setTimeout(function(){
						$("#buttonNameDelete").text("Done");
						click = 0;
						putNoticeJavascript("Notice !! ","Successfully Reset Daily Spread.");
						// },5000);
						// $("#Reset").prop("disabled",true);
						// $("#Reset").css("background-color","#2c186f");
					}
				});
			}
		});
		$("#Reset2").click(function(){
			var confirmReset2 = confirm("The Reset Report Can't be Restored!");
			var resetBtn2 = $("#Reset2").val();
			if (confirmReset2 == true) {
				click = 1;
				$("#loadingAnimateDelete2").show();
				$("#buttonNameDelete2").text("Loading...");
				$.ajax({
					url: 'utilities/resetMonthlyProfit.php',
					// data : {resetButton:resetBtn},
					type: 'post',
					// dataType: 'json',

					success:function(response){
						// alert("done");
						// var success = response[0]['success'];
						$("#loadingAnimateDelete2").hide();
						// setTimeout(function(){
						$("#buttonNameDelete2").text("Done");
						click = 0;
						putNoticeJavascript("Notice !! ","Successfully Reset Monthly Profit.");
						// },5000);
						// $("#Reset").prop("disabled",true);
						// $("#Reset").css("background-color","#2c186f");
					}
				});
			}
		});

		$("#ResetMembers").click(function(){
			var confirmResetMember = confirm("The Reset Balance Can't be Restored!");
			var resetBtnMember = $("#ResetMembers").val();
			if (confirmResetMember == true) {
				click = 1;
				$("#loadingAnimateDeleteMembers").show();
				$("#buttonNameDeleteMembers").text("Loading...");
				$.ajax({
					url: 'utilities/resetBalance.php',
					// data : {resetButton:resetBtn},
					type: 'post',
					// dataType: 'json',

					success:function(response){
						// alert("done");
						// var success = response[0]['success'];
						$("#loadingAnimateDeleteMembers").hide();
						// setTimeout(function(){
						$("#buttonNameDeleteMembers").text("Done");
						click = 0;
						putNoticeJavascript("Notice !! ","Successfully Reset Member's Balance.");
						// },5000);
						// $("#Reset").prop("disabled",true);
						// $("#Reset").css("background-color","#2c186f");
					}
				});
			}
		});

		$(".reset-checkbox").change(function(){
			if ($(".reset-checkbox").is(':checked')) {
				$("#Reset").fadeToggle();
			}
			if ($(".reset-checkbox").is(':not(:checked)')) {
				$("#Reset").fadeToggle();
			}
		});

		if ($(".reset-checkbox").is(':checked')) {
			$("#Reset").show();
		}
		else if ($(".reset-checkbox").is(':not(:checked)')) {
			$("#Reset").hide();
		}
		$("#nameCheck").click(function(){
			if ($(".reset-checkbox").is(':checked')) {
				$(".reset-checkbox").prop("checked",false);
				$("#Reset").fadeToggle();
			}else if ($(".reset-checkbox").is(':not(:checked)')) {
				$(".reset-checkbox").prop("checked",true);
				$("#Reset").fadeToggle();
			}
		});
		$(".reset-checkbox2").change(function(){
			if ($(".reset-checkbox2").is(':checked')) {
				$("#monthlyProfitBtn").toggle();
				$("#Reset2").fadeToggle();
			}
			if ($(".reset-checkbox2").is(':not(:checked)')) {
				$("#monthlyProfitBtn").fadeToggle();
				$("#Reset2").toggle();
			}
		});
		$("#nameCheck2").click(function(){
			if ($(".reset-checkbox2").is(':checked')) {
				$(".reset-checkbox2").prop("checked",false);
				$("#monthlyProfitBtn").fadeToggle();
				$("#Reset2").toggle();
			}else if ($(".reset-checkbox2").is(':not(:checked)')) {
				$(".reset-checkbox2").prop("checked",true);
				$("#monthlyProfitBtn").toggle();
				$("#Reset2").fadeToggle();
			}
		});

		$(".reset-checkboxMembers").change(function(){
			if ($(".reset-checkboxMembers").is(':checked')) {
				$("#Rank").toggle();
				$("#ResetMembers").fadeToggle();
			}
			if ($(".reset-checkboxMembers").is(':not(:checked)')) {
				$("#Rank").fadeToggle();
				$("#ResetMembers").toggle();
			}
		});

		if ($(".reset-checkbox").is(':checked')) {
			$("#Reset").show();
		}
		else if ($(".reset-checkbox").is(':not(:checked)')) {
			$("#Reset").hide();
		}

		$("#nameCheckMembers").click(function(){
			if ($(".reset-checkboxMembers").is(':checked')) {
				$(".reset-checkboxMembers").prop("checked",false);
				$("#Rank").fadeToggle();
				$("#ResetMembers").toggle();
			}else if ($(".reset-checkboxMembers").is(':not(:checked)')) {
				$(".reset-checkboxMembers").prop("checked",true);
				$("#Rank").toggle();
				$("#ResetMembers").fadeToggle();
			}
		});
  });
</script>

<script id="rendered-js">
$("#menu-toggle").click(function (e) {
  e.preventDefault();
  $("#wrapper").toggleClass("toggled");
});
$("#menu-toggle-2").click(function (e) {
  e.preventDefault();
  $("#wrapper").toggleClass("toggled-2");
  $('#menu ul').hide();
});

function initMenu() {
  $('#menu ul').hide();
  $('#menu ul').children('.current').parent().show();
  //$('#menu ul:first').show();
  $('#menu li a').click(
  function () {
    var checkElement = $(this).next();
    if (checkElement.is('ul') && checkElement.is(':visible')) {
      return false;
    }
    if (checkElement.is('ul') && !checkElement.is(':visible')) {
      $('#menu ul:visible').slideUp('normal');
      checkElement.slideDown('normal');
      return false;
    }
  });

}
$(document).ready(function () {
  initMenu();
});
//# sourceURL=pen.js
    </script>
<!--
  <script type="text/javascript" src="js/firefly.js"></script>
<script>
  $.firefly({
    color: '#06f4f4',
    minPixel: 1,
    maxPixel: 4,
    total : 60,
    on: '#firefly'
});</script>-->
<script src="js/jquery.ripples.js"></script>
<script>
$(document).ready(function() {
	try {
		$('.bottom-water-div').ripples({
			resolution: 412,
			dropRadius: 20, //px
			perturbance: 0.007,
			interactive: true
		});		
		$('.bottom-sea-bg').ripples({
			resolution: 412,
			dropRadius: 10, //px
			perturbance: 0.017,
			interactive: true
		});
		$('.bottom-waterfall-bg').ripples({
			resolution: 412,
			dropRadius: 10, //px
			perturbance: 0.017,
			interactive: true
		});		
	}
	catch (e) {
		$('.error').show().text(e);
	}

	$('.js-ripples-disable').on('click', function() {
		$('.bottom-water-div').ripples('destroy');
		$(this).hide();
	});

	$('.js-ripples-play').on('click', function() {
		$('.bottom-water-div').ripples('pause');
	});

	$('.js-ripples-pause').on('click', function() {
		$('.bottom-water-div').ripples('pause');
	});

	// Automatic drops
	setInterval(function() {
		var $el = $('.bottom-water-div');
		var x = Math.random() * $el.outerWidth();
		var y = Math.random() * $el.outerHeight();
		var dropRadius = 20;
		var strength = 0.01 + Math.random() * 0.01;

		$el.ripples('drop', x, y, dropRadius, strength);
	}, 8000);
});
</script>

<!--<script>
        function showDiv() { 
            document.getElementsByClassName('coreli')[0]. 
            style.visibility = 'hidden'; 
        } 
	
		
</script>-->
<script>
function showDiv() {
  document.getElementsByClassName("coreli")[0].style.display = "block";
  document.getElementsByClassName("coreli")[1].style.display = "block";
  document.getElementsByClassName("coreli")[2].style.display = "block";
  document.getElementsByClassName("coreli")[3].style.display = "block";  
}
</script>
<script>
function showDiv1() {
  document.getElementsByClassName("coreli1")[0].style.display = "block";
  document.getElementsByClassName("coreli1")[1].style.display = "block";
  document.getElementsByClassName("coreli1")[2].style.display = "block";
}
</script>
<!--<script>
function showDiv() {
var element = document.getElementsByClassName("coreli");
	
       if(element[0].style.display == 'block')
          element[0].style.display = 'none';
       else
          element[0].style.display = 'block';}
</script>-->
    
<script>
	//Contact
	var login = document.getElementById("login-modal");
	var forgot = document.getElementById("forgot-modal");
	var pro1 = document.getElementById("pro1");
	var pro2 = document.getElementById("pro2");
	var pro3 = document.getElementById("pro3");
	var pro4 = document.getElementById("pro4");
	var pro5 = document.getElementById("pro5");
	var pro6 = document.getElementById("pro6");	
	var pro7 = document.getElementById("pro7");
	var pro8 = document.getElementById("pro8");
	var pro9 = document.getElementById("pro9");
	var pro10 = document.getElementById("pro10");
	var pro11 = document.getElementById("pro11");
	var pro12 = document.getElementById("pro12");		
	var pro13 = document.getElementById("pro13");
	
	var openLogin = document.getElementsByClassName("open-login")[0];
	var openForgot = document.getElementsByClassName("open-forgot")[0];
	var openPro1 = document.getElementsByClassName("open-pro1")[0];
	var openPro1a = document.getElementsByClassName("open-pro1")[1];	
	var openPro1b = document.getElementsByClassName("open-pro1")[2];
	var openPro2 = document.getElementsByClassName("open-pro2")[0];
	var openPro2a = document.getElementsByClassName("open-pro2")[1];	
	var openPro2b = document.getElementsByClassName("open-pro2")[2];
	var openPro3 = document.getElementsByClassName("open-pro3")[0];
	var openPro3a = document.getElementsByClassName("open-pro3")[1];	
	var openPro3b = document.getElementsByClassName("open-pro3")[2];
	var openPro4 = document.getElementsByClassName("open-pro4")[0];
	var openPro4a = document.getElementsByClassName("open-pro4")[1];	
	var openPro4b = document.getElementsByClassName("open-pro4")[2];
	var openPro5 = document.getElementsByClassName("open-pro5")[0];
	var openPro5a = document.getElementsByClassName("open-pro5")[1];	
	var openPro5b = document.getElementsByClassName("open-pro5")[2];
	var openPro6 = document.getElementsByClassName("open-pro6")[0];
	var openPro6a = document.getElementsByClassName("open-pro6")[1];	
	var openPro6b = document.getElementsByClassName("open-pro6")[2];
	var openPro7 = document.getElementsByClassName("open-pro7")[0];
	var openPro7a = document.getElementsByClassName("open-pro7")[1];	
	var openPro7b = document.getElementsByClassName("open-pro7")[2];
	var openPro8 = document.getElementsByClassName("open-pro8")[0];
	var openPro8a = document.getElementsByClassName("open-pro8")[1];	
	var openPro8b = document.getElementsByClassName("open-pro8")[2];
	var openPro9 = document.getElementsByClassName("open-pro9")[0];
	var openPro9a = document.getElementsByClassName("open-pro9")[1];	
	var openPro9b = document.getElementsByClassName("open-pro9")[2];
	var openPro10 = document.getElementsByClassName("open-pro10")[0];
	var openPro10a = document.getElementsByClassName("open-pro10")[1];	
	var openPro10b = document.getElementsByClassName("open-pro10")[2];
	var openPro11 = document.getElementsByClassName("open-pro11")[0];
	var openPro11a = document.getElementsByClassName("open-pro11")[1];	
	var openPro11b = document.getElementsByClassName("open-pro11")[2];
	var openPro12 = document.getElementsByClassName("open-pro12")[0];
	var openPro12a = document.getElementsByClassName("open-pro12")[1];	
	var openPro12b = document.getElementsByClassName("open-pro12")[2];
	var openPro13 = document.getElementsByClassName("open-pro13")[0];
	var openPro13a = document.getElementsByClassName("open-pro13")[1];	
	var openPro13b = document.getElementsByClassName("open-pro13")[2];
	
	var closeLogin = document.getElementsByClassName("close-login")[0];	
	var closeForgot = document.getElementsByClassName("close-forgot")[0];	
	var closePro1 = document.getElementsByClassName("close-pro1")[0];
	var closePro2 = document.getElementsByClassName("close-pro2")[0];
	var closePro3 = document.getElementsByClassName("close-pro3")[0];
	var closePro4 = document.getElementsByClassName("close-pro4")[0];
	var closePro5 = document.getElementsByClassName("close-pro5")[0];
	var closePro6 = document.getElementsByClassName("close-pro6")[0];
	var closePro7 = document.getElementsByClassName("close-pro7")[0];
	var closePro8 = document.getElementsByClassName("close-pro8")[0];
	var closePro9 = document.getElementsByClassName("close-pro9")[0];
	var closePro10 = document.getElementsByClassName("close-pro10")[0];
	var closePro11 = document.getElementsByClassName("close-pro11")[0];
	var closePro12 = document.getElementsByClassName("close-pro12")[0];
	var closePro13 = document.getElementsByClassName("close-pro13")[0];

	if(openLogin){
	openLogin.onclick = function() {
	  login.style.display = "block";
	}
	}	
	if(openForgot){
	openForgot.onclick = function() {
	  forgot.style.display = "block";
	  login.style.display = "none";
	}
	}
	if(openPro1){
	openPro1.onclick = function() {
	  pro1.style.display = "block";
	}
	}	
	if(openPro1a){
	openPro1a.onclick = function() {
 	  pro1.style.display = "block";
	  pro2.style.display = "none";
	  pro13.style.display = "none";
	}
	}
	if(openPro1b){
	openPro1b.onclick = function() {
 	  pro1.style.display = "block";
	  pro2.style.display = "none";
	  pro13.style.display = "none";
	}
	}
	if(openPro2){
	openPro2.onclick = function() {
	  pro2.style.display = "block";
	}
	}	
	if(openPro2a){
	openPro2a.onclick = function() {
 	  pro2.style.display = "block";
	  pro1.style.display = "none";
	  pro3.style.display = "none";
	}
	}
	if(openPro2b){
	openPro2b.onclick = function() {
 	  pro2.style.display = "block";
	  pro1.style.display = "none";
	  pro3.style.display = "none";
	}
	}
	if(openPro3){
	openPro3.onclick = function() {
	  pro3.style.display = "block";
	}
	}	
	if(openPro3a){
	openPro3a.onclick = function() {
 	  pro3.style.display = "block";
	  pro2.style.display = "none";
	  pro4.style.display = "none";
	}
	}
	if(openPro3b){
	openPro3b.onclick = function() {
 	  pro3.style.display = "block";
	  pro2.style.display = "none";
	  pro4.style.display = "none";
	}
	}
	if(openPro4){
	openPro4.onclick = function() {
	  pro4.style.display = "block";
	}
	}	
	if(openPro4a){
	openPro4a.onclick = function() {
 	  pro4.style.display = "block";
	  pro3.style.display = "none";
	  pro5.style.display = "none";
	}
	}
	if(openPro4b){
	openPro4b.onclick = function() {
 	  pro4.style.display = "block";
	  pro3.style.display = "none";
	  pro5.style.display = "none";
	}
	}	
	if(openPro5){
	openPro5.onclick = function() {
	  pro5.style.display = "block";
	}
	}	
	if(openPro5a){
	openPro5a.onclick = function() {
 	  pro5.style.display = "block";
	  pro4.style.display = "none";
	  pro6.style.display = "none";
	}
	}
	if(openPro5b){
	openPro5b.onclick = function() {
 	  pro5.style.display = "block";
	  pro4.style.display = "none";
	  pro6.style.display = "none";
	}
	}		
	if(openPro6){
	openPro6.onclick = function() {
	  pro6.style.display = "block";
	}
	}	
	if(openPro6a){
	openPro6a.onclick = function() {
 	  pro6.style.display = "block";
	  pro5.style.display = "none";
	  pro7.style.display = "none";
	}
	}
	if(openPro6b){
	openPro6b.onclick = function() {
 	  pro6.style.display = "block";
	  pro5.style.display = "none";
	  pro7.style.display = "none";
	}
	}	
	if(openPro7){
	openPro7.onclick = function() {
	  pro7.style.display = "block";
	}
	}	
	if(openPro7a){
	openPro7a.onclick = function() {
 	  pro7.style.display = "block";
	  pro6.style.display = "none";
	  pro8.style.display = "none";
	}
	}
	if(openPro7b){
	openPro7b.onclick = function() {
 	  pro7.style.display = "block";
	  pro6.style.display = "none";
	  pro8.style.display = "none";
	}
	}		
	if(openPro8){
	openPro8.onclick = function() {
	  pro8.style.display = "block";
	}
	}	
	if(openPro8a){
	openPro8a.onclick = function() {
 	  pro8.style.display = "block";
	  pro7.style.display = "none";
	  pro9.style.display = "none";
	}
	}
	if(openPro8b){
	openPro8b.onclick = function() {
 	  pro8.style.display = "block";
	  pro9.style.display = "none";
	  pro7.style.display = "none";
	}
	}
	if(openPro9){
	openPro9.onclick = function() {
	  pro9.style.display = "block";
	}
	}	
	if(openPro9a){
	openPro9a.onclick = function() {
 	  pro9.style.display = "block";
	  pro8.style.display = "none";
	  pro10.style.display = "none";
	}
	}
	if(openPro9b){
	openPro9b.onclick = function() {
 	  pro9.style.display = "block";
	  pro8.style.display = "none";
	  pro10.style.display = "none";
	}
	}
	if(openPro10){
	openPro10.onclick = function() {
	  pro10.style.display = "block";
	}
	}	
	if(openPro10a){
	openPro10a.onclick = function() {
 	  pro10.style.display = "block";
	  pro9.style.display = "none";
	  pro11.style.display = "none";
	}
	}
	if(openPro10b){
	openPro10b.onclick = function() {
 	  pro10.style.display = "block";
	  pro9.style.display = "none";
	  pro11.style.display = "none";
	}
	}
	if(openPro11){
	openPro11.onclick = function() {
	  pro11.style.display = "block";
	}
	}	
	if(openPro11a){
	openPro11a.onclick = function() {
 	  pro11.style.display = "block";
	  pro10.style.display = "none";
	  pro12.style.display = "none";
	}
	}
	if(openPro11b){
	openPro11b.onclick = function() {
 	  pro11.style.display = "block";
	  pro10.style.display = "none";
	  pro12.style.display = "none";
	}
	}
	if(openPro12){
	openPro12.onclick = function() {
	  pro12.style.display = "block";
	}
	}	
	if(openPro12a){
	openPro12a.onclick = function() {
 	  pro12.style.display = "block";
	  pro11.style.display = "none";
	  pro13.style.display = "none";
	}
	}
	if(openPro12b){
	openPro12b.onclick = function() {
 	  pro12.style.display = "block";
	  pro11.style.display = "none";
	  pro13.style.display = "none";
	}
	}
	if(openPro13){
	openPro13.onclick = function() {
	  pro13.style.display = "block";
	}
	}	
	if(openPro13a){
	openPro13a.onclick = function() {
 	  pro13.style.display = "block";
	  pro12.style.display = "none";
	  pro1.style.display = "none";
	}
	}
	if(openPro13b){
	openPro13b.onclick = function() {
 	  pro13.style.display = "block";
	  pro12.style.display = "none";
	  pro1.style.display = "none";
	}
	}	



	if(closeLogin){
	closeLogin.onclick = function() {
 	  login.style.display = "none";
	}
	}
	if(closeForgot){
	closeForgot.onclick = function() {
 	  forgot.style.display = "none";
	}
	}	
	if(closePro1){
	closePro1.onclick = function() {
 	  pro1.style.display = "none";
	}
	}	
	if(closePro2){
	closePro2.onclick = function() {
 	  pro2.style.display = "none";
	}
	}	
	if(closePro3){
	closePro3.onclick = function() {
 	  pro3.style.display = "none";
	}
	}	
	if(closePro4){
	closePro4.onclick = function() {
 	  pro4.style.display = "none";
	}
	}	
	if(closePro5){
	closePro5.onclick = function() {
 	  pro5.style.display = "none";
	}
	}	
	if(closePro6){
	closePro6.onclick = function() {
 	  pro6.style.display = "none";
	}
	}		
	if(closePro7){
	closePro7.onclick = function() {
 	  pro7.style.display = "none";
	}
	}	
	if(closePro8){
	closePro8.onclick = function() {
 	  pro8.style.display = "none";
	}
	}	
	if(closePro9){
	closePro9.onclick = function() {
 	  pro9.style.display = "none";
	}
	}		
	if(closePro10){
	closePro10.onclick = function() {
 	  pro10.style.display = "none";
	}
	}	
	if(closePro11){
	closePro11.onclick = function() {
 	  pro11.style.display = "none";
	}
	}	
	if(closePro12){
	closePro12.onclick = function() {
 	  pro12.style.display = "none";
	}
	}	
	if(closePro13){
	closePro13.onclick = function() {
 	  pro13.style.display = "none";
	}
	}		
	
	window.onclick = function(event) {
	  if (event.target == login) {
		login.style.display = "none";
	  }	
	  if (event.target == forgot) {
		forgot.style.display = "none";
	  }		  		
	  if (event.target == pro1) {
		pro1.style.display = "none";
	  }	
	  if (event.target == pro2) {
		pro2.style.display = "none";
	  }	
	  if (event.target == pro3) {
		pro3.style.display = "none";
	  }	
	  if (event.target == pro4) {
		pro4.style.display = "none";
	  }	
	  if (event.target == pro5) {
		pro5.style.display = "none";
	  }	
	  if (event.target == pro6) {
		pro6.style.display = "none";
	  }		  
	  if (event.target == pro7) {
		pro7.style.display = "none";
	  }	
	  if (event.target == pro8) {
		pro8.style.display = "none";
	  }	
	  if (event.target == pro9) {
		pro9.style.display = "none";
	  }	
	  if (event.target == pro10) {
		pro10.style.display = "none";
	  }	
	  if (event.target == pro11) {
		pro11.style.display = "none";
	  }	
	  if (event.target == pro12) {
		pro12.style.display = "none";
	  }		  
	  if (event.target == pro13) {
		pro13.style.display = "none";
	  }		  	  	  
	 }
</script>