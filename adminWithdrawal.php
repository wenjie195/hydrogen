<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Withdrawal.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$withdrawalDetails = getWithdrawal($conn, "WHERE status = 'PENDING'");
// $totalPayout = 0;

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://hygeniegroup.com/adminWithdrawal.php" />
<link rel="canonical" href="https://hygeniegroup.com/adminWithdrawal.php" />
<meta property="og:title" content="<?php echo _ADMINHEADER_MEMBER_WITHDRAW ?>  | Hygenie Group" />
<title><?php echo _ADMINHEADER_MEMBER_WITHDRAW ?>  | Hygenie Group</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text" id="firefly">
	<h1 class="small-h1-a text-center white-text"><?php echo _ADMINHEADER_MEMBER_WITHDRAW ?> | <a class="blue-link" href="adminWithdrawalHistory.php"><?php echo _ADMINHEADER_MEMBER_WITHDRAW_HISTORY ?></a> </h1>
    <div class="width100 shipping-div2">

    <?php
    if ($withdrawalDetails)
    {
        $totalCurrentAmount = 0; // initital
        for ($b=0; $b <count($withdrawalDetails) ; $b++)
        {
            $totalCurrentAmount += $withdrawalDetails[$b]->getCurrentAmount();
        }
    }
    ?>

    <?php
    if ($withdrawalDetails)
    {
        $totalFinalAmount = 0; // initital
        for ($b=0; $b <count($withdrawalDetails) ; $b++)
        {
            $totalFinalAmount += $withdrawalDetails[$b]->getFinalAmount();
        }
    }
    ?>

    <h3 class="white-text"><?php echo _ADMINVIEWBALANCE_PAYOUT ?> <?php echo $totalCurrentAmount;?> PV </h3>

    <h3 class="white-text"><?php echo _ADMINVIEWBALANCE_PAYOUT ?> RM <?php echo $totalFinalAmount;?></h3>
        <div class="overflow-scroll-div">
            <table class="table-css fix-th">
                <thead>
                    <tr>
                        <th><?php echo _ADMINVIEWBALANCE_NO ?></th>
                        <th><?php echo _JS_FULLNAME ?></th>
                        <th><?php echo _BANKDETAILS_ACC_NAME ?></th>
                        <th><?php echo _BANKDETAILS_BANK ?></th>
                        <th><?php echo _BANKDETAILS_ACC_NO ?></th>
                        <th><?php echo _PRODUCT_AMOUNT ?> (PV)</th>
                        <th><?php echo _PRODUCT_AMOUNT ?> (RM)</th>
                        <th><?php echo _DAILY_DATE ?></th>
                        <th><?php echo _ADMINVIEWBALANCE_ACTION ?> (<?php echo _ADMINVIEWBALANCE_APPROVE ?>)</th>
                        <th><?php echo _ADMINVIEWBALANCE_ACTION ?> (<?php echo _ADMINVIEWBALANCE_REJECT ?>)</th>
                    </tr>
                </thead>
                <tbody>
                  <?php
                  if($withdrawalDetails)
                  {
                    for($cnt = 0;$cnt < count($withdrawalDetails) ;$cnt++)
                    {
                    ?>
                      <tr>
                        <td><?php echo ($cnt+1)?></td>

                        <td>
                            <?php 
                                $userUid = $withdrawalDetails[$cnt]->getUid();
                                $conn = connDB();
                                $withdrawalUser = getUser($conn, "WHERE uid = ?",array("uid"),array($userUid), "s");
                                echo $lastname = $withdrawalUser[0]->getLastname();
                                echo $firstname = $withdrawalUser[0]->getFirstname();
                            ?>
                        </td>

                        <td><?php echo $withdrawalDetails[$cnt]->getBankAccHolder();?></td>
                        <td><?php echo $withdrawalDetails[$cnt]->getBankName();?></td>
                        <td><?php echo $withdrawalDetails[$cnt]->getBankAccNo();?></td>
                        <td><?php echo $withdrawalDetails[$cnt]->getCurrentAmount();?></td>
                        <td><?php echo $withdrawalDetails[$cnt]->getFinalAmount();?></td>
                        <td><?php echo date('d/m/Y',strtotime($withdrawalDetails[$cnt]->getDateCreated()));?></td>

                        <td>
                            <form action="utilities/adminWithdrawalApprovedFunction.php" method="POST">
                                <button class="clean blue-ow-btn" type="submit" name="withdrawal_uid" value="<?php echo $withdrawalDetails[$cnt]->getWithdrawalUid();?>">
                                    <?php echo _ADMINVIEWBALANCE_APPROVE ?>
                                </button>
                            </form>
                        </td>

                        <td>
                            <form action="utilities/adminWithdrawalRejectedFunction.php" method="POST">
                                <button class="clean blue-ow-btn" type="submit" name="withdrawal_uid" value="<?php echo $withdrawalDetails[$cnt]->getWithdrawalUid();?>">
                                    <?php echo _ADMINVIEWBALANCE_REJECT ?>
                                </button>
                            </form>
                        </td>

                      </tr>
                    <?php
                    }
                    ?>
                  <?php
                  }
                  ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php include 'js.php'; ?>

</body>
</html>