<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Bonus.php';
// require_once dirname(__FILE__) . '/classes/BonusOverriding.php';
// require_once dirname(__FILE__) . '/classes/BonusPoolFund.php';
// require_once dirname(__FILE__) . '/classes/BonusRecord.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
	<meta property="og:url" content="https://hygeniegroup.com/userViewTeamSales.php" />
    <link rel="canonical" href="https://hygeniegroup.com/userViewTeamSales.php" /> 
    <meta property="og:title" content="<?php echo _USERDASHBOARD_TEAM_SALES ?> (PV)   | Hygenie Group" />
    <title><?php echo _USERDASHBOARD_TEAM_SALES ?> (PV)  | Hygenie Group</title>

	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance darkbg min-height big-black-text user-dash user-dash2 ow-white-css" id="firefly">

  <?php
  $conn = connDB();
  if(isset($_POST['downline_uid']))
  {
    $uid = $_POST['downline_uid'];
    $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
    $userData = $userDetails[0];

    $userRank = $userDetails[0]->getRank();
    if($userRank == 'District Manager')
    {
      $userRanking = 'Region Manager';
    }
    elseif($userRank == 'Manager')
    {
      $userRanking = 'Sales Manager';
    }
    elseif($userRank == 'Senior Manager')
    {
      $userRanking = 'Markerting Manager';
    }
    else
    {
      $userRanking = $userRank;
    }
  }
  ?>

  <?php
  $conn = connDB();
  if(isset($_POST['downline_uid']))
  {
    $uid = $_POST['downline_uid'];
    // $userUid = $getWho[$cnt]->getReferralId();

    $myOrder = getOrders($conn, "WHERE uid = ? AND payment_status = 'APPROVED' ",array("uid"), array($uid), "s");
    $personalSales = 0; // initital
    if($myOrder)
    {
      for ($b=0; $b <count($myOrder) ; $b++)
      {
        $personalSales += $myOrder[$b]->getCommission();
      }
    }

    $myOrder2 = getOrders($conn, "WHERE uid = ? AND payment_status = 'ACCEPTED' ",array("uid"), array($uid), "s");
    $personalSales2 = 0; // initital
    if ($myOrder2)
    {
      for ($b=0; $b <count($myOrder2) ; $b++)
      {
        $personalSales2 += $myOrder2[$b]->getCommission();
      }
    }
    // echo $personalSales2;
    $totalPV = $personalSales + $personalSales2;
  }
  ?>

  <?php
  $conn = connDB();
  if(isset($_POST['downline_uid']))
  {
    $uid = $_POST['downline_uid'];

    $groupSales = 0; // initital
    $groupSalesFormat = number_format(0,4); // initital
    $groupSales2 = 0; // initital
    $groupSales2Format = number_format(0,4); // initital
    $directDownline = 0; // initital
    $personalSales = 0; // initital
    $personalSales2 = 0; // initital    

    $referralDetails = getReferralHistory($conn, "WHERE referral_id = ?",array("referral_id"),array($uid), "s");
    $referralCurrentLevel = $referralDetails[0]->getCurrentLevel();
    $directDownlineLevel = $referralCurrentLevel + 1;
    $referrerDetails = $referrerDetails = getWholeDownlineTree($conn, $uid, false);
    if ($referrerDetails)
    {
      for ($i=0; $i <count($referrerDetails) ; $i++)
      {
        $currentLevel = $referrerDetails[$i]->getCurrentLevel();
        if ($currentLevel == $directDownlineLevel)
        {
          $directDownline++;
        }
        $referralId = $referrerDetails[$i]->getReferralId();
        // $downlineDetails = getOrders($conn, "WHERE uid = ?",array("uid"),array($referralId), "s");
        $downlineDetails = getOrders($conn, "WHERE uid = ? AND payment_status = 'APPROVED' ",array("uid"),array($referralId), "s");
        // $downlineDetailsTwo = getOrders($conn, "WHERE uid = ? AND payment_status = 'ACCEPTED' ",array("uid"),array($referralId), "s");
        if ($downlineDetails)
        {
          for ($b=0; $b <count($downlineDetails) ; $b++)
          {
            // $personalSales += $downlineDetails[$b]->getSubtotal();
            $personalSales += $downlineDetails[$b]->getCommission();
          }
        }
    
        $downlineDetailsTwo = getOrders($conn, "WHERE uid = ? AND payment_status = 'ACCEPTED' ",array("uid"),array($referralId), "s");
        if ($downlineDetailsTwo)
        {
          for ($b=0; $b <count($downlineDetailsTwo) ; $b++)
          {
            $personalSales2 += $downlineDetailsTwo[$b]->getCommission();
          }
        }
      }
      $groupSales += $personalSales;
      $groupSales2 += $personalSales2;
      $groupSalesFormat = number_format($groupSales,4);
      $groupSales2Format = number_format($groupSales2,4);
    }
  }
  ?>

  <div class="clear"></div> 

  <h3 class="white-text"><?php echo $userData->getUsername();?> | <?php echo $userRanking;?></h3>

  <div class="two-div text-center white-div-css">
    <img src="img/user.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_MY_SALES ?>" title="<?php echo _USERDASHBOARD_MY_SALES ?>">
    <div class="inner-white-div">
      <p class="white-div-p"><?php echo _USERDASHBOARD_MY_SALES ?></p>
      <p class="white-div-p white-div-amount">PV <?php echo $totalPV;?></p>
    </div>
  </div>     

  <div class="two-div second-two text-center white-div-css">
    <img src="img/group-member.png" class="white-div-img" alt="<?php echo _USERDASHBOARD_TEAM_SALES ?>" title="<?php echo _USERDASHBOARD_TEAM_SALES ?>">
    <div class="inner-white-div">
      <p class="white-div-p"><?php echo _USERDASHBOARD_TEAM_SALES ?></p>
      <p class="white-div-p white-div-amount">PV <?php echo $totalGS = $groupSales + $groupSales2;?></p>
    </div>
  </div>     

  <div class="clear"></div> 

</div>

<?php include 'js.php'; ?>

</body>
</html>