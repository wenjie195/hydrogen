<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/Stock.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $productId = rewrite($_POST["product_id"]);
    $newId = rewrite($_POST["new_id"]);
    $oldId = rewrite($_POST["old_id"]);
    $newName = rewrite($_POST["new_name"]);
    $oldName = rewrite($_POST["old_name"]);
    $orderId = rewrite($_POST["order_id"]);
    $orderUid = rewrite($_POST["order_uid"]);

    $oldStatus = 'Replacement';
    $newStatus = 'Sold';

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $productId."<br>";

    $product = getStock($conn," id = ? ",array("id"),array($newId),"s");    
    if(!$product)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($newStatus)
        {
            array_push($tableName,"status");
            array_push($tableValue,$newStatus);
            $stringType .=  "s";
        }
        if($orderId)
        {
            array_push($tableName,"order_id");
            array_push($tableValue,$orderId);
            $stringType .=  "s";
        }
        if($orderUid)
        {
            array_push($tableName,"order_uid");
            array_push($tableValue,$orderUid);
            $stringType .=  "s";
        }
        array_push($tableValue,$newId);
        $stringType .=  "s";
        $newProductUpdated = updateDynamicData($conn,"stock"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($newProductUpdated)
        {

            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($oldStatus)
            {
                array_push($tableName,"status");
                array_push($tableValue,$oldStatus);
                $stringType .=  "s";
            }            
            array_push($tableValue,$oldId);
            $stringType .=  "s";
            $newProductUpdated = updateDynamicData($conn,"stock"," WHERE id = ? ",$tableName,$tableValue,$stringType);
            if($newProductUpdated)
            {
                header('Location: ../adminStockView.php');
            }
            else
            {
                echo "FAIL TO UPDATE OLD PRODUCT";
            }

        }
        else
        {
            echo "FAIL TO REPLACE NEW PRODUCT";
        }
    }
    else
    {
        echo "NO SUCH PRODUCT ID";
    }

}
else 
{
    header('Location: ../index.php');
}
?>