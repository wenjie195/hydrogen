<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Withdrawal.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $withdrawalUid = md5(uniqid());

     $withdrawalUid = rewrite($_POST["withdrawal_uid"]);
     $withdrawalStatus = "REJECTED";

     $withdrawalDetails = getWithdrawal($conn," WHERE withdrawal_uid = ? ",array("withdrawal_uid"),array($withdrawalUid),"s");
     $withdrawalAmount =  $withdrawalDetails[0]->getAmount();
     $userUid =  $withdrawalDetails[0]->getUid();

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");
     $userCurrentCredit =  $userDetails[0]->getWallet();

     $refundAmount = $withdrawalAmount + $userCurrentCredit;

     // $method = rewrite($_POST["method"]);
     // $reference = rewrite($_POST["reference"]);
     // $rejectedReason = rewrite($_POST["rejected_reason"]);

     // //   FOR DEBUGGING
     // echo "<br>";
     // echo $withdrawalUid."<br>";

     if(isset($_POST['withdrawal_uid']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";

          if($withdrawalStatus)
          {
               array_push($tableName,"status");
               array_push($tableValue,$withdrawalStatus);
               $stringType .=  "s";
          }
          // if($method)
          // {
          //      array_push($tableName,"method");
          //      array_push($tableValue,$method);
          //      $stringType .=  "s";
          // }
          // if($reference)
          // {
          //      array_push($tableName,"note");
          //      array_push($tableValue,$reference);
          //      $stringType .=  "s";
          // }
          array_push($tableValue,$withdrawalUid);
          $stringType .=  "s";
          $updateWithdrawalStatus = updateDynamicData($conn,"withdrawal"," WHERE withdrawal_uid = ? ",$tableName,$tableValue,$stringType);
          if($updateWithdrawalStatus)
          {
               // echo "<script>alert('withdrawal approved !!');window.location='../adminWithdrawalHistory.php'</script>";

               if(isset($_POST['withdrawal_uid']))
               {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    // //echo "save to database";
     
                    if($refundAmount)
                    {
                         array_push($tableName,"wallet");
                         array_push($tableValue,$refundAmount);
                         $stringType .=  "s";
                    }
                    array_push($tableValue,$userUid);
                    $stringType .=  "s";
                    $updateWithdrawalStatus = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($updateWithdrawalStatus)
                    {
                         // echo "reject and refund success";
                         echo "<script>alert('withdrawal rejected and refunded !!');window.location='../adminWithdrawalHistory.php'</script>";
                    }    
                    else
                    {
                         echo "fail";
                    }
               }
               else
               {
                    echo "error level 2";
               }

          }    
          else
          {
               echo "fail";
          }
     }
     else
     {
          echo "error level 222";
     }
}
else
{
     header('Location: ../index.php');
}
?>