<?php
class ProductOrders {
    /* Member variables */
    var $id,$productId,$productName,$orderId,$quantity,$finalPrice,$originalPrice,$discountGiven,$totalProductPrice,$totalCommission,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @param mixed $productName
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getFinalPrice()
    {
        return $this->finalPrice;
    }

    /**
     * @param mixed $finalPrice
     */
    public function setFinalPrice($finalPrice)
    {
        $this->finalPrice = $finalPrice;
    }

    /**
     * @return mixed
     */
    public function getOriginalPrice()
    {
        return $this->originalPrice;
    }

    /**
     * @param mixed $originalPrice
     */
    public function setOriginalPrice($originalPrice)
    {
        $this->originalPrice = $originalPrice;
    }

    /**
     * @return mixed
     */
    public function getDiscountGiven()
    {
        return $this->discountGiven;
    }

    /**
     * @param mixed $discountGiven
     */
    public function setDiscountGiven($discountGiven)
    {
        $this->discountGiven = $discountGiven;
    }

    /**
     * @return mixed
     */
    public function getTotalProductPrice()
    {
        return $this->totalProductPrice;
    }

    /**
     * @param mixed $totalProductPrice
     */
    public function setTotalProductPrice($totalProductPrice)
    {
        $this->totalProductPrice = $totalProductPrice;
    }

    /**
     * @return mixed
     */
    public function getTotalCommission()
    {
        return $this->totalCommission;
    }

    /**
     * @param mixed $totalCommission
     */
    public function setTotalCommission($totalCommission)
    {
        $this->totalCommission = $totalCommission;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getProductOrders($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","product_id","product_name","order_id","quantity","final_price","original_price","discount_given","totalProductPrice","totalCommission",
                                "date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"product_orders");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$productId,$productName,$orderId,$quantity,$finalPrice,$originalPrice,$discountGiven,$totalProductPrice,$totalCommission,
                                $dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new ProductOrders();
            $class->setId($id);
            $class->setProductId($productId);
            $class->setProductName($productName);
            $class->setOrderId($orderId);
            $class->setQuantity($quantity);
            $class->setFinalPrice($finalPrice);
            $class->setOriginalPrice($originalPrice);
            $class->setDiscountGiven($discountGiven);
            $class->setTotalProductPrice($totalProductPrice);
            $class->setTotalCommission($totalCommission);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}