<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

function registerNewProduct($conn,$uid,$category,$brand,$name,$price,$diamond,$commission,$description,$imageOne,$imageTwo,$status)
{
     if(insertDynamicData($conn,"product",array("uid","category","brand","name","price","diamond","commission","description","image_one","image_two","status"),
          array($uid,$category,$brand,$name,$price,$diamond,$commission,$description,$imageOne,$imageTwo,$status),"sssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

function registerNewProductNoImageTwo($conn,$uid,$category,$brand,$name,$price,$diamond,$commission,$description,$imageOne,$status)
{
     if(insertDynamicData($conn,"product",array("uid","category","brand","name","price","diamond","commission","description","image_one","status"),
          array($uid,$category,$brand,$name,$price,$diamond,$commission,$description,$imageOne,$status),"ssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     // $category= rewrite($_POST['register_category']);
     // $brand = rewrite($_POST['register_brand']);

     $category = "Hydrogen";
     $brand = "Hydrogen";

     $name = rewrite($_POST['register_name']);

     // $stringOne = ($_POST['register_brand']);
     // $brandType = preg_replace('/[^\p{L}\p{N}\s][^(\x20-\x7F)]*/u', '', $stringOne);
     // $updatedBrand = str_replace(' ', '-', trim($brandType));
     $stringTwo = ($_POST['register_name']);
     $productName = preg_replace('/[^\p{L}\p{N}\s][^(\x20-\x7F)]*/u', '', $stringTwo);
     // $updatedProductName = str_replace(' ', '-', trim($productName));
     // $slash = "-";
     // // $slug = $updatedBrand.$slash.$updatedProductName;
     // $slug = $updatedProductName;

     $status = "Available";

     $price = rewrite($_POST['register_price']);
     $diamond = rewrite($_POST['register_price']);

     $commission = rewrite($_POST['register_commission']);

     $description = rewrite($_POST['register_description']);

     $imageOneValidation = $_FILES['image_one']['name'];
     $imageTwoValidation = $_FILES['image_two']['name'];

     if($imageOneValidation != '')
     {
          $imageOne = $timestamp.$_FILES['image_one']['name'];
          $target_dir = "../productImage/";
          $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
          // Select file type
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
          // Valid file extensions
          $extensions_arr = array("jpg","jpeg","png","gif");
          if( in_array($imageFileType,$extensions_arr) )
          {
               move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
          }
     }
     else
     {
          // echo "no image one";
          // echo "<br>";
     }

     if($imageTwoValidation != '')
     {
          $imageTwo = $timestamp.$_FILES['image_two']['name'];
          $target_dir = "../productImage/";
          $target_file = $target_dir . basename($_FILES["image_two"]["name"]);
          // Select file type
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
          // Valid file extensions
          $extensions_arr = array("jpg","jpeg","png","gif");
          if( in_array($imageFileType,$extensions_arr) )
          {
               move_uploaded_file($_FILES['image_two']['tmp_name'],$target_dir.$imageTwo);
          }
     }
     else
     {
          // echo "no image two";
          // echo "<br>";
          $imageTwo = " ";
     }

     // $imageOne = $timestamp.$_FILES['image_one']['name'];
     // $target_dir = "../uploads/";
     // $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // // Select file type
     // $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // // Valid file extensions
     // $extensions_arr = array("jpg","jpeg","png","gif");
     // if( in_array($imageFileType,$extensions_arr) )
     // {
     //      move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     // }

     // //   FOR DEBUGGING
     // echo "<br>";
     // echo $name."<br>";
     // echo $productName."<br>";
     // echo $price."<br>";
     // echo $diamond."<br>";
     // echo $description."<br>";
     // echo $imageOne."<br>";
     // echo $imageTwo."<br>";

     $productNameRows = getProduct($conn," WHERE name = ? ",array("name"),array($_POST['register_name']),"s");
     $existingProductName = $productNameRows[0];

     if(!$existingProductName)
     {
          // echo "SUCCESS 1";

          if($imageTwoValidation != '')
          {
               if(registerNewProduct($conn,$uid,$category,$brand,$name,$price,$diamond,$commission,$description,$imageOne,$imageTwo,$status))
               {
                    // echo "SUCCESS 2";
                    $_SESSION['messageType'] = 3;
                    header('Location: ../adminProductAll.php?type=1');
               }
               else
               {
                    // echo "FAIL";
                    $_SESSION['messageType'] = 3;
                    header('Location: ../adminProductAll.php?type=2');
               }
          }
          else
          {
               if(registerNewProductNoImageTwo($conn,$uid,$category,$brand,$name,$price,$diamond,$commission,$description,$imageOne,$status))
               {
                    // echo "SUCCESS 2";
                    $_SESSION['messageType'] = 3;
                    header('Location: ../adminProductAll.php?type=1');
               }
               else
               {
                    // echo "FAIL";
                    $_SESSION['messageType'] = 3;
                    header('Location: ../adminProductAll.php?type=2');
               }
          }

          // if(registerNewProduct($conn,$uid,$category,$brand,$name,$price,$diamond,$description,$imageOne,$imageTwo,$status))
          // {
          //      // echo "SUCCESS 2";
          //      $_SESSION['messageType'] = 3;
          //      header('Location: ../adminProductAll.php?type=1');
          // }
          // else
          // {
          //      // echo "FAIL";
          //      $_SESSION['messageType'] = 3;
          //      header('Location: ../adminProductAll.php?type=2');
          // }
     }
     else
     {
          // echo "ERROR";
          $_SESSION['messageType'] = 3;
          header('Location: ../adminProductAll.php?type=3');
     }
}
else
{
     header('Location: ../index.php');
}
?>