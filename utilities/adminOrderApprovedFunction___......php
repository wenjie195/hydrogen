<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Bonus.php';
require_once dirname(__FILE__) . '/../classes/BonusPoolFund.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/Sales.php';
require_once dirname(__FILE__) . '/../classes/Stock.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

function directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlTwo,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlTwo,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlFour($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlFive($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlSix($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

//bonus 2 (pool fund)

function bonusTwoPoolFundOne($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundOneAmount,$poolFundOneReceiver,$bonusTypePF)
{
     if(insertDynamicData($conn,"bonus_poolfund",array("order_uid","uid","username","amount","receiver","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$poolFundOneAmount,$poolFundOneReceiver,$bonusTypePF),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function bonusTwoPoolFundTwo($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundTwoAmount,$poolFundTwoReceiver,$bonusTypePF)
{
     if(insertDynamicData($conn,"bonus_poolfund",array("order_uid","uid","username","amount","receiver","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$poolFundTwoAmount,$poolFundTwoReceiver,$bonusTypePF),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function bonusTwoPoolFundThree($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundThreeAmount,$poolFundThreeReceiver,$bonusTypePF)
{
     if(insertDynamicData($conn,"bonus_poolfund",array("order_uid","uid","username","amount","receiver","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$poolFundThreeAmount,$poolFundThreeReceiver,$bonusTypePF),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function bonusTwoPoolFundFour($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundFourAmount,$poolFundFourReceiver,$bonusTypePF)
{
     if(insertDynamicData($conn,"bonus_poolfund",array("order_uid","uid","username","amount","receiver","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$poolFundFourAmount,$poolFundFourReceiver,$bonusTypePF),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function updateSales($conn,$brand,$status,$orderId,$orderUid)
{
     if(insertDynamicData($conn,"sales",array("brand","status","order_id","order_uid"),
     array($brand,$status,$orderId,$orderUid),"ssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{

    $conn = connDB();

    // echo "order id  :  " ; echo $orderUid = rewrite($_POST["order_uid"]);
    // echo "<br>";  

    $orderUid = rewrite($_POST["order_uid"]);
    // echo "<br>";  

    $orderDetails = getOrders($conn, " WHERE order_id = '$orderUid' ");

    // $orderUserUid = $orderDetails[0]->getUid();
    // $orderUserName = $orderDetails[0]->getName();
    // $orderPrice = $orderDetails[0]->getCommission();

    echo $orderUserUid = $orderDetails[0]->getUid();
    echo "<br>";
    echo $orderUserName = $orderDetails[0]->getName();
    echo "<br>";
    // echo $orderPrice = $orderDetails[0]->getSubtotal();
    // echo "<br>";
    echo $orderPrice = $orderDetails[0]->getCommission();
    echo "<br>";

    //user details in referral history
    $userRH = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($orderUserUid),"s");
    $userLvl = $userRH[0]->getCurrentLevel();
    echo "user level";  
    echo $userLevel = $userLvl - 1;
    //Direct Upline
    $userUplineDetails = $userRH[0]->getReferrerId(); //william
    // echo $userUplineDetails = $userRH[0]->getReferrerId(); //william
    // echo "<br>";  

    $paymentStatus = 'APPROVED';

    //start check ordering to verify downline
    $purchaserDetails = getUser($conn, "WHERE uid =?",array("uid"),array($orderUserUid),"s");
    // $purchaserData = $purchaserDetails[0];
    $orderingStatus = 'YES';
    //end check ordering to verify downline

    //stock checking start

    $orderId = $orderDetails[0]->getId();
    // echo $orderId = $orderDetails[0]->getId();
    // echo "<br>";
    // $orderId = $orderDetails[0]->getId();
    // echo "<br>";

    // $orders = getProductOrders($conn, " WHERE order_id = '$orderId' ");
    // if($orders)
    // {
    //     // $totalManagerBonusPool = 0;
    //     for ($cnt=0; $cnt <count($orders) ; $cnt++)
    //     {
    //         echo $orderId;
    //         echo "<br>";
    //         echo $orderUid;
    //         echo "<br>";
    //         echo $orderProduct = $orders[$cnt]->getProductName();
    //         echo "<br>";
    //         echo $orderQuantity = $orders[$cnt]->getQuantity();
    //         echo "<br>";

    //         // $orderProduct = $orders[$cnt]->getProductName();
    //         // $orderQuantity = $orders[$cnt]->getQuantity();
    //         $brand = $orderProduct;
    //         $status = 'Sold';

    //         for ($x = 1; $x <= $orderQuantity; $x++)
    //         {    
    //             if(updateSales($conn,$brand,$status,$orderId,$orderUid))
    //             {
    //                 echo "Sales Updated";
    //             }
    //             else
    //             {
    //                 echo "Fail To Update Sales";
    //             }
    //         }

    //     }
    // }

    $ordersGD = getProductOrders($conn, " WHERE order_id = '$orderId' AND product_name = 'GD M-12' ");
    if($ordersGD)
    {
        // $totalManagerBonusPool = 0;
        for ($cnt=0; $cnt <count($ordersGD) ; $cnt++)
        {
            // echo $orderId;
            // echo "<br>";
            // echo $orderUid;
            // echo "<br>";
            // echo $orderProduct = $ordersGD[$cnt]->getProductName();
            // echo "<br>";
            // echo $orderQuantity = $ordersGD[$cnt]->getQuantity();
            // echo "<br>";

            $orderProduct = $ordersGD[$cnt]->getProductName();
            $orderQuantity = $ordersGD[$cnt]->getQuantity();
            $brand = $orderProduct;
            $status = 'Sold';

            $stockCheckA = getStock($conn, " WHERE status = 'Available' AND brand = 'GD' LIMIT $orderQuantity");
            if($stockCheckA)
            {
                for ($cnt=0; $cnt <count($stockCheckA) ; $cnt++)
                {
                    // echo $stockAName = $stockCheckA[$cnt]->getName();
                    // echo "<br>";
                    $stockAName = $stockCheckA[$cnt]->getName();

                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($status)
                    {
                        array_push($tableName,"status");
                        array_push($tableValue,$status);
                        $stringType .=  "s";
                    }    
                    if($orderId)
                    {
                        array_push($tableName,"order_id");
                        array_push($tableValue,$orderId);
                        $stringType .=  "s";
                    } 
                    if($orderUid)
                    {
                        array_push($tableName,"order_uid");
                        array_push($tableValue,$orderUid);
                        $stringType .=  "s";
                    } 
                    array_push($tableValue,$stockCheckA[$cnt]->getName());
                    $stringType .=  "s";
                    $updateStockA = updateDynamicData($conn,"stock"," WHERE name = ? ",$tableName,$tableValue,$stringType);
                    if($updateStockA)
                    {
                        echo "GD Sales Updated";
                    }
                    else
                    {
                        echo "Fail To Update GD Sales";
                    }

                }
            }
        }
    }

    $ordersGN = getProductOrders($conn, " WHERE order_id = '$orderId' AND product_name = 'GN M-12' ");
    if($ordersGN)
    {
        // $totalManagerBonusPool = 0;
        for ($cnt=0; $cnt <count($ordersGN) ; $cnt++)
        {
            // echo $orderId;
            // echo "<br>";
            // echo $orderUid;
            // echo "<br>";
            // echo $orderProduct = $ordersGN[$cnt]->getProductName();
            // echo "<br>";
            // echo $orderQuantity = $ordersGN[$cnt]->getQuantity();
            // echo "<br>";

            $orderProduct = $ordersGN[$cnt]->getProductName();
            $orderQuantity = $ordersGN[$cnt]->getQuantity();
            $brand = $orderProduct;
            $status = 'Sold';

            $stockCheckB = getStock($conn, " WHERE status = 'Available' AND brand = 'GN' LIMIT $orderQuantity");
            if($stockCheckB)
            {
                for ($cnt=0; $cnt <count($stockCheckB) ; $cnt++)
                {
                    // echo $stockAName = $stockCheckB[$cnt]->getName();
                    // echo "<br>";
                    $stockAName = $stockCheckB[$cnt]->getName();

                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($status)
                    {
                        array_push($tableName,"status");
                        array_push($tableValue,$status);
                        $stringType .=  "s";
                    }    
                    if($orderId)
                    {
                        array_push($tableName,"order_id");
                        array_push($tableValue,$orderId);
                        $stringType .=  "s";
                    } 
                    if($orderUid)
                    {
                        array_push($tableName,"order_uid");
                        array_push($tableValue,$orderUid);
                        $stringType .=  "s";
                    } 
                    array_push($tableValue,$stockCheckB[$cnt]->getName());
                    $stringType .=  "s";
                    $updateStockB = updateDynamicData($conn,"stock"," WHERE name = ? ",$tableName,$tableValue,$stringType);
                    if($updateStockB)
                    {
                        echo "GN Sales Updated";
                    }
                    else
                    {
                        echo "Fail To Update GN Sales";
                    }

                }
            }
        }
    }

    //stock checking end

    //bonus 2 (pool fund) details
    $bonusTypePF = 'Bonus 2 (Pool Fund)';

    // $poolFundOneAmount = (1000 * 0.02);
    // $poolFundOneAmount = ($orderPrice * 0.02);
    $poolFundOneAmount = ($orderPrice * 0.01);
    $poolFundOneReceiver = 'Manager';

    // $poolFundTwoAmount = (1000 * 0.02);
    // $poolFundTwoAmount = ($orderPrice * 0.02);
    $poolFundTwoAmount = ($orderPrice * 0.0125);
    $poolFundTwoReceiver = 'Senior Manager';

    // $poolFundThreeAmount = (1000 * 0.03);
    // $poolFundThreeAmount = ($orderPrice * 0.03);
    $poolFundThreeAmount = ($orderPrice * 0.016);
    $poolFundThreeReceiver = 'Area Manager';

    // $poolFundFourAmount = (1000 * 0.03);
    // $poolFundFourAmount = ($orderPrice * 0.03);
    $poolFundFourAmount = ($orderPrice * 0.0188);
    $poolFundFourReceiver = 'District Manager';

    // if(!$orderDetails)
    if($orderDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($paymentStatus)
        {
            array_push($tableName,"payment_status");
            array_push($tableValue,$paymentStatus);
            $stringType .=  "s";
        }
        array_push($tableValue,$orderUid);
        $stringType .=  "s";
        $updateOrders = updateDynamicData($conn,"orders"," WHERE order_id = ? ",$tableName,$tableValue,$stringType);
        if($updateOrders)
        {
            if($purchaserDetails)
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($orderingStatus)
                {
                    array_push($tableName,"order_status");
                    array_push($tableValue,$orderingStatus);
                    $stringType .=  "s";
                }
                array_push($tableValue,$orderUserUid);
                $stringType .=  "s";
                $updateUserOrder = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($updateUserOrder)
                {
                    if($userRH)
                    {   
                        $tableName = array();
                        $tableValue =  array();
                        $stringType =  "";
                        //echo "save to database";
                        if($orderingStatus)
                        {
                            array_push($tableName,"order_status");
                            array_push($tableValue,$orderingStatus);
                            $stringType .=  "s";
                        }
                        array_push($tableValue,$orderUserUid);
                        $stringType .=  "s";
                        $updateRHOrder = updateDynamicData($conn,"referral_history"," WHERE referral_id = ? ",$tableName,$tableValue,$stringType);
                        if($updateRHOrder)
                        {

                            if(bonusTwoPoolFundOne($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundOneAmount,$poolFundOneReceiver,$bonusTypePF))
                            {
                                if(bonusTwoPoolFundTwo($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundTwoAmount,$poolFundTwoReceiver,$bonusTypePF))
                                {
                                    if(bonusTwoPoolFundThree($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundThreeAmount,$poolFundThreeReceiver,$bonusTypePF))
                                    {
                                        if(bonusTwoPoolFundFour($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundFourAmount,$poolFundFourReceiver,$bonusTypePF))
                                        {
                
                
                                            $upline1stDetails = getReferralHistory($conn," WHERE referral_id = ?  ",array("referral_id"),array($userUplineDetails),"s");
                                            $uplineLevel = $upline1stDetails[0]->getCurrentLevel();
                                        
                                            //identify 2nd upline
                                            echo $uplineDetailsLvl2 = $upline1stDetails[0]->getReferrerId();
                                            echo "<br>"; 
                                            $uplineDetailsLvl2 = $upline1stDetails[0]->getReferrerId();
                                        
                                            $uplineStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($userUplineDetails),"s");
                                            $uplineCredit = $uplineStatus[0]->getWallet();
                                            echo $uplineUsername = $uplineStatus[0]->getUsername();
                                            echo "<br>"; 
                                            $uplineUsername = $uplineStatus[0]->getUsername();
                                            // $uplineOrderStatus = $uplineStatus[0]->getOrderStatus();
                                            // $bonusLvlOne = (1000 * 0.4);
                                            // $bonusLvlOne = ($orderPrice * 0.4);
                                            // $bonusName = 'Direct Sponsor (40%)';
                                            $bonusLvlOne = ($orderPrice * 0.16);
                                            $bonusName = 'Direct Sponsor (16%)';
                                            $newUplineCredit = $uplineCredit + $bonusLvlOne;
                                        
                                        
                                            $tableName = array();
                                            $tableValue =  array();
                                            $stringType =  "";
                                            //echo "save to database";
                                            if($newUplineCredit)
                                            {
                                                array_push($tableName,"wallet");
                                                array_push($tableValue,$newUplineCredit);
                                                $stringType .=  "d";
                                            }
                                            array_push($tableValue,$userUplineDetails);
                                            $stringType .=  "s";
                                            // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                            // 301220 new requirement 
                                            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
                                            if($passwordUpdated)
                                            {

                                                $uplineOrderStatus = $uplineStatus[0]->getOrderStatus();
                                                if($uplineOrderStatus == 'YES')
                                                {
                                                    if(directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName))
                                                    {
                                                        echo "success 1";  
                                                        echo "<br>";  
                                                        // header('Location: ../adminOrderPending.php');
                                                    }
                                                    else
                                                    {
                                                        echo "fail 1";    
                                                        echo "<br>";  
                                                    } 
                                                }
                                                else
                                                {
                                                    // header('Location: ../adminOrderPending.php');
                                                    echo "NO 1";    
                                                    echo "<br>";  
                                                }

                                                // if(directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName))
                                                // {
                                                //     // echo "success 1";  
                                                //     // echo "<br>";  
                                                //     header('Location: ../adminOrderPending.php');
                                                // }
                                                // else
                                                // {
                                                //     echo "fail 1";    
                                                //     echo "<br>";  
                                                // } 
                                        
                                            }
                                            else
                                            {
                                                echo "fail";
                                                // header('Location: ../adminOrderPending.php');
                                            }
                                        
                                            //level 2 bonus
                                            if(!$uplineDetailsLvl2)
                                            {  
                                                echo "undefined upline level 2";   
                                                echo "<br>"; 
                                            }
                                            else
                                            {  
                                        
                                                $upline2ndDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl2),"s");
                                                // identify how many direct downline
                                                $upline2ndDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl2),"s");
                                        
                                                //identify 3rd upline
                                                echo "identify 3rd upline <br>";
                                                echo $uplineDetailsLvl3 = $upline2ndDetails[0]->getReferrerId();
                                                echo "<br>"; 
                                                $uplineDetailsLvl3 = $upline2ndDetails[0]->getReferrerId();
                                        
                                                if ($upline2ndDownlineNum)
                                                {
                                        
                                                    echo $totalDownlineTwo = count($upline2ndDownlineNum);
                                                    echo "<br>"; 
                                                    
                                                    // if($totalDownlineOne >= 3)
                                                    if($totalDownlineTwo >= $userLevel)
                                                    {   
                                                        // proceed with bonus calculation
                                                        
                                                        $upline2ndStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl2),"s");
                                                        $upline2ndCredit = $upline2ndStatus[0]->getWallet();
                                                        echo $upline2ndUsername = $upline2ndStatus[0]->getUsername();
                                                        echo "<br>"; 
                                                        // $bonusLvlTwo = (1000 * 0.12);
                                                        // $bonusLvlTwo = ($orderPrice * 0.12);
                                                        // $bonusName = 'Direct Sponsor (12%)';
                                                        $bonusLvlTwo = ($orderPrice * 0.05);
                                                        $bonusName = 'Direct Sponsor (5%)';
                                                        $newUpline2ndCredit = $upline2ndCredit + $bonusLvlTwo;
                                        
                                                        $tableName = array();
                                                        $tableValue =  array();
                                                        $stringType =  "";
                                                        //echo "save to database";
                                                        if($newUpline2ndCredit)
                                                        {
                                                            array_push($tableName,"wallet");
                                                            array_push($tableValue,$newUpline2ndCredit);
                                                            $stringType .=  "d";
                                                        }
                                                        array_push($tableValue,$uplineDetailsLvl2);
                                                        $stringType .=  "s";
                                                        // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                        // 301220 new requirement 
                                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
                                                        if($passwordUpdated)
                                                        {
                                        
                                                            $upline2ndOrderStatus = $upline2ndStatus[0]->getOrderStatus();
                                                            if($upline2ndOrderStatus == 'YES')
                                                            {
                                                                if(directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlTwo,$bonusName))
                                                                {
                                                                    echo "success 2";  
                                                                    echo "<br>";  
                                                                    // header('Location: ../adminOrderPending.php');
                                                                }
                                                                else
                                                                {
                                                                    echo "fail 2";    
                                                                    echo "<br>";  
                                                                } 
                                                            }
                                                            else
                                                            {
                                                                // header('Location: ../adminOrderPending.php');
                                                                echo "NO 2";    
                                                                echo "<br>";  
                                                            }

                                                            // if(directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlTwo,$bonusName))
                                                            // {
                                                            //     // echo "success 2";  
                                                            //     // echo "<br>";  
                                                            //     header('Location: ../adminOrderPending.php');
                                                            // }
                                                            // else
                                                            // {
                                                            //     echo "fail 2";    
                                                            //     echo "<br>";  
                                                            // } 
                                        
                                                        }
                                                        else
                                                        {
                                                            echo "fail";
                                                            // header('Location: ../adminOrderPending.php');
                                                        }
                                        
                                                    }
                                                    // elseif($totalDownlineOne < 3)
                                                    elseif($totalDownlineTwo < $userLevel)
                                                    {  
                                                        //   no bonus
                                                        echo "no bonus due to not enough direct downline lvl 2";   
                                                        echo "<br>"; 
                                                    }
                                        
                                                }
                                        
                                            }
                                        
                                            //level 3 bonus
                                            if(!$uplineDetailsLvl3)
                                            {  
                                                echo "undefined upline level 3";   
                                                echo "<br>"; 
                                            }
                                            else
                                            {  
                                        
                                                $upline3rdDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl3),"s");
                                                // identify how many direct downline
                                                $upline3rddDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl3),"s");
                                        
                                                //identify 4th upline
                                                echo "identify 4th upline <br>";
                                                echo $uplineDetailsLvl4 = $upline3rdDetails[0]->getReferrerId();
                                                echo "<br>"; 
                                        
                                                if ($upline3rddDownlineNum)
                                                {
                                        
                                                    echo $totalDownlineThree = count($upline3rddDownlineNum);
                                                    echo "<br>"; 
                                        
                                                    if($totalDownlineThree >= $userLevel)
                                                    {   
                                                        // proceed with bonus calculation
                                                        
                                                        $upline3rdStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl3),"s");
                                                        $upline3rdCredit = $upline3rdStatus[0]->getWallet();
                                                        echo $upline3rdUsername = $upline3rdStatus[0]->getUsername();
                                                        echo "<br>"; 
                                                        // $bonusLvlThree = (1000 * 0.1);
                                                        // $bonusLvlThree = ($orderPrice * 0.1);
                                                        // $bonusName = 'Direct Sponsor (10%)';
                                                        $bonusLvlThree = ($orderPrice * 0.04);
                                                        $bonusName = 'Direct Sponsor (4%)';
                                                        $newUpline3rdCredit = $upline3rdCredit + $bonusLvlThree;
                                        
                                        
                                                        $tableName = array();
                                                        $tableValue =  array();
                                                        $stringType =  "";
                                                        //echo "save to database";
                                                        if($newUpline3rdCredit)
                                                        {
                                                            array_push($tableName,"wallet");
                                                            array_push($tableValue,$newUpline3rdCredit);
                                                            $stringType .=  "d";
                                                        }
                                                        array_push($tableValue,$uplineDetailsLvl3);
                                                        $stringType .=  "s";
                                                        // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                        // 301220 new requirement 
                                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
                                                        if($passwordUpdated)
                                                        {
                                        
                                                            $upline3rdOrderStatus = $upline3rdStatus[0]->getOrderStatus();
                                                            if($upline3rdOrderStatus == 'YES')
                                                            {
                                                                if(directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName))
                                                                {
                                                                    echo "success 3";  
                                                                    echo "<br>";  
                                                                    // header('Location: ../adminOrderPending.php');
                                                                }
                                                                else
                                                                {
                                                                    echo "fail 3";    
                                                                    echo "<br>";  
                                                                } 
                                                            }
                                                            else
                                                            {
                                                                // header('Location: ../adminOrderPending.php');
                                                                echo "NO 3";    
                                                                echo "<br>";  
                                                            }

                                                            // if(directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName))
                                                            // {
                                                            //     // echo "success 3";  
                                                            //     // echo "<br>";  
                                                            //     header('Location: ../adminOrderPending.php');
                                                            // }
                                                            // else
                                                            // {
                                                            //     echo "fail 3";    
                                                            //     echo "<br>";  
                                                            // } 
                                        
                                                        }
                                                        else
                                                        {
                                                            echo "fail 333";
                                                            // header('Location: ../adminOrderPending.php');
                                                        }
                                        
                                                    }
                                                    elseif($totalDownlineThree < $userLevel)
                                                    {  
                                                        //   no bonus
                                                        echo "no bonus due to not enough direct downline lvl 3";   
                                                        echo "<br>"; 
                                                    }
                                        
                                                }
                                        
                                            }
                                        
                                            //level 4 bonus
                                            if(!$uplineDetailsLvl4)
                                            {  
                                                echo "undefined upline level 4";   
                                                echo "<br>"; 
                                            }
                                            else
                                            {  
                                        
                                                $upline4thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl4),"s");
                                                // identify how many direct downline
                                                $upline4thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl4),"s");
                                        
                                                //identify 5th upline
                                                echo "identify 5thh upline <br>";
                                                echo $uplineDetailsLvl5 = $upline4thDetails[0]->getReferrerId();
                                                echo "<br>"; 
                                        
                                                if ($upline4thDownlineNum)
                                                {
                                        
                                                    echo $totalDownlineFour = count($upline4thDownlineNum);
                                                    echo "<br>"; 
                                        
                                                    if($totalDownlineFour >= $userLevel)
                                                    {   
                                                        // proceed with bonus calculation
                                                        
                                                        $upline4thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl4),"s");
                                                        $upline4thCredit = $upline4thStatus[0]->getWallet();
                                                        echo $upline4thUsername = $upline4thStatus[0]->getUsername();
                                                        echo "<br>"; 
                                                        // $bonusLvlFour = (1000 * 0.05);
                                                        // $bonusLvlFour = ($orderPrice * 0.05);
                                                        // $bonusName = 'Direct Sponsor (5%)';
                                                        $bonusLvlFour = ($orderPrice * 0.02);
                                                        $bonusName = 'Direct Sponsor (2%)';
                                                        $newUpline4thCredit = $upline4thCredit + $bonusLvlFour;
                                        
                                        
                                                        $tableName = array();
                                                        $tableValue =  array();
                                                        $stringType =  "";
                                                        //echo "save to database";
                                                        if($newUpline4thCredit)
                                                        {
                                                            array_push($tableName,"wallet");
                                                            array_push($tableValue,$newUpline4thCredit);
                                                            $stringType .=  "d";
                                                        }
                                                        array_push($tableValue,$uplineDetailsLvl4);
                                                        $stringType .=  "s";
                                                        // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                        // 301220 new requirement 
                                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
                                                        if($passwordUpdated)
                                                        {
                                        
                                                            $upline4thOrderStatus = $upline4thStatus[0]->getOrderStatus();
                                                            if($upline4thOrderStatus == 'YES')
                                                            {
                                                                if(directSponsorBonusLvlFour($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName))
                                                                {
                                                                    echo "success 4";  
                                                                    echo "<br>";  
                                                                    // header('Location: ../adminOrderPending.php');
                                                                }
                                                                else
                                                                {
                                                                    echo "fail 4";    
                                                                    echo "<br>";  
                                                                } 
                                                            }
                                                            else
                                                            {
                                                                // header('Location: ../adminOrderPending.php');
                                                                echo "NO 4";    
                                                                echo "<br>";  
                                                            }

                                                            // if(directSponsorBonusLvlFour($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName))
                                                            // {
                                                            //     // echo "success 4";  
                                                            //     // echo "<br>";  
                                                            //     header('Location: ../adminOrderPending.php');
                                                            // }
                                                            // else
                                                            // {
                                                            //     echo "fail 4";    
                                                            //     echo "<br>";  
                                                            // } 
                                        
                                                        }
                                                        else
                                                        {
                                                            echo "fail 444";
                                                            // header('Location: ../adminOrderPending.php');
                                                        }
                                        
                                                    }
                                                    elseif($totalDownlineFour < $userLevel)
                                                    {  
                                                        //   no bonus
                                                        echo "no bonus due to not enough direct downline lvl 4";   
                                                        echo "<br>"; 
                                                    }
                                        
                                                }
                                        
                                            }
                                        
                                            //level 5 bonus
                                            if(!$uplineDetailsLvl5)
                                            {  
                                                echo "undefined upline level 5";   
                                                echo "<br>"; 
                                            }
                                            else
                                            {  
                                        
                                                $upline5thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl5),"s");
                                                // identify how many direct downline
                                                $upline5thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl5),"s");
                                        
                                                //identify 6th upline
                                                echo "identify 6th upline <br>";
                                                echo $uplineDetailsLvl6 = $upline5thDetails[0]->getReferrerId();
                                                echo "<br>"; 
                                        
                                                if ($upline5thDownlineNum)
                                                {
                                        
                                                    echo $totalDownlineFive = count($upline5thDownlineNum);
                                                    echo "<br>"; 
                                        
                                                    if($totalDownlineFive >= $userLevel)
                                                    {   
                                                        // proceed with bonus calculation
                                                        
                                                        $upline5thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl5),"s");
                                                        $upline5thCredit = $upline5thStatus[0]->getWallet();
                                                        echo $upline5thUsername = $upline5thStatus[0]->getUsername();
                                                        echo "<br>"; 
                                                        // $bonusLvlFive = (1000 * 0.03);
                                                        // $bonusLvlFive = ($orderPrice * 0.03);
                                                        // $bonusName = 'Direct Sponsor (3%)';
                                                        $bonusLvlFive = ($orderPrice * 0.0125);
                                                        $bonusName = 'Direct Sponsor (1.25%)';
                                                        $newUpline5thCredit = $upline5thCredit + $bonusLvlFive;
                                        
                                        
                                                        $tableName = array();
                                                        $tableValue =  array();
                                                        $stringType =  "";
                                                        //echo "save to database";
                                                        if($newUpline5thCredit)
                                                        {
                                                            array_push($tableName,"wallet");
                                                            array_push($tableValue,$newUpline5thCredit);
                                                            $stringType .=  "d";
                                                        }
                                                        array_push($tableValue,$uplineDetailsLvl5);
                                                        $stringType .=  "s";
                                                        // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                        // 301220 new requirement 
                                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
                                                        if($passwordUpdated)
                                                        {
                                        
                                                            $upline5thOrderStatus = $upline5thStatus[0]->getOrderStatus();
                                                            if($upline5thOrderStatus == 'YES')
                                                            {
                                                                if(directSponsorBonusLvlFive($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName))
                                                                {
                                                                    echo "success 5";  
                                                                    echo "<br>";  
                                                                    // header('Location: ../adminOrderPending.php');
                                                                }
                                                                else
                                                                {
                                                                    echo "fail 5";    
                                                                    echo "<br>";  
                                                                } 
                                                            }
                                                            else
                                                            {
                                                                // header('Location: ../adminOrderPending.php');
                                                                echo "NO 5";    
                                                                echo "<br>";  
                                                            }

                                                            // if(directSponsorBonusLvlFive($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName))
                                                            // {
                                                            //     // echo "success 5";  
                                                            //     // echo "<br>";  
                                                            //     header('Location: ../adminOrderPending.php');
                                                            // }
                                                            // else
                                                            // {
                                                            //     echo "fail 5";    
                                                            //     echo "<br>";  
                                                            // } 
                                        
                                                        }
                                                        else
                                                        {
                                                            echo "fail 555";
                                                            // header('Location: ../adminOrderPending.php');
                                                        }
                                        
                                                    }
                                                    elseif($totalDownlineFive < $userLevel)
                                                    {  
                                                        //   no bonus
                                                        echo "no bonus due to not enough direct downline lvl 5";   
                                                        echo "<br>"; 
                                                    }
                                        
                                                }
                                        
                                            }
                                        
                                            //level 6 bonus
                                            if(!$uplineDetailsLvl6)
                                            {  
                                                echo "undefined upline level 6";   
                                                echo "<br>"; 
                                            }
                                            else
                                            {  
                                        
                                                $upline6thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl6),"s");
                                                // identify how many direct downline
                                                $upline6thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl6),"s");
                                        
                                                // //identify 6th upline
                                                echo "identify 7th upline <br>";
                                                echo $uplineDetailsLvl7 = $upline6thDetails[0]->getReferrerId();
                                                echo "<br>"; 
                                        
                                                if ($upline6thDownlineNum)
                                                {
                                        
                                                    echo $totalDownlineSix = count($upline6thDownlineNum);
                                                    echo "<br>"; 
                                        
                                                    if($totalDownlineSix >= $userLevel)
                                                    {   
                                                        // proceed with bonus calculation
                                                        
                                                        $upline6thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl6),"s");
                                                        $upline6thCredit = $upline6thStatus[0]->getWallet();
                                                        echo $upline6thUsername = $upline6thStatus[0]->getUsername();
                                                        echo "<br>"; 
                                                        echo "AAAAA"; 
                                                        // $bonusLvlSix = (1000 * 0.02);
                                                        // $bonusLvlSix = ($orderPrice * 0.02);
                                                        // $bonusName = 'Direct Sponsor (2%)';
                                                        $bonusLvlSix = ($orderPrice * 0.01);
                                                        $bonusName = 'Direct Sponsor (1%)';
                                                        $newUpline6thCredit = $upline6thCredit + $bonusLvlSix;
                                        
                                        
                                                        $tableName = array();
                                                        $tableValue =  array();
                                                        $stringType =  "";
                                                        //echo "save to database";
                                                        if($newUpline6thCredit)
                                                        {
                                                            array_push($tableName,"wallet");
                                                            array_push($tableValue,$newUpline6thCredit);
                                                            $stringType .=  "d";
                                                        }
                                                        array_push($tableValue,$uplineDetailsLvl6);
                                                        $stringType .=  "s";
                                                        // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                        // 301220 new requirement 
                                                        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? AND order_status = 'YES' ",$tableName,$tableValue,$stringType);
                                                        if($passwordUpdated)
                                                        {
                                        
                                                            $upline6thOrderStatus = $upline6thStatus[0]->getOrderStatus();
                                                            if($upline6thOrderStatus == 'YES')
                                                            {
                                                                if(directSponsorBonusLvlSix($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName))
                                                                {
                                                                    echo "success 6";  
                                                                    // echo "<br>";  
                                                                    // header('Location: ../adminOrderPending.php');
                                                                }
                                                                else
                                                                {
                                                                    echo "fail 6";    
                                                                    echo "<br>";  
                                                                }
                                                            }
                                                            else
                                                            {
                                                                // header('Location: ../adminOrderPending.php');
                                                                echo "NO 6";    
                                                                echo "<br>";  
                                                            }

                                                            // if(directSponsorBonusLvlSix($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName))
                                                            // {
                                                            //     // echo "success 6";  
                                                            //     // echo "<br>";  
                                                            //     header('Location: ../adminOrderPending.php');
                                                            // }
                                                            // else
                                                            // {
                                                            //     echo "fail 6";    
                                                            //     echo "<br>";  
                                                            // } 
                                        
                                                        }
                                                        else
                                                        {
                                                            echo "fail 666";
                                                            // header('Location: ../adminOrderPending.php');
                                                        }                                        
                                        
                                                    }
                                                    elseif($totalDownlineSix < $userLevel)
                                                    {  
                                                        //   no bonus
                                                        echo "no bonus due to not enough direct downline lvl 6";   
                                                        echo "<br>"; 
                                                    }
                                        
                                                }
                                        
                                            }
                
                                            
                                        }
                                        else
                                        {
                                            echo "fail pool fund bonus 4";    
                                            echo "<br>";  
                                        } 
                                    }
                                    else
                                    {
                                        echo "fail pool fund bonus 3";    
                                        echo "<br>";  
                                    } 
                                }
                                else
                                {
                                    echo "fail pool fund bonus 2";    
                                    echo "<br>";  
                                } 
                            }
                            else
                            {
                                echo "fail pool fund bonus 1";     
                                echo "<br>";  
                            } 

                        }
                        else
                        {
                            echo "FAIL TO UPDATE RH ORDER STATUS";
                        }
                    }
                    else
                    {
                        echo "ERROR FOR RH ORDER STATUS";
                    }
                }
                else
                {
                    echo "FAIL TO UPDATE USER ORDER STATUS";
                }
            }
            else
            {
                echo "ERROR FOR USER ORDER STATUS";
            }
        }
        else
        {
            echo "fail update order details";
            echo "<br>";  
        }
    }
    else
    {
        echo "invalid order details";
        echo "<br>";  
    }


    // $upline1stDetails = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($userUplineDetails),"s");
    // $upline1stDetails = getReferralHistory($conn," WHERE referrer_id = ? AND referral_id = '$orderUserUid' ",array("referrer_id"),array($userUplineDetails),"s");
    // $upline1stDetails = getReferralHistory($conn," WHERE referrer_id = ?  ",array("referrer_id"),array($userUplineDetails),"s");
    // $upline1stDetails = getReferralHistory($conn," WHERE referral_id = ?  ",array("referral_id"),array($userUplineDetails),"s");
    // $uplineLevel = $upline1stDetails[0]->getCurrentLevel();

    // //identify 2nd upline
    // echo $uplineDetailsLvl2 = $upline1stDetails[0]->getReferrerId();
    // echo "<br>"; 

    // $uplineStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($userUplineDetails),"s");
    // $uplineCredit = $uplineStatus[0]->getWallet();
    // echo $uplineUsername = $uplineStatus[0]->getUsername();
    // echo "<br>"; 
    // $bonusLvlOne = (1000 * 0.4);
    // $bonusName = 'Direct Sponsor (40%)';
    // $newUplineCredit = $uplineCredit + $bonusLvlOne;


    // $tableName = array();
    // $tableValue =  array();
    // $stringType =  "";
    // //echo "save to database";
    // if($newUplineCredit)
    // {
    //     array_push($tableName,"wallet");
    //     array_push($tableValue,$newUplineCredit);
    //     $stringType .=  "d";
    // }
    // array_push($tableValue,$userUplineDetails);
    // $stringType .=  "s";
    // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    // if($passwordUpdated)
    // {
 
    //     if(directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName))
    //     {
    //         echo "success 1";  
    //         echo "<br>";  
    //         // header('Location: ../adminOrderPending.php');
    //     }
    //     else
    //     {
    //         echo "fail 1";    
    //         echo "<br>";  
    //     } 

    // }
    // else
    // {
    //     echo "fail";
    // }


    // // if(directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName))
    // // {
    // //     // echo "success 1";  
    // //     // echo "<br>";  
    // //     header('Location: ../adminOrderPending.php');
    // // }
    // // else
    // // {
    // //     echo "fail 1";    
    // //     echo "<br>";  
    // // } 

    // //level 2 bonus
    // if(!$uplineDetailsLvl2)
    // {  
    //     echo "undefined upline level 2";   
    //     echo "<br>"; 
    // }
    // else
    // {  

    //     $upline2ndDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl2),"s");
    //     // identify how many direct downline
    //     $upline2ndDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl2),"s");

    //     //identify 3rd upline
    //     echo "identify 3rd upline <br>";
    //     echo $uplineDetailsLvl3 = $upline2ndDetails[0]->getReferrerId();
    //     echo "<br>"; 

    //     if ($upline2ndDownlineNum)
    //     {

    //         echo $totalDownlineTwo = count($upline2ndDownlineNum);
    //         echo "<br>"; 
            
    //         // if($totalDownlineOne >= 3)
    //         if($totalDownlineTwo >= $userLevel)
    //         {   
    //             // proceed with bonus calculation
                
    //             $upline2ndStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl2),"s");
    //             $upline2ndCredit = $upline2ndStatus[0]->getWallet();
    //             echo $upline2ndUsername = $upline2ndStatus[0]->getUsername();
    //             echo "<br>"; 
    //             $bonusLvlTwo = (1000 * 0.12);
    //             $bonusName = 'Direct Sponsor (12%)';
    //             $newUpline2ndCredit = $upline2ndCredit + $bonusLvlTwo;


    //             // $tableName = array();
    //             // $tableValue =  array();
    //             // $stringType =  "";
    //             // //echo "save to database";
    //             // if($newUpline2ndCredit)
    //             // {
    //             //     array_push($tableName,"wallet");
    //             //     array_push($tableValue,$newUpline2ndCredit);
    //             //     $stringType .=  "d";
    //             // }
    //             // array_push($tableValue,$uplineDetailsLvl2);
    //             // $stringType .=  "s";
    //             // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //             // if($passwordUpdated)
    //             // {
    //             //     echo "success";
    //             //     // header('Location: ../userBankDetails.php');    
    //             // }
    //             // else
    //             // {
    //             //     echo "fail";
    //             // }

    //             $tableName = array();
    //             $tableValue =  array();
    //             $stringType =  "";
    //             //echo "save to database";
    //             if($newUpline2ndCredit)
    //             {
    //                 array_push($tableName,"wallet");
    //                 array_push($tableValue,$newUpline2ndCredit);
    //                 $stringType .=  "d";
    //             }
    //             array_push($tableValue,$uplineDetailsLvl2);
    //             $stringType .=  "s";
    //             $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //             if($passwordUpdated)
    //             {

    //                 if(directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlOne,$bonusName))
    //                 {
    //                     echo "success 2";  
    //                     echo "<br>";  
    //                     // header('Location: ../adminOrderPending.php');
    //                 }
    //                 else
    //                 {
    //                     echo "fail 2";    
    //                     echo "<br>";  
    //                 } 

    //             }
    //             else
    //             {
    //                 echo "fail";
    //             }
                

    //             // if(directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlOne,$bonusName))
    //             // {
    //             //     // echo "success 2";  
    //             //     // echo "<br>";  
    //             //     header('Location: ../adminOrderPending.php');
    //             // }
    //             // else
    //             // {
    //             //     echo "fail 2";    
    //             //     echo "<br>";  
    //             // } 

    //         }
    //         // elseif($totalDownlineOne < 3)
    //         elseif($totalDownlineTwo < $userLevel)
    //         {  
    //             //   no bonus
    //             echo "no bonus due to not enough direct downline lvl 2";   
    //             echo "<br>"; 
    //         }

    //     }

    // }

    // //level 3 bonus
    // if(!$uplineDetailsLvl3)
    // {  
    //     echo "undefined upline level 3";   
    //     echo "<br>"; 
    // }
    // else
    // {  

    //     $upline3rdDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl3),"s");
    //     // identify how many direct downline
    //     $upline3rddDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl3),"s");

    //     //identify 4th upline
    //     echo "identify 4th upline <br>";
    //     echo $uplineDetailsLvl4 = $upline3rdDetails[0]->getReferrerId();
    //     echo "<br>"; 

    //     if ($upline3rddDownlineNum)
    //     {

    //         echo $totalDownlineThree = count($upline3rddDownlineNum);
    //         echo "<br>"; 

    //         if($totalDownlineThree >= $userLevel)
    //         {   
    //             // proceed with bonus calculation
                
    //             $upline3rdStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl3),"s");
    //             $upline3rdCredit = $upline3rdStatus[0]->getWallet();
    //             echo $upline3rdUsername = $upline3rdStatus[0]->getUsername();
    //             echo "<br>"; 
    //             $bonusLvlThree = (1000 * 0.1);
    //             $bonusName = 'Direct Sponsor (10%)';
    //             $newUpline3rdCredit = $upline3rdCredit + $bonusLvlThree;


    //             $tableName = array();
    //             $tableValue =  array();
    //             $stringType =  "";
    //             //echo "save to database";
    //             if($newUpline3rdCredit)
    //             {
    //                 array_push($tableName,"wallet");
    //                 array_push($tableValue,$newUpline3rdCredit);
    //                 $stringType .=  "d";
    //             }
    //             array_push($tableValue,$uplineDetailsLvl3);
    //             $stringType .=  "s";
    //             $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //             if($passwordUpdated)
    //             {

    //                 if(directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName))
    //                 {
    //                     echo "success 3";  
    //                     echo "<br>";  
    //                     // header('Location: ../adminOrderPending.php');
    //                 }
    //                 else
    //                 {
    //                     echo "fail 3";    
    //                     echo "<br>";  
    //                 } 

    //             }
    //             else
    //             {
    //                 echo "fail";
    //             }


    //             // if(directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName))
    //             // {
    //             //     // echo "success 3";  
    //             //     // echo "<br>";  
    //             //     header('Location: ../adminOrderPending.php');
    //             // }
    //             // else
    //             // {
    //             //     echo "fail 3";    
    //             //     echo "<br>";  
    //             // } 

    //         }
    //         elseif($totalDownlineThree < $userLevel)
    //         {  
    //             //   no bonus
    //             echo "no bonus due to not enough direct downline lvl 3";   
    //             echo "<br>"; 
    //         }

    //     }

    // }

    // //level 4 bonus
    // if(!$uplineDetailsLvl4)
    // {  
    //     echo "undefined upline level 4";   
    //     echo "<br>"; 
    // }
    // else
    // {  

    //     $upline4thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl4),"s");
    //     // identify how many direct downline
    //     $upline4thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl4),"s");

    //     //identify 5th upline
    //     echo "identify 5thh upline <br>";
    //     echo $uplineDetailsLvl5 = $upline4thDetails[0]->getReferrerId();
    //     echo "<br>"; 

    //     if ($upline4thDownlineNum)
    //     {

    //         echo $totalDownlineFour = count($upline4thDownlineNum);
    //         echo "<br>"; 

    //         if($totalDownlineFour >= $userLevel)
    //         {   
    //             // proceed with bonus calculation
                
    //             $upline4thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl4),"s");
    //             $upline4thCredit = $upline4thStatus[0]->getWallet();
    //             echo $upline4thUsername = $upline4thStatus[0]->getUsername();
    //             echo "<br>"; 
    //             $bonusLvlFour = (1000 * 0.05);
    //             $bonusName = 'Direct Sponsor (5%)';
    //             $newUpline4thCredit = $upline4thCredit + $bonusLvlFour;


    //             $tableName = array();
    //             $tableValue =  array();
    //             $stringType =  "";
    //             //echo "save to database";
    //             if($newUpline4thCredit)
    //             {
    //                 array_push($tableName,"wallet");
    //                 array_push($tableValue,$newUpline4thCredit);
    //                 $stringType .=  "d";
    //             }
    //             array_push($tableValue,$uplineDetailsLvl4);
    //             $stringType .=  "s";
    //             $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //             if($passwordUpdated)
    //             {

    //                 if(directSponsorBonusLvlFour($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName))
    //                 {
    //                     echo "success 4";  
    //                     echo "<br>";  
    //                     // header('Location: ../adminOrderPending.php');
    //                 }
    //                 else
    //                 {
    //                     echo "fail 4";    
    //                     echo "<br>";  
    //                 } 

    //             }
    //             else
    //             {
    //                 echo "fail";
    //             }


    //             // if(directSponsorBonusLvlFour($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName))
    //             // {
    //             //     // echo "success 4";  
    //             //     // echo "<br>";  
    //             //     header('Location: ../adminOrderPending.php');
    //             // }
    //             // else
    //             // {
    //             //     echo "fail 4";    
    //             //     echo "<br>";  
    //             // } 

    //         }
    //         elseif($totalDownlineFour < $userLevel)
    //         {  
    //             //   no bonus
    //             echo "no bonus due to not enough direct downline lvl 4";   
    //             echo "<br>"; 
    //         }

    //     }

    // }

    // //level 5 bonus
    // if(!$uplineDetailsLvl5)
    // {  
    //     echo "undefined upline level 5";   
    //     echo "<br>"; 
    // }
    // else
    // {  

    //     $upline5thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl5),"s");
    //     // identify how many direct downline
    //     $upline5thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl5),"s");

    //     //identify 6th upline
    //     echo "identify 6th upline <br>";
    //     echo $uplineDetailsLvl6 = $upline5thDetails[0]->getReferrerId();
    //     echo "<br>"; 

    //     if ($upline5thDownlineNum)
    //     {

    //         echo $totalDownlineFive = count($upline5thDownlineNum);
    //         echo "<br>"; 

    //         if($totalDownlineFive >= $userLevel)
    //         {   
    //             // proceed with bonus calculation
                
    //             $upline5thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl5),"s");
    //             $upline5thCredit = $upline5thStatus[0]->getWallet();
    //             echo $upline5thUsername = $upline5thStatus[0]->getUsername();
    //             echo "<br>"; 
    //             $bonusLvlFive = (1000 * 0.03);
    //             $bonusName = 'Direct Sponsor (3%)';
    //             $newUpline5thCredit = $upline5thCredit + $bonusLvlFive;


    //             $tableName = array();
    //             $tableValue =  array();
    //             $stringType =  "";
    //             //echo "save to database";
    //             if($newUpline5thCredit)
    //             {
    //                 array_push($tableName,"wallet");
    //                 array_push($tableValue,$newUpline5thCredit);
    //                 $stringType .=  "d";
    //             }
    //             array_push($tableValue,$uplineDetailsLvl5);
    //             $stringType .=  "s";
    //             $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //             if($passwordUpdated)
    //             {

    //                 if(directSponsorBonusLvlFive($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName))
    //                 {
    //                     echo "success 5";  
    //                     echo "<br>";  
    //                     // header('Location: ../adminOrderPending.php');
    //                 }
    //                 else
    //                 {
    //                     echo "fail 5";    
    //                     echo "<br>";  
    //                 } 

    //             }
    //             else
    //             {
    //                 echo "fail";
    //             }


    //             // if(directSponsorBonusLvlFive($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName))
    //             // {
    //             //     // echo "success 5";  
    //             //     // echo "<br>";  
    //             //     header('Location: ../adminOrderPending.php');
    //             // }
    //             // else
    //             // {
    //             //     echo "fail 5";    
    //             //     echo "<br>";  
    //             // } 

    //         }
    //         elseif($totalDownlineFive < $userLevel)
    //         {  
    //             //   no bonus
    //             echo "no bonus due to not enough direct downline lvl 5";   
    //             echo "<br>"; 
    //         }

    //     }

    // }

    // //level 6 bonus
    // if(!$uplineDetailsLvl6)
    // {  
    //     echo "undefined upline level 6";   
    //     echo "<br>"; 
    // }
    // else
    // {  

    //     $upline6thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl6),"s");
    //     // identify how many direct downline
    //     $upline6thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl6),"s");

    //     // //identify 6th upline
    //     // echo "identify 7th upline <br>";
    //     // echo $uplineDetailsLvl7 = $upline6thDetails[0]->getReferrerId();
    //     // echo "<br>"; 

    //     if ($upline6thDownlineNum)
    //     {

    //         echo $totalDownlineSix = count($upline6thDownlineNum);
    //         echo "<br>"; 

    //         if($totalDownlineSix >= $userLevel)
    //         {   
    //             // proceed with bonus calculation
                
    //             $upline6thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl6),"s");
    //             $upline6thCredit = $upline6thStatus[0]->getWallet();
    //             echo $upline6thUsername = $upline6thStatus[0]->getUsername();
    //             echo "<br>"; 
    //             $bonusLvlSix = (1000 * 0.02);
    //             $bonusName = 'Direct Sponsor (2%)';
    //             $newUpline6thCredit = $upline6thCredit + $bonusLvlSix;


    //             $tableName = array();
    //             $tableValue =  array();
    //             $stringType =  "";
    //             //echo "save to database";
    //             if($newUpline6thCredit)
    //             {
    //                 array_push($tableName,"wallet");
    //                 array_push($tableValue,$newUpline6thCredit);
    //                 $stringType .=  "d";
    //             }
    //             array_push($tableValue,$uplineDetailsLvl6);
    //             $stringType .=  "s";
    //             $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //             if($passwordUpdated)
    //             {

    //                 if(directSponsorBonusLvlSix($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName))
    //                 {
    //                     echo "success 6";  
    //                     echo "<br>";  
    //                     // header('Location: ../adminOrderPending.php');
    //                 }
    //                 else
    //                 {
    //                     echo "fail 6";    
    //                     echo "<br>";  
    //                 } 

    //             }
    //             else
    //             {
    //                 echo "fail";
    //             }


    //             // if(directSponsorBonusLvlSix($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName))
    //             // {
    //             //     // echo "success 6";  
    //             //     // echo "<br>";  
    //             //     header('Location: ../adminOrderPending.php');
    //             // }
    //             // else
    //             // {
    //             //     echo "fail 6";    
    //             //     echo "<br>";  
    //             // } 

    //         }
    //         elseif($totalDownlineSix < $userLevel)
    //         {  
    //             //   no bonus
    //             echo "no bonus due to not enough direct downline lvl 6";   
    //             echo "<br>"; 
    //         }

    //     }

    // }

}
else 
{
    header('Location: ../index.php');
}
?>