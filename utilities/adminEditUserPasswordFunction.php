<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/languageFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     // $uid  = $_SESSION['uid'];

     $userUid = rewrite($_POST["user_uid"]);
     $editPassword_new = rewrite($_POST["new_password"]);

     // $userUid  = $_POST['user_uid'];
     // // $editPassword_current  = $_POST['current_password'];
     // $editPassword_new  = $_POST['new_password'];
     // // $editPassword_reenter  = $_POST['retype_new_password'];

     $user = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");
     // $userDetails = $user[0];
     // echo $userDetails->getUsername();


     // // FOR DEBUGGING 
     // echo "<br>";
     // echo $userUid."<br>";
     // echo $editPassword_new."<br>";

     // if(!$user)
     if($user)
     {
          if(strlen($editPassword_new) >= 6 )
          {
               $password = hash('sha256',$editPassword_new);
               $salt = substr(sha1(mt_rand()), 0, 100);
               $finalPassword = hash('sha256', $salt.$password);
     
               $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("password","salt"),array($finalPassword,$salt,$userUid),"sss");
               if($passwordUpdated)
               {
                    echo "<script>alert('Update Password success !');window.location='../adminViewMember.php'</script>";
               }
               else 
               {
                    echo "<script>alert('Fail to update password ');window.location='../adminViewMember.php'</script>";
               }
          }
          else 
          {
               echo "<script>alert('password length must be more than 5');window.location='../adminViewMember.php'</script>";
          }
     }
     else
     {
          echo "<script>alert('no such user !');window.location='../adminViewMember.php'</script>";
     }

     // if(strlen($editPassword_new) >= 6 )
     // {
     //      $password = hash('sha256',$editPassword_new);
     //      $salt = substr(sha1(mt_rand()), 0, 100);
     //      $finalPassword = hash('sha256', $salt.$password);

     //      $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("password","salt"),array($finalPassword,$salt,$userUid),"sss");
     //      if($passwordUpdated)
     //      {
     //           echo "<script>alert('Update Password success !');window.location='../adminViewMember.php'</script>";
     //      }
     //      else 
     //      {
     //           echo "<script>alert('Fail to update password ');window.location='../adminViewMember.php'</script>";
     //      }
     // }
     // else 
     // {
     //      echo "<script>alert('password length must be more than 5');window.location='../adminViewMember.php'</script>";
     // }  
}
?>