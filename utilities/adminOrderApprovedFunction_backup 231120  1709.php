<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Bonus.php';
require_once dirname(__FILE__) . '/../classes/BonusPoolFund.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

function directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlTwo,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlTwo,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlFour($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlFive($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlSix($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

//bonus 2 (pool fund)

function bonusTwoPoolFundOne($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundOneAmount,$poolFundOneReceiver,$bonusTypePF)
{
     if(insertDynamicData($conn,"bonus_poolfund",array("order_uid","uid","username","amount","receiver","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$poolFundOneAmount,$poolFundOneReceiver,$bonusTypePF),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function bonusTwoPoolFundTwo($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundTwoAmount,$poolFundTwoReceiver,$bonusTypePF)
{
     if(insertDynamicData($conn,"bonus_poolfund",array("order_uid","uid","username","amount","receiver","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$poolFundTwoAmount,$poolFundTwoReceiver,$bonusTypePF),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function bonusTwoPoolFundThree($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundThreeAmount,$poolFundThreeReceiver,$bonusTypePF)
{
     if(insertDynamicData($conn,"bonus_poolfund",array("order_uid","uid","username","amount","receiver","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$poolFundThreeAmount,$poolFundThreeReceiver,$bonusTypePF),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function bonusTwoPoolFundFour($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundFourAmount,$poolFundFourReceiver,$bonusTypePF)
{
     if(insertDynamicData($conn,"bonus_poolfund",array("order_uid","uid","username","amount","receiver","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$poolFundFourAmount,$poolFundFourReceiver,$bonusTypePF),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{

    $conn = connDB();

    echo "order id  :  " ; echo $orderUid = rewrite($_POST["order_uid"]);
    echo "<br>";  

    $orderDetails = getOrders($conn, " WHERE order_id = '$orderUid' ");
    echo $orderUserUid = $orderDetails[0]->getUid();
    echo "<br>";
    echo $orderUserName = $orderDetails[0]->getName();
    echo "<br>";

    //user details in referral history
    $userRH = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($orderUserUid),"s");
    $userLvl = $userRH[0]->getCurrentLevel();
    $userLevel = $userLvl - 1;
    //Direct Upline
    echo $userUplineDetails = $userRH[0]->getReferrerId(); //william
    echo "<br>";  

    $paymentStatus = 'APPROVED';

    //bonus 2 (pool fund) details
    $bonusTypePF = 'Bonus 2 (Pool Fund)';

    $poolFundOneAmount = (1000 * 0.02);
    $poolFundOneReceiver = 'Manager';

    $poolFundTwoAmount = (1000 * 0.02);
    $poolFundTwoReceiver = 'Senior Manager';

    $poolFundThreeAmount = (1000 * 0.03);
    $poolFundThreeReceiver = 'Area Manager';

    $poolFundFourAmount = (1000 * 0.03);
    $poolFundFourReceiver = 'District Manager';

    // // if(!$orderDetails)
    // if($orderDetails)
    // {   
    //     $tableName = array();
    //     $tableValue =  array();
    //     $stringType =  "";
    //     //echo "save to database";
    //     if($paymentStatus)
    //     {
    //         array_push($tableName,"payment_status");
    //         array_push($tableValue,$paymentStatus);
    //         $stringType .=  "s";
    //     }
    //     array_push($tableValue,$orderUid);
    //     $stringType .=  "s";
    //     $updateOrders = updateDynamicData($conn,"orders"," WHERE order_id = ? ",$tableName,$tableValue,$stringType);
    //     if($updateOrders)
    //     {

    //         if(bonusTwoPoolFundOne($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundOneAmount,$poolFundOneReceiver,$bonusTypePF))
    //         {
    //             if(bonusTwoPoolFundTwo($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundTwoAmount,$poolFundTwoReceiver,$bonusTypePF))
    //             {
    //                 if(bonusTwoPoolFundThree($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundThreeAmount,$poolFundThreeReceiver,$bonusTypePF))
    //                 {
    //                     if(bonusTwoPoolFundFour($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundFourAmount,$poolFundFourReceiver,$bonusTypePF))
    //                     {

    //                         if($userLevel = 0)
    //                         {
                    
    //                             $upline1stDetails = getReferralHistory($conn," WHERE referral_id = ?  ",array("referral_id"),array($userUplineDetails),"s");
    //                             $uplineLevel = $upline1stDetails[0]->getCurrentLevel();
                            
    //                             //identify 2nd upline
    //                             // echo $uplineDetailsLvl2 = $upline1stDetails[0]->getReferrerId();
    //                             // echo "<br>"; 
                            
    //                             $uplineStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($userUplineDetails),"s");
    //                             $uplineCredit = $uplineStatus[0]->getWallet();
    //                             echo $uplineUsername = $uplineStatus[0]->getUsername();
    //                             echo "<br>"; 
    //                             $bonusLvlOne = (1000 * 0.4);
    //                             $bonusName = 'Direct Sponsor (40%)';
    //                             $newUplineCredit = $uplineCredit + $bonusLvlOne;
                            
                            
    //                             $tableName = array();
    //                             $tableValue =  array();
    //                             $stringType =  "";
    //                             //echo "save to database";
    //                             if($newUplineCredit)
    //                             {
    //                                 array_push($tableName,"wallet");
    //                                 array_push($tableValue,$newUplineCredit);
    //                                 $stringType .=  "d";
    //                             }
    //                             array_push($tableValue,$userUplineDetails);
    //                             $stringType .=  "s";
    //                             $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                             if($passwordUpdated)
    //                             {
                             
    //                                 if(directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName))
    //                                 {
    //                                     // echo "success 1";  
    //                                     // echo "<br>";  
    //                                     header('Location: ../adminOrderPending.php');
    //                                 }
    //                                 else
    //                                 {
    //                                     echo "fail 1";    
    //                                     echo "<br>";  
    //                                 } 
                            
    //                             }
    //                             else
    //                             {
    //                                 echo "fail";
    //                             }

    //                         }
    //                         elseif($userLevel = 1)
    //                         {
                
    //                             $upline1stDetails = getReferralHistory($conn," WHERE referral_id = ?  ",array("referral_id"),array($userUplineDetails),"s");
    //                             $uplineLevel = $upline1stDetails[0]->getCurrentLevel();
                            
    //                             //identify 2nd upline
    //                             echo $uplineDetailsLvl2 = $upline1stDetails[0]->getReferrerId();
    //                             echo "<br>"; 
                            
    //                             $uplineStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($userUplineDetails),"s");
    //                             $uplineCredit = $uplineStatus[0]->getWallet();
    //                             echo $uplineUsername = $uplineStatus[0]->getUsername();
    //                             echo "<br>"; 
    //                             $bonusLvlOne = (1000 * 0.4);
    //                             $bonusName = 'Direct Sponsor (40%)';
    //                             $newUplineCredit = $uplineCredit + $bonusLvlOne;
                            
                            
    //                             $tableName = array();
    //                             $tableValue =  array();
    //                             $stringType =  "";
    //                             //echo "save to database";
    //                             if($newUplineCredit)
    //                             {
    //                                 array_push($tableName,"wallet");
    //                                 array_push($tableValue,$newUplineCredit);
    //                                 $stringType .=  "d";
    //                             }
    //                             array_push($tableValue,$userUplineDetails);
    //                             $stringType .=  "s";
    //                             $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                             if($passwordUpdated)
    //                             {
                             
    //                                 if(directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName))
    //                                 {
    //                                     // echo "success 1";  
    //                                     // echo "<br>";  
    //                                     header('Location: ../adminOrderPending.php');
    //                                 }
    //                                 else
    //                                 {
    //                                     echo "fail 1";    
    //                                     echo "<br>";  
    //                                 } 
                            
    //                             }
    //                             else
    //                             {
    //                                 echo "fail";
    //                             }
                                                    
    //                             //level 2 bonus
    //                             if(!$uplineDetailsLvl2)
    //                             {  
    //                                 echo "undefined upline level 2";   
    //                                 echo "<br>"; 
    //                             }
    //                             else
    //                             {  
                            
    //                                 $upline2ndDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl2),"s");
    //                                 // identify how many direct downline
    //                                 $upline2ndDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl2),"s");
                            
    //                                 //identify 3rd upline
    //                                 // echo "identify 3rd upline <br>";
    //                                 // echo $uplineDetailsLvl3 = $upline2ndDetails[0]->getReferrerId();
    //                                 // echo "<br>"; 
                            
    //                                 if ($upline2ndDownlineNum)
    //                                 {
                            
    //                                     echo $totalDownlineTwo = count($upline2ndDownlineNum);
    //                                     echo "<br>"; 
                                        
    //                                     // if($totalDownlineOne >= 3)
    //                                     if($totalDownlineTwo >= $userLevel)
    //                                     {   
    //                                         // proceed with bonus calculation
                                            
    //                                         $upline2ndStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl2),"s");
    //                                         $upline2ndCredit = $upline2ndStatus[0]->getWallet();
    //                                         echo $upline2ndUsername = $upline2ndStatus[0]->getUsername();
    //                                         echo "<br>"; 
    //                                         $bonusLvlTwo = (1000 * 0.12);
    //                                         $bonusName = 'Direct Sponsor (12%)';
    //                                         $newUpline2ndCredit = $upline2ndCredit + $bonusLvlTwo;
                                                        
    //                                         $tableName = array();
    //                                         $tableValue =  array();
    //                                         $stringType =  "";
    //                                         //echo "save to database";
    //                                         if($newUpline2ndCredit)
    //                                         {
    //                                             array_push($tableName,"wallet");
    //                                             array_push($tableValue,$newUpline2ndCredit);
    //                                             $stringType .=  "d";
    //                                         }
    //                                         array_push($tableValue,$uplineDetailsLvl2);
    //                                         $stringType .=  "s";
    //                                         $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                                         if($passwordUpdated)
    //                                         {
                            
    //                                             if(directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlOne,$bonusName))
    //                                             {
    //                                                 // echo "success 2";  
    //                                                 // echo "<br>";  
    //                                                 header('Location: ../adminOrderPending.php');
    //                                             }
    //                                             else
    //                                             {
    //                                                 echo "fail 2";    
    //                                                 echo "<br>";  
    //                                             } 
                            
    //                                         }
    //                                         else
    //                                         {
    //                                             echo "fail";
    //                                         }
                                                                        
    //                                     }
    //                                     // elseif($totalDownlineOne < 3)
    //                                     elseif($totalDownlineTwo < $userLevel)
    //                                     {  
    //                                         //   no bonus
    //                                         echo "no bonus due to not enough direct downline lvl 2";   
    //                                         echo "<br>"; 
    //                                     }
                            
    //                                 }
                            
    //                             }

    //                         }
    //                         elseif($userLevel = 2)
    //                         {
                
    //                             $upline1stDetails = getReferralHistory($conn," WHERE referral_id = ?  ",array("referral_id"),array($userUplineDetails),"s");
    //                             $uplineLevel = $upline1stDetails[0]->getCurrentLevel();
                            
    //                             //identify 2nd upline
    //                             echo $uplineDetailsLvl2 = $upline1stDetails[0]->getReferrerId();
    //                             echo "<br>"; 
                            
    //                             $uplineStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($userUplineDetails),"s");
    //                             $uplineCredit = $uplineStatus[0]->getWallet();
    //                             echo $uplineUsername = $uplineStatus[0]->getUsername();
    //                             echo "<br>"; 
    //                             $bonusLvlOne = (1000 * 0.4);
    //                             $bonusName = 'Direct Sponsor (40%)';
    //                             $newUplineCredit = $uplineCredit + $bonusLvlOne;
                            
                            
    //                             $tableName = array();
    //                             $tableValue =  array();
    //                             $stringType =  "";
    //                             //echo "save to database";
    //                             if($newUplineCredit)
    //                             {
    //                                 array_push($tableName,"wallet");
    //                                 array_push($tableValue,$newUplineCredit);
    //                                 $stringType .=  "d";
    //                             }
    //                             array_push($tableValue,$userUplineDetails);
    //                             $stringType .=  "s";
    //                             $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                             if($passwordUpdated)
    //                             {
                             
    //                                 if(directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName))
    //                                 {
    //                                     // echo "success 1";  
    //                                     // echo "<br>";  
    //                                     header('Location: ../adminOrderPending.php');
    //                                 }
    //                                 else
    //                                 {
    //                                     echo "fail 1";    
    //                                     echo "<br>";  
    //                                 } 
                            
    //                             }
    //                             else
    //                             {
    //                                 echo "fail";
    //                             }
                                                        
    //                             //level 2 bonus
    //                             if(!$uplineDetailsLvl2)
    //                             {  
    //                                 echo "undefined upline level 2";   
    //                                 echo "<br>"; 
    //                             }
    //                             else
    //                             {  
                            
    //                                 $upline2ndDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl2),"s");
    //                                 // identify how many direct downline
    //                                 $upline2ndDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl2),"s");
                            
    //                                 //identify 3rd upline
    //                                 echo "identify 3rd upline <br>";
    //                                 echo $uplineDetailsLvl3 = $upline2ndDetails[0]->getReferrerId();
    //                                 echo "<br>"; 
                            
    //                                 if ($upline2ndDownlineNum)
    //                                 {
                            
    //                                     echo $totalDownlineTwo = count($upline2ndDownlineNum);
    //                                     echo "<br>"; 
                                        
    //                                     // if($totalDownlineOne >= 3)
    //                                     if($totalDownlineTwo >= $userLevel)
    //                                     {   
    //                                         // proceed with bonus calculation
                                            
    //                                         $upline2ndStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl2),"s");
    //                                         $upline2ndCredit = $upline2ndStatus[0]->getWallet();
    //                                         echo $upline2ndUsername = $upline2ndStatus[0]->getUsername();
    //                                         echo "<br>"; 
    //                                         $bonusLvlTwo = (1000 * 0.12);
    //                                         $bonusName = 'Direct Sponsor (12%)';
    //                                         $newUpline2ndCredit = $upline2ndCredit + $bonusLvlTwo;
                            
    //                                         $tableName = array();
    //                                         $tableValue =  array();
    //                                         $stringType =  "";
    //                                         //echo "save to database";
    //                                         if($newUpline2ndCredit)
    //                                         {
    //                                             array_push($tableName,"wallet");
    //                                             array_push($tableValue,$newUpline2ndCredit);
    //                                             $stringType .=  "d";
    //                                         }
    //                                         array_push($tableValue,$uplineDetailsLvl2);
    //                                         $stringType .=  "s";
    //                                         $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                                         if($passwordUpdated)
    //                                         {
                            
    //                                             if(directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlOne,$bonusName))
    //                                             {
    //                                                 // echo "success 2";  
    //                                                 // echo "<br>";  
    //                                                 header('Location: ../adminOrderPending.php');
    //                                             }
    //                                             else
    //                                             {
    //                                                 echo "fail 2";    
    //                                                 echo "<br>";  
    //                                             } 
                            
    //                                         }
    //                                         else
    //                                         {
    //                                             echo "fail";
    //                                         }
                            
    //                                     }
    //                                     // elseif($totalDownlineOne < 3)
    //                                     elseif($totalDownlineTwo < $userLevel)
    //                                     {  
    //                                         //   no bonus
    //                                         echo "no bonus due to not enough direct downline lvl 2";   
    //                                         echo "<br>"; 
    //                                     }
                            
    //                                 }
                            
    //                             }
                            
    //                             //level 3 bonus
    //                             if(!$uplineDetailsLvl3)
    //                             {  
    //                                 echo "undefined upline level 3";   
    //                                 echo "<br>"; 
    //                             }
    //                             else
    //                             {  
                            
    //                                 $upline3rdDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl3),"s");
    //                                 // identify how many direct downline
    //                                 $upline3rddDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl3),"s");
                            
    //                                 //identify 4th upline
    //                                 // echo "identify 4th upline <br>";
    //                                 // echo $uplineDetailsLvl4 = $upline3rdDetails[0]->getReferrerId();
    //                                 // echo "<br>"; 
                            
    //                                 if ($upline3rddDownlineNum)
    //                                 {
                            
    //                                     echo $totalDownlineThree = count($upline3rddDownlineNum);
    //                                     echo "<br>"; 
                            
    //                                     if($totalDownlineThree >= $userLevel)
    //                                     {   
    //                                         // proceed with bonus calculation
                                            
    //                                         $upline3rdStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl3),"s");
    //                                         $upline3rdCredit = $upline3rdStatus[0]->getWallet();
    //                                         echo $upline3rdUsername = $upline3rdStatus[0]->getUsername();
    //                                         echo "<br>"; 
    //                                         $bonusLvlThree = (1000 * 0.1);
    //                                         $bonusName = 'Direct Sponsor (10%)';
    //                                         $newUpline3rdCredit = $upline3rdCredit + $bonusLvlThree;
                            
                            
    //                                         $tableName = array();
    //                                         $tableValue =  array();
    //                                         $stringType =  "";
    //                                         //echo "save to database";
    //                                         if($newUpline3rdCredit)
    //                                         {
    //                                             array_push($tableName,"wallet");
    //                                             array_push($tableValue,$newUpline3rdCredit);
    //                                             $stringType .=  "d";
    //                                         }
    //                                         array_push($tableValue,$uplineDetailsLvl3);
    //                                         $stringType .=  "s";
    //                                         $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                                         if($passwordUpdated)
    //                                         {
                            
    //                                             if(directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName))
    //                                             {
    //                                                 // echo "success 3";  
    //                                                 // echo "<br>";  
    //                                                 header('Location: ../adminOrderPending.php');
    //                                             }
    //                                             else
    //                                             {
    //                                                 echo "fail 3";    
    //                                                 echo "<br>";  
    //                                             } 
                            
    //                                         }
    //                                         else
    //                                         {
    //                                             echo "fail";
    //                                         }     

    //                                     }
    //                                     elseif($totalDownlineThree < $userLevel)
    //                                     {  
    //                                         //   no bonus
    //                                         echo "no bonus due to not enough direct downline lvl 3";   
    //                                         echo "<br>"; 
    //                                     }
                            
    //                                 }
                            
    //                             }

    //                         }
    //                         elseif($userLevel = 3)
    //                         {
                
    //                             $upline1stDetails = getReferralHistory($conn," WHERE referral_id = ?  ",array("referral_id"),array($userUplineDetails),"s");
    //                             $uplineLevel = $upline1stDetails[0]->getCurrentLevel();
                            
    //                             //identify 2nd upline
    //                             echo $uplineDetailsLvl2 = $upline1stDetails[0]->getReferrerId();
    //                             echo "<br>"; 
                            
    //                             $uplineStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($userUplineDetails),"s");
    //                             $uplineCredit = $uplineStatus[0]->getWallet();
    //                             echo $uplineUsername = $uplineStatus[0]->getUsername();
    //                             echo "<br>"; 
    //                             $bonusLvlOne = (1000 * 0.4);
    //                             $bonusName = 'Direct Sponsor (40%)';
    //                             $newUplineCredit = $uplineCredit + $bonusLvlOne;
                            
                            
    //                             $tableName = array();
    //                             $tableValue =  array();
    //                             $stringType =  "";
    //                             //echo "save to database";
    //                             if($newUplineCredit)
    //                             {
    //                                 array_push($tableName,"wallet");
    //                                 array_push($tableValue,$newUplineCredit);
    //                                 $stringType .=  "d";
    //                             }
    //                             array_push($tableValue,$userUplineDetails);
    //                             $stringType .=  "s";
    //                             $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                             if($passwordUpdated)
    //                             {
                             
    //                                 if(directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName))
    //                                 {
    //                                     // echo "success 1";  
    //                                     // echo "<br>";  
    //                                     header('Location: ../adminOrderPending.php');
    //                                 }
    //                                 else
    //                                 {
    //                                     echo "fail 1";    
    //                                     echo "<br>";  
    //                                 } 
                            
    //                             }
    //                             else
    //                             {
    //                                 echo "fail";
    //                             }

    //                             //level 2 bonus
    //                             if(!$uplineDetailsLvl2)
    //                             {  
    //                                 echo "undefined upline level 2";   
    //                                 echo "<br>"; 
    //                             }
    //                             else
    //                             {  
                            
    //                                 $upline2ndDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl2),"s");
    //                                 // identify how many direct downline
    //                                 $upline2ndDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl2),"s");
                            
    //                                 //identify 3rd upline
    //                                 echo "identify 3rd upline <br>";
    //                                 echo $uplineDetailsLvl3 = $upline2ndDetails[0]->getReferrerId();
    //                                 echo "<br>"; 
                            
    //                                 if ($upline2ndDownlineNum)
    //                                 {
                            
    //                                     echo $totalDownlineTwo = count($upline2ndDownlineNum);
    //                                     echo "<br>"; 
                                        
    //                                     // if($totalDownlineOne >= 3)
    //                                     if($totalDownlineTwo >= $userLevel)
    //                                     {   
    //                                         // proceed with bonus calculation
                                            
    //                                         $upline2ndStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl2),"s");
    //                                         $upline2ndCredit = $upline2ndStatus[0]->getWallet();
    //                                         echo $upline2ndUsername = $upline2ndStatus[0]->getUsername();
    //                                         echo "<br>"; 
    //                                         $bonusLvlTwo = (1000 * 0.12);
    //                                         $bonusName = 'Direct Sponsor (12%)';
    //                                         $newUpline2ndCredit = $upline2ndCredit + $bonusLvlTwo;
                            
    //                                         $tableName = array();
    //                                         $tableValue =  array();
    //                                         $stringType =  "";
    //                                         //echo "save to database";
    //                                         if($newUpline2ndCredit)
    //                                         {
    //                                             array_push($tableName,"wallet");
    //                                             array_push($tableValue,$newUpline2ndCredit);
    //                                             $stringType .=  "d";
    //                                         }
    //                                         array_push($tableValue,$uplineDetailsLvl2);
    //                                         $stringType .=  "s";
    //                                         $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                                         if($passwordUpdated)
    //                                         {
                            
    //                                             if(directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlOne,$bonusName))
    //                                             {
    //                                                 // echo "success 2";  
    //                                                 // echo "<br>";  
    //                                                 header('Location: ../adminOrderPending.php');
    //                                             }
    //                                             else
    //                                             {
    //                                                 echo "fail 2";    
    //                                                 echo "<br>";  
    //                                             } 
                            
    //                                         }
    //                                         else
    //                                         {
    //                                             echo "fail";
    //                                         }
                            
    //                                     }
    //                                     // elseif($totalDownlineOne < 3)
    //                                     elseif($totalDownlineTwo < $userLevel)
    //                                     {  
    //                                         //   no bonus
    //                                         echo "no bonus due to not enough direct downline lvl 2";   
    //                                         echo "<br>"; 
    //                                     }
                            
    //                                 }
                            
    //                             }
                            
    //                             //level 3 bonus
    //                             if(!$uplineDetailsLvl3)
    //                             {  
    //                                 echo "undefined upline level 3";   
    //                                 echo "<br>"; 
    //                             }
    //                             else
    //                             {  
                            
    //                                 $upline3rdDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl3),"s");
    //                                 // identify how many direct downline
    //                                 $upline3rddDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl3),"s");
                            
    //                                 //identify 4th upline
    //                                 echo "identify 4th upline <br>";
    //                                 echo $uplineDetailsLvl4 = $upline3rdDetails[0]->getReferrerId();
    //                                 echo "<br>"; 
                            
    //                                 if ($upline3rddDownlineNum)
    //                                 {
                            
    //                                     echo $totalDownlineThree = count($upline3rddDownlineNum);
    //                                     echo "<br>"; 
                            
    //                                     if($totalDownlineThree >= $userLevel)
    //                                     {   
    //                                         // proceed with bonus calculation
                                            
    //                                         $upline3rdStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl3),"s");
    //                                         $upline3rdCredit = $upline3rdStatus[0]->getWallet();
    //                                         echo $upline3rdUsername = $upline3rdStatus[0]->getUsername();
    //                                         echo "<br>"; 
    //                                         $bonusLvlThree = (1000 * 0.1);
    //                                         $bonusName = 'Direct Sponsor (10%)';
    //                                         $newUpline3rdCredit = $upline3rdCredit + $bonusLvlThree;
                            
                            
    //                                         $tableName = array();
    //                                         $tableValue =  array();
    //                                         $stringType =  "";
    //                                         //echo "save to database";
    //                                         if($newUpline3rdCredit)
    //                                         {
    //                                             array_push($tableName,"wallet");
    //                                             array_push($tableValue,$newUpline3rdCredit);
    //                                             $stringType .=  "d";
    //                                         }
    //                                         array_push($tableValue,$uplineDetailsLvl3);
    //                                         $stringType .=  "s";
    //                                         $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                                         if($passwordUpdated)
    //                                         {
                            
    //                                             if(directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName))
    //                                             {
    //                                                 // echo "success 3";  
    //                                                 // echo "<br>";  
    //                                                 header('Location: ../adminOrderPending.php');
    //                                             }
    //                                             else
    //                                             {
    //                                                 echo "fail 3";    
    //                                                 echo "<br>";  
    //                                             } 
                            
    //                                         }
    //                                         else
    //                                         {
    //                                             echo "fail";
    //                                         }
                            
    //                                     }
    //                                     elseif($totalDownlineThree < $userLevel)
    //                                     {  
    //                                         //   no bonus
    //                                         echo "no bonus due to not enough direct downline lvl 3";   
    //                                         echo "<br>"; 
    //                                     }
                            
    //                                 }
                            
    //                             }
                            
    //                             //level 4 bonus
    //                             if(!$uplineDetailsLvl4)
    //                             {  
    //                                 echo "undefined upline level 4";   
    //                                 echo "<br>"; 
    //                             }
    //                             else
    //                             {  
                            
    //                                 $upline4thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl4),"s");
    //                                 // identify how many direct downline
    //                                 $upline4thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl4),"s");
                            
    //                                 //identify 5th upline
    //                                 // echo "identify 5thh upline <br>";
    //                                 // echo $uplineDetailsLvl5 = $upline4thDetails[0]->getReferrerId();
    //                                 // echo "<br>"; 
                            
    //                                 if ($upline4thDownlineNum)
    //                                 {
                            
    //                                     echo $totalDownlineFour = count($upline4thDownlineNum);
    //                                     echo "<br>"; 
                            
    //                                     if($totalDownlineFour >= $userLevel)
    //                                     {   
    //                                         // proceed with bonus calculation
                                            
    //                                         $upline4thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl4),"s");
    //                                         $upline4thCredit = $upline4thStatus[0]->getWallet();
    //                                         echo $upline4thUsername = $upline4thStatus[0]->getUsername();
    //                                         echo "<br>"; 
    //                                         $bonusLvlFour = (1000 * 0.05);
    //                                         $bonusName = 'Direct Sponsor (5%)';
    //                                         $newUpline4thCredit = $upline4thCredit + $bonusLvlFour;
                            
                            
    //                                         $tableName = array();
    //                                         $tableValue =  array();
    //                                         $stringType =  "";
    //                                         //echo "save to database";
    //                                         if($newUpline4thCredit)
    //                                         {
    //                                             array_push($tableName,"wallet");
    //                                             array_push($tableValue,$newUpline4thCredit);
    //                                             $stringType .=  "d";
    //                                         }
    //                                         array_push($tableValue,$uplineDetailsLvl4);
    //                                         $stringType .=  "s";
    //                                         $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                                         if($passwordUpdated)
    //                                         {
                            
    //                                             if(directSponsorBonusLvlFour($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName))
    //                                             {
    //                                                 // echo "success 4";  
    //                                                 // echo "<br>";  
    //                                                 header('Location: ../adminOrderPending.php');
    //                                             }
    //                                             else
    //                                             {
    //                                                 echo "fail 4";    
    //                                                 echo "<br>";  
    //                                             } 
                            
    //                                         }
    //                                         else
    //                                         {
    //                                             echo "fail";
    //                                         }
                                                        
    //                                     }
    //                                     elseif($totalDownlineFour < $userLevel)
    //                                     {  
    //                                         //   no bonus
    //                                         echo "no bonus due to not enough direct downline lvl 4";   
    //                                         echo "<br>"; 
    //                                     }
                            
    //                                 }
                            
    //                             }

    //                         }
    //                         elseif($userLevel = 4)
    //                         {
                
    //                             $upline1stDetails = getReferralHistory($conn," WHERE referral_id = ?  ",array("referral_id"),array($userUplineDetails),"s");
    //                             $uplineLevel = $upline1stDetails[0]->getCurrentLevel();
                            
    //                             //identify 2nd upline
    //                             echo $uplineDetailsLvl2 = $upline1stDetails[0]->getReferrerId();
    //                             echo "<br>"; 
                            
    //                             $uplineStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($userUplineDetails),"s");
    //                             $uplineCredit = $uplineStatus[0]->getWallet();
    //                             echo $uplineUsername = $uplineStatus[0]->getUsername();
    //                             echo "<br>"; 
    //                             $bonusLvlOne = (1000 * 0.4);
    //                             $bonusName = 'Direct Sponsor (40%)';
    //                             $newUplineCredit = $uplineCredit + $bonusLvlOne;
                            
                            
    //                             $tableName = array();
    //                             $tableValue =  array();
    //                             $stringType =  "";
    //                             //echo "save to database";
    //                             if($newUplineCredit)
    //                             {
    //                                 array_push($tableName,"wallet");
    //                                 array_push($tableValue,$newUplineCredit);
    //                                 $stringType .=  "d";
    //                             }
    //                             array_push($tableValue,$userUplineDetails);
    //                             $stringType .=  "s";
    //                             $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                             if($passwordUpdated)
    //                             {
                             
    //                                 if(directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName))
    //                                 {
    //                                     // echo "success 1";  
    //                                     // echo "<br>";  
    //                                     header('Location: ../adminOrderPending.php');
    //                                 }
    //                                 else
    //                                 {
    //                                     echo "fail 1";    
    //                                     echo "<br>";  
    //                                 } 
                            
    //                             }
    //                             else
    //                             {
    //                                 echo "fail";
    //                             }
                            
    //                             //level 2 bonus
    //                             if(!$uplineDetailsLvl2)
    //                             {  
    //                                 echo "undefined upline level 2";   
    //                                 echo "<br>"; 
    //                             }
    //                             else
    //                             {  
                            
    //                                 $upline2ndDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl2),"s");
    //                                 // identify how many direct downline
    //                                 $upline2ndDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl2),"s");
                            
    //                                 //identify 3rd upline
    //                                 echo "identify 3rd upline <br>";
    //                                 echo $uplineDetailsLvl3 = $upline2ndDetails[0]->getReferrerId();
    //                                 echo "<br>"; 
                            
    //                                 if ($upline2ndDownlineNum)
    //                                 {
                            
    //                                     echo $totalDownlineTwo = count($upline2ndDownlineNum);
    //                                     echo "<br>"; 
                                        
    //                                     // if($totalDownlineOne >= 3)
    //                                     if($totalDownlineTwo >= $userLevel)
    //                                     {   
    //                                         // proceed with bonus calculation
                                            
    //                                         $upline2ndStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl2),"s");
    //                                         $upline2ndCredit = $upline2ndStatus[0]->getWallet();
    //                                         echo $upline2ndUsername = $upline2ndStatus[0]->getUsername();
    //                                         echo "<br>"; 
    //                                         $bonusLvlTwo = (1000 * 0.12);
    //                                         $bonusName = 'Direct Sponsor (12%)';
    //                                         $newUpline2ndCredit = $upline2ndCredit + $bonusLvlTwo;
                            
    //                                         $tableName = array();
    //                                         $tableValue =  array();
    //                                         $stringType =  "";
    //                                         //echo "save to database";
    //                                         if($newUpline2ndCredit)
    //                                         {
    //                                             array_push($tableName,"wallet");
    //                                             array_push($tableValue,$newUpline2ndCredit);
    //                                             $stringType .=  "d";
    //                                         }
    //                                         array_push($tableValue,$uplineDetailsLvl2);
    //                                         $stringType .=  "s";
    //                                         $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                                         if($passwordUpdated)
    //                                         {
                            
    //                                             if(directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlOne,$bonusName))
    //                                             {
    //                                                 // echo "success 2";  
    //                                                 // echo "<br>";  
    //                                                 header('Location: ../adminOrderPending.php');
    //                                             }
    //                                             else
    //                                             {
    //                                                 echo "fail 2";    
    //                                                 echo "<br>";  
    //                                             } 
                            
    //                                         }
    //                                         else
    //                                         {
    //                                             echo "fail";
    //                                         }
                            
    //                                     }
    //                                     // elseif($totalDownlineOne < 3)
    //                                     elseif($totalDownlineTwo < $userLevel)
    //                                     {  
    //                                         //   no bonus
    //                                         echo "no bonus due to not enough direct downline lvl 2";   
    //                                         echo "<br>"; 
    //                                     }
                            
    //                                 }
                            
    //                             }
                            
    //                             //level 3 bonus
    //                             if(!$uplineDetailsLvl3)
    //                             {  
    //                                 echo "undefined upline level 3";   
    //                                 echo "<br>"; 
    //                             }
    //                             else
    //                             {  
                            
    //                                 $upline3rdDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl3),"s");
    //                                 // identify how many direct downline
    //                                 $upline3rddDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl3),"s");
                            
    //                                 //identify 4th upline
    //                                 echo "identify 4th upline <br>";
    //                                 echo $uplineDetailsLvl4 = $upline3rdDetails[0]->getReferrerId();
    //                                 echo "<br>"; 
                            
    //                                 if ($upline3rddDownlineNum)
    //                                 {
                            
    //                                     echo $totalDownlineThree = count($upline3rddDownlineNum);
    //                                     echo "<br>"; 
                            
    //                                     if($totalDownlineThree >= $userLevel)
    //                                     {   
    //                                         // proceed with bonus calculation
                                            
    //                                         $upline3rdStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl3),"s");
    //                                         $upline3rdCredit = $upline3rdStatus[0]->getWallet();
    //                                         echo $upline3rdUsername = $upline3rdStatus[0]->getUsername();
    //                                         echo "<br>"; 
    //                                         $bonusLvlThree = (1000 * 0.1);
    //                                         $bonusName = 'Direct Sponsor (10%)';
    //                                         $newUpline3rdCredit = $upline3rdCredit + $bonusLvlThree;
                            
                            
    //                                         $tableName = array();
    //                                         $tableValue =  array();
    //                                         $stringType =  "";
    //                                         //echo "save to database";
    //                                         if($newUpline3rdCredit)
    //                                         {
    //                                             array_push($tableName,"wallet");
    //                                             array_push($tableValue,$newUpline3rdCredit);
    //                                             $stringType .=  "d";
    //                                         }
    //                                         array_push($tableValue,$uplineDetailsLvl3);
    //                                         $stringType .=  "s";
    //                                         $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                                         if($passwordUpdated)
    //                                         {
                            
    //                                             if(directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName))
    //                                             {
    //                                                 // echo "success 3";  
    //                                                 // echo "<br>";  
    //                                                 header('Location: ../adminOrderPending.php');
    //                                             }
    //                                             else
    //                                             {
    //                                                 echo "fail 3";    
    //                                                 echo "<br>";  
    //                                             } 
                            
    //                                         }
    //                                         else
    //                                         {
    //                                             echo "fail";
    //                                         }
                            
    //                                     }
    //                                     elseif($totalDownlineThree < $userLevel)
    //                                     {  
    //                                         //   no bonus
    //                                         echo "no bonus due to not enough direct downline lvl 3";   
    //                                         echo "<br>"; 
    //                                     }
                            
    //                                 }
                            
    //                             }
                            
    //                             //level 4 bonus
    //                             if(!$uplineDetailsLvl4)
    //                             {  
    //                                 echo "undefined upline level 4";   
    //                                 echo "<br>"; 
    //                             }
    //                             else
    //                             {  
                            
    //                                 $upline4thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl4),"s");
    //                                 // identify how many direct downline
    //                                 $upline4thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl4),"s");
                            
    //                                 //identify 5th upline
    //                                 echo "identify 5thh upline <br>";
    //                                 echo $uplineDetailsLvl5 = $upline4thDetails[0]->getReferrerId();
    //                                 echo "<br>"; 
                            
    //                                 if ($upline4thDownlineNum)
    //                                 {
                            
    //                                     echo $totalDownlineFour = count($upline4thDownlineNum);
    //                                     echo "<br>"; 
                            
    //                                     if($totalDownlineFour >= $userLevel)
    //                                     {   
    //                                         // proceed with bonus calculation
                                            
    //                                         $upline4thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl4),"s");
    //                                         $upline4thCredit = $upline4thStatus[0]->getWallet();
    //                                         echo $upline4thUsername = $upline4thStatus[0]->getUsername();
    //                                         echo "<br>"; 
    //                                         $bonusLvlFour = (1000 * 0.05);
    //                                         $bonusName = 'Direct Sponsor (5%)';
    //                                         $newUpline4thCredit = $upline4thCredit + $bonusLvlFour;
                            
                            
    //                                         $tableName = array();
    //                                         $tableValue =  array();
    //                                         $stringType =  "";
    //                                         //echo "save to database";
    //                                         if($newUpline4thCredit)
    //                                         {
    //                                             array_push($tableName,"wallet");
    //                                             array_push($tableValue,$newUpline4thCredit);
    //                                             $stringType .=  "d";
    //                                         }
    //                                         array_push($tableValue,$uplineDetailsLvl4);
    //                                         $stringType .=  "s";
    //                                         $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                                         if($passwordUpdated)
    //                                         {
                            
    //                                             if(directSponsorBonusLvlFour($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName))
    //                                             {
    //                                                 // echo "success 4";  
    //                                                 // echo "<br>";  
    //                                                 header('Location: ../adminOrderPending.php');
    //                                             }
    //                                             else
    //                                             {
    //                                                 echo "fail 4";    
    //                                                 echo "<br>";  
    //                                             } 
                            
    //                                         }
    //                                         else
    //                                         {
    //                                             echo "fail";
    //                                         }
                            
    //                                     }
    //                                     elseif($totalDownlineFour < $userLevel)
    //                                     {  
    //                                         //   no bonus
    //                                         echo "no bonus due to not enough direct downline lvl 4";   
    //                                         echo "<br>"; 
    //                                     }
                            
    //                                 }
                            
    //                             }
                            
    //                             //level 5 bonus
    //                             if(!$uplineDetailsLvl5)
    //                             {  
    //                                 echo "undefined upline level 5";   
    //                                 echo "<br>"; 
    //                             }
    //                             else
    //                             {  
                            
    //                                 $upline5thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl5),"s");
    //                                 // identify how many direct downline
    //                                 $upline5thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl5),"s");
                            
    //                                 //identify 6th upline
    //                                 // echo "identify 6th upline <br>";
    //                                 // echo $uplineDetailsLvl6 = $upline5thDetails[0]->getReferrerId();
    //                                 // echo "<br>"; 
                            
    //                                 if ($upline5thDownlineNum)
    //                                 {
                            
    //                                     echo $totalDownlineFive = count($upline5thDownlineNum);
    //                                     echo "<br>"; 
                            
    //                                     if($totalDownlineFive >= $userLevel)
    //                                     {   
    //                                         // proceed with bonus calculation
                                            
    //                                         $upline5thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl5),"s");
    //                                         $upline5thCredit = $upline5thStatus[0]->getWallet();
    //                                         echo $upline5thUsername = $upline5thStatus[0]->getUsername();
    //                                         echo "<br>"; 
    //                                         $bonusLvlFive = (1000 * 0.03);
    //                                         $bonusName = 'Direct Sponsor (3%)';
    //                                         $newUpline5thCredit = $upline5thCredit + $bonusLvlFive;
                            
                            
    //                                         $tableName = array();
    //                                         $tableValue =  array();
    //                                         $stringType =  "";
    //                                         //echo "save to database";
    //                                         if($newUpline5thCredit)
    //                                         {
    //                                             array_push($tableName,"wallet");
    //                                             array_push($tableValue,$newUpline5thCredit);
    //                                             $stringType .=  "d";
    //                                         }
    //                                         array_push($tableValue,$uplineDetailsLvl5);
    //                                         $stringType .=  "s";
    //                                         $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                                         if($passwordUpdated)
    //                                         {
                            
    //                                             if(directSponsorBonusLvlFive($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName))
    //                                             {
    //                                                 // echo "success 5";  
    //                                                 // echo "<br>";  
    //                                                 header('Location: ../adminOrderPending.php');
    //                                             }
    //                                             else
    //                                             {
    //                                                 echo "fail 5";    
    //                                                 echo "<br>";  
    //                                             } 
                            
    //                                         }
    //                                         else
    //                                         {
    //                                             echo "fail";
    //                                         }
                                                        
    //                                     }
    //                                     elseif($totalDownlineFive < $userLevel)
    //                                     {  
    //                                         //   no bonus
    //                                         echo "no bonus due to not enough direct downline lvl 5";   
    //                                         echo "<br>"; 
    //                                     }
                            
    //                                 }
                            
    //                             }

    //                         }
    //                         elseif($userLevel > 4)
    //                         {
                
    //                             $upline1stDetails = getReferralHistory($conn," WHERE referral_id = ?  ",array("referral_id"),array($userUplineDetails),"s");
    //                             $uplineLevel = $upline1stDetails[0]->getCurrentLevel();
                            
    //                             //identify 2nd upline
    //                             echo $uplineDetailsLvl2 = $upline1stDetails[0]->getReferrerId();
    //                             echo "<br>"; 
                            
    //                             $uplineStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($userUplineDetails),"s");
    //                             $uplineCredit = $uplineStatus[0]->getWallet();
    //                             echo $uplineUsername = $uplineStatus[0]->getUsername();
    //                             echo "<br>"; 
    //                             $bonusLvlOne = (1000 * 0.4);
    //                             $bonusName = 'Direct Sponsor (40%)';
    //                             $newUplineCredit = $uplineCredit + $bonusLvlOne;
                            
                            
    //                             $tableName = array();
    //                             $tableValue =  array();
    //                             $stringType =  "";
    //                             //echo "save to database";
    //                             if($newUplineCredit)
    //                             {
    //                                 array_push($tableName,"wallet");
    //                                 array_push($tableValue,$newUplineCredit);
    //                                 $stringType .=  "d";
    //                             }
    //                             array_push($tableValue,$userUplineDetails);
    //                             $stringType .=  "s";
    //                             $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                             if($passwordUpdated)
    //                             {
                             
    //                                 if(directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName))
    //                                 {
    //                                     // echo "success 1";  
    //                                     // echo "<br>";  
    //                                     header('Location: ../adminOrderPending.php');
    //                                 }
    //                                 else
    //                                 {
    //                                     echo "fail 1";    
    //                                     echo "<br>";  
    //                                 } 
                            
    //                             }
    //                             else
    //                             {
    //                                 echo "fail";
    //                             }
                            
    //                             //level 2 bonus
    //                             if(!$uplineDetailsLvl2)
    //                             {  
    //                                 echo "undefined upline level 2";   
    //                                 echo "<br>"; 
    //                             }
    //                             else
    //                             {  
                            
    //                                 $upline2ndDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl2),"s");
    //                                 // identify how many direct downline
    //                                 $upline2ndDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl2),"s");
                            
    //                                 //identify 3rd upline
    //                                 echo "identify 3rd upline <br>";
    //                                 echo $uplineDetailsLvl3 = $upline2ndDetails[0]->getReferrerId();
    //                                 echo "<br>"; 
                            
    //                                 if ($upline2ndDownlineNum)
    //                                 {
                            
    //                                     echo $totalDownlineTwo = count($upline2ndDownlineNum);
    //                                     echo "<br>"; 
                                        
    //                                     // if($totalDownlineOne >= 3)
    //                                     if($totalDownlineTwo >= $userLevel)
    //                                     {   
    //                                         // proceed with bonus calculation
                                            
    //                                         $upline2ndStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl2),"s");
    //                                         $upline2ndCredit = $upline2ndStatus[0]->getWallet();
    //                                         echo $upline2ndUsername = $upline2ndStatus[0]->getUsername();
    //                                         echo "<br>"; 
    //                                         $bonusLvlTwo = (1000 * 0.12);
    //                                         $bonusName = 'Direct Sponsor (12%)';
    //                                         $newUpline2ndCredit = $upline2ndCredit + $bonusLvlTwo;
                            
    //                                         $tableName = array();
    //                                         $tableValue =  array();
    //                                         $stringType =  "";
    //                                         //echo "save to database";
    //                                         if($newUpline2ndCredit)
    //                                         {
    //                                             array_push($tableName,"wallet");
    //                                             array_push($tableValue,$newUpline2ndCredit);
    //                                             $stringType .=  "d";
    //                                         }
    //                                         array_push($tableValue,$uplineDetailsLvl2);
    //                                         $stringType .=  "s";
    //                                         $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                                         if($passwordUpdated)
    //                                         {
                            
    //                                             if(directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlOne,$bonusName))
    //                                             {
    //                                                 // echo "success 2";  
    //                                                 // echo "<br>";  
    //                                                 header('Location: ../adminOrderPending.php');
    //                                             }
    //                                             else
    //                                             {
    //                                                 echo "fail 2";    
    //                                                 echo "<br>";  
    //                                             } 
                            
    //                                         }
    //                                         else
    //                                         {
    //                                             echo "fail";
    //                                         }
                            
    //                                     }
    //                                     // elseif($totalDownlineOne < 3)
    //                                     elseif($totalDownlineTwo < $userLevel)
    //                                     {  
    //                                         //   no bonus
    //                                         echo "no bonus due to not enough direct downline lvl 2";   
    //                                         echo "<br>"; 
    //                                     }
                            
    //                                 }
                            
    //                             }
                            
    //                             //level 3 bonus
    //                             if(!$uplineDetailsLvl3)
    //                             {  
    //                                 echo "undefined upline level 3";   
    //                                 echo "<br>"; 
    //                             }
    //                             else
    //                             {  
                            
    //                                 $upline3rdDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl3),"s");
    //                                 // identify how many direct downline
    //                                 $upline3rddDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl3),"s");
                            
    //                                 //identify 4th upline
    //                                 echo "identify 4th upline <br>";
    //                                 echo $uplineDetailsLvl4 = $upline3rdDetails[0]->getReferrerId();
    //                                 echo "<br>"; 
                            
    //                                 if ($upline3rddDownlineNum)
    //                                 {
                            
    //                                     echo $totalDownlineThree = count($upline3rddDownlineNum);
    //                                     echo "<br>"; 
                            
    //                                     if($totalDownlineThree >= $userLevel)
    //                                     {   
    //                                         // proceed with bonus calculation
                                            
    //                                         $upline3rdStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl3),"s");
    //                                         $upline3rdCredit = $upline3rdStatus[0]->getWallet();
    //                                         echo $upline3rdUsername = $upline3rdStatus[0]->getUsername();
    //                                         echo "<br>"; 
    //                                         $bonusLvlThree = (1000 * 0.1);
    //                                         $bonusName = 'Direct Sponsor (10%)';
    //                                         $newUpline3rdCredit = $upline3rdCredit + $bonusLvlThree;
                            
                            
    //                                         $tableName = array();
    //                                         $tableValue =  array();
    //                                         $stringType =  "";
    //                                         //echo "save to database";
    //                                         if($newUpline3rdCredit)
    //                                         {
    //                                             array_push($tableName,"wallet");
    //                                             array_push($tableValue,$newUpline3rdCredit);
    //                                             $stringType .=  "d";
    //                                         }
    //                                         array_push($tableValue,$uplineDetailsLvl3);
    //                                         $stringType .=  "s";
    //                                         $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                                         if($passwordUpdated)
    //                                         {
                            
    //                                             if(directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName))
    //                                             {
    //                                                 // echo "success 3";  
    //                                                 // echo "<br>";  
    //                                                 header('Location: ../adminOrderPending.php');
    //                                             }
    //                                             else
    //                                             {
    //                                                 echo "fail 3";    
    //                                                 echo "<br>";  
    //                                             } 
                            
    //                                         }
    //                                         else
    //                                         {
    //                                             echo "fail";
    //                                         }
                            
    //                                     }
    //                                     elseif($totalDownlineThree < $userLevel)
    //                                     {  
    //                                         //   no bonus
    //                                         echo "no bonus due to not enough direct downline lvl 3";   
    //                                         echo "<br>"; 
    //                                     }
                            
    //                                 }
                            
    //                             }
                            
    //                             //level 4 bonus
    //                             if(!$uplineDetailsLvl4)
    //                             {  
    //                                 echo "undefined upline level 4";   
    //                                 echo "<br>"; 
    //                             }
    //                             else
    //                             {  
                            
    //                                 $upline4thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl4),"s");
    //                                 // identify how many direct downline
    //                                 $upline4thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl4),"s");
                            
    //                                 //identify 5th upline
    //                                 echo "identify 5thh upline <br>";
    //                                 echo $uplineDetailsLvl5 = $upline4thDetails[0]->getReferrerId();
    //                                 echo "<br>"; 
                            
    //                                 if ($upline4thDownlineNum)
    //                                 {
                            
    //                                     echo $totalDownlineFour = count($upline4thDownlineNum);
    //                                     echo "<br>"; 
                            
    //                                     if($totalDownlineFour >= $userLevel)
    //                                     {   
    //                                         // proceed with bonus calculation
                                            
    //                                         $upline4thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl4),"s");
    //                                         $upline4thCredit = $upline4thStatus[0]->getWallet();
    //                                         echo $upline4thUsername = $upline4thStatus[0]->getUsername();
    //                                         echo "<br>"; 
    //                                         $bonusLvlFour = (1000 * 0.05);
    //                                         $bonusName = 'Direct Sponsor (5%)';
    //                                         $newUpline4thCredit = $upline4thCredit + $bonusLvlFour;
                            
                            
    //                                         $tableName = array();
    //                                         $tableValue =  array();
    //                                         $stringType =  "";
    //                                         //echo "save to database";
    //                                         if($newUpline4thCredit)
    //                                         {
    //                                             array_push($tableName,"wallet");
    //                                             array_push($tableValue,$newUpline4thCredit);
    //                                             $stringType .=  "d";
    //                                         }
    //                                         array_push($tableValue,$uplineDetailsLvl4);
    //                                         $stringType .=  "s";
    //                                         $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                                         if($passwordUpdated)
    //                                         {
                            
    //                                             if(directSponsorBonusLvlFour($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName))
    //                                             {
    //                                                 // echo "success 4";  
    //                                                 // echo "<br>";  
    //                                                 header('Location: ../adminOrderPending.php');
    //                                             }
    //                                             else
    //                                             {
    //                                                 echo "fail 4";    
    //                                                 echo "<br>";  
    //                                             } 
                            
    //                                         }
    //                                         else
    //                                         {
    //                                             echo "fail";
    //                                         }
                            
    //                                     }
    //                                     elseif($totalDownlineFour < $userLevel)
    //                                     {  
    //                                         //   no bonus
    //                                         echo "no bonus due to not enough direct downline lvl 4";   
    //                                         echo "<br>"; 
    //                                     }
                            
    //                                 }
                            
    //                             }
                            
    //                             //level 5 bonus
    //                             if(!$uplineDetailsLvl5)
    //                             {  
    //                                 echo "undefined upline level 5";   
    //                                 echo "<br>"; 
    //                             }
    //                             else
    //                             {  
                            
    //                                 $upline5thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl5),"s");
    //                                 // identify how many direct downline
    //                                 $upline5thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl5),"s");
                            
    //                                 //identify 6th upline
    //                                 echo "identify 6th upline <br>";
    //                                 echo $uplineDetailsLvl6 = $upline5thDetails[0]->getReferrerId();
    //                                 echo "<br>"; 
                            
    //                                 if ($upline5thDownlineNum)
    //                                 {
                            
    //                                     echo $totalDownlineFive = count($upline5thDownlineNum);
    //                                     echo "<br>"; 
                            
    //                                     if($totalDownlineFive >= $userLevel)
    //                                     {   
    //                                         // proceed with bonus calculation
                                            
    //                                         $upline5thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl5),"s");
    //                                         $upline5thCredit = $upline5thStatus[0]->getWallet();
    //                                         echo $upline5thUsername = $upline5thStatus[0]->getUsername();
    //                                         echo "<br>"; 
    //                                         $bonusLvlFive = (1000 * 0.03);
    //                                         $bonusName = 'Direct Sponsor (3%)';
    //                                         $newUpline5thCredit = $upline5thCredit + $bonusLvlFive;
                            
                            
    //                                         $tableName = array();
    //                                         $tableValue =  array();
    //                                         $stringType =  "";
    //                                         //echo "save to database";
    //                                         if($newUpline5thCredit)
    //                                         {
    //                                             array_push($tableName,"wallet");
    //                                             array_push($tableValue,$newUpline5thCredit);
    //                                             $stringType .=  "d";
    //                                         }
    //                                         array_push($tableValue,$uplineDetailsLvl5);
    //                                         $stringType .=  "s";
    //                                         $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                                         if($passwordUpdated)
    //                                         {
                            
    //                                             if(directSponsorBonusLvlFive($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName))
    //                                             {
    //                                                 // echo "success 5";  
    //                                                 // echo "<br>";  
    //                                                 header('Location: ../adminOrderPending.php');
    //                                             }
    //                                             else
    //                                             {
    //                                                 echo "fail 5";    
    //                                                 echo "<br>";  
    //                                             } 
                            
    //                                         }
    //                                         else
    //                                         {
    //                                             echo "fail";
    //                                         }
                            
    //                                     }
    //                                     elseif($totalDownlineFive < $userLevel)
    //                                     {  
    //                                         //   no bonus
    //                                         echo "no bonus due to not enough direct downline lvl 5";   
    //                                         echo "<br>"; 
    //                                     }
                            
    //                                 }
                            
    //                             }
                            
    //                             //level 6 bonus
    //                             if(!$uplineDetailsLvl6)
    //                             {  
    //                                 echo "undefined upline level 6";   
    //                                 echo "<br>"; 
    //                             }
    //                             else
    //                             {  
                            
    //                                 $upline6thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl6),"s");
    //                                 // identify how many direct downline
    //                                 $upline6thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl6),"s");
                            
    //                                 // //identify 6th upline
    //                                 // echo "identify 7th upline <br>";
    //                                 // echo $uplineDetailsLvl7 = $upline6thDetails[0]->getReferrerId();
    //                                 // echo "<br>"; 
                            
    //                                 if ($upline6thDownlineNum)
    //                                 {
                            
    //                                     echo $totalDownlineSix = count($upline6thDownlineNum);
    //                                     echo "<br>"; 
                            
    //                                     if($totalDownlineSix >= $userLevel)
    //                                     {   
    //                                         // proceed with bonus calculation
                                            
    //                                         $upline6thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl6),"s");
    //                                         $upline6thCredit = $upline6thStatus[0]->getWallet();
    //                                         echo $upline6thUsername = $upline6thStatus[0]->getUsername();
    //                                         echo "<br>"; 
    //                                         $bonusLvlSix = (1000 * 0.02);
    //                                         $bonusName = 'Direct Sponsor (2%)';
    //                                         $newUpline6thCredit = $upline6thCredit + $bonusLvlSix;
                            
                            
    //                                         $tableName = array();
    //                                         $tableValue =  array();
    //                                         $stringType =  "";
    //                                         //echo "save to database";
    //                                         if($newUpline6thCredit)
    //                                         {
    //                                             array_push($tableName,"wallet");
    //                                             array_push($tableValue,$newUpline6thCredit);
    //                                             $stringType .=  "d";
    //                                         }
    //                                         array_push($tableValue,$uplineDetailsLvl6);
    //                                         $stringType .=  "s";
    //                                         $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    //                                         if($passwordUpdated)
    //                                         {
                            
    //                                             if(directSponsorBonusLvlSix($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName))
    //                                             {
    //                                                 // echo "success 6";  
    //                                                 // echo "<br>";  
    //                                                 header('Location: ../adminOrderPending.php');
    //                                             }
    //                                             else
    //                                             {
    //                                                 echo "fail 6";    
    //                                                 echo "<br>";  
    //                                             } 
                            
    //                                         }
    //                                         else
    //                                         {
    //                                             echo "fail";
    //                                         }
                            
    //                                     }
    //                                     elseif($totalDownlineSix < $userLevel)
    //                                     {  
    //                                         //   no bonus
    //                                         echo "no bonus due to not enough direct downline lvl 6";   
    //                                         echo "<br>"; 
    //                                     }
                            
    //                                 }
                            
    //                             }

    //                         }
    //                         else
    //                         {   
    //                             echo "unknown level !!";
    //                             echo "<br>";  
    //                         }
                            
    //                     }
    //                     else
    //                     {
    //                         echo "fail pool fund bonus 4";    
    //                         echo "<br>";  
    //                     } 
    //                 }
    //                 else
    //                 {
    //                     echo "fail pool fund bonus 3";    
    //                     echo "<br>";  
    //                 } 
    //             }
    //             else
    //             {
    //                 echo "fail pool fund bonus 2";    
    //                 echo "<br>";  
    //             } 
    //         }
    //         else
    //         {
    //             echo "fail pool fund bonus 1";     
    //             echo "<br>";  
    //         } 

    //         // if($userLevel = 0)
    //         // {
    
    //         // }
    //         // elseif($userLevel = 1)
    //         // {

    //         // }
    //         // elseif($userLevel = 2)
    //         // {

    //         // }
    //         // elseif($userLevel = 3)
    //         // {

    //         // }
    //         // elseif($userLevel = 4)
    //         // {

    //         // }
    //         // elseif($userLevel = 5)
    //         // {

    //         // }
    //         // elseif($userLevel = 6)
    //         // {

    //         // }
    //         // else
    //         // {   
    //         //     echo "level overload !!";
    //         //     echo "<br>";  
    //         // }
    //     }
    //     else
    //     {
    //         echo "fail update order details";
    //         echo "<br>";  
    //     }
    // }
    // else
    // {
    //     echo "invalid order details";
    //     echo "<br>";  
    // }


    // $upline1stDetails = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($userUplineDetails),"s");
    // $upline1stDetails = getReferralHistory($conn," WHERE referrer_id = ? AND referral_id = '$orderUserUid' ",array("referrer_id"),array($userUplineDetails),"s");
    // $upline1stDetails = getReferralHistory($conn," WHERE referrer_id = ?  ",array("referrer_id"),array($userUplineDetails),"s");
    $upline1stDetails = getReferralHistory($conn," WHERE referral_id = ?  ",array("referral_id"),array($userUplineDetails),"s");
    $uplineLevel = $upline1stDetails[0]->getCurrentLevel();

    //identify 2nd upline
    echo $uplineDetailsLvl2 = $upline1stDetails[0]->getReferrerId();
    echo "<br>"; 

    $uplineStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($userUplineDetails),"s");
    $uplineCredit = $uplineStatus[0]->getWallet();
    echo $uplineUsername = $uplineStatus[0]->getUsername();
    echo "<br>"; 
    $bonusLvlOne = (1000 * 0.4);
    $bonusName = 'Direct Sponsor (40%)';
    $newUplineCredit = $uplineCredit + $bonusLvlOne;


    $tableName = array();
    $tableValue =  array();
    $stringType =  "";
    //echo "save to database";
    if($newUplineCredit)
    {
        array_push($tableName,"wallet");
        array_push($tableValue,$newUplineCredit);
        $stringType .=  "d";
    }
    array_push($tableValue,$userUplineDetails);
    $stringType .=  "s";
    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    if($passwordUpdated)
    {
 
        if(directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName))
        {
            echo "success 1";  
            echo "<br>";  
            // header('Location: ../adminOrderPending.php');
        }
        else
        {
            echo "fail 1";    
            echo "<br>";  
        } 

    }
    else
    {
        echo "fail";
    }


    // if(directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName))
    // {
    //     // echo "success 1";  
    //     // echo "<br>";  
    //     header('Location: ../adminOrderPending.php');
    // }
    // else
    // {
    //     echo "fail 1";    
    //     echo "<br>";  
    // } 

    //level 2 bonus
    if(!$uplineDetailsLvl2)
    {  
        echo "undefined upline level 2";   
        echo "<br>"; 
    }
    else
    {  

        $upline2ndDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl2),"s");
        // identify how many direct downline
        $upline2ndDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl2),"s");

        //identify 3rd upline
        echo "identify 3rd upline <br>";
        echo $uplineDetailsLvl3 = $upline2ndDetails[0]->getReferrerId();
        echo "<br>"; 

        if ($upline2ndDownlineNum)
        {

            echo $totalDownlineTwo = count($upline2ndDownlineNum);
            echo "<br>"; 
            
            // if($totalDownlineOne >= 3)
            if($totalDownlineTwo >= $userLevel)
            {   
                // proceed with bonus calculation
                
                $upline2ndStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl2),"s");
                $upline2ndCredit = $upline2ndStatus[0]->getWallet();
                echo $upline2ndUsername = $upline2ndStatus[0]->getUsername();
                echo "<br>"; 
                $bonusLvlTwo = (1000 * 0.12);
                $bonusName = 'Direct Sponsor (12%)';
                $newUpline2ndCredit = $upline2ndCredit + $bonusLvlTwo;


                // $tableName = array();
                // $tableValue =  array();
                // $stringType =  "";
                // //echo "save to database";
                // if($newUpline2ndCredit)
                // {
                //     array_push($tableName,"wallet");
                //     array_push($tableValue,$newUpline2ndCredit);
                //     $stringType .=  "d";
                // }
                // array_push($tableValue,$uplineDetailsLvl2);
                // $stringType .=  "s";
                // $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                // if($passwordUpdated)
                // {
                //     echo "success";
                //     // header('Location: ../userBankDetails.php');    
                // }
                // else
                // {
                //     echo "fail";
                // }

                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($newUpline2ndCredit)
                {
                    array_push($tableName,"wallet");
                    array_push($tableValue,$newUpline2ndCredit);
                    $stringType .=  "d";
                }
                array_push($tableValue,$uplineDetailsLvl2);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {

                    if(directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlOne,$bonusName))
                    {
                        echo "success 2";  
                        echo "<br>";  
                        // header('Location: ../adminOrderPending.php');
                    }
                    else
                    {
                        echo "fail 2";    
                        echo "<br>";  
                    } 

                }
                else
                {
                    echo "fail";
                }
                

                // if(directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlOne,$bonusName))
                // {
                //     // echo "success 2";  
                //     // echo "<br>";  
                //     header('Location: ../adminOrderPending.php');
                // }
                // else
                // {
                //     echo "fail 2";    
                //     echo "<br>";  
                // } 

            }
            // elseif($totalDownlineOne < 3)
            elseif($totalDownlineTwo < $userLevel)
            {  
                //   no bonus
                echo "no bonus due to not enough direct downline lvl 2";   
                echo "<br>"; 
            }

        }

    }

    //level 3 bonus
    if(!$uplineDetailsLvl3)
    {  
        echo "undefined upline level 3";   
        echo "<br>"; 
    }
    else
    {  

        $upline3rdDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl3),"s");
        // identify how many direct downline
        $upline3rddDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl3),"s");

        //identify 4th upline
        echo "identify 4th upline <br>";
        echo $uplineDetailsLvl4 = $upline3rdDetails[0]->getReferrerId();
        echo "<br>"; 

        if ($upline3rddDownlineNum)
        {

            echo $totalDownlineThree = count($upline3rddDownlineNum);
            echo "<br>"; 

            if($totalDownlineThree >= $userLevel)
            {   
                // proceed with bonus calculation
                
                $upline3rdStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl3),"s");
                $upline3rdCredit = $upline3rdStatus[0]->getWallet();
                echo $upline3rdUsername = $upline3rdStatus[0]->getUsername();
                echo "<br>"; 
                $bonusLvlThree = (1000 * 0.1);
                $bonusName = 'Direct Sponsor (10%)';
                $newUpline3rdCredit = $upline3rdCredit + $bonusLvlThree;


                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($newUpline3rdCredit)
                {
                    array_push($tableName,"wallet");
                    array_push($tableValue,$newUpline3rdCredit);
                    $stringType .=  "d";
                }
                array_push($tableValue,$uplineDetailsLvl3);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {

                    if(directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName))
                    {
                        echo "success 3";  
                        echo "<br>";  
                        // header('Location: ../adminOrderPending.php');
                    }
                    else
                    {
                        echo "fail 3";    
                        echo "<br>";  
                    } 

                }
                else
                {
                    echo "fail";
                }


                // if(directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName))
                // {
                //     // echo "success 3";  
                //     // echo "<br>";  
                //     header('Location: ../adminOrderPending.php');
                // }
                // else
                // {
                //     echo "fail 3";    
                //     echo "<br>";  
                // } 

            }
            elseif($totalDownlineThree < $userLevel)
            {  
                //   no bonus
                echo "no bonus due to not enough direct downline lvl 3";   
                echo "<br>"; 
            }

        }

    }

    //level 4 bonus
    if(!$uplineDetailsLvl4)
    {  
        echo "undefined upline level 4";   
        echo "<br>"; 
    }
    else
    {  

        $upline4thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl4),"s");
        // identify how many direct downline
        $upline4thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl4),"s");

        //identify 5th upline
        echo "identify 5thh upline <br>";
        echo $uplineDetailsLvl5 = $upline4thDetails[0]->getReferrerId();
        echo "<br>"; 

        if ($upline4thDownlineNum)
        {

            echo $totalDownlineFour = count($upline4thDownlineNum);
            echo "<br>"; 

            if($totalDownlineFour >= $userLevel)
            {   
                // proceed with bonus calculation
                
                $upline4thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl4),"s");
                $upline4thCredit = $upline4thStatus[0]->getWallet();
                echo $upline4thUsername = $upline4thStatus[0]->getUsername();
                echo "<br>"; 
                $bonusLvlFour = (1000 * 0.05);
                $bonusName = 'Direct Sponsor (5%)';
                $newUpline4thCredit = $upline4thCredit + $bonusLvlFour;


                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($newUpline4thCredit)
                {
                    array_push($tableName,"wallet");
                    array_push($tableValue,$newUpline4thCredit);
                    $stringType .=  "d";
                }
                array_push($tableValue,$uplineDetailsLvl4);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {

                    if(directSponsorBonusLvlFour($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName))
                    {
                        echo "success 4";  
                        echo "<br>";  
                        // header('Location: ../adminOrderPending.php');
                    }
                    else
                    {
                        echo "fail 4";    
                        echo "<br>";  
                    } 

                }
                else
                {
                    echo "fail";
                }


                // if(directSponsorBonusLvlFour($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName))
                // {
                //     // echo "success 4";  
                //     // echo "<br>";  
                //     header('Location: ../adminOrderPending.php');
                // }
                // else
                // {
                //     echo "fail 4";    
                //     echo "<br>";  
                // } 

            }
            elseif($totalDownlineFour < $userLevel)
            {  
                //   no bonus
                echo "no bonus due to not enough direct downline lvl 4";   
                echo "<br>"; 
            }

        }

    }

    //level 5 bonus
    if(!$uplineDetailsLvl5)
    {  
        echo "undefined upline level 5";   
        echo "<br>"; 
    }
    else
    {  

        $upline5thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl5),"s");
        // identify how many direct downline
        $upline5thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl5),"s");

        //identify 6th upline
        echo "identify 6th upline <br>";
        echo $uplineDetailsLvl6 = $upline5thDetails[0]->getReferrerId();
        echo "<br>"; 

        if ($upline5thDownlineNum)
        {

            echo $totalDownlineFive = count($upline5thDownlineNum);
            echo "<br>"; 

            if($totalDownlineFive >= $userLevel)
            {   
                // proceed with bonus calculation
                
                $upline5thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl5),"s");
                $upline5thCredit = $upline5thStatus[0]->getWallet();
                echo $upline5thUsername = $upline5thStatus[0]->getUsername();
                echo "<br>"; 
                $bonusLvlFive = (1000 * 0.03);
                $bonusName = 'Direct Sponsor (3%)';
                $newUpline5thCredit = $upline5thCredit + $bonusLvlFive;


                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($newUpline5thCredit)
                {
                    array_push($tableName,"wallet");
                    array_push($tableValue,$newUpline5thCredit);
                    $stringType .=  "d";
                }
                array_push($tableValue,$uplineDetailsLvl5);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {

                    if(directSponsorBonusLvlFive($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName))
                    {
                        echo "success 5";  
                        echo "<br>";  
                        // header('Location: ../adminOrderPending.php');
                    }
                    else
                    {
                        echo "fail 5";    
                        echo "<br>";  
                    } 

                }
                else
                {
                    echo "fail";
                }


                // if(directSponsorBonusLvlFive($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName))
                // {
                //     // echo "success 5";  
                //     // echo "<br>";  
                //     header('Location: ../adminOrderPending.php');
                // }
                // else
                // {
                //     echo "fail 5";    
                //     echo "<br>";  
                // } 

            }
            elseif($totalDownlineFive < $userLevel)
            {  
                //   no bonus
                echo "no bonus due to not enough direct downline lvl 5";   
                echo "<br>"; 
            }

        }

    }

    //level 6 bonus
    if(!$uplineDetailsLvl6)
    {  
        echo "undefined upline level 6";   
        echo "<br>"; 
    }
    else
    {  

        $upline6thDetails = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineDetailsLvl6),"s");
        // identify how many direct downline
        $upline6thDownlineNum = getReferralHistory($conn," WHERE referrer_id = ? ",array("referrer_id"),array($uplineDetailsLvl6),"s");

        // //identify 6th upline
        // echo "identify 7th upline <br>";
        // echo $uplineDetailsLvl7 = $upline6thDetails[0]->getReferrerId();
        // echo "<br>"; 

        if ($upline6thDownlineNum)
        {

            echo $totalDownlineSix = count($upline6thDownlineNum);
            echo "<br>"; 

            if($totalDownlineSix >= $userLevel)
            {   
                // proceed with bonus calculation
                
                $upline6thStatus = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineDetailsLvl6),"s");
                $upline6thCredit = $upline6thStatus[0]->getWallet();
                echo $upline6thUsername = $upline6thStatus[0]->getUsername();
                echo "<br>"; 
                $bonusLvlSix = (1000 * 0.02);
                $bonusName = 'Direct Sponsor (2%)';
                $newUpline6thCredit = $upline6thCredit + $bonusLvlSix;


                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($newUpline6thCredit)
                {
                    array_push($tableName,"wallet");
                    array_push($tableValue,$newUpline6thCredit);
                    $stringType .=  "d";
                }
                array_push($tableValue,$uplineDetailsLvl6);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {

                    if(directSponsorBonusLvlSix($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName))
                    {
                        echo "success 6";  
                        echo "<br>";  
                        // header('Location: ../adminOrderPending.php');
                    }
                    else
                    {
                        echo "fail 6";    
                        echo "<br>";  
                    } 

                }
                else
                {
                    echo "fail";
                }


                // if(directSponsorBonusLvlSix($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName))
                // {
                //     // echo "success 6";  
                //     // echo "<br>";  
                //     header('Location: ../adminOrderPending.php');
                // }
                // else
                // {
                //     echo "fail 6";    
                //     echo "<br>";  
                // } 

            }
            elseif($totalDownlineSix < $userLevel)
            {  
                //   no bonus
                echo "no bonus due to not enough direct downline lvl 6";   
                echo "<br>"; 
            }

        }

    }

}
else 
{
    header('Location: ../index.php');
}
?>