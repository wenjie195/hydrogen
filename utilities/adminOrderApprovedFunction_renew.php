<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Bonus.php';
require_once dirname(__FILE__) . '/../classes/BonusPoolFund.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/../classes/Sales.php';
require_once dirname(__FILE__) . '/../classes/Stock.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

function directSponsorBonusLvlOne($conn,$orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$userUplineDetails,$uplineUsername,$bonusLvlOne,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlTwo($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlTwo,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl2,$upline2ndUsername,$bonusLvlTwo,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlThree($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl3,$upline3rdUsername,$bonusLvlThree,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlFour($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl4,$upline4thUsername,$bonusLvlFour,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlFive($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl5,$upline5thUsername,$bonusLvlFive,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function directSponsorBonusLvlSix($conn,$orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName)
{
     if(insertDynamicData($conn,"bonus",array("order_uid","uid","username","receiver_uid","receiver","amount","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$uplineDetailsLvl6,$upline6thUsername,$bonusLvlSix,$bonusName),"sssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

//bonus 2 (pool fund)

function bonusTwoPoolFundOne($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundOneAmount,$poolFundOneReceiver,$bonusTypePF)
{
     if(insertDynamicData($conn,"bonus_poolfund",array("order_uid","uid","username","amount","receiver","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$poolFundOneAmount,$poolFundOneReceiver,$bonusTypePF),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function bonusTwoPoolFundTwo($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundTwoAmount,$poolFundTwoReceiver,$bonusTypePF)
{
     if(insertDynamicData($conn,"bonus_poolfund",array("order_uid","uid","username","amount","receiver","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$poolFundTwoAmount,$poolFundTwoReceiver,$bonusTypePF),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function bonusTwoPoolFundThree($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundThreeAmount,$poolFundThreeReceiver,$bonusTypePF)
{
     if(insertDynamicData($conn,"bonus_poolfund",array("order_uid","uid","username","amount","receiver","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$poolFundThreeAmount,$poolFundThreeReceiver,$bonusTypePF),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function bonusTwoPoolFundFour($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundFourAmount,$poolFundFourReceiver,$bonusTypePF)
{
     if(insertDynamicData($conn,"bonus_poolfund",array("order_uid","uid","username","amount","receiver","bonus_type"),
     array($orderUid,$orderUserUid,$orderUserName,$poolFundFourAmount,$poolFundFourReceiver,$bonusTypePF),"ssssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

function updateSales($conn,$brand,$status,$orderId,$orderUid)
{
     if(insertDynamicData($conn,"sales",array("brand","status","order_id","order_uid"),
     array($brand,$status,$orderId,$orderUid),"ssss") === null)
     {
          return false;
     }
     else
     {}
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    echo $orderUid = rewrite($_POST["order_uid"]);
    echo "<br>";  

    $orderDetails = getOrders($conn, " WHERE order_id = '$orderUid' ");

    echo $orderUserUid = $orderDetails[0]->getUid();
    echo "<br>";
    echo $orderUserName = $orderDetails[0]->getName();
    echo "<br>";
    // echo $orderPrice = $orderDetails[0]->getSubtotal();
    // echo "<br>";
    echo $orderPrice = $orderDetails[0]->getCommission();
    echo "<br>";

    // $orderUserUid = $orderDetails[0]->getUid();
    // $orderUserName = $orderDetails[0]->getName();
    // $orderPrice = $orderDetails[0]->getCommission();

    //user details in referral history
    $userRH = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($orderUserUid),"s");
    // $userLvl = $userRH[0]->getCurrentLevel();
    // $userLevel = $userLvl - 1;
    $userLevel = $userRH[0]->getCurrentLevel();

    //Direct Upline
    echo $userUplineDetails = $userRH[0]->getReferrerId(); //william
    echo "<br>";  
    // $userUplineDetails = $userRH[0]->getReferrerId();

    $paymentStatus = 'APPROVED';

    $monthlyBonusStatus = 'PENDING';

    //start check ordering to verify downline
    $purchaserDetails = getUser($conn, "WHERE uid =?",array("uid"),array($orderUserUid),"s");
    // $purchaserData = $purchaserDetails[0];
    $orderingStatus = 'YES';
    //end check ordering to verify downline

    // ------------------------------ stock checking start ------------------------------
    $orderId = $orderDetails[0]->getId();

    $ordersGD = getProductOrders($conn, " WHERE order_id = '$orderId' AND product_name = 'GD M-12' ");
    if($ordersGD)
    {
        // $totalManagerBonusPool = 0;
        for ($cnt=0; $cnt <count($ordersGD) ; $cnt++)
        {
            echo $orderId;
            echo "<br>";
            echo $orderUid;
            echo "<br>";
            echo $orderProduct = $ordersGD[$cnt]->getProductName();
            echo "<br>";
            echo $orderQuantity = $ordersGD[$cnt]->getQuantity();
            echo "<br>";

            // $orderProduct = $ordersGD[$cnt]->getProductName();
            // $orderQuantity = $ordersGD[$cnt]->getQuantity();
            // $brand = $orderProduct;
            $status = 'Sold';

            $stockCheckA = getStock($conn, " WHERE status = 'Available' AND brand = 'GD' LIMIT $orderQuantity");
            if($stockCheckA)
            {
                for ($cnt=0; $cnt <count($stockCheckA) ; $cnt++)
                {
                    echo $stockAName = $stockCheckA[$cnt]->getName();
                    echo "<br>";
                    // $stockAName = $stockCheckA[$cnt]->getName();

                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($status)
                    {
                        array_push($tableName,"status");
                        array_push($tableValue,$status);
                        $stringType .=  "s";
                    }    
                    if($orderId)
                    {
                        array_push($tableName,"order_id");
                        array_push($tableValue,$orderId);
                        $stringType .=  "s";
                    } 
                    if($orderUid)
                    {
                        array_push($tableName,"order_uid");
                        array_push($tableValue,$orderUid);
                        $stringType .=  "s";
                    } 
                    array_push($tableValue,$stockCheckA[$cnt]->getName());
                    $stringType .=  "s";
                    $updateStockA = updateDynamicData($conn,"stock"," WHERE name = ? ",$tableName,$tableValue,$stringType);
                    if($updateStockA)
                    {
                        echo "GD Sales Updated";
                        echo "<br>";
                    }
                    else
                    {
                        echo "Fail To Update GD Sales";
                    }
                }
            }
        }
    }

    $ordersGN = getProductOrders($conn, " WHERE order_id = '$orderId' AND product_name = 'GN M-12' ");
    if($ordersGN)
    {
        // $totalManagerBonusPool = 0;
        for ($cnt=0; $cnt <count($ordersGN) ; $cnt++)
        {
            echo $orderId;
            echo "<br>";
            echo $orderUid;
            echo "<br>";
            echo $orderProduct = $ordersGN[$cnt]->getProductName();
            echo "<br>";
            echo $orderQuantity = $ordersGN[$cnt]->getQuantity();
            echo "<br>";

            // $orderProduct = $ordersGN[$cnt]->getProductName();
            // $orderQuantity = $ordersGN[$cnt]->getQuantity();
            // $brand = $orderProduct;
            $status = 'Sold';

            $stockCheckB = getStock($conn, " WHERE status = 'Available' AND brand = 'GN' LIMIT $orderQuantity");
            if($stockCheckB)
            {
                for ($cnt=0; $cnt <count($stockCheckB) ; $cnt++)
                {
                    echo $stockAName = $stockCheckB[$cnt]->getName();
                    echo "<br>";
                    // $stockAName = $stockCheckB[$cnt]->getName();

                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($status)
                    {
                        array_push($tableName,"status");
                        array_push($tableValue,$status);
                        $stringType .=  "s";
                    }    
                    if($orderId)
                    {
                        array_push($tableName,"order_id");
                        array_push($tableValue,$orderId);
                        $stringType .=  "s";
                    } 
                    if($orderUid)
                    {
                        array_push($tableName,"order_uid");
                        array_push($tableValue,$orderUid);
                        $stringType .=  "s";
                    } 
                    array_push($tableValue,$stockCheckB[$cnt]->getName());
                    $stringType .=  "s";
                    $updateStockB = updateDynamicData($conn,"stock"," WHERE name = ? ",$tableName,$tableValue,$stringType);
                    if($updateStockB)
                    {
                        echo "GN Sales Updated";
                        echo "<br>";
                    }
                    else
                    {
                        echo "Fail To Update GN Sales";
                    }
                }
            }
        }
    }
    // ------------------------------ stock checking end ------------------------------


    //bonus 2 (pool fund) details
    $bonusTypePF = 'Bonus 2 (Pool Fund)';

    $poolFundOneAmount = ($orderPrice * 0.01);
    $poolFundOneReceiver = 'Manager';

    $poolFundTwoAmount = ($orderPrice * 0.0125);
    $poolFundTwoReceiver = 'Senior Manager';

    $poolFundThreeAmount = ($orderPrice * 0.016);
    $poolFundThreeReceiver = 'Area Manager';

    $poolFundFourAmount = ($orderPrice * 0.0188);
    $poolFundFourReceiver = 'District Manager';

    // if(!$orderDetails)
    if($orderDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($paymentStatus)
        {
            array_push($tableName,"payment_status");
            array_push($tableValue,$paymentStatus);
            $stringType .=  "s";
        }

        if($monthlyBonusStatus)
        {
            array_push($tableName,"state");
            array_push($tableValue,$monthlyBonusStatus);
            $stringType .=  "s";
        }
        if($userLevel)
        {
            array_push($tableName,"zipcode");
            array_push($tableValue,$userLevel);
            $stringType .=  "s";
        }

        array_push($tableValue,$orderUid);
        $stringType .=  "s";
        $updateOrders = updateDynamicData($conn,"orders"," WHERE order_id = ? ",$tableName,$tableValue,$stringType);
        if($updateOrders)
        {
            if($purchaserDetails)
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($orderingStatus)
                {
                    array_push($tableName,"order_status");
                    array_push($tableValue,$orderingStatus);
                    $stringType .=  "s";
                }
                array_push($tableValue,$orderUserUid);
                $stringType .=  "s";
                $updateUserOrder = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($updateUserOrder)
                {
                    if($userRH)
                    {   
                        $tableName = array();
                        $tableValue =  array();
                        $stringType =  "";
                        //echo "save to database";
                        if($orderingStatus)
                        {
                            array_push($tableName,"order_status");
                            array_push($tableValue,$orderingStatus);
                            $stringType .=  "s";
                        }
                        array_push($tableValue,$orderUserUid);
                        $stringType .=  "s";
                        $updateRHOrder = updateDynamicData($conn,"referral_history"," WHERE referral_id = ? ",$tableName,$tableValue,$stringType);
                        if($updateRHOrder)
                        {

                            if(bonusTwoPoolFundOne($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundOneAmount,$poolFundOneReceiver,$bonusTypePF))
                            {
                                if(bonusTwoPoolFundTwo($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundTwoAmount,$poolFundTwoReceiver,$bonusTypePF))
                                {
                                    if(bonusTwoPoolFundThree($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundThreeAmount,$poolFundThreeReceiver,$bonusTypePF))
                                    {
                                        if(bonusTwoPoolFundFour($conn,$orderUid,$orderUserUid,$orderUserName,$poolFundFourAmount,$poolFundFourReceiver,$bonusTypePF))
                                        {
                                            header('Location: ../adminOrderPending.php');
                                        }
                                        else
                                        {
                                            echo "fail pool fund bonus 4";    
                                            echo "<br>";  
                                        } 
                                    }
                                    else
                                    {
                                        echo "fail pool fund bonus 3";    
                                        echo "<br>";  
                                    } 
                                }
                                else
                                {
                                    echo "fail pool fund bonus 2";    
                                    echo "<br>";  
                                } 
                            }
                            else
                            {
                                echo "fail pool fund bonus 1";     
                                echo "<br>";  
                            } 

                        }
                        else
                        {
                            echo "FAIL TO UPDATE RH ORDER STATUS";
                        }
                    }
                    else
                    {
                        echo "ERROR FOR RH ORDER STATUS";
                    }
                }
                else
                {
                    echo "FAIL TO UPDATE USER ORDER STATUS";
                }
            }
            else
            {
                echo "ERROR FOR USER ORDER STATUS";
            }
        }
        else
        {
            echo "fail update order details";
            echo "<br>";  
        }
    }
    else
    {
        echo "invalid order details";
        echo "<br>";  
    }

}
else 
{
    header('Location: ../index.php');
}
?>